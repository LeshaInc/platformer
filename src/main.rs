#![allow(dead_code)]
#![allow(clippy::too_many_arguments)]

mod block;
mod camera;
mod debug_view;
mod entity;
mod navmesh;
mod planet;
mod state;
mod util;
mod wall;

use engine::ui::SkinSet;
use engine::Game;
use eyre::Result;

use crate::block::BlockRegistry;
use crate::entity::character::CharacterSpritesheet;
use crate::planet::BlockTransitions;
use crate::state::loading::LoadingState;
use crate::wall::WallRegistry;

fn main() -> Result<()> {
    let mut game = Game::new();
    game.register_asset::<BlockTransitions>();
    game.register_asset::<BlockRegistry>();
    game.register_asset::<WallRegistry>();
    game.register_asset::<CharacterSpritesheet>();
    game.register_asset::<SkinSet>();
    game.start(LoadingState::new)
}
