use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::path::{Path, PathBuf};

use ahash::AHasher;
use engine::assets::{Asset, AssetDefaultLoader, AssetLoader, Handle, LoadingContext};
use engine::graphics::Image;
use engine::math::Vec2D;
use eyre::{bail, Result};
use serde::{Deserialize, Serialize};

use crate::util::{Map2D, WeightTable};

#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq, Serialize, Deserialize)]
#[serde(transparent)]
pub struct WallId(pub u16);

impl WallId {
    pub const AIR: WallId = WallId(0);

    pub fn is_air(self) -> bool {
        self == Self::AIR
    }
}

#[derive(Debug)]
pub struct Wall {
    pub id: WallId,
    pub tile_size: Vec2D<u32>,
    pub variation_weights: WeightTable,
    pub variations: Vec<WallTextureVariation>,
}

impl Wall {
    pub fn new(id: WallId) -> Wall {
        Wall {
            id,
            tile_size: Vec2D::new(0, 0),
            variation_weights: WeightTable::new(),
            variations: Vec::new(),
        }
    }

    pub fn has_texture(&self) -> bool {
        !self.variations.is_empty()
    }

    pub fn get_texture(&self, pos: Vec2D<u32>) -> &Handle<Image> {
        let tile = pos.elementwise_div(self.tile_size);
        let subtile = pos.elementwise_rem(self.tile_size);

        let mut hasher = AHasher::default();
        tile.hash(&mut hasher);
        let hash = hasher.finish() as u32;
        let variation_idx = self.variation_weights.get(hash);
        let variation = &self.variations[variation_idx];
        &variation.textures[subtile]
    }
}

#[derive(Debug, Deserialize)]
struct WallDef {
    id: WallId,
    width: u32,
    height: u32,
    variations: Vec<WallTextureVariationDef>,
}

#[derive(Debug)]
pub struct WallTextureVariation {
    pub textures: Map2D<Handle<Image>>,
}

#[derive(Debug, Deserialize)]
struct WallTextureVariationDef {
    #[serde(default = "default_weight")]
    weight: u32,
    textures: Vec<PathBuf>,
}

#[derive(Debug)]
pub struct WallRegistry {
    walls: Vec<Wall>,
}

#[derive(Debug, Deserialize)]
#[serde(transparent)]
struct WallRegistryDef {
    walls: Vec<WallDef>,
}

impl WallRegistry {
    pub fn get(&self, id: WallId) -> &Wall {
        &self.walls[id.0 as usize]
    }
}

impl Asset for WallRegistry {}

impl AssetDefaultLoader for WallRegistry {
    type DefaultLoader = WallRegistryLoader;
}

pub struct WallRegistryLoader;

#[async_trait::async_trait]
impl AssetLoader<WallRegistry> for WallRegistryLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<WallRegistry> {
        let string = ctx.read_string(path)?;
        let def: WallRegistryDef = serde_json::from_str(&string)?;

        let mut walls_map = HashMap::new();
        for wall_def in def.walls {
            if wall_def.id.is_air() {
                bail!("ID 0 is reserved for air");
            }

            let tile_size = Vec2D::new(wall_def.width, wall_def.height);

            let it = wall_def.variations.iter();
            let mut variation_weights = WeightTable::new();
            let variations = it.map(|variation| {
                let textures = variation.textures.iter().map(|tex| ctx.load(tex)).collect();
                variation_weights.add(variation.weight);

                WallTextureVariation {
                    textures: Map2D::new(tile_size, textures),
                }
            });

            walls_map.insert(
                wall_def.id,
                Wall {
                    id: wall_def.id,
                    tile_size,
                    variations: variations.collect(),
                    variation_weights,
                },
            );
        }

        let mut walls = Vec::with_capacity(walls_map.len() + 1);
        let mut id = WallId(0);
        while !walls_map.is_empty() {
            let wall = walls_map.remove(&id).unwrap_or_else(|| Wall::new(id));
            walls.push(wall);
            id.0 += 1;
        }

        Ok(WallRegistry { walls })
    }
}

fn default_weight() -> u32 {
    10
}
