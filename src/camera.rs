use engine::graphics::{DebugDrawList, DrawEncoder};
use engine::math::{Box2D, Transform2D, Vec2D};

use crate::planet::SCALE;

#[derive(Clone, Copy, Debug)]
pub struct Camera {
    position: Vec2D<f32>,
    transform: Transform2D,
    pub resolution: Vec2D<u32>,
    pub scale: f32,
}

impl Camera {
    pub fn new() -> Camera {
        let mut camera = Camera {
            position: Vec2D::zero(),
            transform: Transform2D::identity(),
            resolution: Vec2D::zero(),
            scale: 2.0,
        };

        camera.update_transorm();

        camera
    }

    pub fn set_position(&mut self, pos: Vec2D<f32>) {
        self.position = (pos * self.scale).map(|v| v.round()) / self.scale;
        self.update_transorm();
    }

    pub fn inverse(&self, point: Vec2D<f32>) -> Vec2D<f32> {
        (point - (self.resolution / 2).cast::<f32>()) / self.scale + self.position
    }

    pub fn visible_region(&self, limit: Vec2D<u32>, margin: Vec2D<u32>) -> Box2D<u32> {
        let halfres =
            self.resolution.cast::<f32>() / 2.0 / self.scale / SCALE + margin.cast::<f32>();
        let pos = self.position / SCALE;
        let min = (pos - halfres)
            .map(|v| v.max(0.0).floor() as u32)
            .min(limit);
        let max = (pos + halfres).map(|v| v.max(0.0).ceil() as u32).min(limit);
        Box2D::new(min, max)
    }

    pub fn transform(&self) -> &Transform2D {
        &self.transform
    }

    fn update_transorm(&mut self) {
        self.transform = Transform2D::scaling(Vec2D::splat(self.scale))
            * Transform2D::translation(
                -self.position + (self.resolution / 2).cast::<f32>() / self.scale,
            );
        DebugDrawList::get().set_transform(self.transform);
    }

    pub fn push_transform(&self, encoder: &mut DrawEncoder) {
        encoder.push_transform(self.transform);
    }
}
