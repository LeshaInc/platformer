use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::path::{Path, PathBuf};
use std::str::FromStr;

use ahash::AHasher;
use engine::assets::{Asset, AssetDefaultLoader, AssetLoader, Handle, LoadingContext};
use engine::graphics::Image;
use engine::math::{Rgba, Vec2D};
use eyre::{bail, eyre, Result};
use serde::{Deserialize, Serialize};

use crate::util::{Map2D, WeightTable};

#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq, Serialize, Deserialize)]
#[serde(transparent)]
pub struct BlockId(pub u16);

impl BlockId {
    pub const AIR: BlockId = BlockId(0);
    pub const ENTITY_CENTER: BlockId = BlockId(1);
    pub const ENTITY_FILL: BlockId = BlockId(2);

    pub fn is_air(self) -> bool {
        self == Self::AIR
    }
}

#[derive(Debug)]
pub struct Block {
    pub id: BlockId,
    pub light: Rgba<u8>,
    pub is_solid: bool,
    pub opacity: u8,
    pub transition_layer: u8,
    pub outline_layer: u8,
    pub variation_weights: WeightTable,
    pub variations: Vec<BlockTextureVariation>,
    pub overlay_probability: u32,
    pub overlay_weights: WeightTable,
    pub overlays: Vec<BlockOverlay>,
    pub particles: Vec<Rgba>,
}

#[derive(Debug, Deserialize)]
struct BlockDef {
    id: BlockId,
    #[serde(default = "default_is_solid")]
    is_solid: bool,
    #[serde(default = "default_opacity")]
    opacity: u8,
    #[serde(default)]
    light: [u8; 4],
    #[serde(default)]
    transition_layer: u8,
    #[serde(default)]
    outline_layer: u8,
    variations: Vec<BlockTextureVariationDef>,
    #[serde(default = "default_overlay_probability")]
    overlay_probability: u32,
    #[serde(default)]
    overlays: Vec<BlockOverlayDef>,
    #[serde(default)]
    particles: Vec<String>,
}

impl Block {
    pub fn new(id: BlockId) -> Block {
        Block {
            id,
            light: Rgba::new(0, 0, 0, 0),
            is_solid: false,
            opacity: 8,
            outline_layer: 255,
            transition_layer: 254,
            variation_weights: WeightTable::new(),
            variations: Vec::new(),
            overlay_probability: 0,
            overlay_weights: WeightTable::new(),
            overlays: Vec::new(),
            particles: Vec::new(),
        }
    }

    pub fn has_texture(&self) -> bool {
        !self.variations.is_empty()
    }

    pub fn get_texture(&self, pos: Vec2D<u32>) -> &Handle<Image> {
        let mut hasher = AHasher::default();
        pos.hash(&mut hasher);
        let hash = hasher.finish() as u32;
        let variation_idx = self.variation_weights.get(hash);
        let variation = &self.variations[variation_idx];
        let offset = pos.elementwise_rem(variation.textures.size());
        &variation.textures[offset]
    }

    pub fn get_overlay(&self, pos: Vec2D<u32>) -> Option<&BlockOverlay> {
        let mut hasher = AHasher::default();
        pos.hash(&mut hasher);
        let hash = hasher.finish() as u32;

        let variation_idx = self.variation_weights.get(hash);
        let variation = &self.variations[variation_idx];

        let max = self.overlay_weights.weight_sum() + variation.overlay_weights.weight_sum();
        if !self.overlays.is_empty()
            && hash % max < self.overlay_weights.weight_sum()
            && hash % 100 < self.overlay_probability
        {
            let idx = self.overlay_weights.get(hash);
            Some(&self.overlays[idx])
        } else if !variation.overlays.is_empty() && hash % 100 < variation.overlay_probability {
            let idx = variation.overlay_weights.get(hash);
            Some(&variation.overlays[idx])
        } else {
            None
        }
    }

    pub fn is_solid_at(blocks: &Map2D<BlockId>, registry: &BlockRegistry, pos: Vec2D<u32>) -> bool {
        match blocks.try_get(pos) {
            Some(v) if v.is_air() => false,
            Some(&v) => registry.get(v).is_solid,
            _ => true,
        }
    }
}

#[derive(Debug)]
pub struct BlockTextureVariation {
    pub textures: Map2D<Handle<Image>>,
    pub overlay_probability: u32,
    pub overlay_weights: WeightTable,
    pub overlays: Vec<BlockOverlay>,
}

#[derive(Debug, Deserialize)]
struct BlockTextureVariationDef {
    #[serde(default = "default_weight")]
    weight: u32,
    #[serde(default = "default_width")]
    width: u32,
    textures: Vec<PathBuf>,
    #[serde(default = "default_overlay_probability")]
    overlay_probability: u32,
    #[serde(default)]
    overlays: Vec<BlockOverlayDef>,
}

#[derive(Debug)]
pub struct BlockOverlay {
    pub layer: u8,
    pub offset: Vec2D<f32>,
    pub texture: Handle<Image>,
}

#[derive(Debug, Deserialize)]
struct BlockOverlayDef {
    #[serde(default = "default_weight")]
    weight: u32,
    #[serde(default)]
    layer: u8,
    #[serde(default)]
    x: i32,
    #[serde(default)]
    y: i32,
    texture: PathBuf,
}

#[derive(Debug)]
pub struct BlockRegistry {
    blocks: Vec<Block>,
}

#[derive(Debug, Deserialize)]
#[serde(transparent)]
struct BlockRegistryDef {
    blocks: Vec<BlockDef>,
}

impl BlockRegistry {
    pub fn get(&self, id: BlockId) -> &Block {
        &self.blocks[id.0 as usize]
    }
}

impl Asset for BlockRegistry {}

impl AssetDefaultLoader for BlockRegistry {
    type DefaultLoader = BlockRegistryLoader;
}

pub struct BlockRegistryLoader;

#[async_trait::async_trait]
impl AssetLoader<BlockRegistry> for BlockRegistryLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<BlockRegistry> {
        let string = ctx.read_string(path)?;
        let def: BlockRegistryDef = serde_json::from_str(&string)?;

        let reserved = [BlockId::AIR, BlockId::ENTITY_FILL, BlockId::ENTITY_CENTER];

        let mut blocks_map = HashMap::new();
        for block_def in def.blocks {
            if reserved.contains(&block_def.id) {
                bail!("ID {} is reserved", block_def.id.0);
            }

            let it = block_def.variations.iter();
            let mut variation_weights = WeightTable::new();
            let variations = it
                .map(|variation| {
                    let height = variation.textures.len() as u32 / variation.width;
                    let size = Vec2D::new(variation.width, height);
                    let textures = variation.textures.iter().map(|tex| ctx.load(tex)).collect();
                    variation_weights.add(variation.weight);

                    let mut overlay_weights = WeightTable::new();
                    let overlays = variation.overlays.iter().map(|overlay| {
                        overlay_weights.add(overlay.weight);
                        BlockOverlay {
                            layer: overlay.layer,
                            offset: Vec2D::new(overlay.x as f32, overlay.y as f32),
                            texture: ctx.load(&overlay.texture),
                        }
                    });

                    BlockTextureVariation {
                        textures: Map2D::new(size, textures),
                        overlay_probability: variation.overlay_probability,
                        overlays: overlays.collect(),
                        overlay_weights,
                    }
                })
                .collect();

            let mut overlay_weights = WeightTable::new();
            let overlays = block_def.overlays.iter().map(|overlay| {
                overlay_weights.add(overlay.weight);
                BlockOverlay {
                    layer: overlay.layer,
                    offset: Vec2D::new(overlay.x as f32, overlay.y as f32),
                    texture: ctx.load(&overlay.texture),
                }
            });

            let it = block_def.particles.iter();
            let particles = it
                .map(|v| Rgba::from_str(v).map_err(|_| eyre!("Invalid color")))
                .collect::<Result<_>>()?;

            blocks_map.insert(
                block_def.id,
                Block {
                    id: block_def.id,
                    light: block_def.light.into(),
                    is_solid: block_def.is_solid,
                    opacity: block_def.opacity,
                    transition_layer: block_def.transition_layer,
                    outline_layer: block_def.outline_layer,
                    variation_weights,
                    variations,
                    overlay_probability: block_def.overlay_probability,
                    overlays: overlays.collect(),
                    overlay_weights,
                    particles,
                },
            );
        }

        for id in reserved {
            blocks_map.insert(id, Block::new(id));
        }

        let mut blocks = Vec::with_capacity(blocks_map.len());

        let mut id = BlockId(0);
        while !blocks_map.is_empty() {
            let block = blocks_map.remove(&id).unwrap_or_else(|| Block::new(id));
            blocks.push(block);
            id.0 += 1;
        }

        Ok(BlockRegistry { blocks })
    }
}

fn default_is_solid() -> bool {
    true
}

fn default_opacity() -> u8 {
    30
}

fn default_weight() -> u32 {
    10
}

fn default_width() -> u32 {
    1
}

fn default_overlay_probability() -> u32 {
    100
}
