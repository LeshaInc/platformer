use std::f32::consts::{FRAC_1_SQRT_2, PI};

use engine::math::{LayeredValueNoise1D, Vec2D};
use rand::Rng;

use super::Planet;
use crate::block::BlockId;
use crate::util::Map2D;
use crate::wall::WallId;

#[derive(Debug, Clone)]
pub struct Heightmap {
    pub values: Vec<u32>,
}

impl Heightmap {
    pub fn gen<R: Rng>(rng: &mut R, size: u32) -> Heightmap {
        let noise = LayeredValueNoise1D::new(rng, size, 6);

        let values = (0..size)
            .map(|x| (noise.sample(x as f32) * 40.0) as u32 + 500)
            .collect::<Vec<_>>();

        Heightmap { values }
    }

    pub fn smooth(&mut self) {
        for x in 1..self.values.len() - 1 {
            self.values[x] = (self.values[x - 1] + self.values[x] + self.values[x + 1]) / 3;
        }

        loop {
            let mut modified = false;
            for x in 1..self.values.len() - 1 {
                let a = self.values[x - 1];
                let b = self.values[x];
                let c = self.values[x + 1];
                if a == b + 1 && c == b + 1 {
                    self.values[x] = a;
                    modified = true;
                }
                if a + 1 == b && c + 1 == b {
                    self.values[x] = a;
                    modified = true;
                }
            }
            if !modified {
                break;
            }
        }
    }
}

pub struct CaveGenerator<'a> {
    pub blocks: &'a mut Map2D<BlockId>,
    worms: Vec<Worm>,
    worm_id: u16,
}

struct Worm {
    id: u16,
    dir: Vec2D<f32>,
    lifetime: u32,
    pos: Vec2D<u32>,
}

impl<'a> CaveGenerator<'a> {
    pub fn new(blocks: &'a mut Map2D<BlockId>) -> CaveGenerator {
        CaveGenerator {
            blocks,
            worms: Vec::new(),
            worm_id: 1,
        }
    }

    pub fn add_seed(&mut self, pos: Vec2D<u32>, dir: Vec2D<f32>) {
        self.worm_id += 1;
        self.worms.push(Worm {
            id: self.worm_id,
            dir: dir.normalize(),
            lifetime: 0,
            pos,
        });
    }

    pub fn gen<R: Rng>(&mut self, rng: &mut R) {
        while !self.worms.is_empty() {
            let worm_idx = rng.gen_range(0..self.worms.len());
            let worm = &mut self.worms[worm_idx];

            if worm.pos.ge(&self.blocks.size()).any() || (worm.lifetime > 200 && rng.gen_bool(0.01))
            {
                self.worms.swap_remove(worm_idx);
                continue;
            }

            let block = *self.blocks.get(worm.pos);
            if block.0 > 1 && block.0 != worm.id && rng.gen_bool(0.5) {
                self.worms.swap_remove(worm_idx);
                continue;
            }

            self.blocks.set(worm.pos, BlockId(worm.id));

            worm.dir = Vec2D::new(
                worm.dir.x + rng.gen_range(-0.25..0.25),
                worm.dir.y + rng.gen_range(-0.05..0.05),
            )
            .normalize();

            let mut idir = Vec2D::<u32>::zero();
            while idir == Vec2D::zero() {
                let x_p = worm.dir.x.abs().clamp(0.01, 0.99) as f64;
                let y_p = worm.dir.y.abs().clamp(0.01, 0.99) as f64;
                idir = Vec2D::new(rng.gen_bool(x_p) as u32, rng.gen_bool(y_p) as u32);
            }

            worm.pos.x = if worm.dir.x > 0.0 {
                worm.pos.x.saturating_add(idir.x)
            } else {
                worm.pos.x.saturating_sub(idir.x)
            };

            worm.pos.y = if worm.dir.y > 0.0 {
                worm.pos.y.saturating_add(idir.y)
            } else {
                worm.pos.y.saturating_sub(idir.y)
            };

            worm.lifetime += 1;

            if worm.lifetime > 700 && rng.gen_bool(0.8) {
                let pos = worm.pos;
                self.worms.swap_remove(worm_idx);
                self.worm_id += 1;
                self.worms.push(Worm {
                    id: self.worm_id,
                    dir: Vec2D::new(0.0, 1.0),
                    lifetime: 0,
                    pos: pos.map_x(|x| x.saturating_sub(2)).map_y(|y| y + 2),
                });
                self.worm_id += 1;
                self.worms.push(Worm {
                    id: self.worm_id,
                    dir: Vec2D::new(0.0, 1.0),
                    lifetime: 0,
                    pos: pos.map_x(|x| x.saturating_add(2)).map_y(|y| y + 2),
                });
            }
        }

        for x in 0..self.blocks.size().x {
            for y in 0..self.blocks.size().y {
                let pos = Vec2D::new(x, y);
                if self.blocks.get(pos).0 < 2 {
                    continue;
                }

                let r = rng.gen_range(3..5);
                let min = pos.saturating_sub(Vec2D::splat(r));
                let max = pos
                    .saturating_add(Vec2D::splat(r))
                    .min(self.blocks.size() - Vec2D::splat(1));

                for x in min.x..=max.x {
                    for y in min.y..=max.y {
                        let p = Vec2D::new(x, y);
                        if p != pos
                            && (p.cast::<i32>() - pos.cast::<i32>()).map(|x| x * x).sum() // FIXME
                                <= (r * r) as i32
                        {
                            self.blocks.set(Vec2D::new(x, y), BlockId::AIR);
                        }
                    }
                }
            }
        }

        for x in 0..self.blocks.size().x {
            for y in 0..self.blocks.size().y {
                let pos = Vec2D::new(x, y);
                if self.blocks.get(pos).0 > 1 {
                    self.blocks.set(pos, BlockId::AIR);
                }
            }
        }
    }
}

pub fn smooth(blocks: &mut Map2D<BlockId>, target_block: BlockId, min: u32, max: u32, iters: u32) {
    for _ in 0..iters {
        let mut target = blocks.clone();

        for x in 1..blocks.size().x - 1 {
            for y in 1..blocks.size().y - 1 {
                let pos = Vec2D::new(x, y);
                let block = *blocks.get(pos);

                let mut neighbors = 0;
                for sx in -1..=1 {
                    for sy in -1..=1 {
                        if sx == 0 && sy == 0 {
                            continue;
                        }
                        let npos = Vec2D::new((x as i32 + sx) as u32, (y as i32 + sy) as u32);
                        if *blocks.get(npos) == target_block {
                            neighbors += 1;
                        }
                    }
                }

                if block == target_block && neighbors <= min {
                    target.set(pos, BlockId::AIR);
                } else if block == BlockId::AIR && neighbors >= max {
                    target.set(pos, target_block);
                }
            }
        }

        *blocks = target;
    }
}

pub fn cleanup(blocks: &mut Map2D<BlockId>, solid: BlockId, empty: BlockId, min_area: usize) {
    let mut regions = Map2D::new_default(blocks.size());
    let mut region = 1u32;
    let mut equiv = Vec::new();

    for x in 0..blocks.size().x {
        for y in 0..blocks.size().y {
            let pos = Vec2D::new(x, y);

            let is_solid = *blocks.get(pos) == solid;
            if !is_solid {
                continue;
            }

            let pos_t = pos.checked_sub(Vec2D::new(0, 1));
            let pos_l = pos.checked_sub(Vec2D::new(1, 0));

            let is_solid_t = pos_t.map(|pos| *blocks.get(pos) == solid).unwrap_or(false);
            let is_solid_l = pos_l.map(|pos| *blocks.get(pos) == solid).unwrap_or(false);

            let region_t = pos_t.map(|pos| *regions.get(pos)).unwrap_or(0);
            let region_l = pos_l.map(|pos| *regions.get(pos)).unwrap_or(0);

            if (is_solid_t || is_solid_l) && (region_t > 0 || region_l > 0) {
                regions.set(pos, region_t.max(region_l));
                if region_t > 0 && region_l > 0 && region_t != region_l {
                    let (mut a, mut b) = (region_t, region_l);
                    if a < b {
                        std::mem::swap(&mut a, &mut b);
                    }

                    while equiv.len() <= a as usize {
                        equiv.push(equiv.len() as u32);
                    }

                    equiv[a as usize] = b;
                }
            } else {
                region += 1;
                regions.set(pos, region);
            }
        }
    }

    while equiv.len() <= region as usize {
        equiv.push(equiv.len() as u32);
    }

    for i in 0..equiv.len() {
        let a = i as u32;
        let mut b = equiv[i];
        loop {
            let nb = equiv[b as usize];
            if nb == b {
                break;
            }
            b = nb;
        }
        equiv[a as usize] = b;
    }

    let mut areas = vec![0; equiv.len()];

    for x in 0..blocks.size().x {
        for y in 0..blocks.size().y {
            let pos = Vec2D::new(x, y);
            let region = equiv[*regions.get(pos) as usize];
            regions.set(pos, region);
            areas[region as usize] += 1;
        }
    }

    for x in 0..blocks.size().x {
        for y in 0..blocks.size().y {
            let pos = Vec2D::new(x, y);
            let region = *regions.get(pos);
            if areas[region as usize] < min_area {
                blocks.set(pos, empty);
            }
        }
    }
}

pub fn set_grass<R: Rng>(rng: &mut R, blocks: &mut Map2D<BlockId>, grass: BlockId, dirt: BlockId) {
    let noise = LayeredValueNoise1D::new(rng, blocks.size().x, 3);

    for x in 0..blocks.size().x {
        'outer: for y in 0..blocks.size().y {
            let pos = Vec2D::new(x, y);
            if !blocks.get(pos).is_air() {
                blocks.set(pos, grass);
                let depth = ((noise.sample(pos.x as f32) * 4.0) + 1.0) as u32;
                for i in 1..1 + depth {
                    let pos = Vec2D::new(pos.x, pos.y + i);
                    if !blocks.get(pos).is_air() {
                        blocks.set(pos, dirt);
                    } else {
                        break 'outer;
                    }
                }

                let pos = Vec2D::new(pos.x, pos.y + depth + 1);
                if !blocks.get(pos).is_air() && rng.gen_bool(0.3) {
                    blocks.set(pos, dirt);
                }

                break;
            }
        }
    }
}

pub fn replace(blocks: &mut Map2D<BlockId>, from: BlockId, to: BlockId) {
    for x in 0..blocks.size().x {
        for y in 0..blocks.size().y {
            let pos = Vec2D::new(x, y);
            if *blocks.get(pos) == from {
                blocks.set(pos, to);
            }
        }
    }
}

pub fn set_walls(walls: &mut Map2D<WallId>) {
    for x in 0..walls.size().x {
        for y in 300..walls.size().y {
            walls.set(Vec2D::new(x, y), WallId(1));
        }
    }
}

pub fn gen<R: Rng>(rng: &mut R, size: Vec2D<u32>) -> Planet {
    let mut planet = Planet::new(size);
    let blocks = &mut planet.blocks;
    let walls = &mut planet.walls;

    let mut heightmap = Heightmap::gen(rng, size.x);
    heightmap.smooth();

    for (x, &height) in heightmap.values.iter().enumerate() {
        let x = x as u32;
        let pos = Vec2D::new(x, size.y - height);
        blocks.fill(pos, Vec2D::new(1, height), BlockId(1));
    }

    for x in 0..size.x {
        for y in 0..size.y {
            let pos = Vec2D::new(x, y);
            let p = ((pos.y as f64) / 600.0).clamp(0.0, 1.0) * 0.29;
            if !blocks.get(pos).is_air() && rng.gen_bool(p) {
                blocks.set(pos, BlockId::AIR);
            }
        }
    }

    smooth(blocks, BlockId(1), 4, 6, 3);
    cleanup(blocks, BlockId(0), BlockId(1), 100);

    let mut cave_gen = CaveGenerator::new(blocks);

    for x in 0..10 {
        let x = x * size.x / 10 + size.x / 20;
        let y = size.y - heightmap.values[x as usize];
        cave_gen.add_seed(Vec2D::new(x, y), Vec2D::new(0.0, 1.0));
    }

    let cave_points = sample_points(rng, size, 20.0);
    for pos in cave_points {
        let pos = pos.cast::<u32>();
        let p = ((pos.y as f64 - 300.0) / 200.0).clamp(0.0, 1.0) * 0.3;
        if !cave_gen.blocks.get(pos).is_air() && rng.gen_bool(p) {
            let dir = match rng.gen_range(0..=4) {
                0..=2 => Vec2D::new(0.0, 1.0),
                3 => Vec2D::new(-1.0, 0.3),
                _ => Vec2D::new(1.0, 0.3),
            };

            cave_gen.add_seed(pos, dir);
        }
    }

    cave_gen.gen(rng);

    smooth(blocks, BlockId(1), 3, 6, 3);
    cleanup(blocks, BlockId(1), BlockId(0), 30);
    replace(blocks, BlockId(1), BlockId(5));
    set_grass(rng, blocks, BlockId(4), BlockId(3));

    set_walls(walls);

    dump_world(blocks);

    planet
}

fn dump_world(blocks: &Map2D<BlockId>) {
    let mut buf = vec![0; blocks.size().cast::<usize>().prod()];

    for x in 0..blocks.size().x {
        for y in 0..blocks.size().y {
            if blocks.get(Vec2D::new(x, y)).is_air() {
                buf[y as usize * blocks.size().x as usize + x as usize] = 255;
            }
        }
    }

    image::save_buffer(
        "dump.png",
        &buf,
        blocks.size().x,
        blocks.size().y,
        image::ColorType::L8,
    )
    .unwrap();
}

fn sample_points<R: Rng>(rng: &mut R, size: Vec2D<u32>, radius: f32) -> Vec<Vec2D<f32>> {
    let mut points = Vec::new();

    let radius_sq = radius * radius;
    let cell_size = radius * FRAC_1_SQRT_2;
    let grid_size = (size.cast::<f32>() / cell_size).map(|v| v.ceil() as u32);
    let mut grid = Map2D::new_default(grid_size);

    let mut queue = Vec::new();

    let seed_pos = Vec2D::new(
        rng.gen_range(0.0..size.x as f32),
        rng.gen_range(0.0..size.y as f32),
    );

    let seed_grid_pos = (seed_pos / cell_size).cast::<u32>();

    points.push(seed_pos);
    queue.push(seed_pos);
    grid.set(seed_grid_pos, Some(seed_pos));

    'outer: while !queue.is_empty() {
        let idx = rng.gen_range(0..queue.len());
        let parent = queue[idx];

        'inner: for _iter in 0..5 {
            let angle = rng.gen_range(-PI..PI);
            let dist = rng.gen_range(radius..2.0 * radius);
            let (sin, cos) = angle.sin_cos();
            let pos = parent + Vec2D::new(cos * dist, sin * dist);
            if pos.x < 0.0 || pos.y < 0.0 || pos.x >= size.x as f32 || pos.y >= size.y as f32 {
                continue;
            }

            let grid_pos = (pos / cell_size).cast::<u32>();

            for sx in -2..=2 {
                for sy in -2..=2 {
                    let n_grid_pos = (grid_pos.cast::<i32>() + Vec2D::new(sx, sy)).cast::<u32>();
                    let n_pos = match grid.try_get(n_grid_pos).copied().flatten() {
                        Some(v) => v,
                        None => continue,
                    };

                    if (n_pos - pos).mag_squared() < radius_sq {
                        continue 'inner;
                    }
                }
            }

            points.push(pos);
            queue.push(pos);
            grid.set(grid_pos, Some(pos));
            continue 'outer;
        }

        queue.swap_remove(idx);
    }

    points
}
