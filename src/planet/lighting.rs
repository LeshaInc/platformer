use std::collections::VecDeque;

use engine::math::{Rgba, Vec2D};
use rayon::iter::{IndexedParallelIterator, IntoParallelRefMutIterator, ParallelIterator};

use super::{BlockId, Planet};
use crate::block::BlockRegistry;
use crate::util::Map2D;

type Queue = VecDeque<(Vec2D<u32>, u8)>;

#[derive(Default)]
pub struct LightingEngine {
    queues: [Queue; 4],
}

impl LightingEngine {
    #[inline(never)]
    pub fn compute(
        &mut self,
        planet: &Planet,
        block_registry: &BlockRegistry,
        origin: Vec2D<u32>,
        size: Vec2D<u32>,
    ) -> Map2D<Rgba<u8>> {
        let mut lightmaps = [
            Map2D::new_default(size),
            Map2D::new_default(size),
            Map2D::new_default(size),
            Map2D::new_default(size),
        ];

        set_block_lights(&planet.blocks, block_registry, &mut lightmaps, origin);
        set_sky_lights(planet, &mut lightmaps[3], origin);

        lightmaps
            .par_iter_mut()
            .zip(&mut self.queues)
            .for_each(|(lightmap, queue)| {
                enqueue(lightmap, queue);
                propagate(&planet.blocks, block_registry, lightmap, origin, queue);
                blur(lightmap);
            });

        combine(&lightmaps)
    }
}

fn set_sky_lights(planet: &Planet, lightmap: &mut Map2D<u8>, lightmap_origin: Vec2D<u32>) {
    for x in 0..lightmap.size().x {
        for y in 0..lightmap.size().y {
            let lightmap_pos = Vec2D::new(x, y);
            let pos = lightmap_origin + lightmap_pos;
            let wall = *planet.walls.get(pos);
            let block = *planet.blocks.get(pos);
            if wall.is_air() && block.is_air() {
                let channel = lightmap.get_mut(lightmap_pos);
                *channel = channel.saturating_add(255);
            }
        }
    }
}

fn set_block_lights(
    blocks: &Map2D<BlockId>,
    block_registry: &BlockRegistry,
    lightmaps: &mut [Map2D<u8>; 4],
    lightmap_origin: Vec2D<u32>,
) {
    for x in 0..lightmaps[0].size().x {
        for y in 0..lightmaps[0].size().y {
            let lightmap_pos = Vec2D::new(x, y);
            let block_pos = lightmap_origin + lightmap_pos;
            let block = *blocks.get(block_pos);
            if block.is_air() {
                continue;
            }

            let light = block_registry.get(block).light;

            if light.r > 0 {
                let channel = lightmaps[0].get_mut(lightmap_pos);
                *channel = channel.saturating_add(light.r);
            }
            if light.g > 0 {
                let channel = lightmaps[1].get_mut(lightmap_pos);
                *channel = channel.saturating_add(light.g);
            }
            if light.b > 0 {
                let channel = lightmaps[2].get_mut(lightmap_pos);
                *channel = channel.saturating_add(light.b);
            }
            if light.a > 0 {
                let channel = lightmaps[3].get_mut(lightmap_pos);
                *channel = channel.saturating_add(light.a);
            }
        }
    }
}

fn enqueue(lightmap: &mut Map2D<u8>, queue: &mut Queue) {
    for x in 0..lightmap.size().x {
        for y in 0..lightmap.size().y {
            let pos = Vec2D::new(x, y);
            let light = *lightmap.get(pos);
            if light > 0 {
                queue.push_back((pos, light));
            }
        }
    }
}

fn propagate(
    blocks: &Map2D<BlockId>,
    block_registry: &BlockRegistry,
    lightmap: &mut Map2D<u8>,
    lightmap_origin: Vec2D<u32>,
    queue: &mut Queue,
) {
    let lightmap_size = lightmap.size();

    while let Some((pos, light)) = queue.pop_front() {
        let mut propagate = |pos: Vec2D<u32>| {
            propagate_neighbor(
                blocks,
                block_registry,
                lightmap,
                lightmap_origin,
                queue,
                pos,
                light,
            );
        };

        if pos.x > 0 {
            propagate(pos - Vec2D::new(1, 0));
        }
        if pos.y > 0 {
            propagate(pos - Vec2D::new(0, 1));
        }
        if pos.x + 1 < lightmap_size.x {
            propagate(pos + Vec2D::new(1, 0));
        }
        if pos.y + 1 < lightmap_size.y {
            propagate(pos + Vec2D::new(0, 1));
        }
    }
}

#[inline(always)]
fn propagate_neighbor(
    blocks: &Map2D<BlockId>,
    block_registry: &BlockRegistry,
    lightmap: &mut Map2D<u8>,
    lightmap_origin: Vec2D<u32>,
    queue: &mut Queue,
    pos: Vec2D<u32>,
    light: u8,
) {
    let mut neighbor_light = *lightmap.get(pos);
    let block = *blocks.get(pos + lightmap_origin);
    let opacity = block_registry.get(block).opacity;
    let target_light = light.saturating_sub(opacity);

    if target_light > neighbor_light {
        neighbor_light = target_light;
        lightmap.set(pos, neighbor_light);
        queue.push_back((pos, target_light));
    }
}

fn blur(lightmap: &mut Map2D<u8>) {
    for _ in 0..4 {
        lightmap.blur();
    }
}

fn combine(lightmaps: &[Map2D<u8>; 4]) -> Map2D<Rgba<u8>> {
    let mut target = Map2D::new_default(lightmaps[0].size());
    for y in 0..lightmaps[0].size().y {
        for x in 0..lightmaps[0].size().x {
            let pos = Vec2D::new(x, y);
            let r = *lightmaps[0].get(pos);
            let g = *lightmaps[1].get(pos);
            let b = *lightmaps[2].get(pos);
            let a = *lightmaps[3].get(pos);
            target.set(pos, Rgba::new(r, g, b, a));
        }
    }
    target
}
