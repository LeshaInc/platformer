mod draw;
pub mod gen;
mod lighting;

use engine::math::Vec2D;

pub use self::draw::*;
pub use self::lighting::LightingEngine;
use crate::block::BlockId;
use crate::util::Map2D;
use crate::wall::WallId;

pub struct Planet {
    pub blocks: Map2D<BlockId>,
    pub walls: Map2D<WallId>,
}

impl Planet {
    pub fn new(size: Vec2D<u32>) -> Planet {
        Planet {
            blocks: Map2D::new_default(size),
            walls: Map2D::new_default(size),
        }
    }
}

pub const SCALE: f32 = 8.0;
