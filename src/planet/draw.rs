use std::path::{Path, PathBuf};

use engine::assets::{Asset, AssetDefaultLoader, AssetLoader, Assets, Handle, LoadingContext};
use engine::ecs::{Res, ResMut};
use engine::graphics::{DrawEncoder, DrawList, Image, Lighting, Sprite};
use engine::math::{Rgba, Vec2D};
use engine::profiler::Profiler;
use eyre::Result;
use serde::Deserialize;

use super::{LightingEngine, Planet, SCALE};
use crate::block::BlockRegistry;
use crate::camera::Camera;
use crate::state::main::GlobalImages;
use crate::wall::WallRegistry;

#[derive(Debug, Clone)]
pub struct SkyTexture(pub Handle<Image>);

pub fn draw_sky(
    mut draw_list: ResMut<DrawList>,
    camera: Res<Camera>,
    sky_texture: Res<SkyTexture>,
) {
    let mut encoder = DrawEncoder::new(&mut draw_list);
    let sprite = Sprite::new(sky_texture.0.id(), Vec2D::zero()).with_size(camera.resolution.cast());
    encoder.draw_sprite(sprite);
}

pub fn draw_walls(
    assets: Res<Assets>,
    mut draw_list: ResMut<DrawList>,
    planet: Res<Planet>,
    wall_registry: Res<Handle<WallRegistry>>,
    camera: Res<Camera>,
) {
    let _guard = Profiler::get().time_guard(109, "draw_walls");

    let wall_registry = assets.get(&wall_registry).unwrap();

    let walls = &planet.walls;

    let visible = camera.visible_region(walls.size(), Vec2D::splat(0));
    if visible.area() == 0 {
        return;
    }

    let mut encoder = DrawEncoder::new(&mut draw_list);
    camera.push_transform(&mut encoder);

    for x in visible.min.x..visible.max.x {
        for y in visible.min.y..visible.max.y {
            let pos = Vec2D::new(x, y);
            let wall_id = *walls.get(pos);
            if wall_id.is_air() {
                continue;
            }

            let wall = wall_registry.get(wall_id);
            if !wall.has_texture() {
                continue;
            }

            let texture = wall.get_texture(pos);
            encoder.draw_sprite(Sprite::new(texture.id(), pos.cast::<f32>() * SCALE));
        }
    }

    encoder.pop_transform();
}

pub fn draw_blocks(
    assets: Res<Assets>,
    mut draw_list: ResMut<DrawList>,
    planet: Res<Planet>,
    block_registry: Res<Handle<BlockRegistry>>,
    block_transitions: Res<Handle<BlockTransitions>>,
    camera: Res<Camera>,
    global_images: Res<GlobalImages>,
) {
    let _guard = Profiler::get().time_guard(110, "draw_blocks");

    let block_registry = assets.get(&block_registry).unwrap();
    let block_transitions = assets.get(&block_transitions).unwrap();

    let blocks = &planet.blocks;

    let visible = camera.visible_region(blocks.size(), Vec2D::splat(0));
    if visible.area() == 0 {
        return;
    }

    let mut encoder = DrawEncoder::new(&mut draw_list);
    camera.push_transform(&mut encoder);

    for x in visible.min.x..visible.max.x {
        for y in visible.min.y..visible.max.y {
            let pos = Vec2D::new(x, y);
            let block_id = *blocks.get(pos);
            if block_id.is_air() {
                continue;
            }

            let block = block_registry.get(block_id);
            if !block.has_texture() {
                continue;
            }

            let block_texture = block.get_texture(pos).id();

            let neighbors = [
                pos.checked_sub(Vec2D::new(0, 1)),
                pos.checked_add_sub(Vec2D::new(1, 1)),
                pos.checked_add(Vec2D::new(1, 0)),
                pos.checked_add(Vec2D::new(1, 1)),
                pos.checked_add(Vec2D::new(0, 1)),
                pos.checked_sub_add(Vec2D::new(1, 1)),
                pos.checked_sub(Vec2D::new(1, 0)),
                pos.checked_sub(Vec2D::new(1, 1)),
            ];

            let mut transition_bits = 255;
            let mut outline_bits = 0;
            let mut neighbor_texture = None;

            for (i, &neighbor_pos) in neighbors.iter().enumerate() {
                let neighbor_pos = match neighbor_pos {
                    Some(v) if v.ge(&blocks.size()).any() => continue,
                    Some(v) => v,
                    None => continue,
                };

                let neighbor_id = *blocks.get(neighbor_pos);
                let neighbor = block_registry.get(neighbor_id);

                if block.outline_layer == 0
                    || neighbor_id == block_id
                    || block.outline_layer == neighbor.outline_layer
                {
                    outline_bits |= 1 << i;
                }

                if neighbor.transition_layer + 1 == block.transition_layer {
                    outline_bits |= 1 << i;
                }

                if block.transition_layer > 0
                    && neighbor.transition_layer == block.transition_layer + 1
                {
                    if neighbor.has_texture() {
                        neighbor_texture = Some(neighbor.get_texture(neighbor_pos).id());
                    }
                    transition_bits &= !(1 << i);
                }
            }

            let fpos = pos.cast::<f32>() * SCALE;

            let trans_fill = &block_transitions.fill[transition_bits];
            let trans_outline = &block_transitions.outline[transition_bits];

            let outline_color = Rgba::new(0.0, 0.0, 0.0, 0.5);

            if transition_bits != 255 {
                if let Some(nt) = neighbor_texture {
                    encoder.draw_sprite(Sprite::new(nt, fpos))
                }
                encoder.draw_sprite(Sprite::new(block_texture, fpos).with_mask(trans_fill.id()));
                encoder
                    .draw_sprite(Sprite::new(trans_outline.id(), fpos).with_color(outline_color));
                continue;
            } else {
                encoder.draw_sprite(Sprite::new(block_texture, fpos));
            }

            // top
            if transition_bits & 0b1 > 0 && outline_bits & 0b1 == 0 {
                encoder.draw_sprite(
                    Sprite::new(global_images.white_1x1.id(), fpos)
                        .with_size(Vec2D::new(SCALE, 1.0))
                        .with_color(outline_color),
                );
            }

            // right
            if transition_bits & 0b100 > 0 && outline_bits & 0b100 == 0 {
                encoder.draw_sprite(
                    Sprite::new(
                        global_images.white_1x1.id(),
                        fpos + Vec2D::new(SCALE - 1.0, 0.0),
                    )
                    .with_size(Vec2D::new(1.0, SCALE))
                    .with_color(outline_color),
                );
            }

            // bottom
            if transition_bits & 0b10000 > 0 && outline_bits & 0b10000 == 0 {
                encoder.draw_sprite(
                    Sprite::new(
                        global_images.white_1x1.id(),
                        fpos + Vec2D::new(0.0, SCALE - 1.0),
                    )
                    .with_size(Vec2D::new(SCALE, 1.0))
                    .with_color(outline_color),
                );
            }

            // left
            if transition_bits & 0b1000000 > 0 && outline_bits & 0b1000000 == 0 {
                encoder.draw_sprite(
                    Sprite::new(global_images.white_1x1.id(), fpos)
                        .with_size(Vec2D::new(1.0, SCALE))
                        .with_color(outline_color),
                );
            }

            if let Some(overlay) = block.get_overlay(pos) {
                encoder.draw_sprite(Sprite::new(overlay.texture.id(), fpos + overlay.offset));
            }
        }
    }

    encoder.pop_transform();
}

pub fn draw_lighting(
    assets: Res<Assets>,
    mut draw_list: ResMut<DrawList>,
    planet: Res<Planet>,
    block_registry: Res<Handle<BlockRegistry>>,
    camera: Res<Camera>,
    mut lighting: ResMut<LightingEngine>,
) {
    let _guard = Profiler::get().time_guard(120, "draw_lighting");

    let block_registry = assets.get(&block_registry).unwrap();

    let mut encoder = DrawEncoder::new(&mut draw_list);

    let visible = camera.visible_region(planet.blocks.size(), Vec2D::splat(32));
    if visible.area() == 0 {
        return;
    }

    let lightmap = lighting.compute(&planet, &block_registry, visible.min, visible.extents());

    camera.push_transform(&mut encoder);
    encoder.draw_lighting(Lighting {
        pos: visible.min.cast::<f32>() * SCALE,
        size: visible.extents().cast::<f32>() * SCALE,
        texture_size: lightmap.size(),
        data: lightmap.into_inner(),
    });
    encoder.pop_transform();
}

#[derive(Debug)]
pub struct BlockTransitions {
    fill: Vec<Handle<Image>>,
    outline: Vec<Handle<Image>>,
}

impl Asset for BlockTransitions {}
impl AssetDefaultLoader for BlockTransitions {
    type DefaultLoader = BlockTransitionsLoader;
}

pub struct BlockTransitionsLoader;

#[async_trait::async_trait]
impl AssetLoader<BlockTransitions> for BlockTransitionsLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<BlockTransitions> {
        let def: BlockTransitionsDef = serde_json::from_str(&ctx.read_string(path)?)?;
        let it = def.fill.iter();
        let fill = it.map(|path| ctx.load(path)).collect();
        let it = def.outline.iter();
        let outline = it.map(|path| ctx.load(path)).collect();
        Ok(BlockTransitions { fill, outline })
    }
}

#[derive(Clone, Debug, Deserialize)]
struct BlockTransitionsDef {
    fill: Vec<PathBuf>,
    outline: Vec<PathBuf>,
}
