#[derive(Clone, Debug, Default)]
pub struct WeightTable {
    weight_sum: u32,
    cumulative_weights: Vec<u32>,
}

impl WeightTable {
    pub fn new() -> WeightTable {
        Default::default()
    }

    pub fn add(&mut self, weight: u32) {
        self.weight_sum += weight;
        self.cumulative_weights
            .push(weight + self.cumulative_weights.last().copied().unwrap_or(0));
    }

    pub fn get(&self, hash: u32) -> usize {
        match self
            .cumulative_weights
            .binary_search(&(hash % self.weight_sum + 1))
        {
            Ok(v) => v,
            Err(v) => v,
        }
    }

    pub fn weight_sum(&self) -> u32 {
        self.weight_sum
    }
}
