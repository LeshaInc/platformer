use std::ops::{Index, IndexMut, Range};

use engine::math::Vec2D;
use rayon::prelude::*;

#[derive(Clone, Debug)]
pub struct Map2D<T> {
    size: Vec2D<u32>,
    data: Vec<T>,
}

impl<T> Map2D<T> {
    pub fn new_default(size: Vec2D<u32>) -> Map2D<T>
    where
        T: Clone + Default,
    {
        Map2D {
            size,
            data: vec![T::default(); size.cast().prod()],
        }
    }

    pub fn new(size: Vec2D<u32>, data: Vec<T>) -> Map2D<T> {
        assert!(data.len() == size.cast::<usize>().prod());
        Map2D { size, data }
    }

    pub fn size(&self) -> Vec2D<u32> {
        self.size
    }

    fn calc_index(&self, pos: Vec2D<u32>) -> Option<usize> {
        if pos.x >= self.size.x || pos.y >= self.size.y {
            None
        } else {
            let pos = pos.cast::<usize>();
            let width = self.size.x as usize;
            Some(pos.y * width + pos.x)
        }
    }

    fn inv_index(width: u32, idx: usize) -> Vec2D<u32> {
        let x = (idx % (width as usize)) as u32;
        let y = (idx / (width as usize)) as u32;
        Vec2D::new(x, y)
    }

    pub fn get(&self, pos: Vec2D<u32>) -> &T {
        self.calc_index(pos)
            .map(|idx| unsafe { self.data.get_unchecked(idx) })
            .unwrap()
    }

    pub fn try_get(&self, pos: Vec2D<u32>) -> Option<&T> {
        self.calc_index(pos)
            .map(|idx| unsafe { self.data.get_unchecked(idx) })
    }

    pub fn get_mut(&mut self, pos: Vec2D<u32>) -> &mut T {
        self.calc_index(pos)
            .map(move |idx| unsafe { self.data.get_unchecked_mut(idx) })
            .unwrap()
    }

    fn row_range(&self, y: u32) -> Range<usize> {
        if y >= self.size.y {
            panic!("index out of bounds");
        }

        let width = self.size.x as usize;
        let start = y as usize * width;
        let end = start + width;
        start..end
    }

    pub fn row(&self, y: u32) -> impl Iterator<Item = &T> + '_ {
        let range = self.row_range(y);
        unsafe { self.data.get_unchecked(range).iter() }
    }

    pub fn row_mut(&mut self, y: u32) -> impl Iterator<Item = &mut T> + '_ {
        let range = self.row_range(y);
        unsafe { self.data.get_unchecked_mut(range).iter_mut() }
    }

    pub fn set(&mut self, pos: Vec2D<u32>, block: T) -> T {
        std::mem::replace(self.get_mut(pos), block)
    }

    pub fn fill(&mut self, origin: Vec2D<u32>, size: Vec2D<u32>, block: T)
    where
        T: Clone,
    {
        for x in origin.x..origin.x + size.x {
            for y in origin.y..origin.y + size.y {
                self.set(Vec2D::new(x, y), block.clone());
            }
        }
    }

    pub fn coords(&self) -> impl Iterator<Item = Vec2D<u32>> {
        let size = self.size;
        (0..size.y).flat_map(move |y| (0..size.x).map(move |x| Vec2D::new(x, y)))
    }

    pub fn iter(&self) -> impl Iterator<Item = (Vec2D<u32>, &T)> + '_ {
        self.coords().zip(&self.data)
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = (Vec2D<u32>, &mut T)> + '_ {
        self.coords().zip(&mut self.data)
    }

    pub fn par_iter(&self) -> impl ParallelIterator<Item = (Vec2D<u32>, &T)> + '_
    where
        T: Sync,
    {
        let width = self.size.x;
        self.data
            .par_iter()
            .enumerate()
            .map(move |(i, d)| (Self::inv_index(width, i), d))
    }

    pub fn par_iter_mut(&mut self) -> impl ParallelIterator<Item = (Vec2D<u32>, &mut T)> + '_
    where
        T: Send,
    {
        let width = self.size.x;
        self.data
            .par_iter_mut()
            .enumerate()
            .map(move |(i, d)| (Self::inv_index(width, i), d))
    }

    pub fn into_inner(self) -> Vec<T> {
        self.data
    }
}

impl Map2D<u8> {
    pub fn blur(&mut self) {
        unsafe {
            for y in 1..self.size.y - 1 {
                self.blur_line(
                    (y as usize) * (self.size.x as usize),
                    (y as usize + 1) * (self.size.x as usize),
                    1,
                );
            }

            for x in 1..self.size.x - 1 {
                self.blur_line(
                    x as usize,
                    x as usize + (self.size.x as usize) * (self.size.y as usize - 1),
                    self.size.x as usize,
                );
            }
        }
    }

    unsafe fn blur_line(&mut self, start_idx: usize, end_idx: usize, step: usize) {
        let mut idx = start_idx;
        let mut first = *self.data.get_unchecked(idx);
        idx += step;
        let mut sum = (first as u16) + *self.data.get_unchecked(idx) as u16;
        idx += step;

        while idx != end_idx {
            sum += *self.data.get_unchecked(idx) as u16;
            let avg = (sum / 3) as u8;
            let prev = self.data.get_unchecked_mut(idx - step);
            sum -= first as u16;
            first = *prev;
            *prev = avg;
            idx += step;
        }
    }
}

impl<T> Index<Vec2D<u32>> for Map2D<T> {
    type Output = T;
    fn index(&self, pos: Vec2D<u32>) -> &T {
        self.get(pos)
    }
}

impl<T> IndexMut<Vec2D<u32>> for Map2D<T> {
    fn index_mut(&mut self, pos: Vec2D<u32>) -> &mut T {
        self.get_mut(pos)
    }
}
