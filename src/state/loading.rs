use std::sync::mpsc::{self, Receiver};

use engine::assets::{Assets, ProgressCounter};
use engine::ecs::World;
use engine::graphics::{DrawEncoder, DrawList, Font, TextLayoutSettings, TextLayouter};
use engine::math::{Rgba, Vec2D};
use engine::{GameState, StateTransition};
use eyre::Result;

use super::main::{MainState, MainStateAssetSet};
use crate::planet::{gen, Planet};

engine::asset_set!(pub LoadingStateAssetSet {
    font: Font = "fonts/UbuntuMono-Regular.ttf",
});

pub struct LoadingState {
    asset_set: LoadingStateAssetSet,
    main_asset_set: MainStateAssetSet,
    progress_counter: ProgressCounter,
    planet_receiver: Receiver<Planet>,
    planet: Option<Planet>,
}

impl LoadingState {
    pub fn new(world: &mut World) -> Result<LoadingState> {
        let assets = world.resources.get::<Assets>();

        let asset_set = LoadingStateAssetSet::new(&assets);
        asset_set.wait_sync(&assets);

        let progress_counter = assets.progress_counter();
        let main_asset_set = MainStateAssetSet::new(&assets);

        let (planet_sender, planet_receiver) = mpsc::channel();
        rayon::spawn(move || {
            planet_sender.send(generate_planet()).unwrap();
        });

        Ok(LoadingState {
            asset_set,
            main_asset_set,
            progress_counter,
            planet_receiver,
            planet: None,
        })
    }
}

impl GameState for LoadingState {
    fn on_update(&mut self, world: &mut World) -> Result<StateTransition> {
        self.progress_counter.update();

        if self.planet.is_none() {
            if let Ok(planet) = self.planet_receiver.try_recv() {
                self.planet = Some(planet);
            }
        }

        if !self.progress_counter.has_finished() {
            return Ok(StateTransition::Continue);
        }

        if let Some(planet) = self.planet.take() {
            let main_asset_set = self.main_asset_set.clone();
            let main_state = MainState::new(world, main_asset_set, planet);
            return Ok(StateTransition::Push(Box::new(main_state)));
        }

        Ok(StateTransition::Continue)
    }

    fn on_draw(&mut self, world: &mut World) -> Result<()> {
        let assets = world.resources.get::<Assets>();
        let fonts = assets.storage();

        let mut draw_list = world.resources.get_mut::<DrawList>();
        let mut encoder = DrawEncoder::new(&mut draw_list);

        let mut text_layouter = TextLayouter::new(&fonts);

        text_layouter.reset(&TextLayoutSettings::default());

        let color = Rgba::WHITE;
        let font = self.asset_set.font.id();

        let text = format!("Loading: {:.1}%\n\n", self.progress_counter.percentage());
        text_layouter.append(font, 20.0, color, None, &text);

        let text = format!(
            "Finished: {}\nTotal: {}\nErrored: {}\n\n",
            self.progress_counter.num_finished(),
            self.progress_counter.num_total(),
            self.progress_counter.num_errored(),
        );
        text_layouter.append(font, 16.0, color, None, &text);

        for path in self.progress_counter.currently_loading() {
            let text = format!("{}\n", path.display());
            text_layouter.append(font, 16.0, color, None, &text);
        }

        if self.planet.is_some() {
            let text = "\nGenerating world...";
            text_layouter.append(font, 20.0, color, None, text);
        }

        text_layouter.encode(&mut encoder, Vec2D::splat(10.0));

        Ok(())
    }
}

fn generate_planet() -> Planet {
    let mut rng = rand::thread_rng();
    let size = Vec2D::new(1000, 800);
    gen::gen(&mut rng, size)
}
