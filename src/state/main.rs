use engine::assets::{Assets, Handle};
use engine::ecs::{IntoSystem, Res, ResMut, Schedule, World};
use engine::graphics::{DebugDrawList, Font, Image};
use engine::profiler::Profiler;
use engine::ui::SkinSet;
use engine::{GameState, Resolution, StateTransition};
use eyre::Result;
use rayon::{ThreadPool, ThreadPoolBuilder};

use crate::block::BlockRegistry;
use crate::camera::Camera;
use crate::debug_view::DebugView;
use crate::entity::character::{Character, CharacterSpritesheet};
use crate::navmesh::NavMesh;
use crate::planet::{BlockTransitions, LightingEngine, Planet, SkyTexture};
use crate::wall::WallRegistry;

engine::asset_set!(pub MainStateAssetSet {
    skins: SkinSet = "main.skin",
    block_registry: BlockRegistry = "blocks/blocks.json",
    wall_registry: WallRegistry = "walls/walls.json",
    block_transitions: BlockTransitions = "blocks/transitions/map.json",
    character_spritesheet: CharacterSpritesheet = "entities/player/spritesheet.json",
    sky_texture: Image = "blocks/sky/sky.png",
    white_1x1: Image = "images/white-1x1.png",
    tree: Image = "entities/tree/0.png",
    font: Font = "fonts/UbuntuMono-Regular.ttf",
});

pub struct MainState {
    update_schedule: Schedule,
    draw_schedule: Schedule,
    debug_view: DebugView,
    thread_pool: ThreadPool,
}

#[derive(Debug)]
pub struct GlobalImages {
    pub white_1x1: Handle<Image>,
    pub tree: Handle<Image>,
}

impl MainState {
    pub fn new(world: &mut World, asset_set: MainStateAssetSet, planet: Planet) -> MainState {
        let mut navmesh = NavMesh::new(planet.blocks.size());

        {
            let assets = world.resources.get::<Assets>();
            let block_registry = assets.get(&asset_set.block_registry).unwrap();
            navmesh.build(&planet.blocks, &block_registry);
        }

        DebugDrawList::new(asset_set.font.clone(), asset_set.white_1x1.clone()).register_global();

        let mut debug_view = DebugView::new(asset_set.font.clone());
        debug_view.add_default_sections();

        world.resources.insert(asset_set.skins);
        world.resources.insert(asset_set.block_registry);
        world.resources.insert(asset_set.wall_registry);
        world.resources.insert(asset_set.block_transitions);
        world.resources.insert(SkyTexture(asset_set.sky_texture));
        world.resources.insert(GlobalImages {
            white_1x1: asset_set.white_1x1,
            tree: asset_set.tree,
        });
        world.resources.insert(asset_set.font);

        world.resources.insert(planet);
        world.resources.insert(navmesh);

        world.resources.insert(Camera::new());
        world.resources.insert(LightingEngine::default());

        Character::spawn(world, asset_set.character_spritesheet);

        let update_schedule = build_update_schedule(world);
        let draw_schedule = build_draw_schedule(world);

        let thread_pool = ThreadPoolBuilder::new()
            .thread_name(|i| format!("ecs-{}", i))
            .build()
            .unwrap(); // TODO

        MainState {
            update_schedule,
            draw_schedule,
            debug_view,
            thread_pool,
        }
    }
}

impl GameState for MainState {
    fn on_update(&mut self, world: &mut World) -> Result<StateTransition> {
        let _guard = Profiler::get().time_guard(0, "update");
        self.update_schedule.run(&self.thread_pool, world);
        self.debug_view.update(world);
        Ok(StateTransition::Continue)
    }

    fn on_draw(&mut self, world: &mut World) -> Result<()> {
        let _guard = Profiler::get().time_guard(100, "draw");
        self.draw_schedule.run(&self.thread_pool, world);
        self.debug_view.draw(world);
        Ok(())
    }
}

fn build_update_schedule(world: &World) -> Schedule {
    Schedule::builder()
        .add(crate::entity::character::update_ground_node.system())
        .add(crate::entity::apply_gravity.system())
        .add(crate::entity::character::control_character.system())
        .add(crate::entity::agent::follow_player.system())
        .add(crate::entity::collision::move_and_collide.system())
        .add(crate::entity::character::center_camera.system())
        .add(crate::entity::character::place_blocks.system())
        .add(crate::entity::character::spawn_friends.system())
        .add(crate::entity::character::spawn_character_particles.system())
        .add(crate::entity::particle::kill_particles.system())
        .build(world)
}

fn build_draw_schedule(world: &World) -> Schedule {
    Schedule::builder()
        .add(update_camera_resolution.system())
        .add(crate::planet::draw_sky.system())
        .add(crate::planet::draw_walls.system())
        .add(crate::planet::draw_blocks.system())
        .add(crate::entity::block::draw_block_entities.system())
        .add(crate::entity::particle::draw_particles.system())
        .add(crate::entity::character::draw_character.system())
        .add(crate::planet::draw_lighting.system())
        .build(world)
}

fn update_camera_resolution(resolution: Res<Resolution>, mut camera: ResMut<Camera>) {
    camera.resolution = resolution.0;
}
