use std::cmp::Ordering;
use std::collections::hash_map::Entry;
use std::collections::BinaryHeap;

use ahash::AHashMap;
use engine::graphics::DebugDrawList;
use engine::math::collision::aabb_vs_aabb_sweep;
use engine::math::{solve_quadratic, Box2D, Rgba, Vec2D};
use engine::profiler::Profiler;
use rayon::iter::ParallelIterator;

use crate::block::{Block, BlockId, BlockRegistry};
use crate::entity::character::{GRAVITY, HORIZ_SPEED, MAX_JUMP_HEIGHT};
use crate::planet::SCALE;
use crate::util::Map2D;

const MIN_VERT_CLEARANCE: u8 = 4;
const MAX_VERT_CLEARANCE: u8 = 4;
const MIN_HORIZ_CLEARANCE: u8 = 2;
const MAX_HORIZ_CLEARANCE: u8 = 2;
const MIN_HORIZ_CLEARANCE_HALF: f32 = 1.0;
const MAX_HORIZ_CLEARANCE_HALF: f32 = 1.0;

#[derive(Clone, Debug)]
pub struct NavMesh {
    chunks: Map2D<NavMeshChunk>,
    edges: AHashMap<NodeId, Vec<Edge>>,
}

impl NavMesh {
    pub fn new(size: Vec2D<u32>) -> NavMesh {
        NavMesh {
            chunks: Map2D::new_default(size.elementwise_div_ceil(CHUNK_SIZE)),
            edges: AHashMap::default(),
        }
    }

    #[inline(never)]
    pub fn build(&mut self, blocks: &Map2D<BlockId>, registry: &BlockRegistry) {
        self.build_nodes(blocks, registry);
        self.build_edges(blocks, registry);

        let mut sum = 0;
        for (_src, edges) in &self.edges {
            sum += edges.len();
        }
        let b = sum as f32 / self.edges.len() as f32;
        dbg!(b);
    }

    pub fn update(&mut self, blocks: &Map2D<BlockId>, registry: &BlockRegistry, pos: Vec2D<u32>) {
        let center_pos = pos.elementwise_div(CHUNK_SIZE);
        if center_pos.ge(&self.chunks.size()).any() {
            return;
        }

        let chunk = self.chunks.get_mut(center_pos);
        chunk.build_nodes(BuildNodesCtx {
            chunk_pos: center_pos,
            blocks,
            registry,
        });

        self.edges.retain(|src, edges| {
            let center_pos = center_pos.cast::<u16>();
            edges.retain(|edge| edge.dst.chunk != center_pos);
            src.chunk != center_pos && !edges.is_empty()
        });

        let mut appender = EdgeAppender::default();

        let chunk_min = center_pos
            .saturating_sub(Vec2D::splat(1))
            .min(self.chunks.size());
        let chunk_max = center_pos
            .saturating_add(Vec2D::splat(1))
            .min(self.chunks.size());

        for y in chunk_min.y..=chunk_max.y {
            for x in chunk_min.x..=chunk_max.x {
                let chunk_pos = Vec2D::new(x, y);
                let chunk = self.chunks.get(chunk_pos);
                chunk.build_edges(BuildEdgesCtx {
                    chunk_pos,
                    navmesh: self,
                    blocks,
                    registry,
                    appender: &mut appender,
                });
            }
        }

        for ((src, _), edge) in appender.edges.drain() {
            let edges = self.edges.entry(src).or_default();
            edges.push(edge);
        }
    }

    fn build_nodes(&mut self, blocks: &Map2D<BlockId>, registry: &BlockRegistry) {
        self.chunks.par_iter_mut().for_each(move |(pos, chunk)| {
            chunk.build_nodes(BuildNodesCtx {
                chunk_pos: pos,
                blocks,
                registry,
            });
        });
    }

    fn build_edges(&mut self, blocks: &Map2D<BlockId>, registry: &BlockRegistry) {
        self.edges.clear();

        let self_ = &self;
        let it = self.chunks.par_iter();
        let appenders = it
            .fold(EdgeAppender::default, |mut vec, (chunk_pos, chunk)| {
                chunk.build_edges(BuildEdgesCtx {
                    chunk_pos,
                    navmesh: self_,
                    blocks,
                    registry,
                    appender: &mut vec,
                });
                vec
            })
            .collect::<Vec<_>>();

        for mut appender in appenders {
            for ((src, _), edge) in appender.edges.drain() {
                let edges = self.edges.entry(src).or_default();
                edges.push(edge);
            }
        }
    }

    fn find_subnode(&self, pos: Vec2D<f32>) -> Option<SubNode> {
        let chunk_pos = (pos / SCALE)
            .elementwise_div(CHUNK_SIZE.cast::<f32>())
            .cast::<u32>();
        let chunk = self.chunks.try_get(chunk_pos)?;
        chunk.find_subnode(chunk_pos, pos)
    }

    pub fn find_node(&self, pos: Vec2D<f32>) -> Option<NodeId> {
        self.find_subnode(pos).map(|n| n.node)
    }

    fn find_nodes_in(&self, rect: Box2D<u32>) -> impl Iterator<Item = NodeId> + '_ {
        let max_chunk = self.chunks.size() - Vec2D::splat(1);
        let chunk_min = rect.min.elementwise_div(CHUNK_SIZE).min(max_chunk);
        let chunk_max = rect.max.elementwise_div(CHUNK_SIZE).min(max_chunk);

        (chunk_min.y..=chunk_max.y)
            .flat_map(move |y| (chunk_min.x..=chunk_max.x).map(move |x| Vec2D::new(x, y)))
            .flat_map(move |chunk_pos| {
                let chunk = self.chunks.get(chunk_pos);

                let offset = chunk_pos.elementwise_mul(CHUNK_SIZE);
                let rect = Box2D::new(
                    rect.min.saturating_sub(offset).cast(),
                    rect.max
                        .saturating_sub(offset)
                        .min(CHUNK_SIZE - Vec2D::splat(1))
                        .cast(),
                );

                chunk.find_nodes_in(chunk_pos, rect)
            })
    }

    fn find_subnode_by_block(&self, pos: Vec2D<u32>) -> Option<SubNode> {
        let chunk_pos = pos.elementwise_div(CHUNK_SIZE);
        let chunk = self.chunks.try_get(chunk_pos)?;
        chunk.find_subnode_by_block(chunk_pos, pos)
    }

    pub fn debug_draw(&self) {
        let mut debug = DebugDrawList::get();
        for (pos, chunk) in self.chunks.iter() {
            chunk.debug_draw(pos, &mut debug);
        }

        // for (src, edges) in &self.edges {
        //     for edge in edges {
        //         edge.debug_draw(&self, &src, &mut debug, Rgba::new(0.0, 0.0, 1.0, 0.1));
        //     }
        // }
    }
}

#[derive(Clone, Copy)]
struct BuildNodesCtx<'a> {
    chunk_pos: Vec2D<u32>,
    blocks: &'a Map2D<BlockId>,
    registry: &'a BlockRegistry,
}

struct BuildEdgesCtx<'a> {
    chunk_pos: Vec2D<u32>,
    navmesh: &'a NavMesh,
    blocks: &'a Map2D<BlockId>,
    registry: &'a BlockRegistry,
    appender: &'a mut EdgeAppender,
}

impl BuildEdgesCtx<'_> {
    fn reborrow(&mut self) -> BuildEdgesCtx<'_> {
        BuildEdgesCtx {
            chunk_pos: self.chunk_pos,
            navmesh: self.navmesh,
            blocks: self.blocks,
            registry: self.registry,
            appender: self.appender,
        }
    }
}

const CHUNK_SIZE: Vec2D<u32> = Vec2D { x: 32, y: 32 };

#[derive(Clone, Debug, Default)]
struct NavMeshChunk {
    nodes: Vec<Node>,
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
struct Node {
    pos: Vec2D<u8>,
    size: Vec2D<u8>,
    l_clearances: [u8; (MAX_VERT_CLEARANCE as usize) + 1],
    r_clearances: [u8; (MAX_VERT_CLEARANCE as usize) + 1],
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
enum NodeSide {
    Left,
    Right,
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct NodeId {
    pub chunk: Vec2D<u16>,
    pub node: u16,
}

impl NodeId {
    fn new(chunk: Vec2D<u32>, node_idx: usize) -> NodeId {
        NodeId {
            chunk: chunk.cast(),
            node: node_idx as _,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct SubNode {
    node: NodeId,
    offset: u8,
}

#[derive(Clone, Debug)]
struct Edge {
    time: f32,
    src_offset: u8,
    dst_offset: u8,
    dst: NodeId,
    kind: EdgeKind,
    trajectory: Vec<Vec2D<f32>>,
}

impl Edge {
    fn debug_draw(&self, navmesh: &NavMesh, src: &NodeId, debug: &mut DebugDrawList, col: Rgba) {
        let dst = self.dst;
        let src_ofs = navmesh.chunks.get(src.chunk.cast()).nodes[src.node as usize].pos;
        let dst_ofs = navmesh.chunks.get(dst.chunk.cast()).nodes[dst.node as usize].pos;

        let src_pos = (src.chunk.cast::<f32>().elementwise_mul(CHUNK_SIZE.cast())
            + src_ofs.cast::<f32>()
            + Vec2D::new(self.src_offset as f32 + 0.5, -0.5))
            * SCALE;
        let dst_pos = (dst.chunk.cast::<f32>().elementwise_mul(CHUNK_SIZE.cast())
            + dst_ofs.cast::<f32>()
            + Vec2D::new(self.dst_offset as f32 + 0.5, -0.5))
            * SCALE;

        if self.trajectory.is_empty() {
            debug.arrow(col, src_pos, dst_pos);
        }

        for pt in self.trajectory.windows(2) {
            debug.arrow(col, pt[0], pt[1]);
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum EdgeKind {
    Walk,
    Fall,
    Jump(JumpKind),
}

#[derive(Clone, Copy, Debug)]
struct JumpKind(i8);

impl JumpKind {
    fn values() -> impl Iterator<Item = JumpKind> {
        let max = (MAX_JUMP_HEIGHT).floor() as i8;
        (1..=max).flat_map(|h| [JumpKind(-h), JumpKind(h)])
    }

    fn init(&self, dt: f32) -> Vec2D<f32> {
        let x = HORIZ_SPEED * (self.0.signum() as f32);
        let y = (2.0 * GRAVITY * (self.0.abs() as f32 + 0.1)).sqrt();
        Vec2D::new(x, -y - GRAVITY * dt)
    }

    fn update(&self, dt: f32, vel: &mut Vec2D<f32>) {
        vel.y += GRAVITY * dt;
        vel.x = HORIZ_SPEED * (self.0.signum() as f32);
    }
}

#[derive(Default)]
struct EdgeAppender {
    edges: AHashMap<(NodeId, NodeId), Edge>,
}

impl NavMeshChunk {
    fn find_subnode(&self, chunk_pos: Vec2D<u32>, pos: Vec2D<f32>) -> Option<SubNode> {
        if pos.y.fract().abs() > 0.001 {
            return None;
        }

        let pos_x = pos.x / SCALE;
        let pos_y = (pos.y / SCALE + 0.01).round().max(0.0) as u32;

        for (node_idx, node) in self.nodes.iter().enumerate() {
            let node_pos = node.pos.cast::<u32>() + chunk_pos.elementwise_mul(CHUNK_SIZE);
            let node_start = node_pos.x as f32;
            let node_len = node.size.x as f32;
            let node_end = node_start + node_len;
            let node_y = node_pos.y;

            if pos_y == node_y
                && node_start - pos_x < MAX_HORIZ_CLEARANCE_HALF
                && pos_x - node_end < MAX_HORIZ_CLEARANCE_HALF
            {
                return Some(SubNode {
                    node: NodeId::new(chunk_pos, node_idx),
                    offset: (pos_x - node_start).max(0.0).min((node.size.x - 1) as f32) as u8,
                });
            }
        }

        None
    }

    fn find_subnode_by_block(&self, chunk_pos: Vec2D<u32>, pos: Vec2D<u32>) -> Option<SubNode> {
        for (node_idx, node) in self.nodes.iter().enumerate() {
            let node_pos = node.pos.cast::<u32>() + chunk_pos.elementwise_mul(CHUNK_SIZE);
            if pos.y == node_pos.y
                && pos.x >= node_pos.x
                && pos.x < node_pos.x + (node.size.x as u32)
            {
                return Some(SubNode {
                    node: NodeId::new(chunk_pos, node_idx),
                    offset: (pos.x - node_pos.x) as u8,
                });
            }
        }

        None
    }

    fn find_nodes_in(
        &self,
        chunk_pos: Vec2D<u32>,
        rect: Box2D<u8>,
    ) -> impl Iterator<Item = NodeId> + '_ {
        self.nodes
            .iter()
            .enumerate()
            .flat_map(move |(node_idx, node)| {
                if node.pos.x >= rect.min.x
                    && node.pos.x <= rect.max.x
                    && node.pos.y >= rect.min.y
                    && node.pos.y <= rect.max.y
                {
                    let node_rect = Box2D::new(node.pos, node.pos + Vec2D::new(node.size.x, 1));
                    if rect.intersects(&node_rect) {
                        return Some(NodeId {
                            chunk: chunk_pos.cast(),
                            node: node_idx as _,
                        });
                    }
                }
                None
            })
    }

    fn build_nodes(&mut self, ctx: BuildNodesCtx) {
        self.nodes.clear();

        for y in 0..CHUNK_SIZE.y {
            let mut node: Option<Node> = None;

            for x in 0..CHUNK_SIZE.x {
                let pos = Vec2D::new(x, y);
                let node_pos = pos.cast::<u8>();
                let block_pos = ctx.chunk_pos.elementwise_mul(CHUNK_SIZE) + pos;
                if !Block::is_solid_at(ctx.blocks, ctx.registry, block_pos) {
                    if let Some(n) = node {
                        self.nodes.push(n);
                        node = None;
                    }
                    continue;
                }

                let mut vert_clearance = 0;
                for i in 1..=MAX_VERT_CLEARANCE {
                    if let Some(pos) = block_pos.checked_sub(Vec2D::new(0, i as u32)) {
                        if Block::is_solid_at(ctx.blocks, ctx.registry, pos) {
                            break;
                        }
                        vert_clearance = i;
                    } else {
                        break;
                    }
                }

                if vert_clearance < MIN_VERT_CLEARANCE {
                    if let Some(n) = node {
                        self.nodes.push(n);
                        node = None;
                    }
                    continue;
                }

                if let Some(n) = node {
                    if n.size.y != vert_clearance {
                        self.nodes.push(n);
                        node = None;
                    }
                }

                node = Some(match node {
                    Some(prev) => Node {
                        size: prev.size + Vec2D::new(1, 0),
                        ..prev
                    },
                    None => Node {
                        pos: node_pos,
                        size: Vec2D::new(1, vert_clearance),
                        ..Node::default()
                    },
                });
            }

            if let Some(n) = node {
                self.nodes.push(n);
            }
        }

        for node in &mut self.nodes {
            let pos = ctx.chunk_pos.elementwise_mul(CHUNK_SIZE) + node.pos.cast::<u32>();
            node.l_clearances = Self::calc_row_clearances(ctx, pos, node.size.x, NodeSide::Left);
            node.r_clearances = Self::calc_row_clearances(ctx, pos, node.size.x, NodeSide::Right);
        }
    }

    fn calc_row_clearances(
        ctx: BuildNodesCtx,
        pos: Vec2D<u32>,
        length: u8,
        side: NodeSide,
    ) -> [u8; (MAX_VERT_CLEARANCE as usize) + 1] {
        let mut row_clearances = [0; (MAX_VERT_CLEARANCE as usize) + 1];

        for sy in 0..=MAX_VERT_CLEARANCE {
            let mut row_clearance = 0;

            for sx in 1..=MAX_HORIZ_CLEARANCE {
                let offset = Vec2D::new(sx, sy).cast::<u32>();

                let block_pos = match side {
                    NodeSide::Left => pos.checked_sub(offset),
                    NodeSide::Right => {
                        let edge_pos = pos + Vec2D::new(length as u32 - 1, 0);
                        edge_pos.checked_add_sub(offset)
                    }
                };

                let block_pos = match block_pos {
                    Some(v) => v,
                    None => break,
                };

                if Block::is_solid_at(ctx.blocks, ctx.registry, block_pos) {
                    break;
                }

                row_clearance = sx;
            }

            row_clearances[sy as usize] = row_clearance;
        }

        row_clearances
    }

    fn build_edges(&self, mut ctx: BuildEdgesCtx) {
        self.build_walk_and_fall_edges(ctx.reborrow());
        self.build_jump_edges(ctx);
    }

    fn build_walk_and_fall_edges(&self, mut ctx: BuildEdgesCtx) {
        for node_idx in 0..self.nodes.len() {
            self.build_walk_and_fall_edge(ctx.reborrow(), node_idx, NodeSide::Left);
            self.build_walk_and_fall_edge(ctx.reborrow(), node_idx, NodeSide::Right);
        }
    }

    #[inline(always)]
    fn build_walk_and_fall_edge(&self, mut ctx: BuildEdgesCtx, node_idx: usize, side: NodeSide) {
        let src_node = &self.nodes[node_idx];
        let src_node_id = NodeId::new(ctx.chunk_pos, node_idx);
        let src_node_pos = ctx.chunk_pos.elementwise_mul(CHUNK_SIZE) + src_node.pos.cast::<u32>();

        let src_offset = match side {
            NodeSide::Left => 0,
            NodeSide::Right => src_node.size.x - 1,
        };

        let row_clearances = match side {
            NodeSide::Left => &src_node.l_clearances,
            NodeSide::Right => &src_node.r_clearances,
        };

        for vert_clearance in MIN_VERT_CLEARANCE..=MAX_VERT_CLEARANCE {
            let min = |s: &[u8]| s.iter().copied().min().unwrap_or(0);

            let clearance = min(&row_clearances[1..=vert_clearance as usize]);
            let hole = row_clearances[0];

            if clearance < MIN_HORIZ_CLEARANCE {
                continue;
            }

            if hole < MIN_HORIZ_CLEARANCE {
                let dst_pos = match side {
                    NodeSide::Left => {
                        let offset = Vec2D::new((hole + 1) as u32, 0);
                        src_node_pos.checked_sub(offset)
                    }
                    NodeSide::Right => {
                        let offset = Vec2D::new((src_node.size.x as u32) + (hole as u32), 0);
                        src_node_pos.checked_add(offset)
                    }
                };

                let dst = dst_pos
                    .and_then(|pos| ctx.navmesh.find_subnode_by_block(pos).map(|v| (pos, v)));
                if let Some((dst_pos, dst)) = dst {
                    let dist = (dst_pos.x as f32) - (src_node_pos.x as f32) - (src_offset as f32);
                    let time = dist.abs() * SCALE / HORIZ_SPEED;
                    ctx.appender.edges.insert(
                        (src_node_id, dst.node),
                        Edge {
                            time,
                            src_offset,
                            dst_offset: dst.offset,
                            dst: dst.node,
                            kind: EdgeKind::Walk,
                            trajectory: Vec::new(),
                        },
                    );
                }
            } else {
                let pos = match side {
                    NodeSide::Left => src_node_pos.checked_sub_add(Vec2D::new(1, 1)),
                    NodeSide::Right => {
                        src_node_pos.checked_add(Vec2D::new(src_node.size.x as u32, 1))
                    }
                };

                if let Some(pos) = pos {
                    Self::build_fall_edge(ctx.reborrow(), src_node_id, src_offset, pos, side);
                }
            }
        }
    }

    #[inline(always)]
    fn build_fall_edge(
        ctx: BuildEdgesCtx,
        src_node_id: NodeId,
        src_offset: u8,
        mut pos: Vec2D<u32>,
        side: NodeSide,
    ) {
        let start_pos = pos;
        let max_y = pos.y + CHUNK_SIZE.y - 1;

        'outer: while pos.y < max_y {
            for sx in 0..MIN_HORIZ_CLEARANCE {
                let offset = Vec2D::new(sx as u32, 0);
                let block_pos = match side {
                    NodeSide::Left => match pos.checked_sub(offset) {
                        Some(v) => v,
                        None => break 'outer,
                    },
                    NodeSide::Right => pos + offset,
                };

                if Block::is_solid_at(ctx.blocks, ctx.registry, block_pos) {
                    break 'outer;
                }
            }

            pos.y += 1;
        }

        let mut pos = pos.cast::<f32>() - Vec2D::new(0.0, 0.0);
        match side {
            NodeSide::Left => pos.x -= MIN_HORIZ_CLEARANCE_HALF - 1.0,
            NodeSide::Right => pos.x += MIN_HORIZ_CLEARANCE_HALF,
        }

        pos *= SCALE;

        if let Some(dst) = ctx.navmesh.find_subnode(pos) {
            let dst_chunk = ctx.navmesh.chunks.get(dst.node.chunk.cast());
            let dst_node = &dst_chunk.nodes[dst.node.node as usize];
            let height = (dst_node.pos.y as f32)
                + (dst.node.chunk.y as f32) * (CHUNK_SIZE.y as f32)
                - (start_pos.y as f32)
                + 1.0;
            let time = (2.0 * height * SCALE / GRAVITY).sqrt();
            ctx.appender.edges.insert(
                (src_node_id, dst.node),
                Edge {
                    time,
                    src_offset,
                    dst_offset: dst.offset,
                    dst: dst.node,
                    kind: EdgeKind::Fall,
                    trajectory: Vec::new(),
                },
            );
        }
    }

    fn build_jump_edges(&self, mut ctx: BuildEdgesCtx) {
        for (src_node_idx, src_node) in self.nodes.iter().enumerate() {
            let src_node_id = NodeId::new(ctx.chunk_pos, src_node_idx);

            let inflate = Vec2D::new(16, 8);
            let src_chunk_offset = ctx.chunk_pos.cast::<u32>().elementwise_mul(CHUNK_SIZE);
            let rect = Box2D::new(
                (src_node.pos.cast::<u32>() + src_chunk_offset).saturating_sub(inflate),
                (src_node.pos + Vec2D::new(src_node.size.x, 1)).cast::<u32>()
                    + src_chunk_offset
                    + inflate,
            );

            for dst_node_id in ctx.navmesh.find_nodes_in(rect) {
                if dst_node_id == src_node_id {
                    continue;
                }

                Self::build_jump_edge_between(ctx.reborrow(), src_node_id, dst_node_id);
            }
        }
    }

    fn build_jump_edge_between(mut ctx: BuildEdgesCtx, src_node_id: NodeId, dst_node_id: NodeId) {
        let src_chunk = ctx.navmesh.chunks.get(src_node_id.chunk.cast());
        let dst_chunk = ctx.navmesh.chunks.get(dst_node_id.chunk.cast());

        let src_node = src_chunk.nodes[src_node_id.node as usize];
        let dst_node = dst_chunk.nodes[dst_node_id.node as usize];

        let src_chunk_offset = src_node_id.chunk.cast::<u32>().elementwise_mul(CHUNK_SIZE);
        let dst_chunk_offset = dst_node_id.chunk.cast::<u32>().elementwise_mul(CHUNK_SIZE);

        let src_bpos = src_node.pos.cast::<u32>() + src_chunk_offset;
        let dst_bpos = dst_node.pos.cast::<u32>() + dst_chunk_offset;

        let src_y = (src_bpos.y as f32) * SCALE;
        let dst_y = (dst_bpos.y as f32) * SCALE;

        let src_left = (src_bpos.x as f32 - MIN_HORIZ_CLEARANCE_HALF + 0.01) * SCALE;
        let dst_left = (dst_bpos.x as f32 - MIN_HORIZ_CLEARANCE_HALF + 0.01) * SCALE;

        let src_right =
            (src_bpos.x as f32 + src_node.size.x as f32 + MIN_HORIZ_CLEARANCE_HALF - 0.99) * SCALE;
        let dst_right =
            (dst_bpos.x as f32 + dst_node.size.x as f32 + MIN_HORIZ_CLEARANCE_HALF - 0.99) * SCALE;

        let apex = if dst_y < src_y {
            src_y - dst_y + SCALE * 3.0
        } else {
            SCALE * 3.0
        };

        let vel_y = -(2.0 * GRAVITY * apex).sqrt();
        if vel_y > (2.0 * MAX_JUMP_HEIGHT * GRAVITY).sqrt() {
            return;
        }

        let time = match solve_quadratic(GRAVITY / 2.0, vel_y, src_y - dst_y) {
            Some([_, b]) => b,
            None => return,
        };

        let directions = [
            (src_left, dst_left),
            (src_right, dst_right),
            (src_left, dst_right),
            (src_right, dst_left),
        ];

        for (src_x, dst_x) in directions {
            let vel_x = (dst_x - src_x) / time;
            if vel_x.abs() > HORIZ_SPEED {
                continue;
            }

            let pos = Vec2D::new(src_x, src_y);
            Self::simulate_jump(pos, Vec2D::new(vel_x, vel_y), ctx.reborrow());

            if ctx.appender.edges.contains_key(&(src_node_id, dst_node_id)) {
                break;
            }
        }
    }

    fn simulate_jump(mut pos: Vec2D<f32>, mut vel: Vec2D<f32>, ctx: BuildEdgesCtx) {
        let timestep = 1.0 / 20.0;
        let start_pos = pos;

        let src = match ctx.navmesh.find_subnode(pos) {
            Some(v) => v,
            None => return,
        };

        let collider = Box2D::new(Vec2D::new(-8.0, -32.0), Vec2D::new(8.0, 0.0));

        let mut trajectory = vec![pos];
        let mut apex = 0.0;
        let mut time = 0.0;

        while (pos.y < apex || vel.y.abs() > 0.01)
            && pos.y - start_pos.y < (CHUNK_SIZE.y as f32 - 1.0) * SCALE
        {
            let mut dt = timestep;
            time += dt;

            vel.y += GRAVITY * dt;

            let prev_pos = pos;

            loop {
                let mut hit_time = dt;
                let mut hit_normal = Vec2D::zero();

                let collider = collider.translate(pos);
                let extended = collider.extend_towards(vel * dt);
                let bounds = extended.to_tiles(SCALE, ctx.blocks.size());
                for block_pos in (bounds.min.x..=bounds.max.x)
                    .flat_map(|x| (bounds.min.y..=bounds.max.y).map(move |y| Vec2D::new(x, y)))
                {
                    if !Block::is_solid_at(ctx.blocks, ctx.registry, block_pos) {
                        continue;
                    }

                    let block_collider = Box2D::new(Vec2D::zero(), Vec2D::splat(SCALE))
                        .translate(block_pos.cast::<f32>() * SCALE);

                    if let Some(hit) = aabb_vs_aabb_sweep(collider, vel, block_collider) {
                        if hit.time < hit_time {
                            hit_time = hit.time;
                            hit_normal = hit.normal;
                        }
                    }
                }

                if hit_time >= dt {
                    pos += vel * dt;
                    break;
                } else {
                    pos += vel * hit_time;
                    dt -= hit_time;
                    vel -= hit_normal * hit_normal.dot(vel);
                }
            }

            if (prev_pos - pos).mag_squared() < 0.01 {
                break;
            }

            if pos.y > prev_pos.y {
                apex = pos.y;
            }

            trajectory.push(pos);
        }

        if let Some(dst) = ctx.navmesh.find_subnode(pos) {
            if dst.node.chunk == src.node.chunk && dst.node.node == src.node.node {
                return;
            }

            ctx.appender.edges.insert(
                (src.node, dst.node),
                Edge {
                    time,
                    src_offset: src.offset,
                    dst_offset: dst.offset,
                    dst: dst.node,
                    kind: EdgeKind::Jump(JumpKind(0)),
                    trajectory,
                },
            );
        }
    }

    fn debug_draw(&self, chunk_pos: Vec2D<u32>, debug: &mut DebugDrawList) {
        let pos = chunk_pos.elementwise_mul(CHUNK_SIZE).cast::<f32>() * SCALE;
        debug.rect(
            Rgba::WHITE,
            Box2D::new(pos, pos + CHUNK_SIZE.cast::<f32>() * SCALE),
        );

        for node in &self.nodes {
            let line_pos = pos + node.pos.cast::<f32>() * SCALE;
            let line_dir = Vec2D::new((node.size.x as f32) * SCALE, 0.0);
            debug.line(Rgba::GREEN, line_pos, line_pos + line_dir);

            let line_dir = Vec2D::new(0.0, -(node.size.y as f32) * SCALE);
            debug.line(Rgba::GREEN, line_pos, line_pos + line_dir);
        }
    }
}

struct MinPriority {
    priority: f32,
    node: NodeId,
}

impl PartialEq for MinPriority {
    fn eq(&self, other: &MinPriority) -> bool {
        self.priority == other.priority
    }
}

impl Eq for MinPriority {}

impl PartialOrd for MinPriority {
    fn partial_cmp(&self, other: &MinPriority) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for MinPriority {
    fn cmp(&self, other: &MinPriority) -> Ordering {
        other
            .priority
            .partial_cmp(&self.priority)
            .unwrap_or(Ordering::Less)
    }
}

fn heuristic(navmesh: &NavMesh, src: NodeId, dst: NodeId) -> f32 {
    let src_node = &navmesh.chunks.get(src.chunk.cast()).nodes[src.node as usize];
    let dst_node = &navmesh.chunks.get(src.chunk.cast()).nodes[src.node as usize];

    let src_chunk_offset = src.chunk.cast::<u32>().elementwise_mul(CHUNK_SIZE);
    let dst_chunk_offset = dst.chunk.cast::<u32>().elementwise_mul(CHUNK_SIZE);

    let src_pos = src_node.pos.cast::<u32>() + src_chunk_offset;
    let dst_pos = dst_node.pos.cast::<u32>() + dst_chunk_offset;

    (src_pos.cast::<f32>() - dst_pos.cast::<f32>()).mag_squared()
}

pub fn astar(navmesh: &NavMesh, start: NodeId, finish: NodeId) {
    let _guard = Profiler::get().time_guard(0, "astar");

    let mut frontier = BinaryHeap::new();

    let mut parents: AHashMap<NodeId, (NodeId, Edge)> = AHashMap::new();
    let mut cost_so_far: AHashMap<NodeId, f32> = AHashMap::new();

    frontier.push(MinPriority {
        priority: 0.0,
        node: start,
    });

    cost_so_far.insert(start, 0.0);

    while let Some(MinPriority { node, .. }) = frontier.pop() {
        if node == finish {
            let mut path = Vec::new();
            let mut node = finish;
            while let Some((parent, edge)) = parents.get(&node) {
                node = *parent;
                path.push(edge);
                if node == start {
                    break;
                }
            }
            path.reverse();
            // dbg!(&path);
            let mut src = start;
            for edge in path {
                edge.debug_draw(navmesh, &src, &mut DebugDrawList::get(), Rgba::RED);
                src = edge.dst;
            }
            return;
        }

        let edges = match navmesh.edges.get(&node) {
            Some(v) => v,
            None => continue,
        };

        for edge in edges {
            let new_cost = cost_so_far[&node] + edge.time;

            match cost_so_far.entry(edge.dst) {
                Entry::Vacant(e) => {
                    e.insert(new_cost);
                }
                Entry::Occupied(mut e) if *e.get() > new_cost => {
                    e.insert(new_cost);
                }
                _ => continue,
            }

            parents.insert(edge.dst, (node, edge.clone()));
            frontier.push(MinPriority {
                priority: new_cost + heuristic(navmesh, node, edge.dst),
                node: edge.dst,
            });
        }
    }
}
