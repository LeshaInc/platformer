pub mod agent;
pub mod block;
pub mod character;
pub mod collision;
pub mod particle;

use engine::ecs::{Query, Res, WorldView};
use engine::math::Vec2D;
use engine::DeltaTime;

use self::character::GRAVITY;

#[derive(Clone, Copy, Debug)]
pub struct Position(pub Vec2D<f32>);

#[derive(Clone, Copy, Debug)]
pub struct Velocity(pub Vec2D<f32>);

#[derive(Clone, Copy, Debug)]
pub struct BlockPos(pub Vec2D<u32>);

pub fn apply_gravity(world: &mut WorldView, query: Query<&mut Velocity>, dt: Res<DeltaTime>) {
    let dt = dt.0.as_secs_f32();
    for vel in query.iter(world) {
        vel.0.y += GRAVITY * dt;
    }
}
