use engine::ecs::{CommandBuffer, EntityId, Query, Res, ResMut, WorldView};
use engine::graphics::{DrawEncoder, DrawList, Sprite};
use engine::math::{Box2D, Rgba, Rotation2D, Vec2D};
use engine::DeltaTime;
use rand::Rng;

use super::collision::CollisionShape;
use super::{Position, Velocity};
use crate::camera::Camera;
use crate::state::main::GlobalImages;

#[derive(Clone, Copy, Debug)]
pub struct ParticleColor(pub Rgba);

#[derive(Clone, Copy, Debug)]
pub struct ParticleSize(pub f32);

#[derive(Clone, Copy, Debug)]
pub struct ParticleLifetime(pub f32);

pub fn draw_particles(
    world: &mut WorldView,
    query: Query<(&Position, &ParticleColor, &ParticleSize, &ParticleLifetime)>,
    mut draw_list: ResMut<DrawList>,
    camera: Res<Camera>,
    global_images: Res<GlobalImages>,
) {
    let mut encoder = DrawEncoder::new(&mut draw_list);

    camera.push_transform(&mut encoder);

    for (pos, color, size, time) in query.iter(world) {
        let pos = pos.0 - Vec2D::splat(size.0 / 2.0);

        let mut color = color.0;
        let alpha = (time.0 / 0.2).clamp(0.0, 1.0);
        color.a *= alpha;

        encoder.draw_sprite(
            Sprite::new(global_images.white_1x1.id(), pos)
                .with_size(Vec2D::splat(size.0))
                .with_color(color),
        );
    }

    encoder.pop_transform();
}

pub fn kill_particles(
    world: &mut WorldView,
    cbuf: &mut CommandBuffer,
    query: Query<(EntityId, &mut ParticleLifetime)>,
    dt: Res<DeltaTime>,
) {
    let dt = dt.0.as_secs_f32();
    for (entity, lifetime) in query.iter(world) {
        if lifetime.0 < 0.0 {
            cbuf.remove(entity);
        } else {
            lifetime.0 -= dt;
        }
    }
}

pub fn spawn_particle(cbuf: &mut CommandBuffer, pos: Vec2D<f32>, vel: Vec2D<f32>, color: Rgba) {
    let mut rng = rand::thread_rng(); // TODO
    let size = rng.gen_range(2.0..4.0);

    let sin = rng.gen_range(-0.2f32..0.2);
    let cos = (1.0 - sin * sin).sqrt();
    let vel = Rotation2D::new(cos, sin).rotate(vel) * rng.gen_range(0.5..1.5);

    cbuf.push((
        Position(pos),
        Velocity(vel),
        ParticleColor(color),
        ParticleSize(size),
        ParticleLifetime(rng.gen_range(0.3..0.5)),
        CollisionShape(Box2D::new(
            Vec2D::splat(-size / 2.0),
            Vec2D::splat(size / 2.0),
        )),
    ));
}
