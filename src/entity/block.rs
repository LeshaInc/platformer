use engine::assets::Handle;
use engine::ecs::{Query, Res, ResMut, WorldView};
use engine::graphics::{DrawEncoder, DrawList, Image, Sprite};
use engine::math::Vec2D;

use super::BlockPos;
use crate::camera::Camera;
use crate::planet::SCALE;

#[derive(Clone, Debug)]
pub struct BlockEntityImage {
    pub image: Handle<Image>,
    pub offset: Vec2D<f32>,
}

pub fn draw_block_entities(
    world: &mut WorldView,
    query: Query<(&BlockPos, &BlockEntityImage)>,
    mut draw_list: ResMut<DrawList>,
    camera: Res<Camera>,
) {
    let mut encoder = DrawEncoder::new(&mut draw_list);
    camera.push_transform(&mut encoder);

    for (&BlockPos(pos), image) in query.iter(world) {
        encoder.draw_sprite(Sprite::new(
            image.image.id(),
            pos.cast::<f32>() * SCALE + image.offset,
        ));
    }

    encoder.pop_transform();
}
