use engine::ecs::filter::HasComponent;
use engine::ecs::{CommandBuffer, Query, Res, WorldView};
use engine::math::{Box2D, Vec2D};

use super::character::{Character, GroundNode, HORIZ_SPEED};
use super::collision::CollisionShape;
use super::{Position, Velocity};
use crate::navmesh::{astar, NavMesh};

pub struct Agent;

impl Agent {
    pub fn spawn(cbuf: &mut CommandBuffer, pos: Vec2D<f32>) {
        cbuf.push((
            Agent,
            Position(pos),
            Velocity(Vec2D::zero()),
            CollisionShape(Box2D::new(Vec2D::new(-6.0, -30.0), Vec2D::new(6.0, 0.0))),
        ));
    }
}

pub fn follow_player(
    world: &mut WorldView,
    player_query: Query<(&Position, &GroundNode), HasComponent<Character>>,
    agent_query: Query<(&Position, &mut Velocity), HasComponent<Agent>>,
    navmesh: Res<NavMesh>,
) {
    let (player_pos, player_node) = match player_query.iter(world).next() {
        Some((pos, ground)) => (pos.0, ground.0),
        None => return,
    };

    for (Position(pos), Velocity(vel)) in agent_query.iter(world) {
        if let Some((src, dst)) =
            player_node.and_then(|dst| navmesh.find_node(*pos).map(|src| (src, dst)))
        {
            astar(&navmesh, src, dst);
        } else {
            vel.x = ((player_pos.x - pos.x) * 10.0).clamp(-HORIZ_SPEED, HORIZ_SPEED);
        }
    }
}
