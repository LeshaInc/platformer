use engine::assets::{Assets, Handle};
use engine::ecs::{Query, Res, WorldView};
use engine::math::collision::{aabb_vs_aabb_mtv, aabb_vs_aabb_sweep};
use engine::math::{Box2D, Vec2D};
use engine::DeltaTime;

use super::{Position, Velocity};
use crate::block::BlockRegistry;
use crate::planet::{Planet, SCALE};

#[derive(Clone, Copy, Debug)]
pub struct CollisionShape(pub Box2D<f32>);

pub fn move_and_collide(
    world: &mut WorldView,
    query: Query<(&mut Position, &mut Velocity, &CollisionShape)>,
    assets: Res<Assets>,
    planet: Res<Planet>,
    block_registry: Res<Handle<BlockRegistry>>,
    dt: Res<DeltaTime>,
) {
    let dt = dt.0.as_secs_f32();
    let block_registry = assets.get(&block_registry).unwrap();

    for (Position(pos), Velocity(vel), shape) in query.iter(world) {
        move_and_collide_inner(pos, vel, shape, &planet, &block_registry, dt)
    }
}

fn move_and_collide_inner(
    position: &mut Vec2D<f32>,
    velocity: &mut Vec2D<f32>,
    collision_shape: &CollisionShape,
    planet: &Planet,
    block_registry: &BlockRegistry,
    mut dt: f32,
) {
    let mut iter = 0;
    loop {
        let mut hit_time = dt;
        let mut hit_normal = Vec2D::zero();

        let collision_box = collision_shape.0.translate(*position);
        let extended = collision_box.extend_towards(*velocity * dt);
        let bounds = extended.to_tiles(SCALE, planet.blocks.size());
        for pos in (bounds.min.x..=bounds.max.x)
            .flat_map(|x| (bounds.min.y..=bounds.max.y).map(move |y| Vec2D::new(x, y)))
        {
            let block_id = *planet.blocks.get(pos);
            let block = block_registry.get(block_id);
            if !block.is_solid {
                continue;
            }

            let collision_box = collision_shape.0.translate(*position);
            let block_collision_box =
                Box2D::new(Vec2D::zero(), Vec2D::splat(SCALE)).translate(pos.cast::<f32>() * SCALE);

            if iter < 5 {
                if let Some(hit) = aabb_vs_aabb_sweep(collision_box, *velocity, block_collision_box)
                {
                    if hit.time < hit_time {
                        hit_time = hit.time;
                        hit_normal = hit.normal;
                    }
                }
            } else if let Some(mtv) = aabb_vs_aabb_mtv(collision_box, block_collision_box) {
                *position += mtv;
            }
        }

        if iter == 5 {
            break;
        }

        if hit_time >= dt {
            *position += *velocity * dt;
            iter = 5;
            continue;
        }

        *position += *velocity * hit_time;

        dt -= hit_time;
        *velocity -= hit_normal * hit_normal.dot(*velocity);
        iter += 1;
    }
}
