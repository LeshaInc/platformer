use std::path::{Path, PathBuf};

use engine::assets::{Asset, AssetDefaultLoader, AssetLoader, Assets, Handle, LoadingContext};
use engine::ecs::filter::HasComponent;
use engine::ecs::{CommandBuffer, Query, Res, ResMut, World, WorldView};
use engine::graphics::{DrawEncoder, DrawList, Image, Sprite};
use engine::input::{Input, KeyCode, MouseButton};
use engine::math::{Box2D, Vec2D};
use engine::DeltaTime;
use eyre::Result;
use rand::Rng;
use serde::Deserialize;

use super::agent::Agent;
use super::block::BlockEntityImage;
use super::collision::CollisionShape;
use super::particle::spawn_particle;
use super::BlockPos;
use crate::block::{BlockId, BlockRegistry};
use crate::camera::Camera;
use crate::entity::{Position, Velocity};
use crate::navmesh::{astar, NavMesh, NodeId};
use crate::planet::{Planet, SCALE};
use crate::state::main::GlobalImages;

/// Marker component
#[derive(Debug)]
pub struct Character;

impl Character {
    pub fn spawn(world: &mut World, spritesheet: Handle<CharacterSpritesheet>) {
        world.push((
            Character,
            Position(Vec2D::new(0.0, 0.0)),
            Velocity(Vec2D::new(0.0, 0.0)),
            CollisionShape(Box2D::new(Vec2D::new(-6.0, -30.0), Vec2D::new(6.0, 0.0))),
            spritesheet,
            CharacterAnimationState { run_time: 0.0 },
            GroundNode(None),
            JumpState::default(),
        ));
    }
}

#[derive(Debug)]
pub struct CharacterSpritesheet {
    idle_frame: Handle<Image>,
    run_fps: f32,
    run_frames: Vec<Handle<Image>>,
}

impl Asset for CharacterSpritesheet {}

impl AssetDefaultLoader for CharacterSpritesheet {
    type DefaultLoader = CharacterSpritesheetLoader;
}

pub struct CharacterSpritesheetLoader;

#[async_trait::async_trait]
impl AssetLoader<CharacterSpritesheet> for CharacterSpritesheetLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<CharacterSpritesheet> {
        let string = ctx.read_string(path)?;
        let def: CharacterSpritesheetDef = serde_json::from_str(&string)?;
        Ok(CharacterSpritesheet {
            idle_frame: ctx.load(def.idle_frame),
            run_fps: def.run_fps,
            run_frames: def.run_frames.into_iter().map(|v| ctx.load(v)).collect(),
        })
    }
}

#[derive(Debug, Deserialize)]
struct CharacterSpritesheetDef {
    idle_frame: PathBuf,
    run_fps: f32,
    run_frames: Vec<PathBuf>,
}

#[derive(Debug)]
pub struct CharacterAnimationState {
    run_time: f32,
}

pub const GRAVITY: f32 = 100.0 * SCALE;

pub const STEP_HEIGHT: f32 = 1.2 * SCALE;
pub const MAX_JUMP_HEIGHT: f32 = 8.2 * SCALE;
pub const MIN_JUMP_HEIGHT: f32 = 3.2 * SCALE;

pub const MAX_COYOTE_TIME: f32 = 4.0 * 0.016666666;

pub const INIT_JUMP_VELOCITY_SQ: f32 = 2.0 * GRAVITY * MAX_JUMP_HEIGHT;
pub const INIT_STEP_VELOCITY_SQ: f32 = 2.0 * GRAVITY * STEP_HEIGHT;

pub const HORIZ_ACCELERATION: f32 = 150.0 * SCALE;
pub const HORIZ_SPEED: f32 = 15.0 * SCALE;

#[derive(Debug)]
pub struct GroundNode(pub Option<NodeId>);

pub fn update_ground_node(
    world: &mut WorldView,
    query: Query<(&mut Position, &mut GroundNode), HasComponent<Character>>,
    navmesh: Res<NavMesh>,
) {
    for (pos, ground_node) in query.iter(world) {
        ground_node.0 = navmesh.find_node(pos.0);
    }
}

#[derive(Debug)]
pub struct JumpState {
    pub is_jumping: bool,
    pub coyote_time: f32,
}

impl Default for JumpState {
    fn default() -> JumpState {
        JumpState {
            is_jumping: false,
            coyote_time: 0.0,
        }
    }
}

pub fn control_character(
    world: &mut WorldView,
    query: Query<
        (
            &Position,
            &mut Velocity,
            &CollisionShape,
            &mut GroundNode,
            &mut JumpState,
        ),
        HasComponent<Character>,
    >,
    input: Res<Input>,
    dt: Res<DeltaTime>,
    navmesh: Res<NavMesh>,
) {
    let (Position(pos), Velocity(vel), collider, ground_node, jump_state) =
        match query.iter(world).next() {
            Some(v) => v,
            None => return,
        };

    let dt = dt.0.as_secs_f32();
    let go_left = input.is_down(KeyCode::A);
    let go_right = input.is_down(KeyCode::D);
    let jump = input.is_down(KeyCode::Space);
    let is_grounded = ground_node.0.is_some();

    if !go_right && go_left {
        vel.x -= HORIZ_ACCELERATION * dt;
    } else if !go_left && go_right {
        vel.x += HORIZ_ACCELERATION * dt;
    } else if vel.x < 0.0 {
        vel.x += HORIZ_ACCELERATION * dt;
        vel.x = vel.x.min(0.0);
    } else {
        vel.x -= HORIZ_ACCELERATION * dt;
        vel.x = vel.x.max(0.0);
    }

    vel.x = vel.x.max(-HORIZ_SPEED).min(HORIZ_SPEED);

    if is_grounded {
        jump_state.coyote_time = 0.0;
        jump_state.is_jumping = false;
    } else {
        jump_state.coyote_time += dt;
    }

    let can_coyote_jump = jump_state.coyote_time <= MAX_COYOTE_TIME;
    if !jump_state.is_jumping && jump && (is_grounded || can_coyote_jump) {
        vel.y = -INIT_JUMP_VELOCITY_SQ.sqrt();
        jump_state.is_jumping = true;
    }

    if jump_state.is_jumping {
        return;
    }

    let left_edge = pos.x + collider.0.min.x;
    if vel.x < 0.0 && left_edge - left_edge.floor() < 0.5 {
        let pos = Vec2D::new(left_edge, pos.y.floor() - SCALE);
        if navmesh.find_node(pos).is_some() {
            vel.y = -INIT_STEP_VELOCITY_SQ.sqrt();
            jump_state.is_jumping = true;
        }
    }

    let right_edge = pos.x + collider.0.max.x;
    if vel.x > 0.0 && right_edge.ceil() - right_edge < 0.5 {
        let pos = Vec2D::new(right_edge, pos.y.floor() - SCALE);
        if navmesh.find_node(pos).is_some() {
            vel.y = -INIT_STEP_VELOCITY_SQ.sqrt();
            jump_state.is_jumping = true;
        }
    }
}

pub fn center_camera(
    world: &mut WorldView,
    query: Query<&Position, HasComponent<Character>>,
    mut camera: ResMut<Camera>,
) {
    for Position(pos) in query.iter(world) {
        camera.set_position(*pos);
    }
}

pub fn draw_character(
    world: &mut WorldView,
    query: Query<
        (
            &Position,
            &Velocity,
            &Handle<CharacterSpritesheet>,
            &mut CharacterAnimationState,
        ),
        HasComponent<Character>,
    >,
    mut draw_list: ResMut<DrawList>,
    camera: Res<Camera>,
    assets: Res<Assets>,
    dt: Res<DeltaTime>,
) {
    let (&Position(pos), &Velocity(vel), spritesheet, state) = match query.iter(world).next() {
        Some(v) => v,
        None => return,
    };

    let spritesheet = assets.get(spritesheet).unwrap();

    let frame = if vel.x.abs() > 0.1 && vel.y.abs() < 0.001 {
        state.run_time += dt.0.as_secs_f32();
        let frame_idx =
            ((state.run_time * spritesheet.run_fps) as usize) % spritesheet.run_frames.len();
        spritesheet.run_frames[frame_idx].id()
    } else {
        state.run_time = 0.0;
        spritesheet.idle_frame.id()
    };

    let flip = vel.x.signum();

    let mut encoder = DrawEncoder::new(&mut draw_list);

    camera.push_transform(&mut encoder);

    encoder.draw_sprite(
        Sprite::new(frame, (pos - Vec2D::new(16.0 * flip, 32.0)).cast())
            .with_scale(Vec2D::new(flip, 1.0)),
    );

    encoder.pop_transform();
}

pub fn place_blocks(
    world: &mut WorldView,
    cbuf: &mut CommandBuffer,
    query: Query<&GroundNode, HasComponent<Character>>,
    input: Res<Input>,
    assets: Res<Assets>,
    camera: Res<Camera>,
    mut planet: ResMut<Planet>,
    block_registry: Res<Handle<BlockRegistry>>,
    global_images: Res<GlobalImages>,
    mut navmesh: ResMut<NavMesh>,
) {
    let ground_node = match query.iter(world).next() {
        Some(v) => v,
        None => return,
    };

    let block = &mut BlockId(5); // TODO
    if input.has_pressed(KeyCode::Key1) {
        *block = BlockId(5);
    }
    if input.has_pressed(KeyCode::Key2) {
        *block = BlockId(6);
    }
    if input.has_pressed(KeyCode::Key3) {
        *block = BlockId::ENTITY_CENTER;
    }

    let pos = (camera.inverse(input.cursor_pos().cast()) / SCALE).cast::<u32>();
    if pos.ge(&planet.blocks.size()).any() {
        return;
    }

    if input.is_down(MouseButton::Left) || input.is_down(MouseButton::Right) {
        if input.is_down(MouseButton::Left) {
            planet.blocks.set(pos, *block);

            if *block == BlockId::ENTITY_CENTER {
                cbuf.push((
                    BlockEntityImage {
                        image: global_images.tree.clone(),
                        offset: Vec2D::new(-16.0, -63.0),
                    },
                    BlockPos(pos),
                ));
            }
        } else {
            planet.blocks.set(pos, BlockId(0));
        }

        let block_registry = assets.get(&block_registry).unwrap();
        navmesh.update(&planet.blocks, &block_registry, pos);
    } else if let Some(src) = ground_node.0 {
        if let Some(dst) = navmesh.find_node(pos.cast::<f32>() * SCALE) {
            astar(&navmesh, src, dst)
        }
    }
}

pub fn spawn_friends(
    world: &mut WorldView,
    cbuf: &mut CommandBuffer,
    query: Query<&Position, HasComponent<Character>>,
    input: Res<Input>,
) {
    for &Position(pos) in query.iter(world) {
        if input.has_pressed(KeyCode::E) {
            Agent::spawn(cbuf, pos);
        }
    }
}

pub fn spawn_character_particles(
    world: &mut WorldView,
    cbuf: &mut CommandBuffer,
    query: Query<(&Position, &Velocity), HasComponent<Character>>,
    planet: Res<Planet>,
    assets: Res<Assets>,
    block_registry: Res<Handle<BlockRegistry>>,
) {
    let (&Position(pos), &Velocity(vel)) = match query.iter(world).next() {
        Some(v) => v,
        None => return,
    };

    if vel.x.abs() < 0.01 {
        return;
    }

    let block_pos = (pos / SCALE + Vec2D::new(0.0, 0.01)).map(|v| v.round().max(0.0) as u32);

    if block_pos.ge(&planet.blocks.size()).any() || pos.y.fract().abs() > 0.05 {
        return;
    }

    let block_id = *planet.blocks.get(block_pos);
    if block_id.is_air() {
        return;
    }

    let block_registry = assets.get(&block_registry).unwrap();
    let block = block_registry.get(block_id);

    if block.particles.is_empty() {
        return;
    }

    let mut rng = rand::thread_rng();
    let particle_color = block.particles[rng.gen_range(0..block.particles.len())];
    let particle_amt = rng.gen_range(0..2);

    for _ in 0..particle_amt {
        spawn_particle(
            cbuf,
            pos - Vec2D::new(0.0, 2.05),
            Vec2D::new(0.0, -50.0) - vel * 0.3,
            particle_color,
        )
    }
}
