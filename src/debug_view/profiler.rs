use engine::ecs::World;
use engine::input::ActionName;
use engine::profiler::Profiler;

use super::DebugSection;

pub struct ProfilerSection;

impl DebugSection for ProfilerSection {
    fn name(&self) -> &str {
        "Profiler"
    }

    fn toggle_action(&self) -> ActionName {
        "debug_view.profiler.toggle".into()
    }

    fn draw(&mut self, _world: &mut World, text: &mut String) {
        text.push_str(&Profiler::get().report());
    }
}
