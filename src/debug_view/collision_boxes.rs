use engine::ecs::{Query, World};
use engine::graphics::DebugDrawList;
use engine::input::ActionName;
use engine::math::Rgba;

use super::DebugSection;
use crate::entity::collision::CollisionShape;
use crate::entity::Position;

pub struct CollisionBoxesSection;

impl DebugSection for CollisionBoxesSection {
    fn name(&self) -> &str {
        "Collision boxes"
    }

    fn toggle_action(&self) -> ActionName {
        "debug_view.collision_boxes.toggle".into()
    }

    fn draw(&mut self, world: &mut World, _text: &mut String) {
        let mut debug = DebugDrawList::get();

        let query = Query::<(&Position, &CollisionShape)>::new();
        for (pos, shape) in query.iter(&mut world.view()) {
            let rect = shape.0.translate(pos.0);
            debug.rect(Rgba::WHITE, rect);
        }
    }
}
