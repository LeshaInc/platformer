mod collision_boxes;
mod profiler;

use std::fmt::Write;

use engine::assets::{Assets, Handle};
use engine::ecs::World;
use engine::fps_counter::FpsCounter;
use engine::graphics::{DrawEncoder, DrawList, Font, TextLayoutSettings, TextLayouter};
use engine::input::{ActionName, Input};
use engine::math::{Rgba, Vec2D};
use engine::Resolution;

pub struct DebugView {
    font: Handle<Font>,
    is_visible: bool,
    toggle_action: ActionName,
    help_visible: bool,
    toggle_help_action: ActionName,
    sections: Vec<SectionEntry>,
}

struct SectionEntry {
    is_visible: bool,
    section: Box<dyn DebugSection>,
}

impl DebugView {
    pub fn new(font: Handle<Font>) -> DebugView {
        DebugView {
            font,
            is_visible: false,
            toggle_action: "debug_view.toggle".into(),
            help_visible: false,
            toggle_help_action: "debug_view.help.toggle".into(),
            sections: Vec::new(),
        }
    }

    pub fn add_default_sections(&mut self) {
        self.add_section(self::profiler::ProfilerSection);
        self.add_section(self::collision_boxes::CollisionBoxesSection);
    }

    pub fn add_section<S: DebugSection>(&mut self, section: S) {
        self.sections.push(SectionEntry {
            is_visible: false,
            section: Box::new(section),
        });
    }

    pub fn update(&mut self, world: &mut World) {
        self.handle_input(&world.resources.get::<Input>());
        for section in &mut self.sections {
            section.section.update(world);
        }
    }

    fn handle_input(&mut self, input: &Input) {
        if input.has_pressed(self.toggle_action) {
            self.is_visible = !self.is_visible;
        }

        if input.has_pressed(self.toggle_help_action) {
            self.help_visible = !self.help_visible;
        }

        for section in &mut self.sections {
            if input.has_pressed(section.section.toggle_action()) {
                section.is_visible = !section.is_visible;
            }
        }
    }

    pub fn draw(&mut self, world: &mut World) {
        if !self.is_visible {
            return;
        }

        let mut text = String::new();

        let fps = world.resources.get::<FpsCounter>();
        let _ = writeln!(&mut text, "FPS: {:4.2}", fps.fps(),);
        drop(fps);

        for section in &mut self.sections {
            if !section.is_visible {
                continue;
            }

            let old_len = text.len();
            section.section.draw(world, &mut text);
            if text.len() > old_len {
                text.push('\n');
            }
        }

        let input = world.resources.get::<Input>();

        if let Some(binding) = input.map.action_binding(self.toggle_help_action) {
            let _ = writeln!(&mut text, "Press {} for help", binding);
        }

        if self.help_visible {
            let it = self.sections.iter();
            let max_len = it.map(|v| v.section.name().len()).max().unwrap_or(0);
            for section in &self.sections {
                if let Some(binding) = input.map.action_binding(section.section.toggle_action()) {
                    let _ = writeln!(
                        &mut text,
                        "{:<2$}: {}",
                        section.section.name(),
                        binding,
                        max_len
                    );
                }
            }
        }

        let assets = world.resources.get::<Assets>();
        let fonts = assets.storage();
        let mut text_layouter = TextLayouter::new(&fonts);

        let resolution = world.resources.get::<Resolution>().0;
        text_layouter.reset(&TextLayoutSettings {
            bounds: resolution.map(|v| Some(v as f32 - 10.0)),
            ..Default::default()
        });

        text_layouter.append(self.font.id(), 16.0, Rgba::WHITE, Some(Rgba::BLACK), &text);

        let mut draw_list = world.resources.get_mut::<DrawList>();
        let mut encoder = DrawEncoder::new(&mut draw_list);
        text_layouter.encode(&mut encoder, Vec2D::splat(5.0));
    }
}

pub trait DebugSection: 'static {
    fn name(&self) -> &str;

    fn toggle_action(&self) -> ActionName;

    fn update(&mut self, world: &mut World) {
        let _ = world;
    }

    fn draw(&mut self, world: &mut World, text: &mut String) {
        let _ = (world, text);
    }
}
