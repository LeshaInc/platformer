/// Describes access to a resource: either shared immutable (read) or unique mutable (write).
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum Access {
    /// Read-only access.
    Read,
    /// Read and write access.
    Write,
}

impl Access {
    /// Can `self` be used where `other` is expected?
    ///
    /// Returns `true` in all cases, except when `self = Read` and `other = Write`.
    pub fn allows(self, other: Access) -> bool {
        self != Access::Read || other != Access::Write
    }

    /// If we don't have `self` access, does it imply we also don't have `other` access?
    ///
    /// Returns `true` in all cases, except when `self = Write` and `other = Read`.
    pub fn disallows(self, other: Access) -> bool {
        self != Access::Write || other != Access::Read
    }

    /// Flips the access, turning `Read` to `Write` and vice versa.
    pub fn flip(self) -> Access {
        match self {
            Access::Read => Access::Write,
            Access::Write => Access::Read,
        }
    }
}

/// Describes access to a set of resources.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Permissions<T> {
    /// Access is allowed to all resources.
    AllowAll,
    /// Access is allowed only to the listed resources.
    Allow(Vec<(T, Access)>),
    /// Access is allowed to all resources, except the listed ones.
    Disallow(Vec<(T, Access)>),
}

impl<T: Copy + Eq + Ord> Permissions<T> {
    /// Can we access the given resource?
    pub fn allows(&self, resource: T, access: Access) -> bool {
        match self {
            Permissions::AllowAll => true,
            Permissions::Allow(set) => set.iter().any(|(c, a)| c == &resource && a.allows(access)),
            Permissions::Disallow(set) => !set
                .iter()
                .any(|(c, a)| c == &resource && a.disallows(access)),
        }
    }

    /// Can we access all the given resources?
    ///
    /// Both arrays should have the same length, otherwise they will be truncated to the length of
    /// the shortest one.
    pub fn allows_all(&self, resources: &[(T, Access)]) -> bool {
        resources.iter().all(|&(c, a)| self.allows(c, a))
    }

    /// Split the current permissions set, so that the left set allows access only to the given
    /// resources, and the right set to everything else.
    ///
    /// If the current permission set denies access to any of the given resources,
    /// `None` is returned.
    pub fn split(&self, resources: &[(T, Access)]) -> Option<(Permissions<T>, Permissions<T>)> {
        if !self.allows_all(resources) {
            return None;
        }

        let left = Permissions::Allow(resources.into());

        let right = match self {
            Permissions::AllowAll => {
                Permissions::Disallow(resources.iter().map(|&(c, a)| (c, a.flip())).collect())
            }
            Permissions::Allow(set) => {
                let mut set = set.clone();
                for &(resource, access) in resources {
                    let idx = match set.iter().position(|&(c, _)| c == resource) {
                        Some(v) => v,
                        None => continue,
                    };

                    match access {
                        Access::Write => {
                            set.swap_remove(idx);
                        }
                        Access::Read => set[idx].1 = Access::Read,
                    }
                }
                Permissions::Allow(set)
            }
            Permissions::Disallow(old_set) => {
                let mut set = Vec::with_capacity(old_set.len() + resources.len());
                set.extend(old_set.iter().copied());
                set.extend(resources.iter().map(|&(c, a)| (c, a.flip())));
                set.sort();
                set.dedup();
                Permissions::Disallow(set)
            }
        };

        Some((left, right))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ecs::ComponentId;

    fn id0() -> ComponentId {
        ComponentId::of::<i32>()
    }
    fn id1() -> ComponentId {
        ComponentId::of::<String>()
    }

    fn sorted<T: Ord>(mut x: Vec<T>) -> Vec<T> {
        x.sort();
        x.dedup();
        x
    }

    #[test]
    fn allows() {
        let set = Permissions::AllowAll;
        assert!(set.allows(id0(), Access::Read));
        assert!(set.allows(id0(), Access::Write));

        let set = Permissions::Allow(vec![(id0(), Access::Read)]);
        assert!(set.allows(id0(), Access::Read));
        assert!(!set.allows(id0(), Access::Write));
        assert!(!set.allows(id1(), Access::Read));

        let set = Permissions::Allow(vec![(id0(), Access::Write)]);
        assert!(set.allows(id0(), Access::Read));
        assert!(set.allows(id0(), Access::Write));
        assert!(!set.allows(id1(), Access::Read));

        let set = Permissions::Disallow(vec![(id0(), Access::Read)]);
        assert!(!set.allows(id0(), Access::Read));
        assert!(!set.allows(id0(), Access::Write));
        assert!(set.allows(id1(), Access::Read));

        let set = Permissions::Disallow(vec![(id0(), Access::Write)]);
        assert!(set.allows(id0(), Access::Read));
        assert!(!set.allows(id0(), Access::Write));
        assert!(set.allows(id1(), Access::Read));
    }

    #[test]
    fn split_allow_all() {
        let set = Permissions::AllowAll;
        let (l, r) = set.split(&[(id0(), Access::Read)]).unwrap();
        assert_eq!(l, Permissions::Allow(vec![(id0(), Access::Read)]));
        assert_eq!(r, Permissions::Disallow(vec![(id0(), Access::Write)]));

        let set = Permissions::AllowAll;
        let (l, r) = set.split(&[(id0(), Access::Write)]).unwrap();
        assert_eq!(l, Permissions::Allow(vec![(id0(), Access::Write)]));
        assert_eq!(r, Permissions::Disallow(vec![(id0(), Access::Read)]));
    }

    #[test]
    fn split_allow_some() {
        let set = Permissions::Allow(vec![(id0(), Access::Read)]);
        let (l, r) = set.split(&[(id0(), Access::Read)]).unwrap();
        assert_eq!(l, Permissions::Allow(vec![(id0(), Access::Read)]));
        assert_eq!(r, Permissions::Allow(vec![(id0(), Access::Read)]));

        let set = Permissions::Allow(vec![(id0(), Access::Write)]);
        let (l, r) = set.split(&[(id0(), Access::Read)]).unwrap();
        assert_eq!(l, Permissions::Allow(vec![(id0(), Access::Read)]));
        assert_eq!(r, Permissions::Allow(vec![(id0(), Access::Read)]));

        let set = Permissions::Allow(vec![(id0(), Access::Write)]);
        let (l, r) = set.split(&[(id0(), Access::Write)]).unwrap();
        assert_eq!(l, Permissions::Allow(vec![(id0(), Access::Write)]));
        assert_eq!(r, Permissions::Allow(vec![]));
    }

    #[test]
    fn split_disallow_some() {
        let set = Permissions::Disallow(vec![(id0(), Access::Read)]);
        let (l, r) = set.split(&[(id1(), Access::Read)]).unwrap();
        assert_eq!(l, Permissions::Allow(vec![(id1(), Access::Read)]));
        assert_eq!(
            r,
            Permissions::Disallow(sorted(vec![(id0(), Access::Read), (id1(), Access::Write)]))
        );

        let set = Permissions::Disallow(vec![(id0(), Access::Write)]);
        let (l, r) = set.split(&[(id0(), Access::Read)]).unwrap();
        assert_eq!(l, Permissions::Allow(vec![(id0(), Access::Read)]));
        assert_eq!(r, Permissions::Disallow(vec![(id0(), Access::Write)]));
    }
}
