//! Entity layout filters.

use std::fmt;
use std::marker::PhantomData;
use std::ops::{BitAnd, BitOr, Not as NotOp};

use super::component::Component;
use super::table::Table;
use super::ComponentId;

/// Entity layout filter. Discriminates entity tables, not the entities themselves.
pub trait Filter {
    fn filter(&self, table: &Table) -> bool;
}

macro_rules! impl_filter_helpers {
    ($filter:ident<$($generic:ident,)*>) => {
        impl<F: Filter, $($generic,)*> BitAnd<F> for $filter<$($generic,)*> {
            type Output = And<Self, F>;

            fn bitand(self, rhs: F) -> Self::Output {
                And(self, rhs)
            }
        }

        impl<F: Filter, $($generic,)*> BitOr<F> for $filter<$($generic,)*> {
            type Output = Or<Self, F>;

            fn bitor(self, rhs: F) -> Self::Output {
                Or(self, rhs)
            }
        }

        impl<$($generic,)*> NotOp for $filter<$($generic,)*> {
            type Output = Not<Self>;

            fn not(self) -> Self::Output {
                Not(self)
            }
        }
    };
}

/// Pass filter. Equivalent to `true`.
#[derive(Clone, Copy, Debug, Default)]
pub struct Pass;

#[rustfmt::skip] // turns Pass<> into Pass causing an error
impl_filter_helpers!(Pass<>);

/// Create a pass filter, which is passes every entity table.
pub fn pass() -> Pass {
    Pass
}

impl Filter for Pass {
    #[inline(always)]
    fn filter(&self, _: &Table) -> bool {
        true
    }
}

/// Reject filter. Equivalent to `false`.
#[derive(Clone, Copy, Debug, Default)]
pub struct Reject;

#[rustfmt::skip]
impl_filter_helpers!(Reject<>);

/// Create a pass filter, which is rejects every entity table.
pub fn reject() -> Reject {
    Reject
}

impl Filter for Reject {
    #[inline(always)]
    fn filter(&self, _: &Table) -> bool {
        false
    }
}

/// Logical NOT filter. Created by using the `!` operator.
#[derive(Clone, Copy, Debug, Default)]
pub struct Not<T>(pub T);

impl_filter_helpers!(Not<T,>);

impl<T: Filter> Filter for Not<T> {
    #[inline(always)]
    fn filter(&self, table: &Table) -> bool {
        !self.0.filter(table)
    }
}

/// Logical AND filter. Created by using the `&` operator.
#[derive(Clone, Copy, Debug, Default)]
pub struct And<A, B>(pub A, pub B);

impl_filter_helpers!(And<A, B,>);

impl<A: Filter, B: Filter> Filter for And<A, B> {
    #[inline(always)]
    fn filter(&self, table: &Table) -> bool {
        self.0.filter(table) && self.1.filter(table)
    }
}

/// Logical OR filter. Created by using the `|` operator.
#[derive(Clone, Copy, Debug, Default)]
pub struct Or<A, B>(pub A, pub B);

impl_filter_helpers!(Or<A, B,>);

impl<A: Filter, B: Filter> Filter for Or<A, B> {
    #[inline(always)]
    fn filter(&self, table: &Table) -> bool {
        self.0.filter(table) || self.1.filter(table)
    }
}

/// Filter which selects entities having a given component.
pub struct HasComponent<T>(PhantomData<fn() -> T>);

/// Create a filter which selects entities having a given component.
pub fn has_component<T: Component>() -> HasComponent<T> {
    HasComponent::default()
}

impl_filter_helpers!(HasComponent<T,>);

impl<T> Clone for HasComponent<T> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<T> Copy for HasComponent<T> {}

impl<T> fmt::Debug for HasComponent<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "HasComponent({})", std::any::type_name::<T>())
    }
}

impl<T> Default for HasComponent<T> {
    fn default() -> Self {
        Self(PhantomData)
    }
}

impl<T: Component> Filter for HasComponent<T> {
    #[inline(always)]
    fn filter(&self, table: &Table) -> bool {
        table.components().any(|id| id == ComponentId::of::<T>())
    }
}

/// Filter backed by a function.
#[derive(Clone, Copy, Debug, Default)]
pub struct FnFilter<F>(pub F);

impl_filter_helpers!(FnFilter<T,>);

/// Create a filter backed by the given function.
pub fn filter<F: Fn(&Table) -> bool>(f: F) -> FnFilter<F> {
    FnFilter(f)
}

impl<F: Fn(&Table) -> bool> Filter for FnFilter<F> {
    #[inline(always)]
    fn filter(&self, table: &Table) -> bool {
        (self.0)(table)
    }
}
