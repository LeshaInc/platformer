use std::alloc::Layout;
use std::ops::ControlFlow;

use super::component::{Component, ComponentId, ComponentMeta};
use super::entity::{EntityId, EntityIndex};
use super::tuple::ComponentTuple;
use crate::ecs::tuple::ComponentConsumer;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct TableIndex(pub usize);

pub struct Table {
    cap: usize,
    len: usize,
    entities_ptr: *mut EntityId,
    columns: Box<[Column]>,
}

unsafe impl Send for Table {}
unsafe impl Sync for Table {}

#[derive(Clone)]
pub struct Column {
    ptr: *mut u8,
    meta: ComponentMeta,
}

impl Column {
    pub fn new(meta: ComponentMeta) -> Column {
        let ptr = std::ptr::null_mut();
        Column { ptr, meta }
    }
}

pub struct TableBuilder {
    columns: Vec<Column>,
}

impl Table {
    pub fn builder() -> TableBuilder {
        TableBuilder {
            columns: Vec::new(),
        }
    }

    pub fn new<T: ComponentTuple>() -> Table {
        Self::builder().with_tuple::<T>().build()
    }
}

impl TableBuilder {
    pub fn with_column(mut self, meta: ComponentMeta) -> Self {
        self.columns.push(Column::new(meta));
        self
    }

    pub fn with_table_columns(mut self, table: &Table) -> Self {
        self.columns
            .extend(table.columns.iter().map(|c| Column::new(c.meta)));
        self
    }

    pub fn with_tuple<T: ComponentTuple>(mut self) -> Self {
        self.columns.reserve(T::SIZE);
        T::for_each_meta(|meta| {
            self.columns.push(Column::new(meta));
            ControlFlow::<(), ()>::Continue(())
        });

        self
    }

    pub fn build(mut self) -> Table {
        self.columns.sort_by(|a, b| a.meta.id.cmp(&b.meta.id));
        Table {
            cap: 0,
            len: 0,
            entities_ptr: std::ptr::null_mut(),
            columns: self.columns.into(),
        }
    }
}

impl Table {
    pub fn components(&self) -> impl Iterator<Item = ComponentId> + Clone + '_ {
        self.columns.iter().map(|v| v.meta.id)
    }

    pub fn reserve(&mut self, additional: usize) {
        let req_len = self.len.checked_add(additional);
        let should_grow = req_len.filter(|v| v <= &self.cap).is_none();
        if should_grow {
            self.grow(additional);
        }
    }

    pub unsafe fn set_len(&mut self, len: usize) {
        self.len = len;
    }

    #[cold]
    pub fn grow(&mut self, additional: usize) {
        if self.try_grow(additional).is_none() {
            panic!("capacity overflow");
        }
    }

    fn try_grow(&mut self, additional: usize) -> Option<()> {
        let req_len = self.len.checked_add(additional)?;
        let old_cap = self.cap;
        let new_cap = req_len.checked_next_power_of_two()?;
        if new_cap == 0 {
            return Some(());
        }

        if self.cap == 0 {
            let layout = Layout::array::<EntityId>(new_cap).ok()?;
            if layout.size() > isize::MAX as usize {
                return None;
            }

            // SAFETY: new_cap_usize is nonzero, we've checked for that,
            // so layout's size is also nonzero
            let entities_ptr = unsafe { std::alloc::alloc(layout) };
            if entities_ptr.is_null() {
                std::alloc::handle_alloc_error(layout);
            }

            self.entities_ptr = entities_ptr as *mut EntityId;

            for column in self.columns.iter_mut() {
                let layout = array_layout(column.meta.stride, column.meta.align, new_cap)?;
                if layout.size() == 0 {
                    continue;
                }
                if layout.size() > isize::MAX as usize {
                    return None;
                }

                // SAFETY: layout size is nonzero
                column.ptr = unsafe { std::alloc::alloc(layout) };
                if column.ptr.is_null() {
                    std::alloc::handle_alloc_error(layout);
                }
            }
        } else {
            let old_layout = Layout::array::<EntityId>(old_cap).ok()?;
            let new_layout = Layout::array::<EntityId>(new_cap).ok()?;
            if new_layout.size() > isize::MAX as usize {
                return None;
            }

            // SAFETY: new_cap_usize is nonzero, we've checked for that,
            // so new_layout's size is also nonzero
            let entities_ptr = unsafe {
                std::alloc::realloc(self.entities_ptr as *mut u8, old_layout, new_layout.size())
            };

            if entities_ptr.is_null() {
                std::alloc::handle_alloc_error(new_layout);
            }

            self.entities_ptr = entities_ptr as *mut EntityId;

            for column in self.columns.iter_mut() {
                let old_layout = array_layout(column.meta.stride, column.meta.align, old_cap)?;
                let new_layout = array_layout(column.meta.stride, column.meta.align, new_cap)?;
                if new_layout.size() == 0 {
                    continue;
                }
                if new_layout.size() > isize::MAX as usize {
                    return None;
                }

                // SAFETY: new_layout size is nonzero
                column.ptr =
                    unsafe { std::alloc::realloc(column.ptr, old_layout, new_layout.size()) };
                if column.ptr.is_null() {
                    std::alloc::handle_alloc_error(new_layout);
                }
            }
        }

        self.cap = new_cap;

        Some(())
    }

    pub fn get_column_ptrs(&mut self) -> impl Iterator<Item = (&ComponentMeta, *mut u8)> {
        self.columns.iter().map(|column| (&column.meta, column.ptr))
    }

    pub unsafe fn get_write_ptrs(&mut self) -> impl Iterator<Item = (&ComponentMeta, *mut u8)> {
        let len = self.len;
        self.get_column_ptrs()
            .map(move |(meta, ptr)| (meta, ptr.add(meta.stride * len)))
    }

    pub unsafe fn get_write_ptr(&mut self, id: ComponentId) -> Option<*mut u8> {
        self.get_write_ptrs()
            .filter(|(meta, _)| meta.id == id)
            .map(|(_, ptr)| ptr)
            .next()
    }

    /// SAFETY: T should match the one provided to Table::new
    pub unsafe fn push<T: ComponentTuple>(&mut self, entity: EntityId, tuple: T) -> EntityIndex {
        let idx = EntityIndex(self.len);

        self.reserve(1);
        self.entities_ptr.add(self.len).write(entity);

        struct Writer<'a> {
            table: &'a mut Table,
        }

        impl ComponentConsumer for Writer<'_> {
            fn consume<T: Component>(&mut self, component: T) {
                unsafe {
                    match self.table.get_write_ptr(ComponentId::of::<T>()) {
                        Some(ptr) => (ptr as *mut T).write(component),
                        None => std::hint::unreachable_unchecked(),
                    }
                }
            }
        }

        tuple.for_each(Writer { table: self });

        self.len += 1;
        idx
    }

    /// Swap entity at given index with the last entity, then remove the last entity.
    /// Returns id and the new index of the swapped entity.
    pub fn remove(&mut self, index: EntityIndex) -> Option<(EntityId, EntityIndex)> {
        self._remove(index, true)
    }

    pub fn remove_nodrop(&mut self, index: EntityIndex) -> Option<(EntityId, EntityIndex)> {
        self._remove(index, false)
    }

    fn _remove(&mut self, index: EntityIndex, drop: bool) -> Option<(EntityId, EntityIndex)> {
        if index.0 >= self.len {
            return None;
        }

        if drop {
            for column in self.columns.iter() {
                // SAFETY: computed offset in bytes can't exceed isize::MAX,
                // because we've checked for that in the `grow` method
                let ptr = unsafe { column.ptr.add(index.0 as usize * column.meta.stride) };
                unsafe { (column.meta.drop)(ptr, 1) };
            }
        }

        // because the index is valid, len >= 1
        self.len -= 1;

        let last_id = unsafe { (self.entities_ptr).add(self.len as usize).read() };

        if index.0 == self.len {
            return None;
        }

        unsafe {
            (self.entities_ptr).add(index.0 as usize).write(last_id);
        }

        for column in self.columns.iter() {
            unsafe {
                let src = column.ptr.add(self.len as usize * column.meta.stride);
                let dst = column.ptr.add(index.0 as usize * column.meta.stride);
                src.copy_to_nonoverlapping(dst, column.meta.size);
            }
        }

        Some((last_id, EntityIndex(index.0)))
    }

    pub fn len(&self) -> usize {
        self.len as usize
    }

    pub fn get_entities(&self) -> &[EntityId] {
        unsafe { std::slice::from_raw_parts(self.entities_ptr, self.len as usize) }
    }

    pub fn get_entities_mut(&mut self) -> &mut [EntityId] {
        unsafe { std::slice::from_raw_parts_mut(self.entities_ptr, self.len as usize) }
    }

    pub unsafe fn get_column<T: Component>(&self) -> Option<&[T]> {
        for column in self.columns.iter() {
            if column.meta.id == ComponentId::of::<T>() {
                let slice = std::slice::from_raw_parts(column.ptr as *const T, self.len as usize);
                return Some(slice);
            }
        }
        None
    }

    /// SAFETY: index should be valid and not aliased, T should be the correct type
    #[allow(clippy::mut_from_ref)]
    pub unsafe fn get_column_mut<T: Component>(&self) -> Option<&mut [T]> {
        for column in self.columns.iter() {
            if column.meta.id == ComponentId::of::<T>() {
                let slice = std::slice::from_raw_parts_mut(column.ptr as *mut T, self.len as usize);
                return Some(slice);
            }
        }

        None
    }

    /// Move all components and entities from other to self, leaving other empty.
    ///
    /// SAFETY: layouts of tables must match.
    pub unsafe fn append(&mut self, other: &mut Table) {
        self.reserve(other.len);

        std::ptr::copy_nonoverlapping(
            other.entities_ptr,
            self.entities_ptr.add(self.len as usize),
            other.len as usize,
        );

        for (dst, src) in self.columns.iter_mut().zip(other.columns.iter_mut()) {
            // SAFETY: reserve never allocates more than isize::MAX bytes
            let dst_ptr = dst.ptr.add((self.len as usize) * dst.meta.size);
            let len = (other.len as usize) * dst.meta.size;

            // SAFETY: tables are mutably borrowed, so they can't alias
            std::ptr::copy_nonoverlapping(src.ptr, dst_ptr, len);
        }

        self.len += other.len;
        other.len = 0;
    }
}

impl Drop for Table {
    fn drop(&mut self) {
        if self.cap == 0 {
            return;
        }

        for column in self.columns.iter() {
            unsafe {
                (column.meta.drop)(column.ptr, self.len as usize);
            }
            if let Some(layout) =
                array_layout(column.meta.stride, column.meta.align, self.cap as usize)
            {
                if column.ptr as usize != column.meta.align {
                    unsafe { std::alloc::dealloc(column.ptr as *mut u8, layout) };
                }
            }
        }

        if let Ok(layout) = Layout::array::<EntityId>(self.cap as usize) {
            unsafe { std::alloc::dealloc(self.entities_ptr as *mut u8, layout) };
        }
    }
}

fn array_layout(stride: usize, align: usize, len: usize) -> Option<Layout> {
    let alloc_size = stride.checked_mul(len)?;
    Layout::from_size_align(alloc_size, align).ok()
}
