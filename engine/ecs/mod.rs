//! ECS (Entity Component System) design pattern implementation.

mod component;
mod entity;
pub mod filter;
mod par_zip;
mod permissions;
mod query;
mod resources;
mod schedule;
mod table_set;
// pub mod serde;
mod system;
mod table;
#[cfg(test)]
mod test_components;
mod tuple;
mod view;
mod world;
mod zip;

pub use self::component::{Component, ComponentId};
pub use self::entity::EntityId;
pub use self::permissions::{Access, Permissions};
pub use self::query::Query;
pub use self::resources::{Resource, Resources};
pub use self::schedule::Schedule;
pub use self::system::{IntoSystem, Res, ResMut, System};
pub use self::tuple::ComponentTuple;
pub use self::view::{MultiView, View};
pub use self::world::{CommandBuffer, World, WorldView, EntityEntry, EntityEntryMut};
