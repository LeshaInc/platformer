use std::sync::Arc;

use ahash::AHashMap;

use super::component::ComponentId;
use super::entity::{EntityId, EntityLocation, IdAllocator};
use super::permissions::Permissions;
use super::table::{Table, TableIndex};
use super::table_set::TableSet;
use super::tuple::ComponentTuple;
use super::view::MultiView;
use super::{Access, Component, Resources};

/// World for entities to live in. Entities are identified by [EntityId] and can have at most 16
/// [components](super::Component) attached.
pub struct World {
    pub resources: Resources,
    entity_locations: AHashMap<EntityId, EntityLocation>,
    id_allocator: Arc<IdAllocator>,
    tables: TableSet,
}

impl World {
    /// Create an empty world.
    pub fn new() -> World {
        World {
            resources: Resources::new(),
            entity_locations: AHashMap::new(),
            id_allocator: Arc::new(IdAllocator::new()),
            tables: TableSet::default(),
        }
    }

    pub fn get(&self, id: EntityId) -> Option<EntityEntry<'_>> {
        let location = *self.entity_locations.get(&id)?;
        Some(EntityEntry {
            id,
            location,
            tables: &self.tables,
        })
    }

    pub fn get_mut(&mut self, id: EntityId) -> Option<EntityEntryMut<'_>> {
        let location = *self.entity_locations.get(&id)?;
        Some(EntityEntryMut {
            id,
            location,
            world: self,
        })
    }

    /// Push a new entity to the world, returning its ID.
    ///
    /// Components must be specified in a tuple.
    /// In case of an entity with only one component, `(component,)` syntax must be used.
    pub fn push<T: ComponentTuple>(&mut self, components: T) -> EntityId {
        let id = self.id_allocator.alloc();
        let location = self.tables.push_entity(id, components);
        self.entity_locations.insert(id, location);
        id
    }

    /// Push multiple new entities to the world, returning their IDs through an iterator.
    ///
    /// The iterator must be consumed for the entites to be pushed.
    pub fn extend<'a, T, I>(&'a mut self, entities: I) -> impl Iterator<Item = EntityId> + 'a
    where
        T: ComponentTuple,
        I: IntoIterator<Item = T> + 'a,
        I::IntoIter: 'a,
    {
        let entities = entities.into_iter();
        let id_allocator = &self.id_allocator;
        let entity_locations = &mut self.entity_locations;

        self.tables
            .extend_entities(entities.map(move |components| (id_allocator.alloc(), components)))
            .map(move |(id, location)| {
                entity_locations.insert(id, location);
                id
            })
    }

    /// Remove an entity from the world.
    pub fn remove(&mut self, id: EntityId) {
        let location = match self.entity_locations.get(&id) {
            Some(v) => *v,
            None => return,
        };

        self.entity_locations.remove(&id);

        if let Some((id, location)) = self.tables.remove_entity(location) {
            self.entity_locations.insert(id, location);
        }
    }

    /// Create a view to this world with unrestricted access to all components.
    pub fn view(&mut self) -> WorldView<'_> {
        WorldView::new(self, Permissions::AllowAll)
    }
}

impl Default for World {
    fn default() -> Self {
        Self::new()
    }
}

/// View to the world with potentially limited component access.
pub struct WorldView<'w> {
    world: &'w World,
    permissions: Permissions<ComponentId>,
}

impl<'w> WorldView<'w> {
    pub(crate) fn new(world: &'w World, permissions: Permissions<ComponentId>) -> WorldView<'w> {
        WorldView { world, permissions }
    }

    /// Get permissions of this view, describing component access.
    pub fn permissions(&self) -> &Permissions<ComponentId> {
        &self.permissions
    }

    /// Get shared reference to the resource storage
    pub fn resources(&self) -> &'w Resources {
        &self.world.resources
    }

    pub fn get(&self, id: EntityId) -> Option<EntityView<'_>> {
        let location = *self.world.entity_locations.get(&id)?;
        Some(EntityView {
            id,
            location,
            tables: &self.world.tables,
            permissions: &self.permissions,
        })
    }

    pub fn get_mut(&mut self, id: EntityId) -> Option<EntityViewMut<'_>> {
        self.get(id).map(|view| EntityViewMut { view })
    }

    pub(super) fn find_tables(
        &self,
        components: impl Iterator<Item = ComponentId> + Clone + 'w,
    ) -> impl Iterator<Item = (TableIndex, &'w Table)> + 'w {
        self.world.tables.find_tables(components)
    }

    /// Split the world view, so that the left set allows access only to the components,
    /// specified by the [MultiView], and the right set to everything else.
    pub fn split<V: MultiView>(&mut self) -> Option<(WorldView<'_>, WorldView<'_>)> {
        let (l, r) = self.permissions.split(&V::components())?;
        Some((
            WorldView {
                world: self.world,
                permissions: l,
            },
            WorldView {
                world: self.world,
                permissions: r,
            },
        ))
    }
}

/// Command buffer used to queue world modifications.
pub struct CommandBuffer {
    tables: TableSet,
    id_allocator: Arc<IdAllocator>,
    to_remove: Vec<EntityId>,
    closures: Vec<Box<dyn FnOnce(&mut World) + Send + Sync>>,
}

impl CommandBuffer {
    /// Create an empty command buffer for to the given world.
    pub fn new(world: &World) -> CommandBuffer {
        CommandBuffer {
            tables: TableSet::default(),
            id_allocator: world.id_allocator.clone(),
            to_remove: Vec::new(),
            closures: Vec::new(),
        }
    }

    /// Queue insertion of a single entity into the world.
    pub fn push<T: ComponentTuple>(&mut self, components: T) -> EntityId {
        let id = self.id_allocator.alloc();
        self.tables.push_entity(id, components);
        id
    }

    /// Queue insertion of multiple entities into the world, returning their IDs through an
    /// iterator.
    ///
    /// The iterator must be consumed for the entites to be queued.
    pub fn extend<'a, T, I>(&'a mut self, entities: I) -> impl Iterator<Item = EntityId> + 'a
    where
        T: ComponentTuple,
        I: IntoIterator<Item = T> + 'a,
        I::IntoIter: 'a,
    {
        let id_allocator = &self.id_allocator;
        let entities = entities.into_iter();
        self.tables
            .extend_entities(entities.map(move |components| (id_allocator.alloc(), components)))
            .map(|(id, _)| id)
    }

    /// Queue an entity removal.
    pub fn remove(&mut self, id: EntityId) {
        self.to_remove.push(id);
    }

    /// Queue an arbitrary closure to be executed with mutable world access.
    pub fn exec<F: FnOnce(&mut World) + Send + Sync + 'static>(&mut self, f: F) {
        self.closures.push(Box::new(f));
    }

    /// Flush the command buffer, draining all the queued world modifications.
    ///
    /// The world should be the same as the one provided during command buffer construction
    pub fn flush(&mut self, world: &mut World) {
        self.flush_pushed(world);
        self.flush_removed(world);
        self.flush_closures(world);
    }

    fn flush_pushed(&mut self, world: &mut World) {
        world.tables.append(&mut self.tables, |id, location| {
            world.entity_locations.insert(id, location);
        });
    }

    fn flush_removed(&mut self, world: &mut World) {
        for id in self.to_remove.drain(..) {
            world.remove(id);
        }
    }

    fn flush_closures(&mut self, world: &mut World) {
        for closure in self.closures.drain(..) {
            closure(world);
        }
    }
}

pub struct EntityEntry<'a> {
    id: EntityId,
    location: EntityLocation,
    tables: &'a TableSet,
}

impl EntityEntry<'_> {
    pub fn id(&self) -> EntityId {
        self.id
    }

    pub fn has<T: Component>(&self) -> bool {
        self.tables.entity_has::<T>(self.location)
    }

    pub fn get<T: Component>(&self) -> Option<&T> {
        unsafe { self.tables.entity_get(self.location) }
    }
}

pub struct EntityEntryMut<'a> {
    id: EntityId,
    location: EntityLocation,
    world: &'a mut World,
}

impl EntityEntryMut<'_> {
    pub fn id(&self) -> EntityId {
        self.id
    }

    pub fn has<T: Component>(&self) -> bool {
        self.world.tables.entity_has::<T>(self.location)
    }

    pub fn get<T: Component>(&self) -> Option<&T> {
        unsafe { self.world.tables.entity_get(self.location) }
    }

    pub fn get_mut<T: Component>(&mut self) -> Option<&mut T> {
        unsafe { self.world.tables.entity_get_mut(self.location) }
    }

    pub fn add<T: Component>(&mut self, component: T) -> Option<T> {
        let update_location = |id, location| {
            self.world.entity_locations.insert(id, location);
        };

        unsafe {
            self.world
                .tables
                .entity_add(self.id, self.location, component, update_location)
        }
    }
}

pub struct EntityView<'a> {
    id: EntityId,
    location: EntityLocation,
    tables: &'a TableSet,
    permissions: &'a Permissions<ComponentId>,
}

impl EntityView<'_> {
    pub fn id(&self) -> EntityId {
        self.id
    }

    pub fn has<T: Component>(&self) -> bool {
        self.tables.entity_has::<T>(self.location)
    }

    pub fn get<T: Component>(&self) -> Option<&T> {
        let id = ComponentId::of::<T>();
        if self.permissions.allows(id, Access::Read) {
            unsafe { self.tables.entity_get(self.location) }
        } else {
            None
        }
    }
}

pub struct EntityViewMut<'a> {
    view: EntityView<'a>,
}

impl EntityViewMut<'_> {
    pub fn id(&self) -> EntityId {
        self.view.id
    }

    pub fn has<T: Component>(&self) -> bool {
        self.view.has::<T>()
    }

    pub fn get<T: Component>(&self) -> Option<&T> {
        self.view.get::<T>()
    }

    pub fn get_mut<T: Component>(&mut self) -> Option<&mut T> {
        let id = ComponentId::of::<T>();
        if self.view.permissions.allows(id, Access::Write) {
            unsafe { self.view.tables.entity_get_mut(self.view.location) }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn entry() {
        let mut world = World::new();
        let id = world.push((1, 2u8));

        let entry = world.get(id).unwrap();
        assert_eq!(entry.get::<i32>(), Some(&1));
        assert_eq!(entry.get::<u8>(), Some(&2u8));

        let mut entry = world.get_mut(id).unwrap();
        entry.add("hello");

        let entry = world.get(id).unwrap();
        assert_eq!(entry.get::<i32>(), Some(&1));
        assert_eq!(entry.get::<u8>(), Some(&2u8));
        assert_eq!(entry.get::<&str>(), Some(&"hello"));
    }
}
