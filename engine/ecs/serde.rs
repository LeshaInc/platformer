use std::fmt;
use std::marker::PhantomData;

use ahash::AHashMap;
use erased_serde::{
    Deserializer as ErasedDeserializer, Error as ErasedSerdeErr, Serialize as ErasedSerialize,
    Serializer as ErasedSerializer,
};
use eyre::Result;
use serde::de::{DeserializeOwned, DeserializeSeed, Deserializer, Error as _, SeqAccess, Visitor};
use serde::ser::{Serialize, SerializeSeq, SerializeTuple, Serializer};

use super::table::{Column, Table};
use super::{Component, ComponentId, EntityId, World};
use crate::ecs::entity::{EntityIndex, EntityLocation};
use crate::ecs::table::TableId;

type ErasedSerdeOk<'a> = <&'a mut dyn ErasedSerializer as Serializer>::Ok; // they made it "private"

pub trait ComponetSerDer: 'static {
    type Component: Component;

    const VERSION: u32;

    fn serialize<S: Serializer>(
        &self,
        serializer: S,
        component: &Self::Component,
    ) -> Result<S::Ok, S::Error>;

    fn deserialize<'a, D: Deserializer<'a>>(
        &self,
        deserializer: D,
    ) -> Result<Self::Component, D::Error>;
}

#[derive(Debug, Default)]
pub struct DefaultVer1SerDer<T>(PhantomData<fn() -> T>);

impl<T: Component + Serialize + DeserializeOwned> DefaultVer1SerDer<T> {
    pub fn new() -> DefaultVer1SerDer<T> {
        DefaultVer1SerDer(PhantomData)
    }
}

impl<T: Component + Serialize + DeserializeOwned> ComponetSerDer for DefaultVer1SerDer<T> {
    type Component = T;

    const VERSION: u32 = 1;

    fn serialize<S: Serializer>(&self, serializer: S, component: &T) -> Result<S::Ok, S::Error> {
        component.serialize(serializer)
    }

    fn deserialize<'a, D: Deserializer<'a>>(&self, deserializer: D) -> Result<T, D::Error> {
        T::deserialize(deserializer)
    }
}

trait ErasedComponentSerDer {
    fn version(&self) -> u32;

    fn component(&self) -> ComponentId;

    fn create_column(&self) -> Column;

    unsafe fn serialize(
        &self,
        serializer: &mut dyn ErasedSerializer,
        ptr: *const u8,
        len: u32,
    ) -> Result<ErasedSerdeOk, ErasedSerdeErr>;

    unsafe fn deserialize(
        &self,
        deserializer: &mut dyn ErasedDeserializer,
        ptr: *mut u8,
        len: u32,
    ) -> Result<(), ErasedSerdeErr>;
}

impl<T: ComponetSerDer> ErasedComponentSerDer for T {
    fn version(&self) -> u32 {
        T::VERSION
    }

    fn component(&self) -> ComponentId {
        T::Component::ID
    }

    fn create_column(&self) -> Column {
        Column::new::<T::Component>()
    }

    unsafe fn serialize(
        &self,
        serializer: &mut dyn ErasedSerializer,
        ptr: *const u8,
        len: u32,
    ) -> Result<ErasedSerdeOk, ErasedSerdeErr> {
        let mut tuple = serializer.serialize_tuple(len as usize)?;

        for i in 0..len {
            let component = &*(ptr as *const T::Component).add(i as usize);
            SerializeTuple::serialize_element(
                &mut tuple,
                &SerializeComponent {
                    ser_der: self,
                    component,
                },
            )?;
        }

        SerializeTuple::end(tuple)
    }

    unsafe fn deserialize(
        &self,
        deserializer: &mut dyn ErasedDeserializer,
        ptr: *mut u8,
        len: u32,
    ) -> Result<(), ErasedSerdeErr> {
        deserializer.deserialize_tuple(
            len as usize,
            ComponentVisitor {
                ser_der: self,
                ptr: ptr as *mut T::Component,
                len,
            },
        )
    }
}

pub struct SerDerRegistry {
    component_ser_ders: AHashMap<(ComponentId, u32), Box<dyn ErasedComponentSerDer>>,
    newest_versions: AHashMap<ComponentId, u32>,
}

impl SerDerRegistry {
    pub fn new() -> SerDerRegistry {
        SerDerRegistry {
            component_ser_ders: AHashMap::new(),
            newest_versions: AHashMap::new(),
        }
    }

    pub fn register_component<S: ComponetSerDer>(&mut self, ser_der: S) {
        self._register_component(Box::new(ser_der));
    }

    fn _register_component(&mut self, ser_der: Box<dyn ErasedComponentSerDer>) {
        let component = ser_der.component();
        let version = ser_der.version();
        self.component_ser_ders
            .insert((component, version), ser_der);

        let old_version = self.newest_versions.entry(component).or_insert(version);
        if *old_version < version {
            *old_version = version;
        }
    }

    pub fn serialize<S>(&self, world: &World, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let num_tables = world.tables().len();
        let mut seq = serializer.serialize_seq(Some(num_tables))?;

        for table in world.tables() {
            SerializeSeq::serialize_element(
                &mut seq,
                &SerializeTable {
                    registry: self,
                    table,
                },
            )?;
        }

        SerializeSeq::end(seq)
    }

    pub fn deserialize<'a, D>(&self, world: &mut World, deserializer: D) -> Result<(), D::Error>
    where
        D: Deserializer<'a>,
    {
        deserializer.deserialize_seq(WorldVisitor {
            registry: self,
            world,
        })
    }
}

impl Default for SerDerRegistry {
    fn default() -> Self {
        Self::new()
    }
}

struct SerializeComponent<'a, T: ComponetSerDer> {
    ser_der: &'a T,
    component: &'a T::Component,
}

impl<'a, T: ComponetSerDer> Serialize for SerializeComponent<'a, T> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        self.ser_der.serialize(serializer, self.component)
    }
}

struct DeserializeComponent<'a, T: ComponetSerDer> {
    ser_der: &'a T,
}

impl<'a, 'de, T: ComponetSerDer> DeserializeSeed<'de> for DeserializeComponent<'a, T> {
    type Value = T::Component;

    fn deserialize<D>(self, deserializer: D) -> Result<T::Component, D::Error>
    where
        D: Deserializer<'de>,
    {
        self.ser_der.deserialize(deserializer)
    }
}

struct ComponentVisitor<'a, T: ComponetSerDer> {
    ser_der: &'a T,
    ptr: *mut T::Component,
    len: u32,
}

impl<'a, 'de, T: ComponetSerDer> Visitor<'de> for ComponentVisitor<'a, T> {
    type Value = ();

    fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "sequence of {} components of type {}",
            self.len,
            std::any::type_name::<T::Component>()
        )
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<(), A::Error>
    where
        A: SeqAccess<'de>,
    {
        let mut len = 0;

        while let Some(component) = seq.next_element_seed(DeserializeComponent {
            ser_der: self.ser_der,
        })? {
            unsafe { self.ptr.add(len as usize).write(component) };

            len += 1;
            if len > self.len {
                break;
            }
        }

        if len != self.len {
            Err(A::Error::invalid_length(len as usize, &self))
        } else {
            Ok(())
        }
    }
}

struct SerializeTable<'a> {
    registry: &'a SerDerRegistry,
    table: &'a Table,
}

impl<'a> Serialize for SerializeTable<'a> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let it = self.table.components().iter().enumerate();
        let ser_ders = it.filter_map(|(i, component_id)| {
            let version = self.registry.newest_versions.get(component_id)?;
            self.registry
                .component_ser_ders
                .get(&(*component_id, *version))
                .map(|ser_der| (i, *component_id, ser_der))
        });

        let num_components = ser_ders.clone().count();
        let mut seq = serializer.serialize_seq(Some(2 + num_components * 2))?;

        seq.serialize_element(self.table.get_entities())?;

        let components = ser_ders.clone().map(|v| v.1).collect::<Vec<_>>(); // TODO
        seq.serialize_element(&components)?;

        for (idx, _, ser_der) in ser_ders {
            seq.serialize_element(&ser_der.version())?;

            let col: &dyn ErasedSerialize = &SerializeCol {
                table: self.table,
                idx,
                ser_der: &**ser_der,
            };
            seq.serialize_element(&col)?;
        }

        seq.end()
    }
}

struct SerializeEntities<'a> {
    entities: &'a [EntityId],
}

impl<'a> Serialize for SerializeEntities<'a> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut tuple = serializer.serialize_tuple(self.entities.len())?;
        for entity in self.entities {
            tuple.serialize_element(entity)?;
        }
        tuple.end()
    }
}

struct SerializeCol<'a> {
    ser_der: &'a dyn ErasedComponentSerDer,
    table: &'a Table,
    idx: usize,
}

impl<'a> ErasedSerialize for SerializeCol<'a> {
    fn erased_serialize(
        &self,
        serializer: &mut dyn ErasedSerializer,
    ) -> Result<ErasedSerdeOk, ErasedSerdeErr> {
        let len = self.table.len();

        unsafe {
            let ptr = self.table.get_raw_col(self.idx);
            self.ser_der.serialize(serializer, ptr, len as u32)
        }
    }
}

struct WorldVisitor<'a> {
    registry: &'a SerDerRegistry,
    world: &'a mut World,
}

impl<'a, 'de> Visitor<'de> for WorldVisitor<'a> {
    type Value = ();

    fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("world")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<(), A::Error>
    where
        A: SeqAccess<'de>,
    {
        loop {
            let seed = DeserializeTable {
                registry: self.registry,
                world: self.world,
            };
            if seq.next_element_seed(seed)?.is_none() {
                break;
            }
        }

        Ok(())
    }
}

struct DeserializeTable<'a> {
    registry: &'a SerDerRegistry,
    world: &'a mut World,
}

impl<'a, 'de> DeserializeSeed<'de> for DeserializeTable<'a> {
    type Value = ();

    fn deserialize<D>(self, deserializer: D) -> Result<(), D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_seq(self)
    }
}

impl<'a, 'de> Visitor<'de> for DeserializeTable<'a> {
    type Value = ();

    fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str("table")
    }

    fn visit_seq<A>(self, mut seq: A) -> Result<(), A::Error>
    where
        A: SeqAccess<'de>,
    {
        let entities = match seq.next_element::<Vec<EntityId>>()? {
            Some(v) => v,
            None => return Err(A::Error::invalid_length(0, &self)),
        };

        for &id in entities.iter() {
            self.world.remove(id);
        }

        let components = match seq.next_element::<Vec<ComponentId>>()? {
            Some(v) => v,
            None => return Err(A::Error::invalid_length(1, &self)),
        };

        for id in &components {
            if !self.registry.newest_versions.contains_key(id) {
                return Err(A::Error::custom("no such component"));
            }
        }

        // TODO:
        // if !components.is_sorted() {
        //     return Err(A::Error::custom("components not sorted"));
        // }

        let (table_id, table) = match self.world.find_table(&components) {
            Some(v) => (v, &mut self.world.tables[v.0 as usize]),
            None => {
                let components = components.iter().copied();
                let cols = components.clone().map(|comp| {
                    let version = self.registry.newest_versions[&comp];
                    let ser_der = &self.registry.component_ser_ders[&(comp, version)];
                    ser_der.create_column()
                });
                let table = unsafe { Table::new_unchecked(components, cols) };
                let id = TableId(self.world.tables.len() as u32);
                self.world.tables.push(table);
                (id, self.world.tables.last_mut().unwrap())
            }
        };

        unsafe {
            table.reserve(entities.len() as u32);
        }

        for (i, &entity) in entities.iter().enumerate() {
            unsafe { table.get_entities_write_ptr().add(i).write(entity) };
        }

        for (i, &component) in components.iter().enumerate() {
            let version = match seq.next_element::<u32>()? {
                Some(v) => v,
                None => return Err(A::Error::invalid_length(2 + i * 2, &self)),
            };

            let opt = self.registry.component_ser_ders.get(&(component, version));
            let ser_der = opt.ok_or_else(|| A::Error::custom("no such component or version"))?;

            let seed = DeserialzeCol {
                ser_der: &**ser_der,
                table,
                idx: i,
                len: entities.len() as u32,
            };

            if seq.next_element_seed(seed)?.is_none() {
                return Err(A::Error::invalid_length(2 + i * 2 + 1, &self));
            }
        }

        for (i, &entity) in entities.iter().enumerate() {
            let loc = EntityLocation::new(table_id, EntityIndex(i as u32));
            self.world.entity_locations.insert(entity, loc);
        }

        unsafe {
            table.set_len(entities.len() as u32);
        }

        Ok(())
    }
}

struct DeserialzeCol<'a> {
    ser_der: &'a dyn ErasedComponentSerDer,
    table: &'a mut Table,
    idx: usize,
    len: u32,
}

impl<'a, 'de> DeserializeSeed<'de> for DeserialzeCol<'a> {
    type Value = ();

    fn deserialize<D>(self, deserializer: D) -> Result<(), D::Error>
    where
        D: Deserializer<'de>,
    {
        let mut erased = <dyn ErasedDeserializer>::erase(deserializer);
        unsafe {
            let ptr = self.table.get_write_ptr(self.idx) as *mut _;
            self.ser_der
                .deserialize(&mut erased, ptr, self.len)
                .map_err(D::Error::custom)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ecs::test_components::{Pos, Vel, Zst};

    fn setup_world() -> World {
        let mut world = World::new();
        world.push((Pos(1), Vel(1)));
        world.push((Pos(2), Vel(2)));
        world.push((Pos(3),));
        world.push((Zst,));
        world.push((Vel(5), Zst));
        world
    }

    fn setup_registry() -> SerDerRegistry {
        let mut registry = SerDerRegistry::new();
        registry.register_component(DefaultVer1SerDer::<Pos>::new());
        registry.register_component(DefaultVer1SerDer::<Vel>::new());
        registry.register_component(DefaultVer1SerDer::<Zst>::new());
        registry
    }

    #[test]
    fn serialize_json() {
        let (world, registry) = (setup_world(), setup_registry());

        let mut buf = Vec::new();
        let mut serializer = serde_json::Serializer::new(&mut buf);
        registry.serialize(&world, &mut serializer).unwrap();
        let json = String::from_utf8(buf).unwrap();

        assert_eq!(
            json,
            "[[[1,2],[0,1],1,[1,2],1,[1,2]],[[3],[0],1,[3]],[[4],[2],1,[null]],[[5],[1,2],1,[5],1,[null]]]"
        );
    }

    #[test]
    fn deserialize_json() {
        let registry = setup_registry();
        let mut world = World::new();

        let json =
            "[[[1,2],[0,1],1,[1,2],1,[1,2]],[[3],[0],1,[3]],[[4],[2],1,[null]],[[5],[1,2],1,[5],1,[null]]]";
        let mut deserializer = serde_json::Deserializer::from_str(&json);

        registry.deserialize(&mut world, &mut deserializer).unwrap();

        let mut buf = Vec::new();
        let mut serializer = serde_json::Serializer::new(&mut buf);
        registry.serialize(&world, &mut serializer).unwrap();
        let new_json = String::from_utf8(buf).unwrap();

        assert_eq!(json, new_json);
    }

    #[test]
    fn serialize_bincode() {
        let (world, registry) = (setup_world(), setup_registry());

        let mut buf = Vec::new();
        let mut serializer = bincode::Serializer::new(&mut buf, bincode::options());
        registry.serialize(&world, &mut serializer).unwrap();

        #[rustfmt::skip]
        let exp = [
            4, // 4 tables

            6, // 2 + 2*num_components
            2, 1, 2, // 2 entities: 1, 2
            2, 0, 1, // 2 components: Pos, Vel
            1, 2, 4, // Pos version 1: 1, 2 (zigzag encoded)
            1, 2, 4, // Vel version 1: 1, 2

            4,
            1, 3, // 1 entity: 3
            1, 0, // 1 component: Pos
            1, 6, // Pos version 1: 3

            4,
            1, 4, // 1 entity: 4
            1, 2, // 1 component: Zst
            1, // Zst version 1

            6,
            1, 5, // 1 entity: 5
            2, 1, 2, // 2 components: Vel, Zst
            1, 10, // Vel version 1: 5
            1, // Zst version 1
        ];
        assert_eq!(buf, &exp);
    }

    #[test]
    fn deserialize_bincode() {
        let registry = setup_registry();
        let mut world = World::new();

        let bytes = &[
            4, 6, 2, 1, 2, 2, 0, 1, 1, 2, 4, 1, 2, 4, 4, 1, 3, 1, 0, 1, 6, 4, 1, 4, 1, 2, 1, 6, 1,
            5, 2, 1, 2, 1, 10, 1,
        ][..];

        let mut deserializer = bincode::Deserializer::from_slice(bytes, bincode::options());
        registry.deserialize(&mut world, &mut deserializer).unwrap();

        let mut buf = Vec::new();
        let mut serializer = bincode::Serializer::new(&mut buf, bincode::options());
        registry.serialize(&world, &mut serializer).unwrap();

        assert_eq!(bytes, &buf);
    }
}
