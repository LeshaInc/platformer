use std::ops::ControlFlow;

use super::component::ComponentMeta;
use super::entity::{EntityIndex, EntityLocation};
use super::table::{Table, TableIndex};
use super::{Component, ComponentId, ComponentTuple, EntityId};

#[derive(Default)]
pub struct TableSet {
    tables: Vec<Table>,
}

impl TableSet {
    pub fn find_table<'a>(
        &'a self,
        components: impl Iterator<Item = ComponentId> + Clone + 'a,
    ) -> Option<(TableIndex, &'a Table)> {
        self.tables
            .iter()
            .enumerate()
            .filter(move |(_, table)| table.components().eq(components.clone()))
            .map(|(i, table)| (TableIndex(i), table))
            .next()
    }

    pub fn find_tables<'a>(
        &'a self,
        components: impl Iterator<Item = ComponentId> + Clone + 'a,
    ) -> impl Iterator<Item = (TableIndex, &'a Table)> {
        self.tables
            .iter()
            .enumerate()
            .filter(move |(_, table)| {
                components
                    .clone()
                    .all(|c1| table.components().any(|c2| c1 == c2))
            })
            .map(|(i, table)| (TableIndex(i), table))
    }

    pub fn find_or_insert_table<T: ComponentTuple>(&mut self) -> (TableIndex, &mut Table) {
        let id = self._find_or_insert_table::<T>();
        (id, &mut self.tables[id.0 as usize])
    }

    fn _find_or_insert_table<T: ComponentTuple>(&mut self) -> TableIndex {
        for (i, table) in self.tables.iter_mut().enumerate() {
            let mut components = table.components();

            let res = T::for_each_meta(|meta| {
                if components.next() == Some(meta.id) {
                    ControlFlow::Continue(())
                } else {
                    ControlFlow::Break(())
                }
            });

            if res == ControlFlow::Continue(()) {
                return TableIndex(i);
            }
        }

        self.insert_table::<T>();
        TableIndex(self.tables.len() - 1)
    }

    #[cold]
    pub fn insert_table<T: ComponentTuple>(&mut self) {
        self.tables.push(Table::new::<T>());
    }

    pub fn push_entity<T: ComponentTuple>(
        &mut self,
        id: EntityId,
        components: T,
    ) -> EntityLocation {
        let (table_id, table) = self.find_or_insert_table::<T>();
        let index = unsafe { table.push(id, components) };
        EntityLocation::new(table_id, index)
    }

    pub fn extend_entities<'a, T, I>(
        &'a mut self,
        entities: I,
    ) -> impl Iterator<Item = (EntityId, EntityLocation)> + 'a
    where
        T: ComponentTuple,
        I: Iterator<Item = (EntityId, T)> + 'a,
    {
        let (table_id, table) = self.find_or_insert_table::<T>();
        table.reserve(entities.size_hint().0);

        entities.map(move |(id, components)| {
            let index = unsafe { table.push(id, components) };
            (id, EntityLocation::new(table_id, index))
        })
    }

    pub fn remove_entity(
        &mut self,
        location: EntityLocation,
    ) -> Option<(EntityId, EntityLocation)> {
        let table = self.tables.get_mut(location.table.0 as usize)?;
        table
            .remove(location.index)
            .map(|(id, index)| (id, EntityLocation::new(location.table, index)))
    }

    pub fn append(
        &mut self,
        src: &mut TableSet,
        mut update_location: impl FnMut(EntityId, EntityLocation),
    ) {
        for src_table in &mut src.tables {
            let (table_id, offset, entities) = match self.find_table(src_table.components()) {
                Some((dst_table_id, _)) => {
                    let dst_table = &mut self.tables[dst_table_id.0 as usize];
                    let offset = dst_table.len();
                    unsafe { dst_table.append(src_table) };
                    (dst_table_id, offset, &dst_table.get_entities()[offset..])
                }
                None => {
                    let mut dst_table = Table::builder().with_table_columns(src_table).build();
                    unsafe { dst_table.append(src_table) };
                    self.tables.push(dst_table);
                    let idx = self.tables.len() - 1;
                    let dst_table = &mut self.tables[idx];
                    (TableIndex(idx), 0, dst_table.get_entities())
                }
            };

            for (i, &entity_id) in entities.iter().enumerate() {
                let location = EntityLocation::new(table_id, EntityIndex(i + offset));
                update_location(entity_id, location);
            }
        }
    }

    pub fn entity_has<T: Component>(&self, location: EntityLocation) -> bool {
        let table = match self.tables.get(location.table.0) {
            Some(v) => v,
            None => return false,
        };

        table.components().any(|id| id == ComponentId::of::<T>())
    }

    pub unsafe fn entity_get<T: Component>(&self, location: EntityLocation) -> Option<&T> {
        let table = self.tables.get(location.table.0)?;
        let column = table.get_column::<T>()?;
        column.get(location.index.0)
    }

    pub unsafe fn entity_get_mut<T: Component>(&self, location: EntityLocation) -> Option<&mut T> {
        let table = self.tables.get(location.table.0)?;
        let column = table.get_column_mut::<T>()?;
        column.get_mut(location.index.0)
    }

    pub fn entity_add<T: Component>(
        &mut self,
        entity_id: EntityId,
        location: EntityLocation,
        component: T,
        mut update_location: impl FnMut(EntityId, EntityLocation),
    ) -> Option<T> {
        let component_id = ComponentId::of::<T>();

        let old_table = self.tables.get(location.table.0)?;

        if let Some(column) = unsafe { old_table.get_column_mut::<T>() } {
            if let Some(dest) = column.get_mut(location.index.0) {
                return Some(std::mem::replace(dest, component));
            }
        }

        let mut new_table_idx = self.tables.len();
        let mut new_table = None;
        for (table_idx, table) in self.tables.iter().enumerate() {
            if table
                .components()
                .filter(|&c| c != component_id)
                .eq(old_table.components())
                && table.components().any(|c| c == component_id)
            {
                new_table_idx = table_idx;
                new_table = Some(std::mem::replace(
                    &mut self.tables[table_idx],
                    Table::new::<()>(),
                ));
                break;
            }
        }

        let old_table = &mut self.tables[location.table.0];
        let mut new_table = match new_table {
            Some(v) => v,
            None => Table::builder()
                .with_table_columns(old_table)
                .with_column(ComponentMeta::of::<T>())
                .build(),
        };

        new_table.reserve(1);
        for (meta, src_ptr) in old_table.get_column_ptrs() {
            unsafe {
                let dst_ptr = match new_table.get_write_ptr(meta.id) {
                    Some(v) => v,
                    None => std::hint::unreachable_unchecked(),
                };
                std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, meta.size);
            }
        }

        unsafe {
            let dst_ptr = match new_table.get_write_ptr(component_id) {
                Some(v) => v,
                None => std::hint::unreachable_unchecked(),
            };

            (dst_ptr as *mut T).write(component);

            new_table.set_len(new_table.len() + 1);
        }

        if let Some((id, index)) = old_table.remove_nodrop(location.index) {
            let location = EntityLocation::new(location.table, index);
            update_location(id, location);
        }

        let new_index = EntityIndex(new_table.len() - 1);

        if new_table_idx < self.tables.len() {
            self.tables[new_table_idx] = new_table;
        } else {
            self.tables.push(new_table);
        }

        let location = EntityLocation::new(TableIndex(new_table_idx), new_index);
        update_location(entity_id, location);

        None
    }
}
