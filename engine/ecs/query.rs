use std::marker::PhantomData;

use rayon::iter::{IntoParallelIterator, ParallelIterator};

use super::filter::{And, Filter, Pass};
use super::table::Table;
use super::view::{MultiView, MultiViewFetch};
use super::world::WorldView;

/// Query which is a combination of a [MultiView] and a [Filter].
pub struct Query<V, F = Pass> {
    view: PhantomData<V>,
    pub filter: F,
}

impl<V, F> Query<V, F>
where
    V: MultiView,
    F: Filter,
{
    /// Create a new query with the filter instaniated through the [Default] trait.
    pub fn new() -> Query<V, F>
    where
        F: Default,
    {
        Query {
            view: PhantomData,
            filter: F::default(),
        }
    }

    /// Add an additional filter to the query.
    pub fn filter<T: Filter>(self, filter: T) -> Query<V, And<F, T>> {
        Query {
            view: PhantomData,
            filter: And(self.filter, filter),
        }
    }

    fn find_tables<'w>(&self, view: &mut WorldView<'w>) -> Vec<&'w Table>
    where
        V::Fetch: 'w,
    {
        let components = V::components();

        if !view.permissions().allows_all(&components) {
            panic!("access denied"); // TODO: why
        }

        let required_components = components
            .into_iter()
            .take(V::num_required_components())
            .map(|(c, _)| c);

        view.find_tables(required_components)
            .map(|(_, table)| table)
            .filter(move |table| self.filter.filter(table))
            .collect::<Vec<_>>()
    }

    /// Iterate through all entities matching the query.
    pub fn iter<'w>(
        &self,
        view: &mut WorldView<'w>,
    ) -> impl Iterator<Item = <V::Fetch as MultiViewFetch<'w>>::Item> + 'w
    where
        V::Fetch: 'w,
    {
        self.find_tables(view)
            .into_iter()
            .flat_map(|table| unsafe { V::Fetch::iter(table) })
    }

    /// Iterate through all entities matching the query, in parallel.
    pub fn par_iter<'w>(
        &self,
        view: &mut WorldView<'w>,
    ) -> impl ParallelIterator<Item = <V::Fetch as MultiViewFetch<'w>>::Item> + 'w
    where
        V::Fetch: 'w,
    {
        self.find_tables(view)
            .into_par_iter()
            .flat_map(|table| unsafe { V::Fetch::par_iter(table) })
    }
}

impl<V, F> Default for Query<V, F>
where
    V: MultiView,
    F: Filter + Default,
{
    fn default() -> Self {
        Self::new()
    }
}
