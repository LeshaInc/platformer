use std::sync::atomic::{AtomicUsize, Ordering};

use parking_lot::Mutex;
use rayon::{Scope, ThreadPool};

use super::system::{AccessSet, System};
use super::{CommandBuffer, ComponentId, Permissions, World, WorldView};

pub struct Schedule {
    jobs: Vec<SystemJob>,
}

struct SystemJob {
    system: Mutex<Box<dyn System>>, // TODO: get rid of the mutexes
    cbuf: Mutex<CommandBuffer>,
    permissions: Permissions<ComponentId>,
    num_dependencies: usize,
    num_dependencies_remaining: AtomicUsize,
    descendants: Vec<usize>,
}

impl Schedule {
    pub fn builder() -> ScheduleBuilder {
        ScheduleBuilder {
            systems: Vec::new(),
        }
    }

    pub fn run(&mut self, thread_pool: &ThreadPool, world: &mut World) {
        for job in &mut self.jobs {
            job.num_dependencies_remaining = AtomicUsize::new(job.num_dependencies);
        }

        let jobs = &self.jobs;

        thread_pool.in_place_scope(|scope| {
            for job in jobs {
                if job.num_dependencies != 0 {
                    continue;
                }

                let world = &world;
                scope.spawn(move |s| Self::run_job(s, world, job, jobs));
            }
        });

        for job in &mut self.jobs {
            job.cbuf.get_mut().flush(world);
        }
    }

    fn run_job<'a, 'b: 'a>(
        scope: &Scope<'a>,
        world: &'b World,
        job: &'b SystemJob,
        jobs: &'b [SystemJob],
    ) {
        let mut view = WorldView::new(world, job.permissions.clone());
        let mut cbuf = job.cbuf.lock();
        job.system.lock().run(&mut cbuf, &mut view);

        for &i in &job.descendants {
            let descendant = &jobs[i];
            let remaining = &descendant.num_dependencies_remaining;
            if remaining.fetch_sub(1, Ordering::SeqCst) == 1 {
                scope.spawn(move |s| Self::run_job(s, world, descendant, jobs));
            }
        }
    }
}

pub struct ScheduleBuilder {
    systems: Vec<SystemEntry>,
}

struct SystemEntry {
    system: Box<dyn System>,
    access: AccessSet,
    dependencies: Vec<usize>,
}

impl ScheduleBuilder {
    pub fn add<S: System + 'static>(mut self, system: S) -> Self {
        let access = S::access();
        access.validate();

        let mut dependencies = Vec::new();

        for (i, system) in self.systems.iter().enumerate() {
            if system.access.aliases(&access) {
                dependencies.push(i);
            }
        }

        self.systems.push(SystemEntry {
            system: Box::new(system),
            access,
            dependencies,
        });

        self
    }

    pub fn build(self, world: &World) -> Schedule {
        let mut descendants = vec![vec![]; self.systems.len()];

        for (i, system) in self.systems.iter().enumerate() {
            for &j in &system.dependencies {
                descendants[j].push(i);
            }
        }

        let it = self.systems.into_iter().zip(descendants);
        let jobs = it
            .map(|(sys, descendants)| SystemJob {
                system: Mutex::new(sys.system),
                cbuf: Mutex::new(CommandBuffer::new(world)),
                permissions: sys.access.component_permissions(),
                num_dependencies: sys.dependencies.len(),
                num_dependencies_remaining: AtomicUsize::new(sys.dependencies.len()),
                descendants,
            })
            .collect();

        Schedule { jobs }
    }
}

#[cfg(test)]
#[allow(dead_code)]
mod tests {
    use rayon::ThreadPoolBuilder;

    use crate::ecs::*;

    fn panic_works() {
        panic!("it works");
    }

    #[test]
    #[should_panic(expected = "it works")]
    fn simple() {
        let thread_pool = ThreadPoolBuilder::new().build().unwrap();

        let mut world = World::new();
        let mut schedule = Schedule::builder().add(panic_works.system()).build(&world);

        schedule.run(&thread_pool, &mut world);
    }
}
