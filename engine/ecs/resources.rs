use std::any::{type_name, Any, TypeId};

use ahash::AHashMap;
use atomic_refcell::{AtomicRef, AtomicRefCell, AtomicRefMut};
use eyre::eyre;

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct ResourceId(TypeId);

impl ResourceId {
    pub fn of<T: Resource>() -> ResourceId {
        ResourceId(TypeId::of::<T>())
    }
}

pub trait Resource: 'static + Send + Sync {}

impl<T: 'static + Send + Sync> Resource for T {}

/// Storage of resources of any type (one per type).
pub struct Resources {
    map: AHashMap<ResourceId, Box<dyn Any + Send + Sync>>,
}

impl Resources {
    /// Create an empty resource storage.
    pub fn new() -> Resources {
        Resources {
            map: AHashMap::default(),
        }
    }

    /// Insert a resource, returning the old value, if it existed.
    pub fn insert<T: Resource>(&mut self, resource: T) -> Option<T> {
        let key = ResourceId::of::<T>();
        let val = Box::new(AtomicRefCell::new(resource));
        let val = self.map.insert(key, val)?;
        let cell = unsafe { Box::from_raw(Box::into_raw(val) as *mut _ as *mut AtomicRefCell<T>) };
        Some(cell.into_inner())
    }

    fn get_cell<T: Resource>(&self) -> Option<&AtomicRefCell<T>> {
        let key = ResourceId::of::<T>();
        let val = self.map.get(&key);
        val.map(|v| unsafe { &*(&**v as *const _ as *const AtomicRefCell<T>) })
    }

    /// Immutably borrow a resource.
    ///
    /// Panics if the resource doesn't exist, or is mutably borrowed.
    pub fn get<T: Resource>(&self) -> AtomicRef<'_, T> {
        self.try_get::<T>()
            .ok_or_else(|| eyre!("No such resource: {}", type_name::<T>()))
            .unwrap()
    }

    /// Mutably borrow a resource.
    ///
    /// Panics if the resource doesn't exist, or is already borrowed.
    pub fn get_mut<T: Resource>(&self) -> AtomicRefMut<'_, T> {
        self.try_get_mut::<T>()
            .ok_or_else(|| eyre!("No such resource: {}", type_name::<T>()))
            .unwrap()
    }

    /// Insert a resource if it doesn't exist, and then borrow it.
    ///
    /// Panics if the resource is already borrowed.
    pub fn get_or_default<T: Resource + Default>(&mut self) -> AtomicRefMut<'_, T> {
        if self.try_get::<T>().is_none() {
            self.insert(T::default());
        }

        self.get_mut::<T>()
    }

    /// Immutably borrow a resource, returning None if it doesn't exsist.
    ///
    /// Panics if the resource is mutably borrowed.
    pub fn try_get<T: Resource>(&self) -> Option<AtomicRef<'_, T>> {
        self.get_cell::<T>().map(|v| v.borrow())
    }

    /// Mutably borrow a resource, returning None if it doesn't exsist.
    ///
    /// Panics if the resource is already borrowed.
    pub fn try_get_mut<T: Resource>(&self) -> Option<AtomicRefMut<'_, T>> {
        self.get_cell::<T>().map(|v| v.borrow_mut())
    }

    /// Remove a resource returning it, or do nothing if the resource doesn't exist.
    pub fn remove<T: Resource>(&mut self) -> Option<T> {
        let key = ResourceId::of::<T>();
        let val = self.map.remove(&key)?;
        let cell = unsafe { Box::from_raw(Box::into_raw(val) as *mut _ as *mut AtomicRefCell<T>) };
        Some(cell.into_inner())
    }
}

impl Default for Resources {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use super::*;

    #[test]
    fn basic() {
        let mut resources = Resources::new();

        resources.insert(0u8);
        resources.insert(1u16);
        resources.insert("hi");

        assert_eq!(*resources.get::<u8>(), 0);
        assert_eq!(*resources.get::<u16>(), 1);
        assert_eq!(*resources.get::<&str>(), "hi");

        assert_eq!(resources.remove::<u8>(), Some(0u8));
        assert!(resources.try_get::<u8>().is_none());
    }

    #[test]
    fn concurrent() {
        let mut resources = Resources::new();
        resources.insert(0u8);
        resources.insert(1u16);

        let resources = Arc::new(resources);

        let resources1 = resources.clone();
        let a_thread = std::thread::spawn(move || {
            for _ in 0..1000 {
                assert_eq!(*resources1.get_mut::<u8>(), 0);
            }
        });

        let b_thread = std::thread::spawn(move || {
            for _ in 0..1000 {
                assert_eq!(*resources.get_mut::<u16>(), 1);
            }
        });

        a_thread.join().unwrap();
        b_thread.join().unwrap();
    }
}
