use std::hint::unreachable_unchecked;
use std::iter::{Copied, Zip};
use std::slice;

use rayon::iter::plumbing::{Consumer, ProducerCallback, UnindexedConsumer};
use rayon::iter::{
    IndexedParallelIterator, IntoParallelIterator, IntoParallelRefIterator,
    IntoParallelRefMutIterator, ParallelIterator,
};

use super::component::{Component, ComponentId};
use super::entity::EntityId;
use super::par_zip::ParMultiZip;
use super::permissions::Access;
use super::table::Table;
use super::zip::MultiZip;

pub trait View {
    type Fetch: for<'a> ViewFetch<'a>;
    /// Type of the accessed component.
    type Component: Component;

    /// Component access.
    const ACCESS: Access;
    /// Is the component required?
    const REQUIRED: bool;
}

pub trait ViewFetch<'a> {
    type Item: 'a;
    type Iter: Iterator<Item = Self::Item>;
    type ParIter: IndexedParallelIterator<Item = Self::Item>;

    /// Create an iterator of table's components.
    ///
    /// # Safety
    /// Component must exist in the table, and be not mutably aliased.
    unsafe fn iter(table: &'a Table) -> Self::Iter;

    unsafe fn par_iter(table: &'a Table) -> Self::ParIter;
}

impl<T: Component> View for &T {
    type Fetch = ReadFetch<T>;
    type Component = T;

    const ACCESS: Access = Access::Read;
    const REQUIRED: bool = true;
}

pub struct ReadFetch<T>(T);

impl<'a, T: Component> ViewFetch<'a> for ReadFetch<T> {
    type Item = &'a T;
    type Iter = slice::Iter<'a, T>;
    type ParIter = rayon::slice::Iter<'a, T>;

    unsafe fn iter(table: &'a Table) -> Self::Iter {
        match table.get_column() {
            Some(v) => v.iter(),
            None => unreachable_unchecked(),
        }
    }

    unsafe fn par_iter(table: &'a Table) -> Self::ParIter {
        match table.get_column() {
            Some(v) => v.par_iter(),
            None => unreachable_unchecked(),
        }
    }
}

impl<T: Component> View for &mut T {
    type Fetch = WriteFetch<T>;
    type Component = T;

    const ACCESS: Access = Access::Write;
    const REQUIRED: bool = true;
}

pub struct WriteFetch<T>(T);

impl<'a, T: Component> ViewFetch<'a> for WriteFetch<T> {
    type Item = &'a mut T;
    type Iter = slice::IterMut<'a, T>;
    type ParIter = rayon::slice::IterMut<'a, T>;

    unsafe fn iter(table: &'a Table) -> Self::Iter {
        match table.get_column_mut() {
            Some(v) => v.iter_mut(),
            None => unreachable_unchecked(),
        }
    }

    unsafe fn par_iter(table: &'a Table) -> Self::ParIter {
        match table.get_column_mut() {
            Some(v) => v.par_iter_mut(),
            None => unreachable_unchecked(),
        }
    }
}

impl<T: Component> View for Option<&T> {
    type Fetch = OptionalReadFetch<T>;
    type Component = T;

    const ACCESS: Access = Access::Read;
    const REQUIRED: bool = false;
}

pub struct OptionalReadFetch<T>(T);

impl<'a, T: Component> ViewFetch<'a> for OptionalReadFetch<T> {
    type Item = Option<&'a T>;
    type Iter = OptionalIter<slice::Iter<'a, T>>;
    type ParIter = OptionalIter<rayon::slice::Iter<'a, T>>;

    unsafe fn iter(table: &'a Table) -> Self::Iter {
        match table.get_column() {
            Some(v) => OptionalIter::Some(v.iter()),
            None => OptionalIter::None(table.len()),
        }
    }

    unsafe fn par_iter(table: &'a Table) -> Self::ParIter {
        match table.get_column() {
            Some(v) => OptionalIter::Some(v.par_iter()),
            None => OptionalIter::None(table.len()),
        }
    }
}

impl<T: Component> View for Option<&mut T> {
    type Fetch = OptionalWriteFetch<T>;
    type Component = T;

    const ACCESS: Access = Access::Write;
    const REQUIRED: bool = false;
}

pub struct OptionalWriteFetch<T>(T);

impl<'a, T: Component> ViewFetch<'a> for OptionalWriteFetch<T> {
    type Item = Option<&'a mut T>;
    type Iter = OptionalIter<slice::IterMut<'a, T>>;
    type ParIter = OptionalIter<rayon::slice::IterMut<'a, T>>;

    unsafe fn iter(table: &'a Table) -> Self::Iter {
        match table.get_column_mut() {
            Some(v) => OptionalIter::Some(v.iter_mut()),
            None => OptionalIter::None(table.len()),
        }
    }

    unsafe fn par_iter(table: &'a Table) -> Self::ParIter {
        match table.get_column_mut() {
            Some(v) => OptionalIter::Some(v.par_iter_mut()),
            None => OptionalIter::None(table.len()),
        }
    }
}

pub enum OptionalIter<T> {
    Some(T),
    None(usize),
}

impl<T: Iterator> Iterator for OptionalIter<T> {
    type Item = Option<T::Item>;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            OptionalIter::Some(it) => it.next().map(Some),
            OptionalIter::None(len) => {
                if *len == 0 {
                    return None;
                }
                *len -= 1;
                Some(None)
            }
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        match self {
            OptionalIter::Some(it) => it.size_hint(),
            &OptionalIter::None(len) => (len, Some(len)),
        }
    }
}

impl<T: IndexedParallelIterator> ParallelIterator for OptionalIter<T> {
    type Item = Option<T::Item>;

    fn drive_unindexed<C>(self, consumer: C) -> C::Result
    where
        C: UnindexedConsumer<Self::Item>,
    {
        self.drive(consumer)
    }

    fn opt_len(&self) -> Option<usize> {
        Some(self.len())
    }
}

impl<T: IndexedParallelIterator> IndexedParallelIterator for OptionalIter<T> {
    fn len(&self) -> usize {
        match self {
            OptionalIter::Some(v) => v.len(),
            &OptionalIter::None(len) => len,
        }
    }

    fn drive<C>(self, consumer: C) -> C::Result
    where
        C: Consumer<Self::Item>,
    {
        match self {
            OptionalIter::Some(v) => v.map(Some).drive(consumer),
            OptionalIter::None(len) => (0..len).into_par_iter().map(|_| None).drive(consumer),
        }
    }

    fn with_producer<CB>(self, callback: CB) -> CB::Output
    where
        CB: ProducerCallback<Self::Item>,
    {
        match self {
            OptionalIter::Some(v) => v.map(Some).with_producer(callback),
            OptionalIter::None(len) => (0..len)
                .into_par_iter()
                .map(|_| None)
                .with_producer(callback),
        }
    }
}

/// View to multiple components, as well as the [EntityId].
///
/// Implemented for tuples in the form of `(T: View, ...)` up to 16 elements,
/// as well as for tuples in the form of `(EntityId, T: View, ...)` up to 17 elements.
pub trait MultiView {
    type Fetch: for<'a> MultiViewFetch<'a>;

    fn components() -> Vec<(ComponentId, Access)>;

    fn num_required_components() -> usize;
}

pub trait MultiViewFetch<'a> {
    type Item: 'a;
    /// Type of the iterator returned from the `iter` method.
    type Iter: Iterator<Item = Self::Item>;
    /// Type of the iterator returned from the `par_iter` method.
    type ParIter: IndexedParallelIterator<Item = Self::Item>;

    /// Create an iterator of table's components and entity IDs.
    ///
    /// # Safety
    /// Each component must exist in the table, and be not mutably aliased.
    unsafe fn iter(table: &'a Table) -> Self::Iter;

    unsafe fn par_iter(table: &'a Table) -> Self::ParIter;
}

macro_rules! impl_multi_view {
    (@fold $A:ident,) => {};

    (@fold $A:ident, $B:ident, $($tail:ident,)*) => {
        impl_multi_view!($A, $B, $($tail,)*);
        impl_multi_view!(@fold $B, $($tail,)*);
    };

    ($($T:ident,)+) => {
        impl<$($T: View,)+> MultiView for ($($T,)+) {
            type Fetch = ($($T::Fetch,)+);

            fn components() -> Vec<(ComponentId, Access)> {
                let mut components = vec![$((ComponentId::of::<$T::Component>(), $T::ACCESS, $T::REQUIRED),)+];
                components.sort_by(|a, b| b.2.cmp(&a.2));
                components.into_iter().map(|(c, a, _)| (c, a)).collect()
            }

            fn num_required_components() -> usize {
                0 $( + if $T::REQUIRED { 1 } else { 0 })+
            }
        }

        impl<'a, $($T: ViewFetch<'a>,)+> MultiViewFetch<'a> for ($($T,)+) {
            type Item = ($($T::Item,)+);
            type Iter = <($($T::Iter,)*) as MultiZip>::Iter;
            type ParIter = <($($T::ParIter,)*) as ParMultiZip>::ParIter;

            unsafe fn iter(table: &'a Table) -> Self::Iter {
                ($($T::iter(table),)*).multizip()
            }

            unsafe fn par_iter(table: &'a Table) -> Self::ParIter {
                ($($T::par_iter(table),)*).par_multizip()
            }
        }

        impl<$($T: View,)+> MultiView for (EntityId, $($T,)+) {
            type Fetch = ($($T::Fetch,)+);

            fn components() -> Vec<(ComponentId, Access)> {
                vec![$((ComponentId::of::<$T::Component>(), $T::ACCESS),)+]
            }

            fn num_required_components() -> usize {
                0 $( + if $T::REQUIRED { 1 } else { 0 })+
            }
        }

        impl<'a, $($T: ViewFetch<'a>,)+> MultiViewFetch<'a> for (EntityId, $($T,)+) {
            type Item = (EntityId, $($T::Item,)+);
            type Iter = <(Copied<slice::Iter<'a, EntityId>>, $($T::Iter,)*) as MultiZip>::Iter;
            type ParIter = <(rayon::iter::Copied<rayon::slice::Iter<'a, EntityId>>, $($T::ParIter,)*) as ParMultiZip>::ParIter;

            unsafe fn iter(table: &'a Table) -> Self::Iter {
                let entities = table.get_entities().iter().copied();
                (entities, $($T::iter(table),)*).multizip()
            }

            unsafe fn par_iter(table: &'a Table) -> Self::ParIter {
                let entities = table.get_entities().par_iter().copied();
                (entities, $($T::par_iter(table),)*).par_multizip()
            }
        }
    };
}

impl_multi_view!(@fold A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P,); // up to 16

impl<T: View> MultiView for T {
    type Fetch = T::Fetch;

    fn components() -> Vec<(ComponentId, Access)> {
        vec![(ComponentId::of::<T::Component>(), T::ACCESS)]
    }

    fn num_required_components() -> usize {
        T::REQUIRED as usize
    }
}

impl<'a, T: ViewFetch<'a>> MultiViewFetch<'a> for T {
    type Item = T::Item;
    type Iter = T::Iter;
    type ParIter = T::ParIter;

    unsafe fn iter(table: &'a Table) -> Self::Iter {
        T::iter(table)
    }

    unsafe fn par_iter(table: &'a Table) -> Self::ParIter {
        T::par_iter(table)
    }
}

impl MultiView for EntityId {
    type Fetch = EntityId;

    fn components() -> Vec<(ComponentId, Access)> {
        vec![]
    }

    fn num_required_components() -> usize {
        0
    }
}

impl<'a> MultiViewFetch<'a> for EntityId {
    type Item = EntityId;
    type Iter = Copied<slice::Iter<'a, EntityId>>;
    type ParIter = rayon::iter::Copied<rayon::slice::Iter<'a, EntityId>>;

    unsafe fn iter(table: &'a Table) -> Self::Iter {
        table.get_entities().iter().copied()
    }

    unsafe fn par_iter(table: &'a Table) -> Self::ParIter {
        table.get_entities().par_iter().copied()
    }
}

impl<T: View> MultiView for (EntityId, T) {
    type Fetch = (EntityId, T::Fetch);

    fn components() -> Vec<(ComponentId, Access)> {
        vec![(ComponentId::of::<T::Component>(), T::ACCESS)]
    }

    fn num_required_components() -> usize {
        T::REQUIRED as usize
    }
}

impl<'a, T: ViewFetch<'a>> MultiViewFetch<'a> for (EntityId, T) {
    type Item = (EntityId, T::Item);
    type Iter = Zip<Copied<slice::Iter<'a, EntityId>>, T::Iter>;
    type ParIter =
        rayon::iter::Zip<rayon::iter::Copied<rayon::slice::Iter<'a, EntityId>>, T::ParIter>;

    unsafe fn iter(table: &'a Table) -> Self::Iter {
        table.get_entities().iter().copied().zip(T::iter(table))
    }

    unsafe fn par_iter(table: &'a Table) -> Self::ParIter {
        table
            .get_entities()
            .par_iter()
            .copied()
            .zip(T::par_iter(table))
    }
}
