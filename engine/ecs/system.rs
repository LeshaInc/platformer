use std::any::TypeId;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

use atomic_refcell::{AtomicRef, AtomicRefMut};

use super::filter::Filter;
use super::{
    Access, CommandBuffer, ComponentId, MultiView, Permissions, Query, Resource, WorldView,
};

#[derive(Clone, Debug, Default)]
pub struct AccessSet {
    pub components: Vec<ComponentId>,
    pub component_access: Vec<Access>,
    pub resources: Vec<TypeId>,
    pub resource_access: Vec<Access>,
}

impl AccessSet {
    pub fn component_permissions(&self) -> Permissions<ComponentId> {
        let it_a = self.components.iter().copied();
        let it_b = self.component_access.iter().copied();
        Permissions::Allow(it_a.zip(it_b).collect())
    }

    pub fn aliases(&self, other: &AccessSet) -> bool {
        Self::aliases_list(
            &self.resources,
            &self.resource_access,
            &other.resources,
            &other.resource_access,
        ) || Self::aliases_list(
            &self.components,
            &self.component_access,
            &other.components,
            &other.component_access,
        )
    }

    fn aliases_list<T: Copy + Eq>(
        ids_a: &[T],
        access_a: &[Access],
        ids_b: &[T],
        access_b: &[Access],
    ) -> bool {
        for (&component_a, &access_a) in ids_a.iter().zip(access_a) {
            for (&component_b, &access_b) in ids_b.iter().zip(access_b) {
                if component_a == component_b
                    && (access_a == Access::Write || access_b == Access::Write)
                {
                    return true;
                }
            }
        }

        false
    }

    pub fn validate(&self) {
        Self::validate_list(&self.components, &self.component_access);
        Self::validate_list(&self.resources, &self.resource_access);
    }

    fn validate_list<T: std::fmt::Debug + Copy + Eq>(ids: &[T], access: &[Access]) {
        let it = ids.iter().zip(access).enumerate();

        for (i, (&component_a, &access_a)) in it.clone() {
            for (j, (&component_b, &access_b)) in it.clone() {
                if i != j
                    && component_a == component_b
                    && (access_a == Access::Write || access_b == Access::Write)
                {
                    panic!(
                        "{:?} has {:?} and {:?} at the same time",
                        component_a, access_a, access_a
                    );
                }
            }
        }
    }
}

pub trait SystemParam: Sized {
    type Fetch: for<'a> SystemParamFetch<'a>;

    fn append_access(set: &mut AccessSet) {
        let _ = set;
    }
}

pub trait SystemParamFetch<'w> {
    type Item: SystemParam;

    fn fetch(world: &WorldView<'w>) -> Self::Item;
}

pub struct Res<'w, R: Resource>(AtomicRef<'w, R>);

impl<R: Resource> Deref for Res<'_, R> {
    type Target = R;

    fn deref(&self) -> &R {
        &self.0
    }
}

impl<'w, R: Resource> SystemParam for Res<'w, R> {
    type Fetch = ResFetch<R>;

    fn append_access(set: &mut AccessSet) {
        set.resources.push(TypeId::of::<R>());
        set.resource_access.push(Access::Read);
    }
}

pub struct ResFetch<R>(R);

impl<'w, R: Resource> SystemParamFetch<'w> for ResFetch<R> {
    type Item = Res<'w, R>;

    fn fetch(world: &WorldView<'w>) -> Self::Item {
        Res(world.resources().get())
    }
}

pub struct ResMut<'w, R: Resource>(AtomicRefMut<'w, R>);

impl<R: Resource> Deref for ResMut<'_, R> {
    type Target = R;

    fn deref(&self) -> &R {
        &self.0
    }
}

impl<R: Resource> DerefMut for ResMut<'_, R> {
    fn deref_mut(&mut self) -> &mut R {
        &mut self.0
    }
}

impl<'w, R: Resource> SystemParam for ResMut<'w, R> {
    type Fetch = ResMutFetch<R>;

    fn append_access(set: &mut AccessSet) {
        set.resources.push(TypeId::of::<R>());
        set.resource_access.push(Access::Write);
    }
}

pub struct ResMutFetch<R>(R);

impl<'w, R: Resource> SystemParamFetch<'w> for ResMutFetch<R> {
    type Item = ResMut<'w, R>;

    fn fetch(world: &WorldView<'w>) -> Self::Item {
        ResMut(world.resources().get_mut())
    }
}

impl<V, F> SystemParam for Query<V, F>
where
    V: MultiView,
    F: Filter + Default,
{
    type Fetch = Query<V, F>;

    fn append_access(set: &mut AccessSet) {
        let components = V::components();
        set.components.reserve(components.len());
        set.component_access.reserve(components.len());
        for (c, a) in V::components() {
            set.components.push(c);
            set.component_access.push(a);
        }
    }
}

impl<'w, V, F> SystemParamFetch<'w> for Query<V, F>
where
    V: MultiView,
    F: Filter + Default,
{
    type Item = Query<V, F>;

    fn fetch(_: &WorldView<'w>) -> Self::Item {
        Self::Item::default()
    }
}

pub struct WorldViewMarker;

pub struct CBufMarker;

pub trait System: Send + Sync {
    fn access() -> AccessSet
    where
        Self: Sized;

    fn run(&mut self, cbuf: &mut CommandBuffer, world: &mut WorldView);
}

pub trait IntoSystem<P> {
    type System: System;

    fn system(self) -> Self::System;
}

pub struct FunctionSystem<Params, F> {
    func: F,
    args: PhantomData<fn(Params)>,
}

macro_rules! impl_system {
    (@fold) => {
        impl_system!();
    };

    (@fold $T:ident, $($Tail:ident,)*) => {
        impl_system!($T, $($Tail,)*);
        impl_system!(@fold $($Tail,)*);
    };

    ($($T:ident,)*) => {
        impl<Func, $($T,)*> IntoSystem<($($T,)*)> for Func
        where
            $($T: SystemParam,)*
            Func: FnMut($($T,)*)
                + FnMut($(<$T::Fetch as SystemParamFetch>::Item,)*)
                + Send + Sync,
        {
            type System = FunctionSystem<($($T,)*), Func>;

            fn system(self) -> Self::System {
                FunctionSystem {
                    func: self,
                    args: PhantomData,
                }
            }
        }

        #[allow(non_snake_case)]
        #[allow(unused_variables)]
        impl<Func, $($T,)*> System for FunctionSystem<($($T,)*), Func>
        where
            $($T: SystemParam,)*
            Func: FnMut($($T,)*)
                + FnMut($(<$T::Fetch as SystemParamFetch>::Item,)*)
                + Send + Sync,
        {
            #[allow(unused_mut)]
            fn access() -> AccessSet {
                let mut set = AccessSet::default();
                $($T::append_access(&mut set);)*
                set
            }

            fn run(&mut self, _: &mut CommandBuffer, world: &mut WorldView) {
                $(let $T = $T::Fetch::fetch(world);)*
                (self.func)($($T,)*);
            }
        }

        impl<Func, $($T,)*> IntoSystem<(WorldViewMarker, $($T,)*)> for Func
        where
            $($T: SystemParam,)*
            Func: FnMut(&mut WorldView<'_>, $($T,)*)
                + FnMut(&mut WorldView<'_>, $(<$T::Fetch as SystemParamFetch>::Item,)*)
                + Send + Sync,
        {
            type System = FunctionSystem<(WorldViewMarker, $($T,)*), Func>;

            fn system(self) -> Self::System {
                FunctionSystem {
                    func: self,
                    args: PhantomData,
                }
            }
        }

        #[allow(non_snake_case)]
        #[allow(unused_variables)]
        impl<Func, $($T,)*> System for FunctionSystem<(WorldViewMarker, $($T,)*), Func>
        where
            $($T: SystemParam,)*
            Func: FnMut(&mut WorldView<'_>, $($T,)*)
                + FnMut(&mut WorldView<'_>, $(<$T::Fetch as SystemParamFetch>::Item,)*)
                + Send + Sync,
        {
            #[allow(unused_mut)]
            fn access() -> AccessSet {
                let mut set = AccessSet::default();
                $($T::append_access(&mut set);)*
                set
            }

            fn run(&mut self, _: &mut CommandBuffer, world: &mut WorldView) {
                $(let $T = $T::Fetch::fetch(world);)*
                (self.func)(world, $($T,)*);
            }
        }

        impl<Func, $($T,)*> IntoSystem<(CBufMarker, $($T,)*)> for Func
        where
            $($T: SystemParam,)*
            Func: FnMut(&mut CommandBuffer, $($T,)*)
                + FnMut(&mut CommandBuffer, $(<$T::Fetch as SystemParamFetch>::Item,)*)
                + Send + Sync,
        {
            type System = FunctionSystem<(CBufMarker, $($T,)*), Func>;

            fn system(self) -> Self::System {
                FunctionSystem {
                    func: self,
                    args: PhantomData,
                }
            }
        }

        #[allow(non_snake_case)]
        #[allow(unused_variables)]
        impl<Func, $($T,)*> System for FunctionSystem<(CBufMarker, $($T,)*), Func>
        where
            $($T: SystemParam,)*
            Func: FnMut(&mut CommandBuffer, $($T,)*)
                + FnMut(&mut CommandBuffer, $(<$T::Fetch as SystemParamFetch>::Item,)*)
                + Send + Sync,
        {
            #[allow(unused_mut)]
            fn access() -> AccessSet {
                let mut set = AccessSet::default();
                $($T::append_access(&mut set);)*
                set
            }

            fn run(&mut self, cbuf: &mut CommandBuffer, world: &mut WorldView) {
                $(let $T = $T::Fetch::fetch(world);)*
                (self.func)(cbuf, $($T,)*);
            }
        }

        impl<Func, $($T,)*> IntoSystem<(WorldViewMarker, CBufMarker, $($T,)*)> for Func
        where
            $($T: SystemParam,)*
            Func: FnMut(&mut WorldView<'_>, &mut CommandBuffer, $($T,)*)
                + FnMut(&mut WorldView<'_>, &mut CommandBuffer, $(<$T::Fetch as SystemParamFetch>::Item,)*)
                + Send + Sync,
        {
            type System = FunctionSystem<(WorldViewMarker, CBufMarker, $($T,)*), Func>;

            fn system(self) -> Self::System {
                FunctionSystem {
                    func: self,
                    args: PhantomData,
                }
            }
        }

        #[allow(non_snake_case)]
        #[allow(unused_variables)]
        impl<Func, $($T,)*> System for FunctionSystem<(WorldViewMarker, CBufMarker, $($T,)*), Func>
        where
            $($T: SystemParam,)*
            Func: FnMut(&mut WorldView<'_>, &mut CommandBuffer, $($T,)*)
                + FnMut(&mut WorldView<'_>, &mut CommandBuffer, $(<$T::Fetch as SystemParamFetch>::Item,)*)
                + Send + Sync,
        {
            #[allow(unused_mut)]
            fn access() -> AccessSet {
                let mut set = AccessSet::default();
                $($T::append_access(&mut set);)*
                set
            }

            fn run(&mut self, cbuf: &mut CommandBuffer, world: &mut WorldView) {
                $(let $T = $T::Fetch::fetch(world);)*
                (self.func)(world, cbuf, $($T,)*);
            }
        }
    };
}

impl_system!(@fold A, B, C, D, E, F, G, H, I, J,); // up to 10

#[cfg(test)]
#[allow(dead_code)]
mod tests {
    use rayon::iter::ParallelIterator;

    use super::*;
    use crate::ecs::test_components::Pos;

    fn foo(world: &mut WorldView, query: Query<&Pos>) {
        for _pos in query.iter(world) {
            // do something
        }

        query.par_iter(world).for_each(|_pos| {
            // do something
        });
    }

    fn foobar() {
        foo.system();
    }
}
