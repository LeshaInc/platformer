use rayon::iter::plumbing::{Consumer, ProducerCallback, UnindexedConsumer};
use rayon::iter::{IndexedParallelIterator, ParallelIterator};

pub trait ParMultiZip {
    type ParIter;

    fn par_multizip(self) -> Self::ParIter;
}

pub struct ParMultiZipIter<T>(T);

macro_rules! impl_multizip {
    (@fold $A:ident,) => {};

    (@fold $A:ident, $B:ident, $($Tail:ident,)*) => {
        impl_multizip!($A, $B, $($Tail,)*);
        impl_multizip!(@fold $B, $($Tail,)*);
    };

    (@cons $A:ident, $B:ident,) => (
        ($A, $B)
    );

    (@cons $T:ident, $($Tail:ident,)+) => (
        ($T, impl_multizip!(@cons $($Tail,)+))
    );

    (@zip $A:ident, $B:ident,) => (
        $A.zip($B)
    );

    (@zip $T:ident, $($Tail:ident,)+) => (
        $T.zip(impl_multizip!(@zip $($Tail,)+))
    );

    ($($T:ident,)+) => {
        impl<$($T: IndexedParallelIterator,)+> ParMultiZip for ($($T,)+) {
            type ParIter = ParMultiZipIter<($($T,)+)>;

            fn par_multizip(self) -> Self::ParIter {
                ParMultiZipIter(self)
            }
        }

        impl<$($T: IndexedParallelIterator,)+> ParallelIterator for ParMultiZipIter<($($T,)+)> {
            type Item = ($($T::Item,)+);

            fn drive_unindexed<CO>(self, consumer: CO) -> CO::Result
            where
                CO: UnindexedConsumer<Self::Item>
            {
                self.drive(consumer)
            }

            fn opt_len(&self) -> Option<usize> {
                Some(self.len())
            }
        }

        #[allow(non_snake_case)]
        impl<$($T: IndexedParallelIterator,)+> IndexedParallelIterator for ParMultiZipIter<($($T,)+)> {
            fn len(&self) -> usize {
                let ($($T,)+) = &self.0;
                [$($T.len(),)+].iter().copied().min().unwrap_or(0)
            }

            fn drive<CO>(self, consumer: CO) -> CO::Result
            where
                CO: Consumer<Self::Item>
            {
                let ($($T,)+) = self.0;
                impl_multizip!(@zip $($T,)+)
                    .map(|impl_multizip!(@cons $($T,)+)| ($($T,)+))
                    .drive(consumer)
            }

            fn with_producer<CB>(self, callback: CB) -> CB::Output
            where
                CB: ProducerCallback<Self::Item>
            {
                let ($($T,)+) = self.0;
                impl_multizip!(@zip $($T,)+)
                    .map(|impl_multizip!(@cons $($T,)+)| ($($T,)+))
                    .with_producer(callback)
            }
        }
    };
}

impl_multizip!(@fold A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q,); // up to 17
