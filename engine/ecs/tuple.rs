use std::ops::ControlFlow;

use super::component::{Component, ComponentMeta};

pub trait ComponentTuple: 'static + Send + Sync {
    type Head: Component;
    type Tail: ComponentTuple;

    const SIZE: usize;

    fn for_each<T: ComponentConsumer>(self, consumer: T);

    fn for_each_meta<T>(consumer: impl FnMut(ComponentMeta) -> ControlFlow<T>) -> ControlFlow<T>;
}

pub trait ComponentConsumer {
    fn consume<T: Component>(&mut self, component: T);
}

impl ComponentTuple for () {
    type Head = ();
    type Tail = ();

    const SIZE: usize = 0;

    fn for_each<T: ComponentConsumer>(self, _consumer: T) {}

    fn for_each_meta<T>(_consumer: impl FnMut(ComponentMeta) -> ControlFlow<T>) -> ControlFlow<T> {
        ControlFlow::Continue(())
    }
}

macro_rules! impl_tuple {
    (@fold) => {};

    (@fold $Head:ident, $($Tail:ident,)*) => {
        impl_tuple!($Head, $($Tail,)*);
        impl_tuple!(@fold $($Tail,)*);
    };

    ($Head:ident, $($Tail:ident,)*) => {
        #[allow(non_snake_case)]
        impl<$Head: Component, $($Tail: Component,)*> ComponentTuple for ($Head, $($Tail,)*) {
            type Head = $Head;
            type Tail = ($($Tail,)*);

            const SIZE: usize = 1 + Self::Tail::SIZE;

            fn for_each<T: ComponentConsumer>(self, mut consumer: T) {
                let ($Head, $($Tail,)*) = self;
                consumer.consume($Head);
                ComponentTuple::for_each(($($Tail,)*), consumer);
            }

            fn for_each_meta<T>(mut consumer: impl FnMut(ComponentMeta) -> ControlFlow<T>) -> ControlFlow<T> {
                match consumer(ComponentMeta::of::<$Head>()) {
                    ControlFlow::Continue(_) => <($($Tail,)*) as ComponentTuple>::for_each_meta(consumer),
                    v => v,
                }
            }
        }
    };
}

impl_tuple!(@fold A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P,); // up to 16
