use std::any::TypeId;

/// Unique component's ID, used in (de)serialization.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct ComponentId(TypeId);

impl ComponentId {
    pub fn of<T: Component>() -> ComponentId {
        ComponentId(TypeId::of::<T>())
    }
}

pub trait Component: 'static + Send + Sync {}

impl<T: 'static + Send + Sync> Component for T {}

#[derive(Clone, Copy, Debug)]
pub struct ComponentMeta {
    pub id: ComponentId,
    pub size: usize,
    pub align: usize,
    pub stride: usize,
    pub drop: unsafe fn(*mut u8, usize),
}

impl ComponentMeta {
    pub fn of<T: Component>() -> ComponentMeta {
        let size = std::mem::size_of::<T>();
        let align = std::mem::align_of::<T>();
        let stride =
            array_stride(size, align).expect("integer overflow while computing array stride");
        ComponentMeta {
            id: ComponentId::of::<T>(),
            size,
            align,
            stride,
            drop: drop_components::<T>,
        }
    }
}

unsafe fn drop_components<T: Component>(ptr: *mut u8, count: usize) {
    let ptr = ptr as *mut T;
    for i in 0..count {
        std::ptr::drop_in_place(ptr.add(i));
    }
}

fn array_stride(size: usize, align: usize) -> Option<usize> {
    Some(size.checked_add(align)?.checked_sub(1)? & !align.checked_sub(1)?)
}
