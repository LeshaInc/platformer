use std::marker::PhantomData;

pub trait MultiZip {
    type Iter;

    fn multizip(self) -> Self::Iter;
}

pub struct Flattener<T, const N: usize>(T, PhantomData<[(); N]>);

macro_rules! impl_multizip {
    (@fold $A:ident,) => {};

    (@fold $A:ident, $B:ident, $($Tail:ident,)*) => {
        impl_multizip!($A, $B, $($Tail,)*);
        impl_multizip!(@fold $B, $($Tail,)*);
    };

    (@cons $A:ident, $B:ident,) => (
        ($A, $B)
    );

    (@cons $T:ident, $($Tail:ident,)+) => (
        ($T, impl_multizip!(@cons $($Tail,)+))
    );

    (@zip $A:ident, $B:ident,) => (
        $A.into_iter().zip($B.into_iter())
    );

    (@zip $T:ident, $($Tail:ident,)+) => (
        $T.into_iter().zip(impl_multizip!(@zip $($Tail,)+))
    );

    (@zip_ty $A:ident, $B:ident,) => (
        std::iter::Zip<$A, $B>
    );

    (@zip_ty $T:ident, $($Tail:ident,)+) => (
        std::iter::Zip<$T, impl_multizip!(@zip_ty $($Tail,)+)>
    );

    (@zip_ty_it $A:ident, $B:ident,) => (
        std::iter::Zip<$A::IntoIter, $B::IntoIter>
    );

    (@zip_ty_it $T:ident, $($Tail:ident,)+) => (
        std::iter::Zip<$T::IntoIter, impl_multizip!(@zip_ty_it $($Tail,)+)>
    );

    (@count $A:ident, $B:ident,) => (
        2
    );

    (@count $T:ident, $($Tail:ident,)+) => (
        1 + impl_multizip!(@count $($Tail,)+)
    );

    ($($T:ident,)+) => {
        #[allow(non_snake_case)]
        impl<$($T: Iterator,)+>
            Iterator
        for Flattener<
                impl_multizip!(@zip_ty $($T,)+),
                { impl_multizip!(@count $($T,)+) }
            >
        {
            type Item = ($($T::Item,)+);

            fn next(&mut self) -> Option<Self::Item> {
                let impl_multizip!(@cons $($T,)+) = self.0.next()?;
                Some(($($T,)+))
            }

            fn size_hint(&self) -> (usize, Option<usize>) {
                self.0.size_hint()
            }

            fn nth(&mut self, n: usize) -> Option<Self::Item> {
                let impl_multizip!(@cons $($T,)+) = self.0.nth(n)?;
                Some(($($T,)+))
            }
        }

        #[allow(non_snake_case)]
        impl<$($T: DoubleEndedIterator + ExactSizeIterator,)+>
            DoubleEndedIterator
        for Flattener<
                impl_multizip!(@zip_ty $($T,)+),
                { impl_multizip!(@count $($T,)+) }
            >
        {
            fn next_back(&mut self) -> Option<Self::Item> {
                let impl_multizip!(@cons $($T,)+) = self.0.next_back()?;
                Some(($($T,)+))
            }
        }

        #[allow(non_snake_case)]
        impl<$($T: ExactSizeIterator,)+>
            ExactSizeIterator
        for Flattener<
                impl_multizip!(@zip_ty $($T,)+),
                { impl_multizip!(@count $($T,)+) }
            >
        {
            fn len(&self) -> usize {
                self.0.len()
            }
        }

        #[allow(non_snake_case)]
        impl<$($T: IntoIterator,)+> MultiZip for ($($T,)+) {
            type Iter = Flattener<
                impl_multizip!(@zip_ty_it $($T,)+),
                { impl_multizip!(@count $($T,)+) }
            >;

            fn multizip(self) -> Self::Iter {
                let ($($T,)+) = self;
                Flattener(impl_multizip!(@zip $($T,)+), PhantomData)
            }
        }
    };
}

impl_multizip!(@fold A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q,); // up to 17
