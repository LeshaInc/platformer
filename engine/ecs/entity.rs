use std::num::NonZeroU64;
use std::sync::atomic::{AtomicU64, Ordering};

use super::table::TableIndex;

/// ID of an entity.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, serde::Serialize, serde::Deserialize)]
#[serde(transparent)]
pub struct EntityId(NonZeroU64);

impl EntityId {
    pub const FIRST: EntityId = EntityId(unsafe { NonZeroU64::new_unchecked(1) });
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct EntityIndex(pub usize);

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct EntityLocation {
    pub table: TableIndex,
    pub index: EntityIndex,
}

impl EntityLocation {
    pub fn new(table: TableIndex, index: EntityIndex) -> EntityLocation {
        EntityLocation { table, index }
    }
}

#[derive(Debug)]
pub struct IdAllocator {
    next: AtomicU64,
}

impl IdAllocator {
    pub fn new() -> IdAllocator {
        IdAllocator {
            next: AtomicU64::new(1),
        }
    }

    pub fn alloc(&self) -> EntityId {
        let id = self.next.fetch_add(1, Ordering::Relaxed);
        match NonZeroU64::new(id) {
            Some(v) => EntityId(v),
            None => id_overflow(),
        }
    }
}

#[cold]
fn id_overflow() -> ! {
    panic!("entity id overflow");
}
