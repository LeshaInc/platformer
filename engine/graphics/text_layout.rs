use std::str::FromStr;

use fontdue::Metrics;
use unicode_linebreak::BreakOpportunity;

use super::{DrawEncoder, Font, Glyph};
use crate::assets::{AssetStorage, Id};
use crate::math::{Box2D, Rgba, Vec2D};

/// Horizontal alignment
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum HorizontalAlignment {
    /// Left
    Left,
    /// Center
    Center,
    /// Right
    Right,
}

impl Default for HorizontalAlignment {
    fn default() -> Self {
        HorizontalAlignment::Left
    }
}

impl FromStr for HorizontalAlignment {
    type Err = ();

    fn from_str(mut s: &str) -> Result<HorizontalAlignment, ()> {
        if !s.starts_with("horizontal.") {
            return Err(());
        }
        s = &s[11..];
        match s {
            "left" => Ok(HorizontalAlignment::Left),
            "center" => Ok(HorizontalAlignment::Center),
            "right" => Ok(HorizontalAlignment::Right),
            _ => Err(()),
        }
    }
}

/// Horizontal alignment
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum VerticalAlignment {
    /// Top
    Top,
    /// Center
    Center,
    /// Bottom
    Bottom,
}

impl Default for VerticalAlignment {
    fn default() -> Self {
        VerticalAlignment::Top
    }
}

impl FromStr for VerticalAlignment {
    type Err = ();

    fn from_str(mut s: &str) -> Result<VerticalAlignment, ()> {
        if !s.starts_with("vertical.") {
            return Err(());
        }
        s = &s[9..];
        match s {
            "top" => Ok(VerticalAlignment::Top),
            "center" => Ok(VerticalAlignment::Center),
            "bottom" => Ok(VerticalAlignment::Bottom),
            _ => Err(()),
        }
    }
}

/// Text layout settings
#[derive(Clone, Copy, Debug)]
pub struct TextLayoutSettings {
    /// Text bounds. `None` means unbounded.
    pub bounds: Vec2D<Option<f32>>,
    /// Vertical alignment.
    pub vert_alignment: VerticalAlignment,
    /// Horizontal alignment.
    pub horiz_alignment: HorizontalAlignment,
    /// Fallback font ID.
    pub fallback_font_id: Option<Id<Font>>,
    /// Line height. 1.0 by default.
    pub line_height: f32,
}

impl Default for TextLayoutSettings {
    fn default() -> TextLayoutSettings {
        TextLayoutSettings {
            bounds: Vec2D::splat(None),
            vert_alignment: VerticalAlignment::Top,
            horiz_alignment: HorizontalAlignment::Left,
            fallback_font_id: None,
            line_height: 1.0,
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct NewGlyph {
    font: Id<Font>,
    glyph_index: u16,
    metrics: Metrics,
    offset: f32,
    scale: f32,
    color: Rgba,
    outline_color: Option<Rgba>,
}

#[derive(Debug, Default)]
struct TextLayoutScratchBuffers {
    glyphs: Vec<NewGlyph>,
    lines: Vec<Line>,
}

#[derive(Clone, Copy, Debug, Default)]
struct Line {
    width: f32,
    height: f32,
    ascent: f32,
    num_glyphs: usize,
}

/// Text layouter
#[derive(Debug)]
pub struct TextLayouter<'a> {
    fonts: &'a AssetStorage<Font>,
    settings: TextLayoutSettings,
    scratch_buffers: TextLayoutScratchBuffers,

    offset: f32,
    whitespace: f32,
    width: f32,
    height: f32,
    finished: bool,
}

impl<'a> TextLayouter<'a> {
    /// Create a new text layouter.
    pub fn new(fonts: &'a AssetStorage<Font>) -> TextLayouter<'a> {
        TextLayouter {
            fonts,
            settings: TextLayoutSettings::default(),
            scratch_buffers: TextLayoutScratchBuffers::default(),
            offset: 0.0,
            whitespace: 0.0,
            width: 0.0,
            height: 0.0,
            finished: false,
        }
    }

    /// Reset the layouter, specifying the new settings.
    pub fn reset(&mut self, settings: &TextLayoutSettings) {
        self.scratch_buffers.glyphs.clear();
        self.scratch_buffers.lines.clear();
        self.settings = *settings;
        self.offset = 0.0;
        self.whitespace = 0.0;
        self.width = 0.0;
        self.height = 0.0;
    }

    /// Append some text.
    pub fn append(
        &mut self,
        main_font_id: Id<Font>,
        scale: f32,
        color: Rgba,
        outline_color: Option<Rgba>,
        text: &str,
    ) {
        if text.is_empty() {
            return;
        }

        let glyphs = &mut self.scratch_buffers.glyphs;
        let lines = &mut self.scratch_buffers.lines;

        let offset = &mut self.offset;
        let whitespace = &mut self.whitespace;

        let main_font = self.fonts.get_by_id(main_font_id).unwrap(); // FIXME
        let fallback_font_id = self.settings.fallback_font_id.unwrap_or(main_font_id);
        let fallback_font = self.fonts.get_by_id(fallback_font_id).unwrap();

        let mut default_ascent = 0f32;
        let mut default_height = 0f32;

        if let Some(metrics) = main_font.horizontal_line_metrics(scale) {
            default_ascent = default_ascent.max(metrics.ascent);
            default_height = default_height.max(metrics.new_line_size); // FIXME: this includes line gap
        } else {
            log::warn!("Font {:?} has no horizontal metrics", main_font_id);
        }

        let mut has_fallback = false;

        let mut last_break = 0;
        for (break_pos, linebreak) in unicode_linebreak::linebreaks(text) {
            let chunk = &text[last_break..break_pos];
            let chunk_trimmed = chunk.trim_end();
            let chunk_remaining = &chunk[chunk_trimmed.len()..];
            last_break = break_pos;

            let mut glyph_offset = 0.0;
            let mut chunk_width = 0.0;
            let mut num_glyphs = 0;

            glyphs.extend(chunk_trimmed.chars().map(|c| {
                let main_idx = main_font.lookup_glyph_index(c);
                let (font_id, glyph_index) = if main_idx == 0 {
                    let fallback_idx = fallback_font.lookup_glyph_index(c);
                    if fallback_idx == 0 {
                        (main_font_id, main_idx)
                    } else {
                        has_fallback = true;
                        (fallback_font_id, fallback_idx)
                    }
                } else {
                    (main_font_id, main_idx)
                };

                let metrics = if font_id == main_font_id {
                    main_font.metrics_indexed(glyph_index, scale)
                } else {
                    fallback_font.metrics_indexed(glyph_index, scale)
                };

                let width = metrics.advance_width.ceil();
                let offset = glyph_offset;
                glyph_offset += width;
                chunk_width += width;
                num_glyphs += 1;

                NewGlyph {
                    font: font_id,
                    glyph_index: glyph_index as u16,
                    metrics,
                    offset,
                    scale,
                    color,
                    outline_color,
                }
            }));

            let max_width_opt = self.settings.bounds.x;

            let is_overflowing = max_width_opt
                .map(|max_width| *offset + *whitespace + chunk_width > max_width)
                .unwrap_or(false);
            if is_overflowing || lines.is_empty() {
                *offset = chunk_width;
                self.width = self.width.max(chunk_width);
                self.height += default_height;
                lines.push(Line {
                    width: chunk_width,
                    ascent: default_ascent,
                    height: default_height,
                    num_glyphs,
                });
            } else if let Some(line) = lines.last_mut() {
                for glyph in glyphs.iter_mut().rev().take(num_glyphs).rev() {
                    glyph.offset += *whitespace + *offset;
                }

                line.width += *whitespace + chunk_width;
                line.num_glyphs += num_glyphs;

                let old_height = line.height;
                line.height = line.height.max(default_height);
                line.ascent = line.ascent.max(default_ascent);

                self.width = self.width.max(line.width);
                self.height += line.height - old_height;

                *offset += *whitespace + chunk_width;
            }

            *whitespace = 0.0;

            let is_hard_break = linebreak == BreakOpportunity::Mandatory
                && (break_pos != text.len() || text.ends_with('\n'));
            if is_hard_break {
                self.height += default_height;
                *offset = 0.0;
                lines.push(Line {
                    ascent: default_ascent,
                    height: default_height,
                    ..Line::default()
                });
            } else {
                for c in chunk_remaining.chars() {
                    let metrics = main_font.metrics(c, scale);
                    *whitespace += metrics.advance_width.ceil();
                }
            }
        }

        self.finished = false;
    }

    /// Text size so far.
    pub fn size(&self) -> Vec2D<f32> {
        Vec2D::new(self.width, self.height)
    }

    /// Text width so far.
    pub fn width(&self) -> f32 {
        self.width
    }

    /// Text height so far.
    pub fn height(&self) -> f32 {
        self.height
    }

    fn layout(&mut self, origin: Vec2D<f32>) -> impl Iterator<Item = (Glyph, Option<Rgba>)> + '_ {
        let mut glyphs = self.scratch_buffers.glyphs.iter().copied();
        let mut lines = self.scratch_buffers.lines.iter().copied();
        let mut line_opt = None;

        let vert_alignment = self.settings.vert_alignment;
        let horiz_alignment = self.settings.horiz_alignment;

        let max_width = match self.settings.bounds.x {
            Some(v) => v.max(self.width),
            None => self.width,
        };

        let max_height = match self.settings.bounds.y {
            Some(v) => v.max(self.height),
            None => self.height,
        };

        let mut y_offset = match vert_alignment {
            VerticalAlignment::Top => 0.0,
            VerticalAlignment::Center => (max_height - self.height) / 2.0,
            VerticalAlignment::Bottom => max_height - self.height,
        };

        std::iter::from_fn(move || {
            let mut line = loop {
                if line_opt.is_none() {
                    line_opt = lines.next();
                }

                let line = line_opt.as_mut()?;
                if line.num_glyphs > 0 {
                    break line;
                } else {
                    y_offset += line.height;
                    line_opt = None;
                }
            };

            let glyph = glyphs.next()?;

            let x = match horiz_alignment {
                HorizontalAlignment::Left => glyph.offset,
                HorizontalAlignment::Center => (max_width - line.width) / 2.0 + glyph.offset,
                HorizontalAlignment::Right => max_width - line.width + glyph.offset,
            };

            let min = origin
                + Vec2D::new(
                    x + glyph.metrics.xmin as f32,
                    y_offset + line.ascent
                        - glyph.metrics.ymin as f32
                        - glyph.metrics.height as f32,
                );
            let max = min + Vec2D::new(glyph.metrics.width as f32, glyph.metrics.height as f32);

            let result = Some((
                Glyph {
                    font: glyph.font,
                    rect: Box2D::new(min.map(|v| v.floor()), max.map(|v| v.floor())),
                    scale: glyph.scale,
                    glyph_index: glyph.glyph_index,
                    color: glyph.color,
                },
                glyph.outline_color,
            ));

            line.num_glyphs -= 1;
            if line.num_glyphs == 0 {
                y_offset += line.height;
                line_opt = None;
            }

            result
        })
    }

    /// Encode text drawing commands.
    pub fn encode(&mut self, encoder: &mut DrawEncoder<'_>, origin: Vec2D<f32>) {
        for (glyph, outline_color) in self.layout(origin) {
            if let Some(outline_color) = outline_color {
                for &dir in &Vec2D::DIRECTIONS {
                    encoder.draw_glyph(Glyph {
                        color: outline_color,
                        rect: glyph.rect.translate(dir.cast::<f32>()),
                        ..glyph
                    });
                }
            }
        }

        for (glyph, _) in self.layout(origin) {
            encoder.draw_glyph(glyph);
        }
    }
}
