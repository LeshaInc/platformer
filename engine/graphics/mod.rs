//! Graphics system

mod atlas;
mod debug_draw;
mod draw_list;
mod dynamic_buffer;
mod font;
mod image;
mod pipeline;
mod shader;
mod text_layout;

use eyre::{eyre, Result};
use winit::window::Window;

use self::atlas::{FontAtlas, ImageAtlases};
pub use self::debug_draw::DebugDrawList;
pub use self::draw_list::{
    DrawCommand, DrawEncoder, DrawList, Glyph, Lighting, NinePatchSprite, Sprite,
};
pub use self::font::*;
pub use self::image::{Image, ImageLoader, NinePatchImage, NinePatchImageLoader};
use self::pipeline::Pipeline;
use self::shader::ShaderSource;
pub use self::text_layout::{
    HorizontalAlignment, TextLayoutSettings, TextLayouter, VerticalAlignment,
};
use crate::assets::{AssetRegistry, Assets};
use crate::math::Vec2D;

/// Graphics system entry point
#[derive(Debug)]
pub struct Graphics {
    device: wgpu::Device,
    queue: wgpu::Queue,
    surface: wgpu::Surface,
    surface_format: wgpu::TextureFormat,
    depth_texture: wgpu::Texture,
    depth_texture_view: wgpu::TextureView,
    color_texture: wgpu::Texture,
    color_texture_view: wgpu::TextureView,
    image_atlases: ImageAtlases,
    font_atlas: FontAtlas,
    pipeline: Pipeline,
    resolution: Vec2D<u32>,
}

impl Graphics {
    /// Register all assets used by the graphics system
    pub fn register_assets(registry: &mut AssetRegistry) {
        registry.register_with_flushing::<Image>();
        registry.register::<Font>();
        registry.register::<ShaderSource>();
        registry.register::<NinePatchImage>();
    }

    /// Create the graphics system
    pub async fn new(window: &Window, assets: &Assets) -> Result<Self> {
        let backend = wgpu::util::backend_bits_from_env().unwrap_or(wgpu::Backends::PRIMARY);
        let instance = wgpu::Instance::new(backend);
        let surface = unsafe { instance.create_surface(window) };

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::HighPerformance,
                force_fallback_adapter: false,
                compatible_surface: Some(&surface),
            })
            .await
            .ok_or_else(|| eyre!("No adapter"))?;

        let desc = &wgpu::DeviceDescriptor {
            label: None,
            features: wgpu::Features::empty(),
            limits: wgpu::Limits::default(),
        };
        let (device, queue) = adapter.request_device(desc, None).await?;

        let resolution = get_resolution(window);

        let surface_format = surface
            .get_preferred_format(&adapter)
            .unwrap_or(wgpu::TextureFormat::Bgra8UnormSrgb);

        surface.configure(
            &device,
            &wgpu::SurfaceConfiguration {
                usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
                format: surface_format,
                width: resolution.x,
                height: resolution.y,
                present_mode: wgpu::PresentMode::Fifo,
            },
        );

        let (depth_texture, depth_texture_view) = create_depth_texture(&device, resolution);
        let (color_texture, color_texture_view) =
            create_color_texture(&device, resolution, surface_format);

        let image_atlases = ImageAtlases::new(&device);
        let font_atlas = FontAtlas::new(&device);

        let pipeline_handles = Pipeline::load_assets(assets);
        let pipeline = Pipeline::new(
            &device,
            assets,
            &image_atlases,
            &font_atlas,
            surface_format,
            pipeline_handles,
        )
        .await?;

        Ok(Self {
            device,
            queue,
            surface,
            surface_format,
            depth_texture,
            depth_texture_view,
            color_texture,
            color_texture_view,
            image_atlases,
            font_atlas,
            pipeline,
            resolution,
        })
    }

    /// Maintain assets: upload new images, delete dead images, etc.
    pub fn maintain(&mut self, assets: &Assets) {
        self.image_atlases
            .maintain(&self.device, &self.queue, assets);
    }

    /// Get current resolution.
    pub fn resolution(&self) -> Vec2D<u32> {
        self.resolution
    }

    /// Resize internal frame buffers.
    pub fn resize(&mut self, new_resolution: Vec2D<u32>) {
        self.resolution = new_resolution;
        if self.resolution.eq(&Vec2D::zero()).any() {
            return;
        }

        let (depth_texture, depth_texture_view) =
            create_depth_texture(&self.device, new_resolution);
        self.depth_texture = depth_texture;
        self.depth_texture_view = depth_texture_view;
        let (color_texture, color_texture_view) =
            create_color_texture(&self.device, new_resolution, self.surface_format);
        self.color_texture = color_texture;
        self.color_texture_view = color_texture_view;
        self.surface.configure(
            &self.device,
            &wgpu::SurfaceConfiguration {
                usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
                format: self.surface_format,
                width: new_resolution.x,
                height: new_resolution.y,
                present_mode: wgpu::PresentMode::Fifo,
            },
        );
    }

    /// Process a draw list and submit commands to the GPU.
    pub fn draw(&mut self, assets: &Assets, draw_list: &DrawList) -> Result<()> {
        if self.resolution.eq(&Vec2D::zero()).any() {
            return Ok(());
        }

        self.font_atlas.cleanup();

        let mut encoder = self.device.create_command_encoder(&Default::default());
        let frame = self.surface.get_current_texture()?;
        let frame_view = frame.texture.create_view(&Default::default());

        self.pipeline.draw(
            &self.device,
            &self.queue,
            &mut encoder,
            assets,
            &self.image_atlases,
            &mut self.font_atlas,
            &self.color_texture_view,
            &frame_view,
            &self.depth_texture_view,
            self.resolution,
            draw_list,
        );

        self.queue.submit(Some(encoder.finish()));
        frame.present();

        Ok(())
    }
}

fn get_resolution(window: &Window) -> Vec2D<u32> {
    let size = window.inner_size();
    Vec2D::new(size.width, size.height)
}

fn create_depth_texture(
    device: &wgpu::Device,
    size: Vec2D<u32>,
) -> (wgpu::Texture, wgpu::TextureView) {
    let texture = device.create_texture(&wgpu::TextureDescriptor {
        label: None,
        size: wgpu::Extent3d {
            width: size.x,
            height: size.y,
            depth_or_array_layers: 1,
        },
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Depth24Plus,
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
    });

    let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
    (texture, view)
}

fn create_color_texture(
    device: &wgpu::Device,
    size: Vec2D<u32>,
    format: wgpu::TextureFormat,
) -> (wgpu::Texture, wgpu::TextureView) {
    let texture = device.create_texture(&wgpu::TextureDescriptor {
        label: None,
        size: wgpu::Extent3d {
            width: size.x,
            height: size.y,
            depth_or_array_layers: 1,
        },
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format,
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING,
    });

    let view = texture.create_view(&wgpu::TextureViewDescriptor::default());
    (texture, view)
}

fn slice_as_bytes<T>(slice: &[T]) -> &[u8] {
    unsafe {
        let ptr = slice.as_ptr() as *const u8;
        let len = slice.len() * std::mem::size_of::<T>();
        std::slice::from_raw_parts(ptr, len)
    }
}
