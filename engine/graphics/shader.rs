use std::path::Path;

use eyre::Result;

use crate::assets::{Asset, AssetDefaultLoader, AssetLoader, LoadingContext};

#[derive(Clone, Debug)]
pub struct ShaderSource {
    pub source: String,
}

impl Asset for ShaderSource {}

impl AssetDefaultLoader for ShaderSource {
    type DefaultLoader = ShaderSourceLoader;
}

pub struct ShaderSourceLoader;

#[async_trait::async_trait]
impl AssetLoader<ShaderSource> for ShaderSourceLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<ShaderSource> {
        Ok(ShaderSource {
            source: ctx.read_string(path)?,
        })
    }
}
