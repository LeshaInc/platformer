use std::ops::DerefMut;

use once_cell::sync::OnceCell;
use parking_lot::Mutex;

use super::{
    DrawEncoder, Font, HorizontalAlignment, Image, Sprite, TextLayoutSettings, TextLayouter,
    VerticalAlignment,
};
use crate::assets::{Assets, Handle};
use crate::math::{Box2D, Rgba, Rotation2D, Transform2D, Vec2D};

/// Global draw list for debugging purposes.
#[derive(Clone, Debug)]
pub struct DebugDrawList {
    font: Handle<Font>,
    image_white_1x1: Handle<Image>,
    lines: Vec<Line>,
    texts: Vec<Text>,
    transform: Transform2D,
}

#[derive(Clone, Copy, Debug)]
struct Line {
    color: Rgba,
    a: Vec2D<f32>,
    b: Vec2D<f32>,
}

#[derive(Clone, Debug)]
struct Text {
    color: Rgba,
    pos: Vec2D<f32>,
    text: String,
}

static GLOBAL: OnceCell<Mutex<DebugDrawList>> = OnceCell::new();

impl DebugDrawList {
    /// Create a new debug draw list.
    pub fn new(font: Handle<Font>, image_white_1x1: Handle<Image>) -> DebugDrawList {
        DebugDrawList {
            font,
            image_white_1x1,
            lines: Vec::new(),
            texts: Vec::new(),
            transform: Transform2D::identity(),
        }
    }

    /// Register this draw list as global. Returns `true` on success.
    pub fn register_global(self) -> bool {
        GLOBAL.set(Mutex::new(self)).is_ok()
    }

    /// Try get the globl draw list.
    pub fn try_get() -> Option<impl DerefMut<Target = DebugDrawList>> {
        Some(GLOBAL.get()?.lock())
    }

    /// Get the globl draw list.
    pub fn get() -> impl DerefMut<Target = DebugDrawList> {
        GLOBAL.get().expect("Debug draw list not registered").lock()
    }

    /// Draw a line.
    pub fn line(&mut self, color: Rgba, a: Vec2D<f32>, b: Vec2D<f32>) {
        self.lines.push(Line { color, a, b });
    }

    /// Draw a vector arrow.
    pub fn vector(&mut self, color: Rgba, pos: Vec2D<f32>, vec: Vec2D<f32>) {
        self.arrow(color, pos, pos + vec)
    }

    /// Draw a rectangle.
    pub fn rect(&mut self, color: Rgba, rect: Box2D<f32>) {
        let extent = rect.extents();
        self.line(color, rect.min, rect.min + extent.set_y(0.0));
        self.line(color, rect.min, rect.min + extent.set_x(0.0));
        self.line(color, rect.max, rect.max - extent.set_y(0.0));
        self.line(color, rect.max, rect.max - extent.set_x(0.0));
    }

    /// Draw an arrow.
    pub fn arrow(&mut self, color: Rgba, a: Vec2D<f32>, b: Vec2D<f32>) {
        let dir = (b - a).normalize();

        let dir_rot = Rotation2D::new(dir.x, dir.y);
        let spread = Rotation2D::degrees(7.5);

        let wing = dir_rot.rotate(Vec2D::unit_x());
        let wing0 = spread.rotate(wing);
        let wing1 = spread.conj().rotate(wing);

        self.line(color, a, b);
        self.line(color, b, b - wing0 * 7.0);
        self.line(color, b, b - wing1 * 7.0);
    }

    /// Draw text.
    pub fn text<S: Into<String>>(&mut self, color: Rgba, pos: Vec2D<f32>, text: S) {
        self.texts.push(Text {
            color,
            pos,
            text: text.into(),
        })
    }

    /// Set global transformation.
    pub fn set_transform(&mut self, transform: Transform2D) {
        self.transform = transform;
    }

    /// Encode draw commands.
    pub fn encode(&mut self, assets: &Assets, encoder: &mut DrawEncoder) {
        self.encode_lines(encoder);
        self.encode_texts(assets, encoder);
    }

    fn encode_lines(&mut self, encoder: &mut DrawEncoder) {
        for line in self.lines.drain(..) {
            let dir = (line.b - line.a).normalize();
            let mag = (line.b - line.a).mag();
            let rotation = Rotation2D::new(dir.x, dir.y);
            let mut transform = Transform2D::rotation(rotation);
            transform.post_translate(line.a);
            transform = self.transform * transform;
            let image = self.image_white_1x1.id();
            let sprite = Sprite::new(image, Vec2D::new(0.0, -0.5))
                .with_size(Vec2D::new(mag, 1.0))
                .with_color(line.color);
            encoder.push_transform(transform);
            encoder.draw_sprite(sprite);
            encoder.pop_transform();
        }
    }

    fn encode_texts(&mut self, assets: &Assets, encoder: &mut DrawEncoder) {
        let fonts = assets.storage();
        let mut layouter = TextLayouter::new(&fonts);
        for text in self.texts.drain(..) {
            layouter.reset(&TextLayoutSettings {
                bounds: Vec2D::new(None, None),
                vert_alignment: VerticalAlignment::Center,
                horiz_alignment: HorizontalAlignment::Center,
                fallback_font_id: None,
                line_height: 1.0,
            });

            let outline_color = if text.color.is_dark() {
                Some(Rgba::BLACK)
            } else {
                Some(Rgba::WHITE)
            };

            layouter.append(self.font.id(), 14.0, text.color, outline_color, &text.text);
            layouter.encode(encoder, text.pos);
        }
    }
}
