use super::{Font, Image, NinePatchImage};
use crate::assets::Id;
use crate::math::{Box2D, Rgba, Transform2D, Vec2D};

/// List of draw commands.
#[derive(Clone, Debug, Default)]
pub struct DrawList {
    commands: Vec<DrawCommand>,
}

impl DrawList {
    /// Create an empty draw list.
    pub fn new() -> DrawList {
        Default::default()
    }

    /// Iterate through commands.
    pub fn iter(&self) -> impl Iterator<Item = &DrawCommand> + '_ {
        self.commands.iter()
    }

    /// Clear the list.
    pub fn clear(&mut self) {
        self.commands.clear();
    }

    /// Append commands from another draw list, leaving it empty.
    pub fn append(&mut self, other: &mut DrawList) {
        self.commands.append(&mut other.commands);
    }
}

/// Draw command
#[derive(Clone, Debug)]
pub enum DrawCommand {
    /// Push scissor rectangle.
    PushScissor(Box2D<u32>),
    /// Pop scissor rectangle.
    PopScissor,
    /// Push transform.
    PushTransform(Transform2D),
    /// Pop transform.
    PopTransform,
    /// Draw a sprite.
    DrawSprite(Sprite),
    /// Draw a nine-patch sprite.
    DrawNinePatchSprite(NinePatchSprite),
    /// Draw a glyph.
    DrawGlyph(Glyph),
    /// Draw lighting overlay. Allowed only once per draw list.
    DrawLighting(Lighting),
}

/// Sprite: an image drawn on the screen.
#[derive(Clone, Copy, Debug)]
pub struct Sprite {
    /// ID of the image.
    pub image: Id<Image>,
    /// ID of mask image.
    pub mask: Option<Id<Image>>,
    /// Position.
    pub pos: Vec2D<f32>,
    /// Size. If none, will be calculated from the image size.
    pub size: Option<Vec2D<f32>>,
    /// Scale. `[1.0, 1.0]` by default.
    pub scale: Vec2D<f32>,
    /// Color. White by default.
    pub color: Rgba,
}

impl Sprite {
    /// Create a new sprite.
    pub fn new(image: Id<Image>, pos: Vec2D<f32>) -> Sprite {
        Sprite {
            image,
            mask: None,
            pos,
            size: None,
            scale: Vec2D::splat(1.0),
            color: Rgba::WHITE,
        }
    }

    /// Set the mask image.
    pub fn with_mask(mut self, mask: Id<Image>) -> Sprite {
        self.mask = Some(mask);
        self
    }

    /// Set the size.
    pub fn with_size(mut self, size: Vec2D<f32>) -> Sprite {
        self.size = Some(size);
        self
    }

    /// Set the scale.
    pub fn with_scale(mut self, scale: Vec2D<f32>) -> Sprite {
        self.scale = scale;
        self
    }

    /// Set the color.
    pub fn with_color(mut self, color: Rgba) -> Sprite {
        self.color = color;
        self
    }
}

/// Nine-patch sprite: a nine-patch image drawn on the screen.
#[derive(Clone, Copy, Debug)]
pub struct NinePatchSprite {
    /// Nine-patch image.
    pub image: Id<NinePatchImage>,
    /// Rectangle to fill.
    pub rect: Box2D<f32>,
    /// Color. White by default.
    pub color: Rgba,
    /// Scale of the image (not the rectangle). 1.0 by default.
    pub scale: f32,
}

impl NinePatchSprite {
    /// Create a new nine-patch sprite.
    pub fn new(image: Id<NinePatchImage>, rect: Box2D<f32>) -> NinePatchSprite {
        NinePatchSprite {
            image,
            rect,
            color: Rgba::WHITE,
            scale: 1.0,
        }
    }

    /// Set the color
    pub fn with_color(mut self, color: Rgba) -> NinePatchSprite {
        self.color = color;
        self
    }

    /// Set the scale
    pub fn with_scale(mut self, scale: f32) -> NinePatchSprite {
        self.scale = scale;
        self
    }
}

/// Glyph drawn on the screen.
#[derive(Clone, Copy, Debug)]
pub struct Glyph {
    /// ID of the font.
    pub font: Id<Font>,
    /// Rectangle to fill.
    pub rect: Box2D<f32>,
    /// Font scale.
    pub scale: f32,
    /// Glyph index in the font.
    pub glyph_index: u16,
    /// Color.
    pub color: Rgba,
}

/// Lighting overlay.
#[derive(Clone, Debug)]
pub struct Lighting {
    /// Top-left corner of the overlay.
    pub pos: Vec2D<f32>,
    /// Size.
    pub size: Vec2D<f32>,
    /// Texture size.
    pub texture_size: Vec2D<u32>,
    /// Texture data.
    pub data: Vec<Rgba<u8>>,
}

/// Draw command encoder
pub struct DrawEncoder<'a> {
    list: &'a mut DrawList,
}

impl DrawEncoder<'_> {
    /// Create a new encoder
    pub fn new(list: &mut DrawList) -> DrawEncoder<'_> {
        DrawEncoder { list }
    }

    /// Push scissor rectangle
    pub fn push_scissor(&mut self, scissor: Box2D<u32>) {
        self.list.commands.push(DrawCommand::PushScissor(scissor));
    }

    /// Pop scissor rectangle
    pub fn pop_scissor(&mut self) {
        self.list.commands.push(DrawCommand::PopScissor);
    }

    /// Push transform.
    pub fn push_transform(&mut self, transform: Transform2D) {
        self.list
            .commands
            .push(DrawCommand::PushTransform(transform));
    }

    /// Pop transform.
    pub fn pop_transform(&mut self) {
        self.list.commands.push(DrawCommand::PopTransform);
    }

    /// Draw a sprite.
    pub fn draw_sprite(&mut self, sprite: Sprite) {
        self.list.commands.push(DrawCommand::DrawSprite(sprite));
    }

    /// Draw a nine-patch sprite.
    pub fn draw_nine_patch_sprite(&mut self, sprite: NinePatchSprite) {
        self.list
            .commands
            .push(DrawCommand::DrawNinePatchSprite(sprite));
    }

    /// Draw a glyph. If you want to draw long text, check out
    /// [`TextLayouter`](super::TextLayouter).
    pub fn draw_glyph(&mut self, glyph: Glyph) {
        self.list.commands.push(DrawCommand::DrawGlyph(glyph));
    }

    /// Draw lighting overlay.
    pub fn draw_lighting(&mut self, lighting: Lighting) {
        self.list.commands.push(DrawCommand::DrawLighting(lighting));
    }
}
