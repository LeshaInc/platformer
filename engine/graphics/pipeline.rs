use std::num::NonZeroU32;
use std::ops::Range;

use ahash::{AHashMap, AHashSet};
use eyre::Result;
use wgpu::util::DeviceExt;

use super::atlas::{FontAtlas, GlyphKey, ImageAtlasId, ImageAtlases};
use super::dynamic_buffer::DynamicBuffer;
use super::shader::ShaderSource;
use super::{
    slice_as_bytes, DrawCommand, DrawList, Glyph, Image, Lighting, NinePatchImage, NinePatchSprite,
    Sprite,
};
use crate::assets::{AssetStorage, Assets, Handle, Id};
use crate::math::{Box2D, Transform2D, Vec2D};

#[derive(Debug)]
pub struct Pipeline {
    sprite_pipeline: wgpu::RenderPipeline,
    glyph_pipeline: wgpu::RenderPipeline,
    lighting_pipeline: wgpu::RenderPipeline,
    linear_sampler: wgpu::Sampler,
    lighting_bind_group_layout: wgpu::BindGroupLayout,
    bind_groups: AHashMap<(usize, usize), wgpu::BindGroup>,
    lighting_data: Option<LightingData>,
    quad_buffer: wgpu::Buffer,
    instance_buffer: DynamicBuffer,
    draw_data: DrawData,
}

#[derive(Debug, Default)]
struct DrawData {
    scissor_stack: Vec<Box2D<u32>>,
    transform_stack: Vec<Transform2D>,
    per_group_data: AHashMap<(usize, usize), PerAtlasData>,
    batches: Vec<Batch>,
    glyphs_to_upload: AHashSet<GlyphKey>,
    projection_matrix: Transform2D,
    resolution: Vec2D<u32>,
    z_order: f32,
    z_step: f32,
    num_instances: u32,
    last_instance_was_sprite: bool,
}

#[derive(Debug)]
enum Batch {
    Instance {
        atlases: (usize, usize),
        scissor: Box2D<u32>,
        start_instance: u32,
        end_instance: u32,
    },
    Lighting,
}

#[derive(Debug, Default)]
struct PerAtlasData {
    instances: Vec<InstanceVertex>,
    num_flushed: u32,
    first_instance: u32,
}

#[derive(Debug)]
struct LightingData {
    texture_size: Vec2D<u32>,
    texture: wgpu::Texture,
    texture_view: wgpu::TextureView,
    bind_group: wgpu::BindGroup,
    vbuf: Option<wgpu::Buffer>,
}

pub struct PipelineAssetHandles {
    main_shader_source: Handle<ShaderSource>,
    lighting_shader_source: Handle<ShaderSource>,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct QuadVertex {
    pos: [f32; 2],
}

impl QuadVertex {
    const VERTICES: &'static [QuadVertex] = &[
        QuadVertex { pos: [0.0, 0.0] },
        QuadVertex { pos: [1.0, 0.0] },
        QuadVertex { pos: [0.0, 1.0] },
        QuadVertex { pos: [0.0, 1.0] },
        QuadVertex { pos: [1.0, 0.0] },
        QuadVertex { pos: [1.0, 1.0] },
    ];

    const LAYOUT: wgpu::VertexBufferLayout<'static> = wgpu::VertexBufferLayout {
        array_stride: 8,
        step_mode: wgpu::VertexStepMode::Vertex,
        attributes: &[wgpu::VertexAttribute {
            format: wgpu::VertexFormat::Float32x2,
            offset: 0,
            shader_location: 0,
        }],
    };
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct InstanceVertex {
    z_order: f32,
    transform_row0: [f32; 3],
    transform_row1: [f32; 3],
    uv_rect0: [f32; 4],
    uv_rect1: [f32; 4],
    color: [f32; 4],
}

impl InstanceVertex {
    const SIZE: u64 = 76;

    const LAYOUT: wgpu::VertexBufferLayout<'static> = wgpu::VertexBufferLayout {
        array_stride: 76,
        step_mode: wgpu::VertexStepMode::Instance,
        attributes: &[
            wgpu::VertexAttribute {
                format: wgpu::VertexFormat::Float32,
                offset: 0,
                shader_location: 1,
            },
            wgpu::VertexAttribute {
                format: wgpu::VertexFormat::Float32x3,
                offset: 4,
                shader_location: 2,
            },
            wgpu::VertexAttribute {
                format: wgpu::VertexFormat::Float32x3,
                offset: 16,
                shader_location: 3,
            },
            wgpu::VertexAttribute {
                format: wgpu::VertexFormat::Float32x4,
                offset: 28,
                shader_location: 4,
            },
            wgpu::VertexAttribute {
                format: wgpu::VertexFormat::Float32x4,
                offset: 44,
                shader_location: 5,
            },
            wgpu::VertexAttribute {
                format: wgpu::VertexFormat::Float32x4,
                offset: 60,
                shader_location: 6,
            },
        ],
    };
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Default)]
struct LightingVertex {
    pos: [f32; 2],
    uv: [f32; 2],
}

impl LightingVertex {
    const LAYOUT: wgpu::VertexBufferLayout<'static> = wgpu::VertexBufferLayout {
        array_stride: 16,
        step_mode: wgpu::VertexStepMode::Vertex,
        attributes: &[
            wgpu::VertexAttribute {
                format: wgpu::VertexFormat::Float32x2,
                offset: 0,
                shader_location: 0,
            },
            wgpu::VertexAttribute {
                format: wgpu::VertexFormat::Float32x2,
                offset: 8,
                shader_location: 1,
            },
        ],
    };
}

impl Pipeline {
    pub fn load_assets(assets: &Assets) -> PipelineAssetHandles {
        PipelineAssetHandles {
            main_shader_source: assets.load("shaders/main.wgsl"),
            lighting_shader_source: assets.load("shaders/lighting.wgsl"),
        }
    }

    pub async fn new(
        device: &wgpu::Device,
        assets: &Assets,
        image_atlases: &ImageAtlases,
        font_atlas: &FontAtlas,
        sc_format: wgpu::TextureFormat,
        handles: PipelineAssetHandles,
    ) -> Result<Pipeline> {
        let bind_group_layout = create_bind_group_layout(device);
        let sampler = create_sampler(device);
        let linear_sampler = create_linear_sampler(device);
        let bind_groups = create_bind_groups(
            device,
            &bind_group_layout,
            &sampler,
            image_atlases,
            font_atlas,
        );

        let shader = {
            let shader_source = assets.get_wait(&handles.main_shader_source).await?;
            create_shader_module(device, &shader_source)
        };

        let lighting_shader = {
            let shader_source = assets.get_wait(&handles.lighting_shader_source).await?;
            create_shader_module(device, &shader_source)
        };

        let pipeline_layout = create_pipeline_layout(device, &bind_group_layout);
        let sprite_pipeline =
            create_render_pipeline(device, &pipeline_layout, &shader, sc_format, "frag_sprite");
        let glyph_pipeline =
            create_render_pipeline(device, &pipeline_layout, &shader, sc_format, "frag_glyph");
        let lighting_bgl = create_lighting_bind_group_layout(device);
        let lighting_pipeline =
            create_lighting_pipeline(device, &lighting_shader, sc_format, &lighting_bgl);

        let quad_buffer = create_quad_buffer(device);
        let instance_buffer = DynamicBuffer::new(
            device,
            wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
            1024 * 1024,
        );

        let draw_data = DrawData::default();

        Ok(Pipeline {
            sprite_pipeline,
            glyph_pipeline,
            lighting_pipeline,
            linear_sampler,
            lighting_bind_group_layout: lighting_bgl,
            bind_groups,
            lighting_data: None,
            quad_buffer,
            instance_buffer,
            draw_data,
        })
    }

    pub fn draw(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        encoder: &mut wgpu::CommandEncoder,
        assets: &Assets,
        image_atlases: &ImageAtlases,
        font_atlas: &mut FontAtlas,
        color0_view: &wgpu::TextureView,
        color1_view: &wgpu::TextureView,
        depth_view: &wgpu::TextureView,
        resolution: Vec2D<u32>,
        list: &DrawList,
    ) {
        let nine_patches = assets.storage();
        self.init_draw(resolution, list);
        self.upload_glyphs(device, queue, assets, font_atlas, list);
        self.handle_commands(
            device,
            queue,
            &nine_patches,
            image_atlases,
            font_atlas,
            list,
            color0_view,
        );
        self.upload_instances(device, queue);
        self.encode_draw(encoder, color0_view, color1_view, depth_view);
    }

    fn init_draw(&mut self, resolution: Vec2D<u32>, list: &DrawList) {
        self.draw_data.scissor_stack.clear();
        self.draw_data.transform_stack.clear();
        self.draw_data.projection_matrix = Transform2D::translation(Vec2D::new(-1.0, 1.0))
            * Transform2D::scaling(resolution.map(|x| 2.0 / x as f32).map_y(|y| -y));
        self.draw_data.resolution = resolution;
        self.draw_data.z_order = 1.0;
        self.draw_data.z_step = 1.0 / self.count_num_instances(list).next_power_of_two() as f32;
        self.draw_data.num_instances = 0;

        for (_, group_data) in &mut self.draw_data.per_group_data {
            group_data.instances.clear();
            group_data.num_flushed = 0;
        }
    }

    fn count_num_instances(&self, list: &DrawList) -> u32 {
        list.iter()
            .map(|cmd| match cmd {
                DrawCommand::DrawSprite(_) => 1,
                DrawCommand::DrawNinePatchSprite(_) => 9,
                DrawCommand::DrawGlyph(_) => 1,
                _ => 0,
            })
            .sum()
    }

    fn upload_glyphs(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        assets: &Assets,
        atlas: &mut FontAtlas,
        list: &DrawList,
    ) {
        self.draw_data.glyphs_to_upload.clear();

        for command in list.iter() {
            let glyph = match command {
                DrawCommand::DrawGlyph(glyph) => glyph,
                _ => continue,
            };

            let glyph_key = GlyphKey {
                font: glyph.font,
                glyph_index: glyph.glyph_index,
                scale: glyph.scale as u32,
            };

            if !atlas.contains_glyph(&glyph_key) {
                self.draw_data.glyphs_to_upload.insert(glyph_key);
            }

            atlas.keepalive_glyph(&glyph_key);
        }

        atlas.rasterize(device, queue, assets, &self.draw_data.glyphs_to_upload);
    }

    fn handle_commands(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        nine_patches: &AssetStorage<NinePatchImage>,
        image_atlases: &ImageAtlases,
        font_atlas: &mut FontAtlas,
        list: &DrawList,
        color_texture_view: &wgpu::TextureView,
    ) {
        for command in list.iter() {
            match command {
                DrawCommand::PushScissor(rect) => self.cmd_push_scissor(rect),
                DrawCommand::PopScissor => self.cmd_pop_scissor(),
                DrawCommand::PushTransform(transform) => self.cmd_push_transform(transform),
                DrawCommand::PopTransform => self.cmd_pop_transform(),
                DrawCommand::DrawSprite(sprite) => {
                    self.cmd_draw_sprite(image_atlases, sprite);
                }
                DrawCommand::DrawNinePatchSprite(sprite) => {
                    self.cmd_draw_nine_patch_sprite(nine_patches, image_atlases, sprite)
                }
                DrawCommand::DrawGlyph(glyph) => self.cmd_draw_glyph(font_atlas, glyph),
                DrawCommand::DrawLighting(lighting) => {
                    self.cmd_draw_lighting(device, queue, lighting, color_texture_view)
                }
            }
        }
    }

    fn get_scissor(&self) -> Box2D<u32> {
        let last_scissor = self.draw_data.scissor_stack.last().copied();
        last_scissor.unwrap_or(Box2D::new(Vec2D::new(0, 0), self.draw_data.resolution))
    }

    fn cmd_push_scissor(&mut self, rect: &Box2D<u32>) {
        let prev = self.get_scissor();
        let next = prev.intersection(*rect);
        if prev != next {
            self.flush_batches();
        }
        self.draw_data.scissor_stack.push(next);
    }

    fn cmd_pop_scissor(&mut self) {
        let prev = self.draw_data.scissor_stack.last();
        let next = self.draw_data.scissor_stack.iter().nth_back(1);
        if prev != next {
            self.flush_batches();
        }
        self.draw_data.scissor_stack.pop();
    }

    fn cmd_push_transform(&mut self, transform: &Transform2D) {
        self.draw_data.transform_stack.push(
            self.draw_data
                .transform_stack
                .last()
                .map(|t| *transform * *t)
                .unwrap_or(*transform),
        );
    }

    fn cmd_pop_transform(&mut self) {
        self.draw_data.transform_stack.pop();
    }

    fn get_transform(&self) -> Transform2D {
        let opt = self.draw_data.transform_stack.last().copied();
        opt.unwrap_or(Transform2D::identity())
    }

    fn get_image_size(&self, atlases: &ImageAtlases, image: Id<Image>) -> Vec2D<f32> {
        let atlas_id = atlases.atlas_id_for_image(image).unwrap_or(ImageAtlasId(0));
        let atlas = atlases.atlas_by_id(atlas_id);
        atlas
            .get_rect(&image)
            .map(|r| r.extents().cast::<f32>())
            .unwrap_or(Vec2D::zero())
    }

    fn cmd_draw_sprite(&mut self, image_atlases: &ImageAtlases, sprite: &Sprite) -> Vec2D<f32> {
        if !self.draw_data.last_instance_was_sprite {
            self.flush_batches();
        }

        let atlas0_id = image_atlases
            .atlas_id_for_image(sprite.image)
            .unwrap_or(ImageAtlasId(0));
        let atlas0 = image_atlases.atlas_by_id(atlas0_id);

        let rect = atlas0.get_rect(&sprite.image).unwrap_or_else(|| {
            log::warn!("No such image: {:?}", sprite.image);
            Box2D::zero()
        });
        let uv_rect0 = create_uv_rect(&rect, atlas0.size());

        let (atlas1_id, uv_rect1) = match sprite.mask {
            Some(mask) => {
                let atlas1_id = image_atlases
                    .atlas_id_for_image(mask)
                    .unwrap_or(ImageAtlasId(0));
                let atlas1 = image_atlases.atlas_by_id(atlas1_id);
                let rect = atlas1.get_rect(&mask).unwrap_or_else(|| {
                    log::warn!("No such image: {:?}", sprite.image);
                    Box2D::zero()
                });
                let uv_rect1 = create_uv_rect(&rect, atlas1.size());
                (atlas1_id, uv_rect1)
            }
            None => (atlas0_id, [0.0; 4]),
        };

        let sprite_size = sprite.size.unwrap_or_else(|| rect.extents().cast());
        let model_matrix = Transform2D::translation(sprite.pos)
            * Transform2D::scaling(sprite_size.elementwise_mul(sprite.scale));

        let transform = self.get_transform();
        let transform = self.draw_data.projection_matrix * transform * model_matrix;
        let instance = InstanceVertex {
            z_order: self.draw_data.z_order,
            transform_row0: transform.row0,
            transform_row1: transform.row1,
            uv_rect0,
            uv_rect1,
            color: sprite.color.into(),
        };

        let atlas0 = atlas0_id.0 + 1;
        let atlas1 = atlas1_id.0 + 1;
        let key = (atlas0, atlas1);
        let group_data = self.draw_data.per_group_data.entry(key).or_default();
        group_data.instances.push(instance);
        self.draw_data.z_order -= self.draw_data.z_step;
        self.draw_data.num_instances += 1;
        self.draw_data.last_instance_was_sprite = true;

        sprite_size
    }

    fn cmd_draw_nine_patch_sprite(
        &mut self,
        nine_patches: &AssetStorage<NinePatchImage>,
        image_atlases: &ImageAtlases,
        sprite: &NinePatchSprite,
    ) {
        let image = match nine_patches.get_by_id(sprite.image) {
            Ok(v) => v,
            Err(_) => {
                log::warn!("No such 9-patch image: {:?}", sprite.image);
                return;
            }
        };

        let top_left_size = self.get_image_size(image_atlases, image.top_left.id()) * sprite.scale;
        let bottom_right_size =
            self.get_image_size(image_atlases, image.bottom_right.id()) * sprite.scale;

        let inner_width = sprite.rect.width() - top_left_size.x - bottom_right_size.x;
        let inner_height = sprite.rect.height() - top_left_size.y - bottom_right_size.y;

        // top left
        let patch = Sprite::new(image.top_left.id(), sprite.rect.min)
            .with_color(sprite.color)
            .with_size(top_left_size);
        self.cmd_draw_sprite(image_atlases, &patch);

        // left
        let pos = sprite.rect.min + Vec2D::new(0.0, top_left_size.y);
        let patch = Sprite::new(image.left.id(), pos)
            .with_size(Vec2D::new(top_left_size.x, inner_height))
            .with_color(sprite.color);
        self.cmd_draw_sprite(image_atlases, &patch);

        // bottom left
        let pos = sprite.rect.min + Vec2D::new(0.0, sprite.rect.height() - bottom_right_size.y);
        let patch = Sprite::new(image.bottom_left.id(), pos)
            .with_size(Vec2D::new(top_left_size.x, bottom_right_size.y))
            .with_color(sprite.color);
        self.cmd_draw_sprite(image_atlases, &patch);

        let sx = sprite.rect.width() - bottom_right_size.x;

        // top right
        let pos = sprite.rect.min + Vec2D::new(sx, 0.0);
        let patch = Sprite::new(image.top_right.id(), pos)
            .with_color(sprite.color)
            .with_size(top_left_size);
        self.cmd_draw_sprite(image_atlases, &patch);

        // right
        let pos = sprite.rect.min + Vec2D::new(sx, top_left_size.y);
        let patch = Sprite::new(image.right.id(), pos)
            .with_size(Vec2D::new(top_left_size.x, inner_height))
            .with_color(sprite.color);
        self.cmd_draw_sprite(image_atlases, &patch);

        // bottom right
        let pos = sprite.rect.min + Vec2D::new(sx, sprite.rect.height() - bottom_right_size.y);
        let patch = Sprite::new(image.bottom_right.id(), pos)
            .with_size(Vec2D::new(top_left_size.x, bottom_right_size.y))
            .with_color(sprite.color);
        self.cmd_draw_sprite(image_atlases, &patch);

        // top
        let pos = sprite.rect.min + Vec2D::new(top_left_size.x, 0.0);
        let patch = Sprite::new(image.top.id(), pos)
            .with_size(Vec2D::new(inner_width, top_left_size.y))
            .with_color(sprite.color);
        self.cmd_draw_sprite(image_atlases, &patch);

        // bottom
        let pos = sprite.rect.min
            + Vec2D::new(top_left_size.x, sprite.rect.height() - bottom_right_size.y);
        let patch = Sprite::new(image.bottom.id(), pos)
            .with_size(Vec2D::new(inner_width, bottom_right_size.y))
            .with_color(sprite.color);
        self.cmd_draw_sprite(image_atlases, &patch);

        // center
        let pos = sprite.rect.min + top_left_size;
        let patch = Sprite::new(image.center.id(), pos)
            .with_size(Vec2D::new(inner_width, inner_height))
            .with_color(sprite.color);
        self.cmd_draw_sprite(image_atlases, &patch);
    }

    fn cmd_draw_glyph(&mut self, atlas: &FontAtlas, glyph: &Glyph) {
        if self.draw_data.last_instance_was_sprite {
            self.flush_batches();
        }

        let key = GlyphKey {
            font: glyph.font,
            glyph_index: glyph.glyph_index,
            scale: glyph.scale as u32,
        };

        let rect = match atlas.get_rect(&key) {
            Some(v) => v,
            None => return,
        };
        let uv_rect0 = create_uv_rect(&rect, atlas.size());

        let transform = self.get_transform();
        let model_matrix =
            Transform2D::translation(glyph.rect.min) * Transform2D::scaling(glyph.rect.extents());
        let transform = self.draw_data.projection_matrix * transform * model_matrix;
        let instance = InstanceVertex {
            z_order: self.draw_data.z_order,
            transform_row0: transform.row0,
            transform_row1: transform.row1,
            uv_rect0,
            uv_rect1: [0.0; 4],
            color: glyph.color.into(),
        };

        let per_group = self.draw_data.per_group_data.entry((0, 0)).or_default();
        per_group.instances.push(instance);
        self.draw_data.z_order -= self.draw_data.z_step;
        self.draw_data.num_instances += 1;
        self.draw_data.last_instance_was_sprite = false;
    }

    fn cmd_draw_lighting(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        lighting: &Lighting,
        color_texture_view: &wgpu::TextureView,
    ) {
        self.flush_batches();

        let bind_group_layout = &self.lighting_bind_group_layout;
        let sampler = &self.linear_sampler;
        let mut data = self
            .lighting_data
            .take()
            .filter(|data| data.texture_size == lighting.texture_size)
            .unwrap_or_else(|| {
                let texture_size = lighting.texture_size;
                let texture = create_lighting_texture(device, texture_size);
                let texture_view = texture.create_view(&Default::default());
                let bind_group = create_lighting_bind_group(
                    device,
                    bind_group_layout,
                    sampler,
                    &texture_view,
                    color_texture_view,
                );
                LightingData {
                    texture_size,
                    texture,
                    texture_view,
                    bind_group,
                    vbuf: None,
                }
            });

        let dst = wgpu::ImageCopyTexture {
            texture: &data.texture,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
            aspect: wgpu::TextureAspect::All,
        };
        let tex_data = slice_as_bytes(&lighting.data);
        let layout = wgpu::ImageDataLayout {
            offset: 0,
            bytes_per_row: NonZeroU32::new(tex_data.len() as u32 / data.texture_size.y),
            rows_per_image: None,
        };
        let extent = wgpu::Extent3d {
            width: data.texture_size.x,
            height: data.texture_size.y,
            depth_or_array_layers: 1,
        };
        queue.write_texture(dst, tex_data, layout, extent);

        let transform = self.get_transform();
        let model_matrix = Transform2D::translation(lighting.pos.cast())
            * Transform2D::scaling(lighting.size.cast());
        let matrix = self.draw_data.projection_matrix * transform * model_matrix;

        let mut vertices = [LightingVertex::default(); 6];
        for (i, vertex) in QuadVertex::VERTICES.iter().enumerate() {
            vertices[i].pos = matrix.transform_point(vertex.pos.into()).into();
            vertices[i].uv = vertex.pos;
        }

        data.vbuf = Some(
            device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: None,
                contents: slice_as_bytes(&vertices),
                usage: wgpu::BufferUsages::VERTEX,
            }),
        );

        self.lighting_data = Some(data);
        self.draw_data.batches.push(Batch::Lighting);
    }

    fn upload_instances(&mut self, device: &wgpu::Device, queue: &wgpu::Queue) {
        self.instance_buffer.grow(
            device,
            u64::from(self.draw_data.num_instances) * InstanceVertex::SIZE,
        );

        let mut instance_offset = 0;
        let mut byte_offset = 0;
        for (_, data) in self.draw_data.per_group_data.iter_mut() {
            data.first_instance = instance_offset;
            let bytes = slice_as_bytes(data.instances.as_slice());
            queue.write_buffer(self.instance_buffer.buffer(), byte_offset, bytes);
            instance_offset += data.instances.len() as u32;
            byte_offset += bytes.len() as u64;
        }
    }

    fn flush_batches(&mut self) {
        for (&atlases, data) in self.draw_data.per_group_data.iter_mut() {
            if data.num_flushed == data.instances.len() as u32 {
                continue;
            }

            let last_scissor = self.draw_data.scissor_stack.last().copied();
            let scissor =
                last_scissor.unwrap_or(Box2D::new(Vec2D::new(0, 0), self.draw_data.resolution));
            self.draw_data.batches.push(Batch::Instance {
                atlases,
                scissor,
                start_instance: data.num_flushed,
                end_instance: data.instances.len() as u32,
            });
            data.num_flushed = data.instances.len() as u32;
        }
    }

    fn encode_draw(
        &mut self,
        encoder: &mut wgpu::CommandEncoder,
        color0_view: &wgpu::TextureView,
        color1_view: &wgpu::TextureView,
        depth_view: &wgpu::TextureView,
    ) {
        self.flush_batches();

        let ops_clear_store = wgpu::Operations {
            load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
            store: true,
        };
        let ops_load_store = wgpu::Operations {
            load: wgpu::LoadOp::Load,
            store: true,
        };
        let ops_load_drop_ds = wgpu::Operations {
            load: wgpu::LoadOp::Load,
            store: false,
        };
        let ops_clear_store_ds = wgpu::Operations {
            load: wgpu::LoadOp::Clear(1.0),
            store: true,
        };
        let ops_clear_drop_ds = wgpu::Operations {
            load: wgpu::LoadOp::Clear(1.0),
            store: false,
        };

        let rpass0_desc = wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[wgpu::RenderPassColorAttachment {
                view: color0_view,
                resolve_target: None,
                ops: ops_clear_store,
            }],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: depth_view,
                depth_ops: Some(ops_clear_store_ds),
                stencil_ops: None,
            }),
        };
        let rpass0_alt_desc = wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[wgpu::RenderPassColorAttachment {
                view: color1_view,
                resolve_target: None,
                ops: ops_clear_store,
            }],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: depth_view,
                depth_ops: Some(ops_clear_drop_ds),
                stencil_ops: None,
            }),
        };
        let rpass1_desc = wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[wgpu::RenderPassColorAttachment {
                view: color1_view,
                resolve_target: None,
                ops: ops_clear_store,
            }],
            depth_stencil_attachment: None,
        };
        let rpass2_desc = wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[wgpu::RenderPassColorAttachment {
                view: color1_view,
                resolve_target: None,
                ops: ops_load_store,
            }],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: depth_view,
                depth_ops: Some(ops_load_drop_ds),
                stencil_ops: None,
            }),
        };

        let mut it = self.draw_data.batches.iter();
        let idx_of_lighting = it.position(|v| matches!(v, Batch::Lighting));

        if let Some(idx) = idx_of_lighting {
            let mut rpass = encoder.begin_render_pass(&rpass0_desc);
            self.encode_draw_instances(&mut rpass, 0..idx);
            drop(rpass);

            let mut rpass = encoder.begin_render_pass(&rpass1_desc);
            self.encode_draw_lighting(&mut rpass);
            drop(rpass);

            let mut rpass = encoder.begin_render_pass(&rpass2_desc);
            self.encode_draw_instances(&mut rpass, idx + 1..self.draw_data.batches.len());
            drop(rpass);
        } else {
            let mut rpass = encoder.begin_render_pass(&rpass0_alt_desc);
            self.encode_draw_instances(&mut rpass, 0..self.draw_data.batches.len());
            drop(rpass);
        }

        self.draw_data.batches.clear();
    }

    fn encode_draw_instances<'s>(
        &'s mut self,
        rpass: &mut wgpu::RenderPass<'s>,
        range: Range<usize>,
    ) {
        for batch in &self.draw_data.batches[range] {
            let (atlases, scissor, start_instance, end_instance) = match *batch {
                Batch::Instance {
                    atlases,
                    scissor,
                    start_instance,
                    end_instance,
                } => (atlases, scissor, start_instance, end_instance),
                _ => continue,
            };

            let rect = scissor;
            if rect.area() == 0 {
                continue;
            }

            if atlases.0 == 0 {
                rpass.set_pipeline(&self.glyph_pipeline);
            } else {
                rpass.set_pipeline(&self.sprite_pipeline);
            }

            rpass.set_vertex_buffer(1, self.instance_buffer.buffer().slice(..));
            rpass.set_bind_group(0, &self.bind_groups[&atlases], &[]);
            rpass.set_scissor_rect(
                rect.min.x,
                rect.min.y,
                rect.width().max(1),
                rect.height().max(1),
            );

            let of = self.draw_data.per_group_data[&atlases].first_instance;
            rpass.set_vertex_buffer(0, self.quad_buffer.slice(..));
            rpass.draw(0..6, start_instance + of..end_instance + of);
        }
    }

    fn encode_draw_lighting<'s>(&'s mut self, rpass: &mut wgpu::RenderPass<'s>) {
        let data = self.lighting_data.as_ref().unwrap();
        rpass.set_pipeline(&self.lighting_pipeline);
        rpass.set_bind_group(0, &data.bind_group, &[]);
        let res = self.draw_data.resolution;
        rpass.set_scissor_rect(0, 0, res.x, res.y);
        rpass.set_vertex_buffer(0, data.vbuf.as_ref().unwrap().slice(..));
        rpass.draw(0..6, 0..1);
    }
}

fn create_bind_group_layout(device: &wgpu::Device) -> wgpu::BindGroupLayout {
    device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
        label: None,
        entries: &[
            wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Sampler {
                    filtering: true,
                    comparison: false,
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 1,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture {
                    sample_type: wgpu::TextureSampleType::Float { filterable: true },
                    view_dimension: wgpu::TextureViewDimension::D2,
                    multisampled: false,
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 2,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture {
                    sample_type: wgpu::TextureSampleType::Float { filterable: true },
                    view_dimension: wgpu::TextureViewDimension::D2,
                    multisampled: false,
                },
                count: None,
            },
        ],
    })
}

fn create_lighting_bind_group_layout(device: &wgpu::Device) -> wgpu::BindGroupLayout {
    device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
        label: None,
        entries: &[
            wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Sampler {
                    filtering: true,
                    comparison: false,
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 1,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture {
                    sample_type: wgpu::TextureSampleType::Float { filterable: true },
                    view_dimension: wgpu::TextureViewDimension::D2,
                    multisampled: false,
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 2,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture {
                    sample_type: wgpu::TextureSampleType::Float { filterable: true },
                    view_dimension: wgpu::TextureViewDimension::D2,
                    multisampled: false,
                },
                count: None,
            },
        ],
    })
}

fn create_sampler(device: &wgpu::Device) -> wgpu::Sampler {
    device.create_sampler(&wgpu::SamplerDescriptor {
        mag_filter: wgpu::FilterMode::Nearest,
        min_filter: wgpu::FilterMode::Nearest,
        ..wgpu::SamplerDescriptor::default()
    })
}

fn create_linear_sampler(device: &wgpu::Device) -> wgpu::Sampler {
    device.create_sampler(&wgpu::SamplerDescriptor {
        mag_filter: wgpu::FilterMode::Linear,
        min_filter: wgpu::FilterMode::Linear,
        ..wgpu::SamplerDescriptor::default()
    })
}

fn create_bind_groups(
    device: &wgpu::Device,
    layout: &wgpu::BindGroupLayout,
    sampler: &wgpu::Sampler,
    image_atlases: &ImageAtlases,
    font_atlas: &FontAtlas,
) -> AHashMap<(usize, usize), wgpu::BindGroup> {
    let mut map = AHashMap::new();

    let font_tex = font_atlas.texture_view();
    let bind_group = create_bind_group(device, layout, sampler, font_tex, font_tex);
    map.insert((0, 0), bind_group);

    let num_atlases = image_atlases.num_atlases();
    for atlas0 in 0..num_atlases {
        let tex0 = image_atlases
            .atlas_by_id(ImageAtlasId(atlas0))
            .texture_view();
        for atlas1 in 0..num_atlases {
            let tex1 = image_atlases
                .atlas_by_id(ImageAtlasId(atlas1))
                .texture_view();
            let bind_group = create_bind_group(device, layout, sampler, tex0, tex1);
            map.insert((atlas0 + 1, atlas1 + 1), bind_group);
        }
    }

    map
}

fn create_bind_group(
    device: &wgpu::Device,
    layout: &wgpu::BindGroupLayout,
    sampler: &wgpu::Sampler,
    texture0: &wgpu::TextureView,
    texture1: &wgpu::TextureView,
) -> wgpu::BindGroup {
    device.create_bind_group(&wgpu::BindGroupDescriptor {
        label: None,
        layout,
        entries: &[
            wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::Sampler(sampler),
            },
            wgpu::BindGroupEntry {
                binding: 1,
                resource: wgpu::BindingResource::TextureView(texture0),
            },
            wgpu::BindGroupEntry {
                binding: 2,
                resource: wgpu::BindingResource::TextureView(texture1),
            },
        ],
    })
}

fn create_lighting_bind_group(
    device: &wgpu::Device,
    layout: &wgpu::BindGroupLayout,
    sampler: &wgpu::Sampler,
    lighting_texture: &wgpu::TextureView,
    color_texture: &wgpu::TextureView,
) -> wgpu::BindGroup {
    device.create_bind_group(&wgpu::BindGroupDescriptor {
        label: None,
        layout,
        entries: &[
            wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::Sampler(sampler),
            },
            wgpu::BindGroupEntry {
                binding: 1,
                resource: wgpu::BindingResource::TextureView(lighting_texture),
            },
            wgpu::BindGroupEntry {
                binding: 2,
                resource: wgpu::BindingResource::TextureView(color_texture),
            },
        ],
    })
}

fn create_shader_module(device: &wgpu::Device, source: &ShaderSource) -> wgpu::ShaderModule {
    device.create_shader_module(&wgpu::ShaderModuleDescriptor {
        label: None,
        source: wgpu::ShaderSource::Wgsl(source.source.as_str().into()),
    })
}

fn create_pipeline_layout(
    device: &wgpu::Device,
    bind_group_layout: &wgpu::BindGroupLayout,
) -> wgpu::PipelineLayout {
    device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: None,
        bind_group_layouts: &[bind_group_layout],
        push_constant_ranges: &[],
    })
}

fn create_render_pipeline(
    device: &wgpu::Device,
    layout: &wgpu::PipelineLayout,
    shader: &wgpu::ShaderModule,
    swap_chain_format: wgpu::TextureFormat,
    frag: &str,
) -> wgpu::RenderPipeline {
    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(layout),
        vertex: wgpu::VertexState {
            module: shader,
            entry_point: "vert_main",
            buffers: &[QuadVertex::LAYOUT, InstanceVertex::LAYOUT],
        },
        primitive: wgpu::PrimitiveState::default(),
        depth_stencil: Some(wgpu::DepthStencilState {
            format: wgpu::TextureFormat::Depth24Plus,
            depth_write_enabled: true,
            depth_compare: wgpu::CompareFunction::LessEqual,
            stencil: wgpu::StencilState::default(),
            bias: wgpu::DepthBiasState::default(),
        }),
        multisample: wgpu::MultisampleState::default(),
        fragment: Some(wgpu::FragmentState {
            module: shader,
            entry_point: frag,
            targets: &[wgpu::ColorTargetState {
                format: swap_chain_format,
                blend: Some(wgpu::BlendState::ALPHA_BLENDING),
                write_mask: wgpu::ColorWrites::ALL,
            }],
        }),
    })
}

fn create_lighting_pipeline(
    device: &wgpu::Device,
    shader: &wgpu::ShaderModule,
    swap_chain_format: wgpu::TextureFormat,
    bind_group_layout: &wgpu::BindGroupLayout,
) -> wgpu::RenderPipeline {
    let layout = create_pipeline_layout(device, bind_group_layout);
    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(&layout),
        vertex: wgpu::VertexState {
            module: shader,
            entry_point: "vert_main",
            buffers: &[LightingVertex::LAYOUT],
        },
        primitive: wgpu::PrimitiveState::default(),
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        fragment: Some(wgpu::FragmentState {
            module: shader,
            entry_point: "frag_main",
            targets: &[wgpu::ColorTargetState {
                format: swap_chain_format,
                blend: Some(wgpu::BlendState::ALPHA_BLENDING),
                write_mask: wgpu::ColorWrites::ALL,
            }],
        }),
    })
}

fn create_lighting_texture(device: &wgpu::Device, size: Vec2D<u32>) -> wgpu::Texture {
    device.create_texture(&wgpu::TextureDescriptor {
        label: None,
        size: wgpu::Extent3d {
            width: size.x,
            height: size.y,
            depth_or_array_layers: 1,
        },
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Rgba8Unorm,
        usage: wgpu::TextureUsages::COPY_DST | wgpu::TextureUsages::TEXTURE_BINDING,
    })
}

fn create_quad_buffer(device: &wgpu::Device) -> wgpu::Buffer {
    device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: None,
        contents: slice_as_bytes(QuadVertex::VERTICES),
        usage: wgpu::BufferUsages::VERTEX,
    })
}

fn create_uv_rect(rect: &Box2D<u32>, res: Vec2D<u32>) -> [f32; 4] {
    [
        rect.min.x as f32 / res.x as f32,
        rect.min.y as f32 / res.y as f32,
        rect.width() as f32 / res.x as f32,
        rect.height() as f32 / res.y as f32,
    ]
}
