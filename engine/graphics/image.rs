use std::path::Path;

use eyre::Result;

use crate::assets::{Asset, AssetDefaultLoader, AssetLoader, Handle, LoadingContext};
use crate::math::Vec2D;

/// Image asset.
#[derive(Clone, Debug)]
pub struct Image {
    /// Size in pixels.
    pub size: Vec2D<u32>,
    /// Raw image data (RGBA 8 bit per channel), None if already uploaded.
    pub data: Option<Vec<u8>>,
}

impl Asset for Image {}

impl AssetDefaultLoader for Image {
    type DefaultLoader = ImageLoader;
}

/// Image loader. Supports PNG format.
pub struct ImageLoader;

#[async_trait::async_trait]
impl AssetLoader<Image> for ImageLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<Image> {
        let bytes = ctx.read_bytes(path)?;
        let image = image::load_from_memory(&bytes)?.into_rgba8();
        let size = Vec2D::new(image.width(), image.height());
        let data = Some(image.into_flat_samples().samples);
        Ok(Image { size, data })
    }
}

/// Nine-patch stretchable image.
#[derive(Clone, Debug)]
pub struct NinePatchImage {
    /// Center image. Loaded from "center.png".
    pub center: Handle<Image>,
    /// Left image. Loaded from "left.png".
    pub left: Handle<Image>,
    /// Right image. Loaded from "right.png".
    pub right: Handle<Image>,
    /// Top image. Loaded from "top.png".
    pub top: Handle<Image>,
    /// Bottom image. Loaded from "botom.png".
    pub bottom: Handle<Image>,
    /// Top left image. Loaded from "top_left.png".
    pub top_left: Handle<Image>,
    /// Top right image. Loaded from "top_right.png".
    pub top_right: Handle<Image>,
    /// Bottom left image. Loaded from "bottom_left.png".
    pub bottom_left: Handle<Image>,
    /// Bottom right image. Loaded from "bottom_right.png".
    pub bottom_right: Handle<Image>,
}

impl Asset for NinePatchImage {}

impl AssetDefaultLoader for NinePatchImage {
    type DefaultLoader = NinePatchImageLoader;
}

/// Nine-patch image loader
pub struct NinePatchImageLoader;

#[async_trait::async_trait]
impl AssetLoader<NinePatchImage> for NinePatchImageLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<NinePatchImage> {
        Ok(NinePatchImage {
            center: ctx.load(&path.join("center.png")),
            left: ctx.load(&path.join("left.png")),
            right: ctx.load(&path.join("right.png")),
            top: ctx.load(&path.join("top.png")),
            bottom: ctx.load(&path.join("bottom.png")),
            top_left: ctx.load(&path.join("top_left.png")),
            top_right: ctx.load(&path.join("top_right.png")),
            bottom_left: ctx.load(&path.join("bottom_left.png")),
            bottom_right: ctx.load(&path.join("bottom_right.png")),
        })
    }
}
