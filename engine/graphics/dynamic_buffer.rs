#[derive(Debug)]
pub struct DynamicBuffer {
    buffer: wgpu::Buffer,
    size: u64,
    usage: wgpu::BufferUsages,
}

impl DynamicBuffer {
    pub fn new(
        device: &wgpu::Device,
        usage: wgpu::BufferUsages,
        initial_size: u64,
    ) -> DynamicBuffer {
        let buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: None,
            size: initial_size,
            usage,
            mapped_at_creation: false,
        });

        DynamicBuffer {
            buffer,
            size: initial_size.next_power_of_two(),
            usage,
        }
    }

    pub fn grow(&mut self, device: &wgpu::Device, new_size: u64) {
        if self.size >= new_size {
            return;
        }

        while self.size < new_size {
            self.size *= 2;
        }

        self.buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: None,
            size: self.size,
            usage: self.usage,
            mapped_at_creation: false,
        });
    }

    pub fn buffer(&self) -> &wgpu::Buffer {
        &self.buffer
    }
}
