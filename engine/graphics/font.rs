use std::path::Path;

use eyre::{eyre, Result};
pub use fontdue::Font;
use fontdue::FontSettings;

use crate::assets::{Asset, AssetDefaultLoader, AssetLoader, LoadingContext};

impl Asset for Font {}

impl AssetDefaultLoader for Font {
    type DefaultLoader = FontLoader;
}

/// Font loader. Supports TTF and OTF formats.
pub struct FontLoader;

#[async_trait::async_trait]
impl AssetLoader<Font> for FontLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<Font> {
        let bytes = ctx.read_bytes(path)?;
        let settings = FontSettings::default();
        Ok(Font::from_bytes(bytes, settings).map_err(|e| eyre!("Failed to parse font: {}", e))?)
    }
}
