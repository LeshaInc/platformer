use ahash::AHashMap;

use super::{Atlas, AtlasImage};
use crate::assets::{AssetStorage, Assets, Id};
use crate::graphics::image::Image;
use crate::math::Vec2D;

const TILE_SIZE: Vec2D<u32> = Vec2D::new(8, 8);
const INIT_TILEMAP_SIZE: Vec2D<u32> = Vec2D::new(128, 128);
const INIT_GUILLOTINE_SIZE: Vec2D<u32> = Vec2D::new(128, 128);

#[derive(Clone, Copy, Eq, Debug, PartialEq)]
pub struct ImageAtlasId(pub usize);

#[derive(Debug)]
pub struct ImageAtlases {
    atlases: Vec<Atlas<Id<Image>>>,
    image_atlas_ids: AHashMap<Id<Image>, ImageAtlasId>,
}

impl ImageAtlases {
    pub fn new(device: &wgpu::Device) -> ImageAtlases {
        let format = wgpu::TextureFormat::Rgba8UnormSrgb;
        ImageAtlases {
            atlases: vec![
                Atlas::new_tilemap(device, format, INIT_TILEMAP_SIZE, TILE_SIZE),
                Atlas::new_guillotine(device, format, INIT_GUILLOTINE_SIZE),
            ],
            image_atlas_ids: AHashMap::default(),
        }
    }

    pub fn maintain(&mut self, device: &wgpu::Device, queue: &wgpu::Queue, assets: &Assets) {
        let mut images = assets.storage_mut();
        self.flush(device, queue, &mut images);
        self.cleanup(&mut images);
    }

    pub fn num_atlases(&self) -> usize {
        self.atlases.len()
    }

    pub fn atlas_id_for_image(&self, image: Id<Image>) -> Option<ImageAtlasId> {
        self.image_atlas_ids.get(&image).copied()
    }

    pub fn atlas_by_id(&self, id: ImageAtlasId) -> &Atlas<Id<Image>> {
        &self.atlases[id.0]
    }

    fn flush(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        images: &mut AssetStorage<Image>,
    ) {
        let mut atlas_images = Vec::new();
        images.flush_with(|image_id, image| {
            if let Some(atlas_id) = self.atlas_id_for_image(image_id) {
                self.atlases[atlas_id.0].free(&image_id);
            }

            log::trace!("Uploading image {:?} to atlas", image_id);
            atlas_images.push(AtlasImage {
                size: image.size,
                data: match image.data.take() {
                    Some(v) => v,
                    None => {
                        log::warn!("Image to flush has no data attached");
                        return;
                    }
                },
                key: image_id,
            });
        });

        let mut out_keys = Vec::new();
        for (atlas_id, atlas) in self.atlases.iter_mut().enumerate() {
            atlas.alloc(device, queue, &mut atlas_images, Some(&mut out_keys));
            for image_id in out_keys.drain(..) {
                self.image_atlas_ids
                    .insert(image_id, ImageAtlasId(atlas_id));
            }
        }
    }

    fn cleanup(&mut self, images: &mut AssetStorage<Image>) {
        images.cleanup_with(|image_id, _| {
            let atlas_id = match self.image_atlas_ids.get(&image_id) {
                Some(v) => *v,
                None => return,
            };
            let atlas = &mut self.atlases[atlas_id.0];
            atlas.free(&image_id);
        });
    }
}
