mod font;
mod guillotine;
mod image;
mod shelf;
mod texture;
mod tilemap;

use std::cmp::Ordering;
use std::fmt::Debug;
use std::hash::Hash;

use ahash::AHashMap;

pub use self::font::{FontAtlas, GlyphKey};
use self::guillotine::GuillotineAllocator;
pub use self::image::{ImageAtlasId, ImageAtlases};
use self::shelf::ShelfAllocator;
use self::texture::{AtlasTexture, UploadRegion};
use self::tilemap::TilemapAllocator;
use crate::math::{Box2D, Vec2D};

const MAX_SIZE: Vec2D<u32> = Vec2D::new(4096, 4096);

#[derive(Debug)]
pub struct Atlas<K> {
    dyn_atlas: Box<dyn DynAtlas<K>>,
}

impl<K: Clone + Hash + Eq + 'static> Atlas<K> {
    pub fn new_tilemap(
        device: &wgpu::Device,
        format: wgpu::TextureFormat,
        size: Vec2D<u32>,
        tile_size: Vec2D<u32>,
    ) -> Atlas<K> {
        Atlas {
            dyn_atlas: Box::new(DynAtlasImpl {
                texture: AtlasTexture::new(device, format, size),
                allocator: TilemapAllocator::new(size, tile_size),
                map: AHashMap::default(),
            }),
        }
    }

    pub fn new_guillotine(
        device: &wgpu::Device,
        format: wgpu::TextureFormat,
        size: Vec2D<u32>,
    ) -> Atlas<K> {
        Atlas {
            dyn_atlas: Box::new(DynAtlasImpl {
                texture: AtlasTexture::new(device, format, size),
                allocator: GuillotineAllocator::new(size),
                map: AHashMap::default(),
            }),
        }
    }

    #[allow(dead_code)]
    pub fn new_shelf(
        device: &wgpu::Device,
        format: wgpu::TextureFormat,
        size: Vec2D<u32>,
    ) -> Atlas<K> {
        Atlas {
            dyn_atlas: Box::new(DynAtlasImpl {
                texture: AtlasTexture::new(device, format, size),
                allocator: ShelfAllocator::new(size),
                map: AHashMap::default(),
            }),
        }
    }

    #[allow(dead_code)]
    pub fn texture(&self) -> &wgpu::Texture {
        self.dyn_atlas.texture()
    }

    pub fn texture_view(&self) -> &wgpu::TextureView {
        self.dyn_atlas.texture_view()
    }

    pub fn alloc(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        images: &mut Vec<AtlasImage<K>>,
        out_keys: Option<&mut Vec<K>>,
    ) {
        if images.is_empty() {
            return;
        }
        self.dyn_atlas.alloc(device, queue, images, out_keys)
    }

    pub fn contains_key(&self, key: &K) -> bool {
        self.dyn_atlas.contains_key(key)
    }

    pub fn get_rect(&self, key: &K) -> Option<Box2D<u32>> {
        self.dyn_atlas.get_rect(key)
    }

    pub fn size(&self) -> Vec2D<u32> {
        self.dyn_atlas.size()
    }

    pub fn free(&mut self, key: &K) {
        self.dyn_atlas.free(key)
    }
}

#[derive(Clone, Debug)]
pub struct AtlasImage<K> {
    pub size: Vec2D<u32>,
    pub data: Vec<u8>,
    pub key: K,
}

trait AtlasAllocator: std::fmt::Debug {
    type Id: Copy;

    fn can_alloc(&self, size: Vec2D<u32>) -> bool;

    fn should_sort() -> bool;

    fn sorter(a: Vec2D<u32>, b: Vec2D<u32>) -> Ordering;

    fn can_grow() -> bool;

    fn grow(&mut self, size: Vec2D<u32>);

    fn alloc(&mut self, size: Vec2D<u32>) -> Option<(Self::Id, Vec2D<u32>)>;

    fn free(&mut self, id: Self::Id);
}

trait DynAtlas<K>: Debug {
    fn texture(&self) -> &wgpu::Texture;

    fn texture_view(&self) -> &wgpu::TextureView;

    fn alloc(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        images: &mut Vec<AtlasImage<K>>,
        out_keys: Option<&mut Vec<K>>,
    );

    fn get_rect(&self, key: &K) -> Option<Box2D<u32>>;

    fn contains_key(&self, key: &K) -> bool;

    fn size(&self) -> Vec2D<u32>;

    fn free(&mut self, key: &K);
}

struct DynAtlasImpl<K, A: AtlasAllocator> {
    texture: AtlasTexture,
    allocator: A,
    map: AHashMap<K, (A::Id, Box2D<u32>)>,
}

impl<K, A: AtlasAllocator> Debug for DynAtlasImpl<K, A> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DynAtlasImpl")
            .field("texture", &self.texture)
            .field("allocator", &self.allocator)
            .finish_non_exhaustive()
    }
}

impl<K: Clone + Hash + Eq, A: AtlasAllocator> DynAtlas<K> for DynAtlasImpl<K, A> {
    fn texture(&self) -> &wgpu::Texture {
        self.texture.texture()
    }

    fn texture_view(&self) -> &wgpu::TextureView {
        self.texture.texture_view()
    }

    fn alloc(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        in_images: &mut Vec<AtlasImage<K>>,
        mut out_keys: Option<&mut Vec<K>>,
    ) {
        let mut images = Vec::new();

        let mut image_idx = 0;
        while image_idx < in_images.len() {
            let image = &in_images[image_idx];
            if self.allocator.can_alloc(image.size) {
                images.push(in_images.swap_remove(image_idx));
            } else {
                image_idx += 1;
            }
        }

        if A::should_sort() {
            // sort images for more efficient packing
            images.sort_by(|a, b| A::sorter(a.size, b.size));
        }

        let mut upload_regions = Vec::with_capacity(images.len());
        let old_size = self.texture.size();
        let mut new_size = old_size;

        while let Some(image) = images.pop() {
            match self.allocator.alloc(image.size) {
                Some((alloc_id, offset)) => {
                    let rect = Box2D::new(offset, offset + image.size);
                    upload_regions.push(UploadRegion {
                        rect,
                        data: image.data,
                    });
                    self.map.insert(image.key.clone(), (alloc_id, rect));
                    if let Some(v) = &mut out_keys {
                        v.push(image.key);
                    }
                }
                None => {
                    images.push(image);

                    if self.texture.size() != MAX_SIZE && A::can_grow() {
                        new_size = if new_size.x < new_size.y {
                            new_size.map_x(|x| x * 2)
                        } else {
                            new_size.map_y(|y| y * 2)
                        };
                        self.allocator.grow(new_size);
                    } else {
                        // try to allocate remaining images
                        for image in images.drain(..) {
                            match self.allocator.alloc(image.size) {
                                Some((alloc_id, offset)) => {
                                    let rect = Box2D::new(offset, offset + image.size);
                                    upload_regions.push(UploadRegion {
                                        rect,
                                        data: image.data,
                                    });
                                    self.map.insert(image.key.clone(), (alloc_id, rect));
                                    if let Some(v) = &mut out_keys {
                                        v.push(image.key);
                                    }
                                }
                                None => {
                                    in_images.push(image);
                                }
                            }
                        }
                    }
                }
            }
        }

        if self.texture.size() != new_size {
            self.texture.grow(device, queue, new_size);
        }

        self.texture.upload(queue, &upload_regions);
    }

    fn get_rect(&self, key: &K) -> Option<Box2D<u32>> {
        self.map.get(key).map(|(_, rect)| *rect)
    }

    fn contains_key(&self, key: &K) -> bool {
        self.map.contains_key(key)
    }

    fn size(&self) -> Vec2D<u32> {
        self.texture.size()
    }

    fn free(&mut self, key: &K) {
        if let Some((id, ..)) = self.map.remove(key) {
            self.allocator.free(id);
        }
    }
}
