use ahash::{AHashMap, AHashSet};

use super::{Atlas, AtlasImage};
use crate::assets::{Assets, Id};
use crate::graphics::Font;
use crate::math::{Box2D, Vec2D};

const ATLAS_SIZE: Vec2D<u32> = Vec2D::new(512, 512);
const MAX_LIFETIME: u32 = 60 * 5;

#[derive(Debug)]
pub struct FontAtlas {
    atlas: Atlas<GlyphKey>,
    lifetimes: AHashMap<GlyphKey, u32>,
    images: Vec<AtlasImage<GlyphKey>>,
    zero_size_glyphs: AHashSet<GlyphKey>,
}

impl FontAtlas {
    pub fn new(device: &wgpu::Device) -> FontAtlas {
        FontAtlas {
            atlas: Atlas::new_shelf(device, wgpu::TextureFormat::R8Unorm, ATLAS_SIZE),
            lifetimes: AHashMap::default(),
            images: Vec::new(),
            zero_size_glyphs: AHashSet::default(),
        }
    }

    pub fn keepalive_glyph(&mut self, key: &GlyphKey) {
        self.lifetimes.insert(*key, 0);
    }

    pub fn contains_glyph(&self, key: &GlyphKey) -> bool {
        self.atlas.contains_key(key) || self.zero_size_glyphs.contains(key)
    }

    pub fn cleanup(&mut self) {
        let atlas = &mut self.atlas;
        self.lifetimes.retain(|key, lifetime| {
            if *lifetime >= MAX_LIFETIME {
                atlas.free(key);
            }
            *lifetime < MAX_LIFETIME
        });

        for lifetime in self.lifetimes.values_mut() {
            *lifetime += 1;
        }
    }

    pub fn rasterize(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        assets: &Assets,
        glyphs: &AHashSet<GlyphKey>,
    ) {
        self.images.clear();
        self.images.reserve(glyphs.len());

        for glyph in glyphs {
            let font = match assets.get_by_id(glyph.font) {
                Ok(v) => v,
                Err(e) => {
                    log::warn!("Glyph to rasterize references invalid font: {:?}", e);
                    continue;
                }
            };

            let (metrics, data) =
                font.rasterize_indexed(glyph.glyph_index as _, glyph.scale as f32);
            if metrics.width > 0 && metrics.height > 0 {
                self.images.push(AtlasImage {
                    size: Vec2D::new(metrics.width as u32, metrics.height as u32),
                    data,
                    key: *glyph,
                });
            } else {
            }
        }

        self.atlas.alloc(device, queue, &mut self.images, None);
    }

    pub fn texture_view(&self) -> &wgpu::TextureView {
        self.atlas.texture_view()
    }

    pub fn get_rect(&self, glyph: &GlyphKey) -> Option<Box2D<u32>> {
        if self.zero_size_glyphs.contains(glyph) {
            Some(Box2D::new(Vec2D::zero(), Vec2D::zero()))
        } else {
            self.atlas.get_rect(glyph)
        }
    }

    pub fn size(&self) -> Vec2D<u32> {
        self.atlas.size()
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct GlyphKey {
    pub font: Id<Font>,
    pub glyph_index: u16,
    pub scale: u32,
}
