use std::cmp::Ordering;

use ahash::AHashSet;

use super::AtlasAllocator;
use crate::math::Vec2D;

#[derive(Debug)]
pub struct TilemapAllocator {
    tile_size: Vec2D<u32>,
    size: Vec2D<u32>,
    free_tiles: AHashSet<Vec2D<u32>>,
}

impl TilemapAllocator {
    pub fn new(size: Vec2D<u32>, tile_size: Vec2D<u32>) -> Self {
        assert!(size.x % tile_size.x == 0 && size.y % tile_size.y == 0);

        let x_tiles = size.x / tile_size.x;
        let y_tiles = size.y / tile_size.y;

        let free_tiles = (0..x_tiles)
            .flat_map(|x| (0..y_tiles).map(move |y| Vec2D::new(x, y)))
            .collect();

        Self {
            tile_size,
            size: Vec2D::new(x_tiles, y_tiles),
            free_tiles,
        }
    }
}

impl AtlasAllocator for TilemapAllocator {
    type Id = Vec2D<u32>;

    fn can_alloc(&self, size: Vec2D<u32>) -> bool {
        size.x <= self.tile_size.x && size.y <= self.tile_size.y
    }

    fn should_sort() -> bool {
        false
    }

    fn sorter(_: Vec2D<u32>, _: Vec2D<u32>) -> Ordering {
        Ordering::Less // should_sort is false
    }

    fn can_grow() -> bool {
        true
    }

    fn grow(&mut self, size: Vec2D<u32>) {
        assert!(size.x % self.tile_size.x == 0 && size.y % self.tile_size.y == 0);

        let x_tiles = size.x / self.tile_size.x;
        let y_tiles = size.y / self.tile_size.y;

        assert!(x_tiles >= self.size.x && y_tiles >= self.size.y);

        for x in self.size.x..x_tiles {
            for y in self.size.y..y_tiles {
                self.free_tiles.insert(Vec2D::new(x, y));
            }
        }
    }

    fn alloc(&mut self, size: Vec2D<u32>) -> Option<(Vec2D<u32>, Vec2D<u32>)> {
        if size.x > self.tile_size.x || size.y > self.tile_size.y {
            return None;
        }

        let tile = *self.free_tiles.iter().next()?;
        self.free_tiles.remove(&tile);

        let pos = self.tile_size.elementwise_mul(tile);
        Some((tile, pos))
    }

    fn free(&mut self, id: Vec2D<u32>) {
        self.free_tiles.insert(id);
    }
}
