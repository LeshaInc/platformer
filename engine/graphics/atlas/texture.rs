use std::num::NonZeroU32;

use crate::math::{Box2D, Vec2D};

#[derive(Debug)]
pub struct AtlasTexture {
    texture: wgpu::Texture,
    texture_view: wgpu::TextureView,
    format: wgpu::TextureFormat,
    size: Vec2D<u32>,
}

impl AtlasTexture {
    fn desc(format: wgpu::TextureFormat, size: Vec2D<u32>) -> wgpu::TextureDescriptor<'static> {
        wgpu::TextureDescriptor {
            label: None,
            size: wgpu::Extent3d {
                width: size.x,
                height: size.y,
                depth_or_array_layers: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format,
            usage: wgpu::TextureUsages::TEXTURE_BINDING
                | wgpu::TextureUsages::COPY_SRC
                | wgpu::TextureUsages::COPY_DST,
        }
    }

    pub fn new(device: &wgpu::Device, format: wgpu::TextureFormat, size: Vec2D<u32>) -> Self {
        let texture = device.create_texture(&Self::desc(format, size));
        let texture_view = texture.create_view(&Default::default());
        Self {
            texture,
            texture_view,
            format,
            size,
        }
    }

    pub fn texture(&self) -> &wgpu::Texture {
        &self.texture
    }

    pub fn texture_view(&self) -> &wgpu::TextureView {
        &self.texture_view
    }

    pub fn grow(&mut self, device: &wgpu::Device, queue: &wgpu::Queue, new_size: Vec2D<u32>) {
        if self.size == new_size {
            return;
        }

        log::debug!("Growing atlas from {:?} to {:?}", self.size, new_size);

        let old_size = self.size;
        self.size = new_size;

        let old_texture = std::mem::replace(
            &mut self.texture,
            device.create_texture(&Self::desc(self.format, self.size)),
        );

        {
            let mut encoder = device.create_command_encoder(&Default::default());
            let src = wgpu::ImageCopyTexture {
                texture: &old_texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            };
            let dst = wgpu::ImageCopyTexture {
                texture: &self.texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            };
            let size = wgpu::Extent3d {
                width: old_size.x,
                height: old_size.y,
                depth_or_array_layers: 1,
            };
            encoder.copy_texture_to_texture(src, dst, size);
            queue.submit(Some(encoder.finish()));
        }

        self.texture_view = self.texture.create_view(&Default::default());
    }

    pub fn upload(&mut self, queue: &wgpu::Queue, regions: &[UploadRegion]) {
        for region in regions {
            let dst = wgpu::ImageCopyTexture {
                texture: &self.texture,
                mip_level: 0,
                origin: wgpu::Origin3d {
                    x: region.rect.min.x,
                    y: region.rect.min.y,
                    z: 0,
                },
                aspect: wgpu::TextureAspect::All,
            };
            let layout = wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(
                    NonZeroU32::new(region.data.len() as u32 / region.rect.height())
                        .expect("trying to upload a zero size image"),
                ),
                rows_per_image: Some(
                    NonZeroU32::new(region.rect.height())
                        .expect("trying to upload a zero size image"),
                ),
            };
            let extent = wgpu::Extent3d {
                width: region.rect.width(),
                height: region.rect.height(),
                depth_or_array_layers: 1,
            };
            queue.write_texture(dst, &region.data, layout, extent);
        }
    }

    pub fn size(&self) -> Vec2D<u32> {
        self.size
    }
}

pub struct UploadRegion {
    pub rect: Box2D<u32>,
    pub data: Vec<u8>,
}
