use std::cmp::Ordering;
use std::fmt::Debug;

use etagere::AllocId;

use super::AtlasAllocator;
use crate::math::Vec2D;

pub struct ShelfAllocator {
    allocator: etagere::AtlasAllocator,
}

impl ShelfAllocator {
    pub fn new(size: Vec2D<u32>) -> Self {
        Self {
            allocator: etagere::AtlasAllocator::new(size2(size)),
        }
    }
}

impl Debug for ShelfAllocator {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ShelfAllocator").finish_non_exhaustive()
    }
}

impl AtlasAllocator for ShelfAllocator {
    type Id = AllocId;

    fn can_alloc(&self, _size: Vec2D<u32>) -> bool {
        true
    }

    fn should_sort() -> bool {
        true
    }

    fn sorter(a: Vec2D<u32>, b: Vec2D<u32>) -> Ordering {
        (b.x * b.y).cmp(&(a.x * a.y)) // sort by descending area
    }

    fn can_grow() -> bool {
        false
    }

    fn grow(&mut self, _size: Vec2D<u32>) {}

    fn alloc(&mut self, size: Vec2D<u32>) -> Option<(AllocId, Vec2D<u32>)> {
        self.allocator.allocate(size2(size)).map(|a| {
            (
                a.id,
                Vec2D::new(a.rectangle.min.x as u32, a.rectangle.min.y as u32),
            )
        })
    }

    fn free(&mut self, id: AllocId) {
        self.allocator.deallocate(id);
    }
}

fn size2(size: Vec2D<u32>) -> etagere::Size {
    etagere::size2(size.x as i32, size.y as i32)
}
