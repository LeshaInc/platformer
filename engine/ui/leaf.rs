use siphasher::sip::SipHasher;

use super::skin::Skin;
use super::state::WidgetState;
use super::{DrawCtx, EventCtx, LayoutConstraints, LayoutCtx};
use crate::math::Vec2D;

pub trait LeafWidget<'a>: 'a {
    type State: WidgetState;

    fn id_hash(&mut self, hasher: &mut SipHasher) {
        let _ = hasher;
    }

    fn skin(&mut self) -> &str {
        ""
    }

    fn init(&mut self, skin: &Skin) -> Self::State;

    fn layout_constraints(&mut self, skin: &Skin, state: &mut Self::State) -> LayoutConstraints {
        let _ = (skin, state);
        LayoutConstraints::default()
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, skin: &Skin, state: &mut Self::State) -> Vec2D<f32>;

    fn handle_event(&mut self, ctx: &mut EventCtx, skin: &Skin, state: &mut Self::State) {
        let _ = (ctx, state, skin);
    }

    fn draw(&mut self, ctx: &mut DrawCtx, skin: &Skin, state: &mut Self::State);
}
