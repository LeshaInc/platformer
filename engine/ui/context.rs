use crate::assets::AssetStorage;
use crate::graphics::{DrawEncoder, Font, TextLayouter};
use crate::input::Event;
use crate::math::{Box2D, Vec2D};

pub struct LayoutCtx<'a, 'b> {
    pub fonts: &'a AssetStorage<Font>,
    pub text_layouter: &'a mut TextLayouter<'b>,
    pub hints: LayoutHints,
}

#[derive(Clone, Copy, Debug)]
pub struct LayoutHints {
    pub size: Vec2D<f32>,
}

#[derive(Clone, Copy, Debug)]
pub struct LayoutConstraints {
    pub stretch_factor: f32,
    pub min_size: Vec2D<f32>,
    pub max_size: Vec2D<f32>,
}

impl Default for LayoutConstraints {
    fn default() -> LayoutConstraints {
        LayoutConstraints {
            stretch_factor: 1.0,
            min_size: Vec2D::zero(),
            max_size: Vec2D::splat(f32::INFINITY),
        }
    }
}

pub struct EventCtx<'a> {
    pub rect: Box2D<f32>,
    pub event: &'a Event,
    pub propagation_stopped: bool,
}

impl EventCtx<'_> {
    pub fn stop_propagation(&mut self) {
        self.propagation_stopped = true;
    }

    pub fn propagation_stopped(&self) -> bool {
        self.propagation_stopped
    }
}

pub struct DrawCtx<'a, 'b> {
    pub encoder: &'a mut DrawEncoder<'b>,
    pub fonts: &'a AssetStorage<Font>,
    pub text_layouter: &'a mut TextLayouter<'b>,
    pub rect: Box2D<f32>,
}
