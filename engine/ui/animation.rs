use crate::math::{lerp, Rgba};

/// Trait for values which can be interpolated
pub trait Interpolate: Clone {
    /// Perform the interpolation, so time = 0 corresponds to `self`, and time = 1 corresponds to `other`
    fn interpolate(&self, other: &Self, time: f32) -> Self;
}

/// Normalized easing function
type EasingFunction = fn(t: f32) -> f32;

/// Defines some easing functions
pub mod easing {
    /// Linear curve (y = x)
    pub fn linear(t: f32) -> f32 {
        t
    }

    /// Quadratic ease-in-out curve
    pub fn ease_in_out(t: f32) -> f32 {
        if t < 0.5 {
            2.0 * t * t
        } else {
            -1.0 + (4.0 - 2.0 * t) * t
        }
    }
}

/// Animation helper
#[derive(Debug)]
pub struct Animation<T: Interpolate> {
    /// Current value
    pub value: T,
    start: T,
    target: T,
    time: f32,
    reached: bool,
    /// Animation speed in units per second. If the speed is 1, it will take one second to go from
    /// start to finish
    pub speed: f32,
    /// Easing function used by animation, can be changed at any time
    pub easing: EasingFunction,
}

impl<T: Interpolate> Animation<T> {
    /// Create animation with initial value and speed
    pub fn new(value: T, speed: f32) -> Animation<T> {
        Animation {
            start: value.clone(),
            target: value.clone(),
            value,
            reached: true,
            time: 0.0,
            speed,
            easing: easing::ease_in_out,
        }
    }

    /// Get current animation time
    pub fn time(&self) -> f32 {
        (self.easing)(self.time)
    }

    /// Update the animation
    pub fn update(&mut self, dt: f32) {
        if self.reached {
            return;
        }

        let new_time = self.time + self.speed * dt;
        self.time = new_time.min(1.0).max(0.0);
        if (self.time - 1.0).abs() < f32::EPSILON {
            self.reached = true;
        }

        self.value = T::interpolate(&self.start, &self.target, (self.easing)(self.time))
    }

    /// Start transitioning to another value
    pub fn go_to(&mut self, target: T) {
        self.start = self.value.clone();
        self.target = target;
        self.time = 0.0;
        self.reached = false;
    }
}

impl Interpolate for f32 {
    fn interpolate(&self, other: &f32, time: f32) -> f32 {
        lerp(*self, *other, time)
    }
}

impl Interpolate for Rgba {
    fn interpolate(&self, other: &Rgba, time: f32) -> Rgba {
        Rgba {
            r: self.r.interpolate(&other.r, time),
            g: self.g.interpolate(&other.g, time),
            b: self.b.interpolate(&other.b, time),
            a: self.a.interpolate(&other.a, time),
        }
    }
}
