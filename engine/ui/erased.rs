use bumpalo::Bump;
use siphasher::sip::SipHasher;

use super::id::WidgetId;
use super::skin::SkinSet;
use super::state::StateStorages;
use super::{DrawCtx, EventCtx, LayoutConstraints, LayoutCtx, LeafWidget};
use crate::math::Vec2D;

pub trait ErasedWidget<'a>: 'a {
    fn id_hash(&mut self, hasher: &mut SipHasher) {
        let _ = hasher;
    }

    fn init(&mut self, skins: &SkinSet, storages: &mut StateStorages, id: WidgetId);

    fn layout_constraints(
        &mut self,
        skins: &SkinSet,
        storages: &mut StateStorages,
        id: WidgetId,
    ) -> LayoutConstraints;

    fn layout(
        &mut self,
        ctx: &mut LayoutCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
        id: WidgetId,
    ) -> Vec2D<f32>;

    fn handle_event(
        &mut self,
        ctx: &mut EventCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
        id: WidgetId,
    );

    fn draw(
        &mut self,
        ctx: &mut DrawCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
        id: WidgetId,
    );
}

impl<'a, W: LeafWidget<'a>> ErasedWidget<'a> for W {
    fn id_hash(&mut self, hasher: &mut SipHasher) {
        LeafWidget::id_hash(self, hasher)
    }

    fn init(&mut self, skins: &SkinSet, storages: &mut StateStorages, id: WidgetId) {
        let skin = skins.get(self.skin());
        let state = LeafWidget::init(self, skin);
        storages.insert(id, state);
    }

    fn layout_constraints(
        &mut self,
        skins: &SkinSet,
        storages: &mut StateStorages,
        id: WidgetId,
    ) -> LayoutConstraints {
        let skin = skins.get(self.skin());
        match storages.get_mut(id) {
            Some(state) => self.layout_constraints(skin, state),
            None => LayoutConstraints::default(),
        }
    }

    fn layout(
        &mut self,
        ctx: &mut LayoutCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
        id: WidgetId,
    ) -> Vec2D<f32> {
        let skin = skins.get(self.skin());
        match storages.get_mut(id) {
            Some(state) => self.layout(ctx, skin, state),
            None => Vec2D::zero(),
        }
    }

    fn handle_event(
        &mut self,
        ctx: &mut EventCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
        id: WidgetId,
    ) {
        let skin = skins.get(self.skin());
        if let Some(state) = storages.get_mut(id) {
            self.handle_event(ctx, skin, state)
        }
    }

    fn draw(
        &mut self,
        ctx: &mut DrawCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
        id: WidgetId,
    ) {
        let skin = skins.get(self.skin());
        if let Some(state) = storages.get_mut(id) {
            self.draw(ctx, skin, state)
        }
    }
}

pub struct WidgetObject<'a> {
    id: WidgetId,
    obj: &'a mut dyn ErasedWidget<'a>,
}

impl<'a> WidgetObject<'a> {
    pub fn new<W: ErasedWidget<'a>>(bump: &'a Bump, id: WidgetId, widget: W) -> WidgetObject<'a> {
        let obj = bump.alloc(widget);
        WidgetObject { id, obj }
    }

    pub fn init(&mut self, skins: &SkinSet, storages: &mut StateStorages) {
        self.obj.init(skins, storages, self.id);
    }

    pub fn layout_constraints(
        &mut self,
        skins: &SkinSet,
        storages: &mut StateStorages,
    ) -> LayoutConstraints {
        self.obj.layout_constraints(skins, storages, self.id)
    }

    pub fn layout(
        &mut self,
        ctx: &mut LayoutCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
    ) -> Vec2D<f32> {
        self.obj.layout(ctx, skins, storages, self.id)
    }

    pub fn handle_event(
        &mut self,
        ctx: &mut EventCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
    ) {
        self.obj.handle_event(ctx, skins, storages, self.id);
    }

    pub fn draw(&mut self, ctx: &mut DrawCtx, skins: &SkinSet, storages: &mut StateStorages) {
        self.obj.draw(ctx, skins, storages, self.id);
    }
}

impl Drop for WidgetObject<'_> {
    fn drop(&mut self) {
        // self.obj is allocated in a Bump allocator, which doesn't call destructors,
        // so this is safe
        unsafe { std::ptr::drop_in_place(self.obj) }
    }
}
