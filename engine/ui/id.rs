use std::hash::Hasher;

use siphasher::sip::SipHasher;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct WidgetId {
    hash: u64,
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub struct WidgetIdHasher {
    hash: u64,
}

impl Hasher for WidgetIdHasher {
    fn finish(&self) -> u64 {
        self.hash
    }

    fn write(&mut self, _bytes: &[u8]) {
        unimplemented!()
    }

    fn write_u64(&mut self, hash: u64) {
        self.hash = hash;
    }
}

#[derive(Debug, Default)]
pub struct WidgetIdBuilder {
    stack: Vec<u64>,
}

impl WidgetIdBuilder {
    pub fn join(&mut self, callback: impl FnOnce(&mut SipHasher)) -> WidgetId {
        let mut hasher = SipHasher::new();
        if let Some(last) = self.stack.last() {
            hasher.write_u64(*last);
        }
        callback(&mut hasher);
        WidgetId {
            hash: hasher.finish(),
        }
    }

    pub fn push(&mut self, callback: impl FnOnce(&mut SipHasher)) {
        let hash = self.join(callback).hash;
        self.stack.push(hash);
    }

    pub fn pop(&mut self) {
        self.stack.pop();
    }

    pub fn get(&self) -> WidgetId {
        WidgetId {
            hash: self.stack.last().copied().unwrap_or(0),
        }
    }
}
