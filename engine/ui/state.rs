use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::hash::BuildHasherDefault;

use ahash::AHashMap;

use super::id::{WidgetId, WidgetIdHasher};

pub trait WidgetState: 'static + Send + Sync {
    type Storage: WidgetStateStorage<State = Self>;
}

pub trait WidgetStateStorage: 'static + Send + Sync + Default {
    type State;

    fn insert(&mut self, id: WidgetId, state: Self::State);

    fn get_mut(&mut self, id: WidgetId) -> Option<&mut Self::State>;
}

pub struct GenericWidgetStateStorage<T> {
    data: HashMap<WidgetId, T, BuildHasherDefault<WidgetIdHasher>>,
}

impl<T> Default for GenericWidgetStateStorage<T> {
    fn default() -> Self {
        GenericWidgetStateStorage {
            data: HashMap::default(),
        }
    }
}

impl<T> WidgetStateStorage for GenericWidgetStateStorage<T>
where
    T: 'static + Send + Sync,
{
    type State = T;

    fn insert(&mut self, id: WidgetId, state: T) {
        self.data.insert(id, state);
    }

    fn get_mut(&mut self, id: WidgetId) -> Option<&mut T> {
        self.data.get_mut(&id)
    }
}

#[derive(Default)]
pub struct EmptyWidgetStateStorage;

impl WidgetStateStorage for EmptyWidgetStateStorage {
    type State = ();

    fn insert(&mut self, _: WidgetId, _: ()) {}

    fn get_mut(&mut self, _: WidgetId) -> Option<&mut ()> {
        static mut NOTHING: () = ();
        Some(unsafe { &mut NOTHING })
    }
}

impl WidgetState for () {
    type Storage = EmptyWidgetStateStorage;
}

#[derive(Default)]
pub struct StateStorages {
    storages: AHashMap<TypeId, Box<dyn Any + Send + Sync>>,
}

impl StateStorages {
    pub fn insert<T: WidgetState>(&mut self, id: WidgetId, state: T) {
        let type_id = TypeId::of::<T>();
        let erased = self
            .storages
            .entry(type_id)
            .or_insert_with(|| Box::new(T::Storage::default()));
        if let Some(storage) = erased.downcast_mut::<T::Storage>() {
            storage.insert(id, state)
        }
    }

    pub fn get_mut<T: WidgetState>(&mut self, id: WidgetId) -> Option<&mut T> {
        let type_id = TypeId::of::<T>();
        let erased = self.storages.get_mut(&type_id)?;
        let storage = erased.downcast_mut::<T::Storage>()?;
        storage.get_mut(id)
    }
}
