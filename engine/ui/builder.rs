use bumpalo::Bump;

use super::id::WidgetIdBuilder;
use super::unsized_box::UnsizedBox;
use super::widgets::{Button, Text};
use super::{ErasedWidget, WidgetObject};

pub trait UiBuilder<'a> {
    fn get_arena(&self) -> &'a Bump;

    fn get_id_builder(&mut self) -> &mut WidgetIdBuilder;

    fn alloc<T>(&self, val: T) -> &'a mut T {
        self.get_arena().alloc(val)
    }

    fn widget<W: ErasedWidget<'a>>(&mut self, widget: W);

    fn text(&mut self, text: &str) {
        let text = self.get_arena().alloc_str(text);
        self.widget(Text::new(text))
    }

    fn button(&mut self, text: &str, callback: impl FnMut() + 'a) {
        let text = self.get_arena().alloc_str(text);
        let callback = UnsizedBox::new_callback(self.get_arena(), callback);
        self.widget(Button::new(text, callback))
    }

    fn finish(self) -> WidgetObject<'a>;
}
