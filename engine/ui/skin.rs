use std::path::{Path, PathBuf};
use std::str::FromStr;

use ahash::AHashMap;
use eyre::{bail, eyre, Error, Result};

use crate::assets::{Asset, AssetDefaultLoader, AssetLoader, Handle, Id, LoadingContext};
use crate::graphics::{Font, HorizontalAlignment, Image, NinePatchImage, VerticalAlignment};
use crate::math::{Padding, Rgba};

#[derive(Clone, Debug)]
pub enum Property {
    Number(f32),
    Padding(Padding),
    Color(Rgba),
    VerticalAlignment(VerticalAlignment),
    HorizontalAlignment(HorizontalAlignment),
    Font(Handle<Font>),
    FontPath(PathBuf),
    Image(Handle<Image>),
    ImagePath(PathBuf),
    NinePatchImage(Handle<NinePatchImage>),
    NinePatchImagePath(PathBuf),
}

pub trait FromProperty: Sized {
    fn from_property(property: &Property) -> Option<Self>;
}

impl FromProperty for f32 {
    fn from_property(v: &Property) -> Option<f32> {
        match *v {
            Property::Number(v) => Some(v),
            _ => None,
        }
    }
}

impl FromProperty for Padding {
    fn from_property(v: &Property) -> Option<Padding> {
        match *v {
            Property::Padding(v) => Some(v),
            _ => None,
        }
    }
}

impl FromProperty for Rgba {
    fn from_property(v: &Property) -> Option<Rgba> {
        match *v {
            Property::Color(v) => Some(v),
            _ => None,
        }
    }
}

impl FromProperty for VerticalAlignment {
    fn from_property(v: &Property) -> Option<VerticalAlignment> {
        match *v {
            Property::VerticalAlignment(v) => Some(v),
            _ => None,
        }
    }
}

impl FromProperty for HorizontalAlignment {
    fn from_property(v: &Property) -> Option<HorizontalAlignment> {
        match *v {
            Property::HorizontalAlignment(v) => Some(v),
            _ => None,
        }
    }
}

impl FromProperty for Id<Font> {
    fn from_property(v: &Property) -> Option<Id<Font>> {
        match v {
            Property::Font(v) => Some(v.id()),
            _ => None,
        }
    }
}

impl FromProperty for Id<Image> {
    fn from_property(v: &Property) -> Option<Id<Image>> {
        match v {
            Property::Image(v) => Some(v.id()),
            _ => None,
        }
    }
}

impl FromProperty for Id<NinePatchImage> {
    fn from_property(v: &Property) -> Option<Id<NinePatchImage>> {
        match v {
            Property::NinePatchImage(v) => Some(v.id()),
            _ => None,
        }
    }
}

impl FromStr for Property {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        if s.starts_with('#') || s.starts_with("rgb") {
            s.parse()
                .map(Property::Color)
                .map_err(|_| eyre!("Bad RGBA property"))
        } else if s.starts_with("padding") {
            s.parse()
                .map(Property::Padding)
                .map_err(|_| eyre!("Bad padding property"))
        } else if s.starts_with("horizontal") {
            s.parse()
                .map(Property::HorizontalAlignment)
                .map_err(|_| eyre!("Bad horizontal alignment property"))
        } else if s.starts_with("vertical") {
            s.parse()
                .map(Property::VerticalAlignment)
                .map_err(|_| eyre!("Bad vertical alignment property"))
        } else if let Some(stripped) = s.strip_prefix("font(") {
            parse_path(stripped)
                .map(Property::FontPath)
                .map_err(|_| eyre!("Bad font property"))
        } else if let Some(stripped) = s.strip_prefix("img(") {
            parse_path(stripped)
                .map(Property::ImagePath)
                .map_err(|_| eyre!("Bad image property"))
        } else if let Some(stripped) = s.strip_prefix("img9(") {
            parse_path(stripped)
                .map(Property::NinePatchImagePath)
                .map_err(|_| eyre!("Bad 9-patch image property"))
        } else if let Ok(v) = s.parse::<f32>() {
            Ok(Property::Number(v))
        } else {
            bail!("Bad property")
        }
    }
}

fn parse_path(s: &str) -> Result<PathBuf, ()> {
    if !s.ends_with(')') {
        return Err(());
    }

    Ok(PathBuf::from(&s[..s.len() - 1].trim().trim_matches('"')))
}

impl Property {
    fn resolve_assets(&mut self, assets: &LoadingContext) {
        match self {
            Property::FontPath(path) => {
                let path = std::mem::take(path);
                *self = Property::Font(assets.load(path));
            }
            Property::ImagePath(path) => {
                let path = std::mem::take(path);
                *self = Property::Image(assets.load(path));
            }
            Property::NinePatchImagePath(path) => {
                let path = std::mem::take(path);
                *self = Property::NinePatchImage(assets.load(path));
            }
            _ => {}
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Skin {
    properties: AHashMap<String, Property>,
}

impl Skin {
    fn include(&mut self, other: &Skin) {
        for (name, property) in &other.properties {
            self.properties.insert(name.clone(), property.clone());
        }
    }

    fn resolve_assets(&mut self, assets: &LoadingContext) {
        for property in self.properties.values_mut() {
            property.resolve_assets(assets);
        }
    }

    pub fn try_get<P>(&self, name: &str) -> Option<P>
    where
        P: FromProperty,
    {
        self.properties
            .get(name)
            .or_else(|| {
                let (_, rest) = name.split_once('_')?;
                self.properties.get(rest)
            })
            .and_then(|p| P::from_property(p))
    }

    pub fn get<P>(&self, name: &str) -> P
    where
        P: FromProperty + Default,
    {
        self.try_get(name).unwrap_or_default()
    }

    fn parse(s: &str, skins: &SkinSet) -> Result<Skin> {
        let mut res = Skin::default();

        for property in s.split(';') {
            let property = property.trim();
            if property.is_empty() {
                continue;
            }

            if let Some(rest) = property.strip_prefix("include ") {
                let name = rest.trim();
                res.include(
                    skins
                        .skins
                        .get(name)
                        .ok_or_else(|| eyre!("No such skin: {}", name))?,
                );
                continue;
            }

            let mut split = property.splitn(2, ':');
            let k = split.next().ok_or_else(|| eyre!("Bad skin"))?.trim_end();
            let v = split.next().ok_or_else(|| eyre!("Bad skin"))?.trim_start();

            let property = Property::from_str(v)?;
            res.properties.insert(k.into(), property);
        }

        Ok(res)
    }
}

#[derive(Clone, Debug, Default)]
pub struct SkinSet {
    skins: AHashMap<String, Skin>,
    empty: Skin,
}

impl SkinSet {
    pub fn get(&self, name: &str) -> &Skin {
        self.skins.get(name).unwrap_or(&self.empty)
    }

    fn resolve_assets(&mut self, assets: &LoadingContext) {
        for skin in self.skins.values_mut() {
            skin.resolve_assets(assets);
        }
    }
}

impl FromStr for SkinSet {
    type Err = Error;

    fn from_str(mut s: &str) -> Result<Self> {
        let mut skins = SkinSet::default();

        while let Some(lbrace) = s.find('{') {
            let name = s[..lbrace].trim();
            s = &s[lbrace + 1..];
            let rbrace = s.find('}').ok_or_else(|| eyre!("Unbalanced {"))?;
            let inner = &s[..rbrace];
            let skin = Skin::parse(inner, &skins)?;
            skins.skins.insert(name.into(), skin);
            s = &s[rbrace + 1..];
        }

        Ok(skins)
    }
}

impl SkinSet {}

impl Asset for SkinSet {}

impl AssetDefaultLoader for SkinSet {
    type DefaultLoader = SkinSetLoader;
}

pub struct SkinSetLoader;

#[async_trait::async_trait]
impl AssetLoader<SkinSet> for SkinSetLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<SkinSet> {
        let text = ctx.read_string(path)?;
        let mut skins = SkinSet::from_str(&text)?;
        skins.resolve_assets(&ctx);
        Ok(skins)
    }
}
