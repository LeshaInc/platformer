use std::ops::{Deref, DerefMut};

use bumpalo::boxed::Box as BumpBox;
use bumpalo::Bump;

pub struct UnsizedBox<'a, T: ?Sized> {
    inner: BumpBox<'a, T>,
}

impl<'a, T: ?Sized> UnsizedBox<'a, T> {
    /// SAFETY: The reference should come from the Bump allocator
    pub unsafe fn new(inner: &'a mut T) -> UnsizedBox<'a, T> {
        UnsizedBox {
            inner: BumpBox::from_raw(inner),
        }
    }
}

impl<'a, T: FnMut() + 'a> UnsizedBox<'a, T> {
    pub fn new_callback(arena: &'a Bump, f: T) -> UnsizedBox<'a, dyn FnMut() + 'a> {
        let r = arena.alloc(f);
        unsafe { UnsizedBox::new(r) }
    }
}

impl<'a, T: ?Sized> Deref for UnsizedBox<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        &self.inner
    }
}

impl<'a, T: ?Sized> DerefMut for UnsizedBox<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.inner
    }
}
