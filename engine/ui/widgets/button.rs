use super::{PaddingBox, Text};
use crate::graphics::NinePatchSprite;
use crate::input::Event;
use crate::math::{Rgba, Vec2D};
use crate::ui::animation::Animation;
use crate::ui::skin::Skin;
use crate::ui::unsized_box::UnsizedBox;
use crate::ui::{
    DrawCtx, EventCtx, GenericWidgetStateStorage, LayoutConstraints, LayoutCtx, LeafWidget,
    WidgetState,
};

pub struct Button<'a> {
    padding_box: PaddingBox<Text<'a>>,
    callback: UnsizedBox<'a, dyn FnMut() + 'a>,
}

impl<'a> Button<'a> {
    pub fn new(text: &'a str, callback: UnsizedBox<'a, dyn FnMut() + 'a>) -> Button<'a> {
        Button {
            padding_box: PaddingBox::new(Text::new(text)),
            callback,
        }
    }
}

pub struct ButtonState {
    pub bg_color_animation: Animation<Rgba>,
}

impl WidgetState for ButtonState {
    type Storage = GenericWidgetStateStorage<Self>;
}

impl<'a> LeafWidget<'a> for Button<'a> {
    type State = ButtonState;

    fn init(&mut self, skin: &Skin) -> ButtonState {
        ButtonState {
            bg_color_animation: Animation::new(skin.get("button_pressed_bg"), 0.3),
        }
    }

    fn skin(&mut self) -> &str {
        "button"
    }

    fn layout_constraints(&mut self, skin: &Skin, _: &mut ButtonState) -> LayoutConstraints {
        self.padding_box.layout_constraints(skin, &mut ())
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, skin: &Skin, _: &mut ButtonState) -> Vec2D<f32> {
        self.padding_box.layout(ctx, skin, &mut ())
    }

    fn handle_event(&mut self, ctx: &mut EventCtx, _: &Skin, _: &mut Self::State) {
        if let Event::MouseButton(event) = ctx.event {
            if ctx.rect.contains(event.cursor_pos().cast::<f32>()) {
                (self.callback)();
                ctx.stop_propagation();
            }
        }
    }

    fn draw(&mut self, ctx: &mut DrawCtx, skin: &Skin, state: &mut ButtonState) {
        if let Some(image) = skin.try_get("button_image") {
            ctx.encoder.draw_nine_patch_sprite(
                NinePatchSprite::new(image, ctx.rect)
                    .with_color(state.bg_color_animation.value)
                    .with_scale(2.0),
            );
        }

        self.padding_box.draw(ctx, skin, &mut ());
    }
}
