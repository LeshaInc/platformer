use siphasher::sip::SipHasher;

use crate::math::{Padding, Vec2D};
use crate::ui::skin::Skin;
use crate::ui::{DrawCtx, EventCtx, LayoutConstraints, LayoutCtx, LayoutHints, LeafWidget};

pub struct PaddingBox<W> {
    inner: W,
}

impl<'a, W: LeafWidget<'a>> PaddingBox<W> {
    pub fn new(inner: W) -> PaddingBox<W> {
        PaddingBox { inner }
    }
}

impl<'a, W: LeafWidget<'a>> LeafWidget<'a> for PaddingBox<W> {
    type State = W::State;

    fn id_hash(&mut self, hasher: &mut SipHasher) {
        self.inner.id_hash(hasher);
    }

    fn skin(&mut self) -> &str {
        self.inner.skin()
    }

    fn init(&mut self, skin: &Skin) -> W::State {
        self.inner.init(skin)
    }

    fn layout_constraints(&mut self, skin: &Skin, state: &mut W::State) -> LayoutConstraints {
        let padding = skin.get::<Padding>("padding");
        let mut constraints = self.inner.layout_constraints(skin, state);
        constraints.min_size += padding.extents();
        constraints.max_size += padding.extents();
        constraints
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, skin: &Skin, state: &mut W::State) -> Vec2D<f32> {
        let padding = skin.get::<Padding>("padding");
        let mut ctx = LayoutCtx {
            hints: LayoutHints {
                size: (ctx.hints.size - padding.extents()).fmax(Vec2D::zero()),
            },
            fonts: ctx.fonts,
            text_layouter: ctx.text_layouter,
        };
        self.inner.layout(&mut ctx, skin, state) + padding.extents()
    }

    fn handle_event(&mut self, ctx: &mut EventCtx, skin: &Skin, state: &mut Self::State) {
        let padding = skin.get::<Padding>("padding");
        let mut ctx = EventCtx {
            rect: padding.shrink_rect(&ctx.rect),
            event: ctx.event,
            propagation_stopped: ctx.propagation_stopped,
        };
        self.inner.handle_event(&mut ctx, skin, state)
    }

    fn draw(&mut self, ctx: &mut DrawCtx, skin: &Skin, state: &mut W::State) {
        let padding = skin.get::<Padding>("padding");
        let mut ctx = DrawCtx {
            encoder: ctx.encoder,
            fonts: ctx.fonts,
            text_layouter: ctx.text_layouter,
            rect: padding.shrink_rect(&ctx.rect),
        };
        self.inner.draw(&mut ctx, skin, state)
    }
}
