use crate::graphics::{TextLayoutSettings, TextLayouter};
use crate::math::Vec2D;
use crate::ui::skin::Skin;
use crate::ui::{DrawCtx, LayoutCtx, LeafWidget};

pub struct Text<'a> {
    text: &'a str,
}

impl<'a> Text<'a> {
    pub fn new(text: &'a str) -> Text<'a> {
        Text { text }
    }

    fn layout_settings(&self, skin: &Skin, bounds: Vec2D<Option<f32>>) -> TextLayoutSettings {
        TextLayoutSettings {
            bounds,
            vert_alignment: skin.get("text_vert_alignment"),
            horiz_alignment: skin.get("text_horiz_alignment"),
            ..Default::default()
        }
    }

    fn append_text(&self, skin: &Skin, layouter: &mut TextLayouter, size: Vec2D<Option<f32>>) {
        layouter.reset(&self.layout_settings(skin, size));
        layouter.append(
            skin.get("text_font"),
            skin.get("text_scale"),
            skin.get("text_color"),
            skin.try_get("text_outline_color"),
            self.text,
        );
    }
}

impl<'a> LeafWidget<'a> for Text<'a> {
    type State = ();

    fn init(&mut self, _: &Skin) {}

    fn layout(&mut self, ctx: &mut LayoutCtx, skin: &Skin, _: &mut ()) -> Vec2D<f32> {
        let size = ctx.hints.size.map(Some);
        self.append_text(skin, &mut ctx.text_layouter, size);
        ctx.text_layouter.size()
    }

    fn draw(&mut self, ctx: &mut DrawCtx, skin: &Skin, _: &mut ()) {
        // TODO: don't layout text twice
        let size = ctx.rect.extents().map(Some);
        self.append_text(skin, &mut ctx.text_layouter, size);
        ctx.text_layouter.encode(&mut ctx.encoder, ctx.rect.min)
    }
}
