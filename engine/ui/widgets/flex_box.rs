use bumpalo::boxed::Box as BumpBox;
use bumpalo::collections::Vec as BumpVec;
use bumpalo::Bump;

use crate::math::{Box2D, Vec2D};
use crate::ui::id::WidgetIdBuilder;
use crate::ui::skin::SkinSet;
use crate::ui::{
    DrawCtx, ErasedWidget, EventCtx, LayoutConstraints, LayoutCtx, LayoutHints, StateStorages,
    UiBuilder, WidgetId, WidgetObject,
};

pub struct FlexBoxBuilder<'a> {
    arena: &'a Bump,
    children: BumpVec<'a, Child<'a>>,
    id_builder: &'a mut WidgetIdBuilder,
}

impl<'a> FlexBoxBuilder<'a> {
    pub fn new(arena: &'a Bump, id_builder: &'a mut WidgetIdBuilder) -> FlexBoxBuilder<'a> {
        FlexBoxBuilder {
            arena,
            children: BumpVec::new_in(arena),
            id_builder,
        }
    }
}

impl<'a> UiBuilder<'a> for FlexBoxBuilder<'a> {
    fn get_arena(&self) -> &'a Bump {
        self.arena
    }

    fn get_id_builder(&mut self) -> &mut WidgetIdBuilder {
        &mut self.id_builder
    }

    fn widget<W: ErasedWidget<'a>>(&mut self, mut widget: W) {
        let id = self.id_builder.join(|hasher| widget.id_hash(hasher));
        self.children.push(Child {
            widget: WidgetObject::new(self.arena, id, widget),
            constraints: LayoutConstraints::default(),
            pos: Vec2D::zero(),
            size: Vec2D::zero(),
        });
    }

    fn finish(self) -> WidgetObject<'a> {
        let id = self.id_builder.get();
        let flex_box = FlexBox {
            children: self.children.into_boxed_slice(),
            stretch_factor: 0.0, // FIXME
        };
        WidgetObject::new(self.arena, id, flex_box)
    }
}

pub struct FlexBox<'a> {
    children: BumpBox<'a, [Child<'a>]>,
    stretch_factor: f32,
}

struct Child<'a> {
    widget: WidgetObject<'a>,
    constraints: LayoutConstraints,
    pos: Vec2D<f32>,
    size: Vec2D<f32>,
}

impl<'a> FlexBox<'a> {}

impl<'a> ErasedWidget<'a> for FlexBox<'a> {
    fn init(&mut self, skins: &SkinSet, storages: &mut StateStorages, _: WidgetId) {
        for child in self.children.iter_mut() {
            child.widget.init(skins, storages);
        }
    }

    fn layout_constraints(
        &mut self,
        skins: &SkinSet,
        storages: &mut StateStorages,
        _: WidgetId,
    ) -> LayoutConstraints {
        let mut result = LayoutConstraints {
            stretch_factor: self.stretch_factor,
            ..Default::default()
        };

        for child in self.children.iter_mut() {
            let constraints = child.widget.layout_constraints(skins, storages);
            result.min_size.y += constraints.min_size.y;
            result.max_size.y += constraints.max_size.y;
            result.min_size.x = result.min_size.x.max(constraints.min_size.x);
            result.max_size.x = result.max_size.x.max(constraints.max_size.x);
            child.constraints = constraints;
        }

        result
    }

    fn layout(
        &mut self,
        ctx: &mut LayoutCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
        _: WidgetId,
    ) -> Vec2D<f32> {
        let hints = ctx.hints;

        let mut total_height = 0.0;
        let mut total_stretch = 0.0;

        for child in self.children.iter_mut() {
            total_stretch += child.constraints.stretch_factor;
            total_height += child.constraints.min_size.y;
            child.size = child.constraints.min_size;
        }

        let mut stretch_unit = (hints.size.y - total_height).max(0.0) / total_stretch;

        while stretch_unit > 0.5 {
            let mut num_maxed = 0;
            for child in self.children.iter_mut() {
                let addition = (stretch_unit * child.constraints.stretch_factor).round();
                let new_height = (child.size.y + addition).min(child.constraints.max_size.y);
                total_height += new_height - child.size.y;
                child.size.y = new_height;
                child.size.x = hints.size.x;
                if child.size.y > child.constraints.max_size.y - 0.5 {
                    num_maxed += 1;
                }
            }

            if num_maxed == self.children.len() {
                break;
            } else {
                stretch_unit = (hints.size.y - total_height).max(0.0) / total_stretch;
            }
        }

        let mut total_width = 0f32;

        for child in self.children.iter_mut() {
            let mut ctx = LayoutCtx {
                fonts: ctx.fonts,
                text_layouter: ctx.text_layouter,
                hints: LayoutHints { size: child.size },
            };
            let actual_size = child.widget.layout(&mut ctx, skins, storages);

            let diff = actual_size.y - child.size.y;
            total_width = total_width.max(actual_size.x);
            total_height += diff;

            child.size = actual_size;
        }

        // TODO: redistribute free space again

        let mut pos = Vec2D::zero();

        // TODO: alignment

        for child in self.children.iter_mut() {
            child.pos = pos;
            pos.y += child.size.y;
        }

        Vec2D::new(total_width, total_height)
    }

    fn handle_event(
        &mut self,
        ctx: &mut EventCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
        _: WidgetId,
    ) {
        for child in self.children.iter_mut() {
            let mut ctx = EventCtx {
                rect: ctx.rect,
                event: ctx.event,
                propagation_stopped: false,
            };
            child.widget.handle_event(&mut ctx, skins, storages);
            if ctx.propagation_stopped() {
                break;
            }
        }
    }

    fn draw(
        &mut self,
        ctx: &mut DrawCtx,
        skins: &SkinSet,
        storages: &mut StateStorages,
        _: WidgetId,
    ) {
        let rect = ctx.rect;

        for child in self.children.iter_mut() {
            let mut ctx = DrawCtx {
                encoder: ctx.encoder,
                fonts: ctx.fonts,
                text_layouter: ctx.text_layouter,
                rect: Box2D::new(rect.min + child.pos, rect.min + child.pos + child.size),
            };
            child.widget.draw(&mut ctx, skins, storages);
        }
    }
}
