use std::fmt::{self, Display};
use std::str::FromStr;

use eyre::{bail, Error, Result};

use super::KeyCode;

bitflags::bitflags! {
    pub struct Modifiers: u16 {
        const CTRL = 1 << 0;
        const ALT = 1 << 1;
        const SHIFT = 1 << 2;
        const LOGO = 1 << 3;
        const F1 = 1 << 4;
        const F2 = 1 << 5;
        const F3 = 1 << 6;
        const F4 = 1 << 7;
        const F5 = 1 << 8;
        const F6 = 1 << 9;
        const F7 = 1 << 10;
        const F8 = 1 << 11;
        const F9 = 1 << 12;
        const F10 = 1 << 13;
        const F11 = 1 << 14;
        const F12 = 1 << 15;
    }
}

impl Modifiers {
    const NAMES: [&'static str; 16] = [
        "Ctrl", "Alt", "Shift", "Logo", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9",
        "F10", "F11", "F12",
    ];

    const KEYS: [KeyCode; 16] = [
        KeyCode::LControl,
        KeyCode::LAlt,
        KeyCode::LShift,
        KeyCode::LWin,
        KeyCode::F1,
        KeyCode::F2,
        KeyCode::F3,
        KeyCode::F4,
        KeyCode::F5,
        KeyCode::F6,
        KeyCode::F7,
        KeyCode::F8,
        KeyCode::F9,
        KeyCode::F10,
        KeyCode::F11,
        KeyCode::F12,
    ];

    pub fn from_key(key: KeyCode) -> Modifiers {
        let key = match key {
            KeyCode::RControl => KeyCode::LControl,
            KeyCode::RAlt => KeyCode::LAlt,
            KeyCode::RShift => KeyCode::LShift,
            KeyCode::RWin => KeyCode::LWin,
            _ => key,
        };

        for (bit, pattern) in Self::KEYS.iter().enumerate() {
            if &key == pattern {
                return Modifiers::from_bits_truncate(1 << (bit as u16));
            }
        }

        Modifiers::empty()
    }
}

impl Display for Modifiers {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut is_first = true;
        let mut add_part = |name| {
            if !is_first {
                f.write_str("-")?;
            }
            is_first = false;
            f.write_str(name)
        };

        for bit in 0..16 {
            if self.bits() & (1 << bit) > 0 {
                add_part(Self::NAMES[bit as usize])?;
            }
        }

        Ok(())
    }
}

impl FromStr for Modifiers {
    type Err = Error;

    fn from_str(s: &str) -> Result<Modifiers> {
        let mut raw = 0;

        'outer: for part in s.split('-') {
            let part = part.trim();

            if part.is_empty() {
                continue;
            }

            for (bit, name) in Self::NAMES.iter().enumerate() {
                if part.eq_ignore_ascii_case(name) {
                    raw |= 1 << bit;
                    continue 'outer;
                }
            }

            bail!("Bad key modifier: {}", part);
        }

        Ok(Modifiers::from_bits_truncate(raw as u16))
    }
}
