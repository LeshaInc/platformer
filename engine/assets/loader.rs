use std::path::Path;

use eyre::Result;

use super::{Asset, LoadingContext};

/// Asset loader
#[async_trait::async_trait]
pub trait AssetLoader<A: Asset> {
    /// Load the asset at the specified path
    async fn load(ctx: LoadingContext, path: &Path) -> Result<A>;
}

/// Trait for assets with a default loader
pub trait AssetDefaultLoader: Asset {
    /// Default asset loader
    type DefaultLoader: AssetLoader<Self>;
}
