use std::path::Path;

use eyre::Result;
use serde::de::DeserializeOwned;

use super::{Asset, AssetLoader, LoadingContext};

/// JSON asset loader. Can load any asset with `#[derive(serde::Deserialize)]`.
pub struct JsonLoader;

#[async_trait::async_trait]
impl<A: Asset + DeserializeOwned> AssetLoader<A> for JsonLoader {
    async fn load(ctx: LoadingContext, path: &Path) -> Result<A> {
        let string = ctx.read_string(path)?;
        Ok(serde_json::from_str(&string)?)
    }
}
