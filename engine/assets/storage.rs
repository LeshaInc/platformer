use std::fmt;
use std::ops::{Index, IndexMut};
use std::sync::Arc;

use ahash::AHashMap;
use crossbeam::queue::SegQueue;
use parking_lot::MappedRwLockWriteGuard;

use super::handle::HandleAllocator;
use super::{AssetAccessError, Handle, Id};

/// Storage for a single asset type.
pub struct AssetStorage<A> {
    assets: AHashMap<Id<A>, A>,
    handle: Arc<StorageHandle<A>>,
}

impl<A> AssetStorage<A> {
    /// Create an empty asset storage.
    pub fn new() -> Self {
        Self {
            assets: AHashMap::default(),
            handle: Arc::new(StorageHandle {
                new_assets: None,
                dead_assets: SegQueue::new(),
            }),
        }
    }

    /// Create an empty asset storage with manual flushing.
    pub fn new_with_flushing() -> Self {
        Self {
            assets: AHashMap::default(),
            handle: Arc::new(StorageHandle {
                new_assets: Some(SegQueue::new()),
                dead_assets: SegQueue::new(),
            }),
        }
    }

    /// Get shared reference to an asset.
    pub fn get(&self, handle: &Handle<A>) -> Result<&A, AssetAccessError> {
        self.get_by_id(handle.id())
    }

    /// Get shared reference to an asset.
    pub fn get_by_id(&self, id: Id<A>) -> Result<&A, AssetAccessError> {
        self.assets
            .get(&id)
            .ok_or_else(|| AssetAccessError::NotFound {
                type_name: std::any::type_name::<A>(),
                id: id.raw(),
            })
    }

    /// Get mutable reference to an asset.
    pub fn get_mut(&mut self, handle: &Handle<A>) -> Result<&mut A, AssetAccessError> {
        self.get_mut_by_id(handle.id())
    }

    /// Get mutable reference to an asset.
    pub fn get_mut_by_id(&mut self, id: Id<A>) -> Result<&mut A, AssetAccessError> {
        self.assets
            .get_mut(&id)
            .ok_or_else(|| AssetAccessError::NotFound {
                type_name: std::any::type_name::<A>(),
                id: id.raw(),
            })
    }

    pub(super) fn insert(&mut self, id: Id<A>, asset: A) -> Option<A> {
        self.assets.insert(id, asset)
    }

    /// Manually flush assets.
    pub fn flush(&mut self) {
        self.flush_with(|_, _| ());
    }

    /// Manually flush assets, calling the `handler` function on each of them.
    pub fn flush_with(&mut self, mut handler: impl FnMut(Id<A>, &mut A)) {
        let queue = match &self.handle.new_assets {
            Some(v) => v,
            None => return,
        };

        while let Some(id) = queue.pop() {
            let asset = match self.assets.get_mut(&id) {
                Some(v) => v,
                None => {
                    log::warn!("trying to flush nonexistent asset {:?}", id);
                    continue;
                }
            };
            handler(id, asset);
        }
    }

    /// Remove dead assets.
    pub fn cleanup(&mut self) {
        self.cleanup_with(|_, _| ());
    }

    /// Remove dead assets, calling the `handler` function on each of them.
    pub fn cleanup_with(&mut self, mut handler: impl FnMut(Id<A>, A)) {
        while let Some(id) = self.handle.dead_assets.pop() {
            let asset = match self.assets.remove(&id) {
                Some(v) => v,
                None => continue,
            };

            handler(id, asset);
        }
    }

    pub(super) fn handle(&self) -> &Arc<StorageHandle<A>> {
        &self.handle
    }
}

impl<A> Default for AssetStorage<A> {
    fn default() -> Self {
        Self::new()
    }
}

impl<A> Index<&Handle<A>> for AssetStorage<A> {
    type Output = A;

    fn index(&self, handle: &Handle<A>) -> &A {
        self.get(handle).expect("no such asset")
    }
}

impl<A> IndexMut<&Handle<A>> for AssetStorage<A> {
    fn index_mut(&mut self, handle: &Handle<A>) -> &mut A {
        self.get_mut(handle).expect("no such asset")
    }
}

impl<A> Index<Id<A>> for AssetStorage<A> {
    type Output = A;

    fn index(&self, id: Id<A>) -> &A {
        self.get_by_id(id).expect("no such asset")
    }
}

impl<A> IndexMut<Id<A>> for AssetStorage<A> {
    fn index_mut(&mut self, id: Id<A>) -> &mut A {
        self.get_mut_by_id(id).expect("no such asset")
    }
}

impl<A> fmt::Debug for AssetStorage<A> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("AssetStorage")
            .field("num_assets", &self.assets.len())
            .field("num_to_drop", &self.handle.dead_assets.len())
            .finish_non_exhaustive()
    }
}

pub struct StorageHandle<A> {
    new_assets: Option<SegQueue<Id<A>>>,
    dead_assets: SegQueue<Id<A>>,
}

impl<A> StorageHandle<A> {
    pub fn append_asset(&self, id: Id<A>) {
        if let Some(queue) = &self.new_assets {
            queue.push(id);
        }
    }

    pub fn drop_asset(&self, id: Id<A>) {
        self.dead_assets.push(id);
    }
}

/// Asset appender.
pub struct AssetAppender<'a, A> {
    storage: MappedRwLockWriteGuard<'a, AssetStorage<A>>,
    allocator: Arc<HandleAllocator>,
}

impl<'a, A> AssetAppender<'a, A> {
    pub(super) fn new(
        storage: MappedRwLockWriteGuard<'a, AssetStorage<A>>,
        allocator: Arc<HandleAllocator>,
    ) -> Self {
        Self { storage, allocator }
    }

    /// Append an asset.
    pub fn append(&mut self, asset: A) -> Handle<A> {
        let handle = self.allocator.alloc(self.storage.handle().clone());
        self.storage.insert(handle.id(), asset);
        handle
    }
}
