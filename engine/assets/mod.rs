//! Asset management

mod handle;
mod json;
mod loader;
mod manager;
mod progress;
mod source;
mod storage;

pub use self::handle::{Handle, Id, RawId};
pub use self::json::JsonLoader;
pub use self::loader::{AssetDefaultLoader, AssetLoader};
pub use self::manager::{AssetAccessError, AssetRegistry, Assets, LoadingContext};
pub use self::progress::ProgressCounter;
pub use self::source::{AssetSource, DirectorySource};
pub use self::storage::{AssetAppender, AssetStorage};

/// Trait for assets. Should be implemented manually for each asset. Before the asset can be used,
/// it must be registered using [`AssetRegistry::register`] or
/// [`Game::register_asset`](crate::Game::register_asset).
pub trait Asset: Sized + Send + Sync + 'static {}

/// Create a set of assets
///
/// # Example
/// ```
/// engine::asset_set!(pub AssetSet {
///     pub skins: SkinSet = "main.skin",
///     pub block_registry: BlockRegistry = "blocks/blocks.json",
///     sky_texture: Image = "blocks/sky/sky.png",
/// });
/// ```
///
/// # Generated code
/// ```ignore
/// pub struct AssetSet {
///     pub skins: Handle<SkinSet>,
///     pub block_registry: Handle<SkinSet>,
///     sky_texture: Handle<Image>,
/// }
///
/// impl AssetSet {
///     pub fn new(assets: &Assets) -> Self { ... }
///     pub async fn wait(&self, assets: &Assets) { ... }
///     pub fn wait_sync(&self, assets: &Assets) { ... }
/// }
/// ```
#[macro_export]
macro_rules! asset_set {
    ($vis:vis $name:ident {
        $( $fvis:vis $field:ident: $ty:ty = $path:literal, )*
    }) => {
        #[derive(Clone, Debug)]
        $vis struct $name {
            $( $fvis $field: $crate::assets::Handle<$ty>, )*
        }

        impl $name {
            $vis fn new(assets: &$crate::assets::Assets) -> Self {
                Self {
                    $( $field: assets.load($path), )*
                }
            }

            $vis async fn wait(&self, assets: &$crate::assets::Assets) {
                $( assets.wait(&self.$field).await; )+
            }

            $vis fn wait_sync(&self, assets: &$crate::assets::Assets) {
                pollster::block_on(self.wait(assets));
            }
        }
    };
}
