use std::any::TypeId;
use std::fmt;
use std::future::Future;
use std::ops::{Deref, DerefMut};
use std::path::{Path, PathBuf};
use std::pin::Pin;
use std::sync::Arc;

use ahash::AHashMap;
use crossbeam::channel::{Receiver, Sender};
use eyre::Result;
use parking_lot::{MappedRwLockWriteGuard, RwLock, RwLockReadGuard, RwLockWriteGuard};
use tokio::runtime::Runtime;
use tokio::sync::watch;

use super::handle::{HandleAllocator, RawId, WeakHandle};
use super::progress::ProgressEvent;
use super::storage::{AssetAppender, AssetStorage};
use super::{Asset, AssetDefaultLoader, AssetLoader, AssetSource, Handle, Id, ProgressCounter};

/// Asset manager.
pub struct Assets {
    // avoid double indirection, even though PerTask has references to everything we need.
    storages: Arc<Storages>,
    allocator: Arc<HandleAllocator>,
    per_task: Arc<PerTask>,
    progress_receiver: Receiver<ProgressEvent>,
}

/// Access registry, used to register asset types.
#[derive(Default)]
pub struct AssetRegistry {
    storages: Storages,
}

type ReloaderFn = for<'a> fn(
    LoadingContext,
    &'a Path,
    handle: Handle<()>,
    storage_lock: &'a RwLock<Box<dyn StorageDyn>>,
) -> Pin<Box<dyn Future<Output = ()> + Send + 'a>>;

struct PerTask {
    storages: Arc<Storages>,
    reloaders: RwLock<AHashMap<WeakHandle<()>, ReloaderFn>>,
    allocator: Arc<HandleAllocator>,
    handle_map: HandleMap,
    scheduled_assets: ScheduledAssets,
    tokio_runtime: Runtime,
    source: Box<dyn AssetSource>,
    progress_sender: Sender<ProgressEvent>,
}

impl AssetRegistry {
    /// Register an asset type
    pub fn register<A: Asset>(&mut self) {
        let id = TypeId::of::<A>();
        let storage = AssetStorage::<A>::new();
        self.storages.0.insert(id, RwLock::new(Box::new(storage)));
    }

    /// Register an asset type with manual flushing. To manually flush new assets, call
    /// [`AssetStorage::flush`] or [`AssetStorage::flush_with`].
    pub fn register_with_flushing<A: Asset>(&mut self) {
        let id = TypeId::of::<A>();
        let storage = AssetStorage::<A>::new_with_flushing();
        self.storages.0.insert(id, RwLock::new(Box::new(storage)));
    }
}

impl Assets {
    /// Create a new asset manager, using the specified source and registry.
    ///
    /// After the asset manager is created, new asset types cannot be added.
    pub fn new<S: AssetSource>(source: S, registry: AssetRegistry) -> Result<Self> {
        let storages = Arc::new(registry.storages);
        let allocator = Arc::new(HandleAllocator::new());
        let tokio_runtime = tokio::runtime::Builder::new_multi_thread()
            .thread_name("assets")
            .max_blocking_threads(32)
            .build()?;

        let (progress_sender, progress_receiver) = crossbeam::channel::unbounded();

        let per_task = Arc::new(PerTask {
            storages: storages.clone(),
            reloaders: RwLock::default(),
            allocator: allocator.clone(),
            handle_map: HandleMap::default(),
            scheduled_assets: ScheduledAssets::default(),
            tokio_runtime,
            source: Box::new(source),
            progress_sender,
        });

        Ok(Self {
            storages,
            allocator,
            per_task,
            progress_receiver,
        })
    }

    /// Get handle to the progress counter.
    pub fn progress_counter(&self) -> ProgressCounter {
        ProgressCounter::new(self.progress_receiver.clone())
    }

    /// Lock an asset storage for reading.
    pub fn storage<A: Asset>(&self) -> impl Deref<Target = AssetStorage<A>> + '_ {
        self.storages.storage()
    }

    /// Lock an asset storage for writing.
    pub fn storage_mut<A: Asset>(&self) -> impl DerefMut<Target = AssetStorage<A>> + '_ {
        self.storages.storage_mut()
    }

    /// Get handle to the asset appender. Prefer this over calling [`append`](Assets::append) in a
    /// loop.
    pub fn appender<A: Asset>(&self) -> AssetAppender<A> {
        AssetAppender::new(self.storages.storage_mut(), self.allocator.clone())
    }

    /// Append a single asset.
    pub fn append<A: Asset>(&self, asset: A) -> Handle<A> {
        let storage = self.storage();
        let handle = storage.handle();
        let handle = self.allocator.alloc(handle.clone());
        self.storage_mut().insert(handle.id(), asset);
        handle
    }

    /// Schedule an asset for loading.
    pub fn load<A, P>(&self, path: P) -> Handle<A>
    where
        A: AssetDefaultLoader,
        P: Into<PathBuf>,
    {
        LoadingContext::new(self.per_task.clone()).load(path)
    }

    /// Schedule an asset for loading, using the specified asset loader.
    pub fn load_with<A, L, P>(&self, path: P) -> Handle<A>
    where
        A: Asset,
        L: AssetLoader<A>,
        P: Into<PathBuf>,
    {
        LoadingContext::new(self.per_task.clone()).load_with::<A, L, P>(path)
    }

    /// Get shared reference to an asset. The whole asset storage will be locked for reading.
    pub fn get<A: Asset>(
        &self,
        handle: &Handle<A>,
    ) -> Result<impl Deref<Target = A> + '_, AssetAccessError> {
        self.get_by_id(handle.id())
    }

    /// Get mutable reference to an asset. The whole asset storage will be locked for writing.
    pub fn get_mut<A: Asset>(
        &self,
        handle: &Handle<A>,
    ) -> Result<impl DerefMut<Target = A> + '_, AssetAccessError> {
        self.get_mut_by_id(handle.id())
    }

    /// Get shared reference to an asset.
    pub fn get_by_id<A: Asset>(
        &self,
        id: Id<A>,
    ) -> Result<impl Deref<Target = A> + '_, AssetAccessError> {
        self.storages.get_by_id(id)
    }

    /// Get mutable reference to an asset.
    pub fn get_mut_by_id<A: Asset>(
        &self,
        id: Id<A>,
    ) -> Result<impl DerefMut<Target = A> + '_, AssetAccessError> {
        self.storages.get_mut_by_id(id)
    }

    /// Wait until an asset is fully loaded.
    pub async fn wait<A: Asset>(&self, handle: &Handle<A>) {
        self.wait_by_id(handle.id()).await
    }

    /// Wait until an asset is fully loaded.
    pub async fn wait_by_id<A: Asset>(&self, id: Id<A>) {
        self.per_task.scheduled_assets.wait(id.raw()).await;
    }

    /// Wait until an asset is fully loaded, then retrieve a shared reference to it.
    pub async fn get_wait<A: Asset>(
        &self,
        handle: &Handle<A>,
    ) -> Result<impl Deref<Target = A> + '_, AssetAccessError> {
        self.get_by_id_wait(handle.id()).await
    }

    /// Wait until an asset is fully loaded, then retrieve a mutable reference to it.
    pub async fn get_mut_wait<A: Asset>(
        &self,
        handle: &Handle<A>,
    ) -> Result<impl DerefMut<Target = A> + '_, AssetAccessError> {
        self.get_mut_by_id_wait(handle.id()).await
    }

    /// Wait until an asset is fully loaded, then retrieve a shared reference to it.
    pub async fn get_by_id_wait<A: Asset>(
        &self,
        id: Id<A>,
    ) -> Result<impl Deref<Target = A> + '_, AssetAccessError> {
        self.per_task.scheduled_assets.wait(id.raw()).await;
        self.get_by_id(id)
    }

    /// Wait until an asset is fully loaded, then retrieve a mutable reference to it.
    pub async fn get_mut_by_id_wait<A: Asset>(
        &self,
        id: Id<A>,
    ) -> Result<impl DerefMut<Target = A> + '_, AssetAccessError> {
        self.per_task.scheduled_assets.wait(id.raw()).await;
        self.get_mut_by_id(id)
    }

    /// Check which assets have been changed and schedule them for reloading.
    pub fn hot_reload(&self) {
        let mut handles = Vec::new();
        let ctx = LoadingContext::new(self.per_task.clone());
        for path in self.per_task.source.changed_paths() {
            self.per_task.handle_map.collect(&path, &mut handles);
            for (type_id, handle) in handles.drain(..) {
                ctx.reload(type_id, &path, handle)
            }
        }
    }

    /// Remove dead assets.
    pub fn cleanup(&self) {
        for storage in self.storages.0.values() {
            storage.write().cleanup();
        }

        self.per_task.handle_map.cleanup();
    }
}

trait StorageDyn: fmt::Debug + Send + Sync {
    fn flush(&mut self);

    fn cleanup(&mut self);
}

impl<A: Asset> StorageDyn for AssetStorage<A> {
    fn flush(&mut self) {
        AssetStorage::flush(self)
    }

    fn cleanup(&mut self) {
        AssetStorage::cleanup(self)
    }
}

impl dyn StorageDyn {
    unsafe fn downcast_ref<A: Asset>(&self) -> &AssetStorage<A> {
        &*(self as *const dyn StorageDyn as *const AssetStorage<A>)
    }

    unsafe fn downcast_mut<A: Asset>(&mut self) -> &mut AssetStorage<A> {
        &mut *(self as *mut dyn StorageDyn as *mut AssetStorage<A>)
    }
}

#[derive(Default)]
struct Storages(AHashMap<TypeId, RwLock<Box<dyn StorageDyn>>>);

impl Storages {
    fn get_storage_lock_by_id(&self, id: TypeId) -> &RwLock<Box<dyn StorageDyn>> {
        self.0.get(&id).unwrap()
    }

    fn get_storage_lock<A: Asset>(&self) -> &RwLock<Box<dyn StorageDyn>> {
        let id = TypeId::of::<A>();
        match self.0.get(&id) {
            Some(v) => v,
            None => {
                panic!("asset {} is not registered", std::any::type_name::<A>());
            }
        }
    }

    fn storage<A: Asset>(&self) -> impl Deref<Target = AssetStorage<A>> + '_ {
        let lock = self.get_storage_lock::<A>().read();
        RwLockReadGuard::map(lock, |l| unsafe { l.downcast_ref() })
    }

    fn storage_mut<A: Asset>(&self) -> MappedRwLockWriteGuard<'_, AssetStorage<A>> {
        let lock = self.get_storage_lock::<A>().write();
        RwLockWriteGuard::map(lock, |l| unsafe { l.downcast_mut() })
    }

    fn get_by_id<A: Asset>(
        &self,
        asset_id: Id<A>,
    ) -> Result<impl Deref<Target = A> + '_, AssetAccessError> {
        let lock = self.get_storage_lock::<A>().read();
        RwLockReadGuard::try_map(lock, |l| unsafe {
            l.downcast_ref().get_by_id(asset_id).ok()
        })
        .map_err(|_| AssetAccessError::NotFound {
            type_name: std::any::type_name::<A>(),
            id: asset_id.raw(),
        })
    }

    fn get_mut_by_id<A: Asset>(
        &self,
        asset_id: Id<A>,
    ) -> Result<impl DerefMut<Target = A> + '_, AssetAccessError> {
        let lock = self.get_storage_lock::<A>().write();
        RwLockWriteGuard::try_map(lock, |l| unsafe {
            l.downcast_mut().get_mut_by_id(asset_id).ok()
        })
        .map_err(|_| AssetAccessError::NotFound {
            type_name: std::any::type_name::<A>(),
            id: asset_id.raw(),
        })
    }
}

#[derive(Default)]
struct HandleMap {
    handles: RwLock<AHashMap<PathBuf, AHashMap<TypeId, WeakHandle<()>>>>,
}

impl HandleMap {
    fn collect(&self, path: &Path, vec: &mut Vec<(TypeId, Handle<()>)>) {
        let handles = self.handles.read();
        vec.extend(handles.get(path).into_iter().flat_map(|handles| {
            handles
                .iter()
                .flat_map(|(id, weak)| weak.upgrade().map(|strong| (*id, strong)))
        }));
    }

    fn get<A: Asset>(&self, path: &Path) -> Option<Handle<A>> {
        let handles = self.handles.read();

        handles
            .get(path)
            .and_then(|handles| handles.get(&TypeId::of::<A>()))
            .and_then(|weak| weak.upgrade())
            .map(|h| unsafe { h.transmute() })
    }

    fn insert<A: Asset>(&self, path: PathBuf, handle: &Handle<A>) {
        let mut handles = self.handles.write();
        let handle = unsafe { handle.downgrade().transmute() };
        handles
            .entry(path)
            .or_default()
            .insert(TypeId::of::<A>(), handle);
    }

    fn cleanup(&self) {
        let mut handles = self.handles.write();
        for handles in handles.values_mut() {
            handles.retain(|_, weak| weak.upgrade().is_some());
        }
        handles.retain(|_, handles| !handles.is_empty());
    }
}

#[derive(Default)]
struct ScheduledAssets {
    map: RwLock<AHashMap<RawId, watch::Receiver<()>>>,
}

impl ScheduledAssets {
    fn begin(&self, id: RawId) -> watch::Sender<()> {
        let (sender, receiver) = watch::channel(());
        self.map.write().insert(id, receiver);
        sender
    }

    async fn wait(&self, id: RawId) {
        let mut receiver = {
            let map = self.map.read();
            match map.get(&id) {
                Some(v) => v.clone(),
                None => return,
            }
        };

        let _ = receiver.changed().await;
    }

    fn end(&self, id: RawId, sender: watch::Sender<()>) {
        let _ = sender.send(());
        self.map.write().remove(&id);
    }
}

/// Context passed to [`AssetLoader::load`] function.
pub struct LoadingContext {
    data: Arc<PerTask>,
}

impl LoadingContext {
    fn new(data: Arc<PerTask>) -> LoadingContext {
        LoadingContext { data }
    }

    /// Read bytes from the specified path.
    pub fn read_bytes(&self, path: impl AsRef<Path>) -> Result<Vec<u8>> {
        self.data.source.read_bytes(path.as_ref())
    }

    /// Read string from the specified path.
    pub fn read_string(&self, path: impl AsRef<Path>) -> Result<String> {
        self.data.source.read_string(path.as_ref())
    }

    /// Schedule an asset for loading.
    pub fn load<A, P>(&self, path: P) -> Handle<A>
    where
        A: AssetDefaultLoader,
        P: Into<PathBuf>,
    {
        self.load_inner::<A, A::DefaultLoader>(path.into())
    }

    /// Schedule an asset for loading, using the specified asset loader.
    pub fn load_with<A, L, P>(&self, path: P) -> Handle<A>
    where
        A: Asset,
        L: AssetLoader<A>,
        P: Into<PathBuf>,
    {
        self.load_inner::<A, L>(path.into())
    }

    fn load_inner<A, L>(&self, path: PathBuf) -> Handle<A>
    where
        A: Asset,
        L: AssetLoader<A>,
    {
        match self.data.handle_map.get::<A>(&path) {
            Some(v) => v,
            None => self.load_spawn::<A, L>(path),
        }
    }

    fn load_spawn<A, L>(&self, path: PathBuf) -> Handle<A>
    where
        A: Asset,
        L: AssetLoader<A>,
    {
        let event = ProgressEvent::Scheduled;
        let _ = self.data.progress_sender.send(event);

        let storage_handle = self.data.storages.storage::<A>().handle().clone();
        let asset_handle = self.data.allocator.alloc(storage_handle.clone());
        let asset_id = asset_handle.id();
        self.data.handle_map.insert(path.clone(), &asset_handle);

        let data = self.data.clone();
        let notifier = data.scheduled_assets.begin(asset_id.raw());

        {
            let reloader: ReloaderFn = |ctx, path, handle, storage_lock| {
                Box::pin(async move {
                    let handle = unsafe { handle.transmute::<A>() };

                    log::trace!("Reloading asset {}", path.display());
                    match L::load(ctx, path).await {
                        Ok(asset) => {
                            let mut storage = storage_lock.write();
                            let storage = unsafe { storage.downcast_mut::<A>() };
                            storage.insert(handle.id(), asset);
                            storage.handle().append_asset(handle.id());
                            log::trace!("Reloaded asset {}", path.display());
                        }
                        Err(error) => {
                            let error = error.wrap_err(format!(
                                "Failed to loat asset `{}` of type {}",
                                path.display(),
                                std::any::type_name::<A>()
                            ));
                            log::error!("{:?}\n", error);
                        }
                    }
                })
            };

            let mut reloaders = data.reloaders.write();
            let handle = unsafe { asset_handle.downgrade().transmute() };
            reloaders.insert(handle, reloader);
        }

        self.data.tokio_runtime.spawn(async move {
            let asset_ty = TypeId::of::<A>();
            let event = ProgressEvent::Started(asset_ty, path.clone());
            let _ = data.progress_sender.send(event);

            let ctx = LoadingContext::new(data.clone());
            log::trace!("Loading asset {}", path.display());
            match L::load(ctx, &path).await {
                Ok(asset) => {
                    data.storages.storage_mut().insert(asset_id, asset);
                    storage_handle.append_asset(asset_id);
                    log::trace!("Loaded asset {}", path.display());
                }
                Err(error) => {
                    let error = error.wrap_err(format!(
                        "Failed to loat asset `{}` of type {}",
                        path.display(),
                        std::any::type_name::<A>()
                    ));
                    log::error!("{:?}\n", error);
                    let event = ProgressEvent::Errored(asset_ty, path.clone(), error);
                    let _ = data.progress_sender.send(event);
                }
            };

            data.scheduled_assets.end(asset_id.raw(), notifier);

            let event = ProgressEvent::Finished(asset_ty, path);
            let _ = data.progress_sender.send(event);
        });

        asset_handle
    }

    fn reload(&self, type_id: TypeId, path: &Path, handle: Handle<()>) {
        let reloaders = self.data.reloaders.read();
        let reloader = match reloaders.get(&handle.downgrade()) {
            Some(v) => *v,
            None => return,
        };

        let data = self.data.clone();
        let path = path.to_owned();
        self.data.tokio_runtime.spawn(async move {
            let storage_lock = data.storages.get_storage_lock_by_id(type_id);
            let ctx = LoadingContext::new(data.clone());
            reloader(ctx, &path, handle, storage_lock).await;
        });
    }

    /// Wait until an asset is fully loaded, then retrieve a shared reference to it.
    pub async fn get<A: Asset>(
        &self,
        handle: &Handle<A>,
    ) -> Result<impl Deref<Target = A> + '_, AssetAccessError> {
        self.get_by_id(handle.id()).await
    }

    /// Wait until an asset is fully loaded, then retrieve a mutable reference to it.
    pub async fn get_mut<A: Asset>(
        &self,
        handle: &Handle<A>,
    ) -> Result<impl DerefMut<Target = A> + '_, AssetAccessError> {
        self.get_mut_by_id(handle.id()).await
    }

    /// Wait until an asset is fully loaded, then retrieve a shared reference to it.
    pub async fn get_by_id<A: Asset>(
        &self,
        id: Id<A>,
    ) -> Result<impl Deref<Target = A> + '_, AssetAccessError> {
        self.data.scheduled_assets.wait(id.raw()).await;
        self.data.storages.get_by_id(id)
    }

    /// Wait until an asset is fully loaded, then retrieve a mutable reference to it.
    pub async fn get_mut_by_id<A: Asset>(
        &self,
        id: Id<A>,
    ) -> Result<impl DerefMut<Target = A> + '_, AssetAccessError> {
        self.data.scheduled_assets.wait(id.raw()).await;
        self.data.storages.get_mut_by_id(id)
    }
}

/// Access access error
#[derive(Debug, thiserror::Error)]
pub enum AssetAccessError {
    /// Asset not found
    #[error("Asset {type_name}({id:?}) not found")]
    NotFound {
        /// Asset type
        type_name: &'static str,
        /// Asset ID
        id: RawId,
    },
}
