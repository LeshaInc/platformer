use std::fmt;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::{Arc, Weak};

use super::storage::StorageHandle;

/// Raw asset ID, represented as a 32-bit integer
#[derive(Clone, Copy, Default, Eq, Hash, PartialEq)]
pub struct RawId(pub u32);

impl fmt::Debug for RawId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Typed asset ID. Represented the same way as a raw ID, but has compile-time type information.
#[repr(transparent)]
pub struct Id<A> {
    raw: RawId,
    _marker: PhantomData<fn(A)>,
}

impl<A> Id<A> {
    /// Create a new typed ID from a raw ID.
    pub fn new(raw: RawId) -> Id<A> {
        Id {
            raw,
            _marker: PhantomData,
        }
    }

    /// Get raw ID.
    pub fn raw(self) -> RawId {
        self.raw
    }
}

impl<A> Default for Id<A> {
    fn default() -> Id<A> {
        Id::new(RawId::default())
    }
}

impl<A> fmt::Debug for Id<A> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}({})", std::any::type_name::<A>(), self.raw.0)
    }
}

impl<A> Copy for Id<A> {}

impl<A> Clone for Id<A> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<A> Eq for Id<A> {}

impl<A> PartialEq for Id<A> {
    fn eq(&self, other: &Self) -> bool {
        self.raw == other.raw
    }
}

impl<A> Hash for Id<A> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.raw.hash(state);
    }
}

/// Reference counted asset handle. Automatically schedules assets for removal when reference
/// count reaches zero.
pub struct Handle<A> {
    inner: Arc<HandleInner<A>>,
}

impl<A> Handle<A> {
    fn new(id: Id<A>, storage: Arc<StorageHandle<A>>) -> Handle<A> {
        Handle {
            inner: Arc::new(HandleInner { id, storage }),
        }
    }

    pub(super) fn downgrade(&self) -> WeakHandle<A> {
        WeakHandle {
            inner: Arc::downgrade(&self.inner),
        }
    }

    /// Get the underlying asset ID.
    pub fn id(&self) -> Id<A> {
        self.inner.id
    }

    pub(super) unsafe fn transmute<B>(self) -> Handle<B> {
        std::mem::transmute(self)
    }
}

impl<A> fmt::Debug for Handle<A> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.id().fmt(f)
    }
}

impl<A> Clone for Handle<A> {
    fn clone(&self) -> Self {
        Self {
            inner: Arc::clone(&self.inner),
        }
    }
}

impl<A> Eq for Handle<A> {}

impl<A> PartialEq for Handle<A> {
    fn eq(&self, other: &Self) -> bool {
        // Don't compare by Id, because one Id will be mapped to at most one Handle
        Arc::ptr_eq(&self.inner, &other.inner)
    }
}

impl<A> Hash for Handle<A> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        // Hash impl should be compatible with PartialEq
        Arc::as_ptr(&self.inner).hash(state);
    }
}

pub struct WeakHandle<A> {
    inner: Weak<HandleInner<A>>,
}

impl<A> WeakHandle<A> {
    pub fn upgrade(&self) -> Option<Handle<A>> {
        Some(Handle {
            inner: self.inner.upgrade()?,
        })
    }

    pub(super) unsafe fn transmute<B>(self) -> WeakHandle<B> {
        std::mem::transmute(self)
    }
}

impl<A> Clone for WeakHandle<A> {
    fn clone(&self) -> Self {
        Self {
            inner: Weak::clone(&self.inner),
        }
    }
}

impl<A> Eq for WeakHandle<A> {}

impl<A> PartialEq for WeakHandle<A> {
    fn eq(&self, other: &Self) -> bool {
        Weak::ptr_eq(&self.inner, &other.inner)
    }
}

impl<A> Hash for WeakHandle<A> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        Weak::as_ptr(&self.inner).hash(state);
    }
}

struct HandleInner<A> {
    id: Id<A>,
    storage: Arc<StorageHandle<A>>,
}

impl<A> Drop for HandleInner<A> {
    fn drop(&mut self) {
        self.storage.drop_asset(self.id);
    }
}

#[derive(Debug, Default)]
pub struct HandleAllocator {
    counter: AtomicU32,
}

impl HandleAllocator {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn alloc<A>(&self, storage: Arc<StorageHandle<A>>) -> Handle<A> {
        let raw = self.alloc_raw();
        Handle::new(Id::new(raw), storage)
    }

    fn alloc_raw(&self) -> RawId {
        let id = self.counter.fetch_add(1, Ordering::SeqCst);
        if id == u32::MAX {
            panic!("reached max asset id");
        }

        RawId(id)
    }
}
