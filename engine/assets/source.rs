use std::path::{Path, PathBuf};
use std::sync::mpsc::{self, Receiver};
use std::time::Duration;

use eyre::Result;
use notify::{watcher, DebouncedEvent, RecommendedWatcher, RecursiveMode, Watcher};
use parking_lot::Mutex;

/// Asset source which provides raw asset data.
pub trait AssetSource: Send + Sync + 'static {
    /// Read bytes from the specified path.
    fn read_bytes(&self, path: &Path) -> Result<Vec<u8>>;

    /// Read string from the specified path.
    fn read_string(&self, path: &Path) -> Result<String>;

    /// Collect all changed paths for hot reloading. If hot reloading is not supported, an empty
    /// vector must be returned.
    fn changed_paths(&self) -> Vec<PathBuf>;
}

/// Directory asset source.
pub struct DirectorySource {
    root: PathBuf,
    _watcher: RecommendedWatcher,
    receiver: Mutex<Receiver<DebouncedEvent>>,
}

impl DirectorySource {
    /// Create a new directory source.
    pub fn new(root: impl Into<PathBuf>) -> Result<Self> {
        let root = root.into();
        let (sender, receiver) = mpsc::channel();
        let mut watcher = watcher(sender, Duration::from_secs(1))?;
        watcher.watch(&root, RecursiveMode::Recursive)?;
        Ok(Self {
            root,
            _watcher: watcher,
            receiver: Mutex::new(receiver),
        })
    }
}

impl AssetSource for DirectorySource {
    fn read_bytes(&self, path: &Path) -> Result<Vec<u8>> {
        Ok(std::fs::read(self.root.join(path))?)
    }

    fn read_string(&self, path: &Path) -> Result<String> {
        Ok(std::fs::read_to_string(self.root.join(path))?)
    }

    fn changed_paths(&self) -> Vec<PathBuf> {
        let receiver = self.receiver.lock();
        receiver
            .try_iter()
            .flat_map(|ev| match ev {
                DebouncedEvent::Write(path) | DebouncedEvent::Create(path) => {
                    path.strip_prefix(&self.root).map(|v| v.to_owned()).ok()
                }
                _ => None,
            })
            .collect()
    }
}
