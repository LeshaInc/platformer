use std::any::TypeId;
use std::path::{Path, PathBuf};

use ahash::RandomState;
use crossbeam::channel::Receiver;
use eyre::Error;
use linked_hash_set::LinkedHashSet;

#[derive(Debug)]
pub enum ProgressEvent {
    Scheduled,
    Started(TypeId, PathBuf),
    Finished(TypeId, PathBuf),
    Errored(TypeId, PathBuf, Error),
}

pub struct ProgressCounter {
    receiver: Receiver<ProgressEvent>,
    num_total: u32,
    loading: LinkedHashSet<(TypeId, PathBuf), RandomState>,
    num_finished: u32,
    errors: Vec<(TypeId, PathBuf, Error)>,
}

impl ProgressCounter {
    pub(super) fn new(receiver: Receiver<ProgressEvent>) -> Self {
        Self {
            receiver,
            num_total: 0,
            loading: LinkedHashSet::default(),
            num_finished: 0,
            errors: Vec::new(),
        }
    }

    pub fn reset(&mut self) {
        self.num_total = 0;
        self.loading.clear();
        self.num_finished = 0;
        self.errors.clear();
    }

    pub fn has_finished(&self) -> bool {
        self.num_finished == self.num_total
    }

    pub fn num_finished(&self) -> u32 {
        self.num_finished
    }

    pub fn num_total(&self) -> u32 {
        self.num_total
    }

    pub fn num_errored(&self) -> u32 {
        self.errors.len() as _
    }

    pub fn percentage(&self) -> f32 {
        if self.num_total == 0 {
            100.0
        } else {
            self.num_finished as f32 * 100.0 / self.num_total as f32
        }
    }

    pub fn currently_loading(&self) -> impl Iterator<Item = &Path> + '_ {
        self.loading.iter().map(|v| v.1.as_path())
    }

    pub fn update(&mut self) {
        while let Ok(event) = self.receiver.try_recv() {
            match event {
                ProgressEvent::Scheduled => self.handle_scheduled(),
                ProgressEvent::Started(ty, path) => self.handle_started(ty, path),
                ProgressEvent::Finished(ty, path) => self.handle_finished(ty, path),
                ProgressEvent::Errored(ty, path, error) => self.handle_errored(ty, path, error),
            }
        }
    }

    fn handle_scheduled(&mut self) {
        self.num_total += 1;
    }

    fn handle_started(&mut self, ty: TypeId, path: PathBuf) {
        self.loading.insert((ty, path));
    }

    fn handle_finished(&mut self, ty: TypeId, path: PathBuf) {
        self.loading.remove(&(ty, path));
        self.num_finished += 1;
    }

    fn handle_errored(&mut self, ty: TypeId, path: PathBuf, error: Error) {
        self.errors.push((ty, path, error));
    }
}
