//! Engine for 2D games

#![allow(clippy::or_fun_call)]
#![allow(clippy::too_many_arguments)]
#![allow(unused_unsafe)] // TODO

pub mod assets;
pub mod ecs;
pub mod fps_counter;
pub mod graphics;
pub mod input;
pub mod math;
pub mod profiler;
#[macro_use]
pub mod ui;

use std::time::{Duration, Instant};

use assets::AssetRegistry;
use eyre::{Result, WrapErr};
use winit::event::{Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Window, WindowBuilder};

use self::assets::{Asset, Assets, DirectorySource};
use self::ecs::Resource;
use self::graphics::{DrawList, Graphics};
use self::input::Input;
use self::math::Vec2D;
use crate::ecs::World;
use crate::fps_counter::FpsCounter;
use crate::graphics::{DebugDrawList, DrawEncoder};
use crate::input::InputMap;
use crate::profiler::Profiler;

/// A game state, such as loading state, menu state, playing state, etc.
///
/// Game states are stored in a stack, only the top state is run.
pub trait GameState: 'static {
    /// Called after the state is pushed into the stack.
    fn on_enter(&mut self, world: &mut World) -> Result<()> {
        let _ = world;
        Ok(())
    }

    /// Called 60 times per second (can be overridden by changing the [`DeltaTime`](crate::DeltaTime) resource),
    /// always before the [`on_draw`](crate::GameState::on_draw) call.
    fn on_update(&mut self, world: &mut World) -> Result<StateTransition> {
        let _ = world;
        Ok(StateTransition::Continue)
    }

    /// Called after a frame is available for drawing. Drawing commands must be submitted to the
    /// [`DrawList`](crate::graphics::DrawList). After this call, the commands will be processed
    /// by the graphics system and submitted to the GPU.
    fn on_draw(&mut self, world: &mut World) -> Result<()> {
        let _ = world;
        Ok(())
    }

    /// Called after the state is popped from the stack.
    fn on_leave(&mut self, world: &mut World) -> Result<()> {
        let _ = world;
        Ok(())
    }
}

/// A state transition.
pub enum StateTransition {
    /// Continue running the current state.
    Continue,
    /// Push a state into the stack.
    Push(Box<dyn GameState>),
    /// Pop the current state.
    Pop,
}

/// Game engine entry point.
pub struct Game {
    world: World,
    asset_registry: AssetRegistry,
}

/// Current tick number, increased after each [`GameState::on_update`] call
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Tick(pub u64);

/// Tick interpolation alpha, ranging from 0 to 1.
#[derive(Clone, Copy, Debug, Default, PartialEq, PartialOrd)]
pub struct TickAlpha(pub f32);

/// Tick delta time. Determines how often [`GameState::on_update`] should be called.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct DeltaTime(pub Duration);

impl Default for DeltaTime {
    fn default() -> Self {
        Self(Duration::from_nanos(16_666_667))
    }
}

/// Maximum time between two subsequent frames, after which the game logic will start to slow down.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct MaxFrameTime(pub Duration);

impl Default for MaxFrameTime {
    fn default() -> Self {
        Self(Duration::from_millis(50))
    }
}

/// Window physical resolution
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Resolution(pub Vec2D<u32>);

impl Game {
    /// Initialize the game engine.
    pub fn new() -> Game {
        let _ = color_eyre::config::HookBuilder::new()
            .add_frame_filter(Box::new(|frames| {
                let it = frames.iter().enumerate();
                let it = it.filter(|(_, frame)| {
                    let name = match frame.name.as_ref() {
                        Some(v) => v.as_str(),
                        _ => return false,
                    };

                    let path = name.trim_start_matches('<');
                    path.starts_with("engine") || path.starts_with("platformer")
                });
                let it = it.map(|(idx, _)| idx);
                let min = it.clone().min().unwrap_or(0);
                let max = it.max().unwrap_or(0);
                frames.drain(max..);
                frames.drain(..min);
            }))
            .install();
        let _ = env_logger::try_init();

        let mut asset_registry = AssetRegistry::default();
        Graphics::register_assets(&mut asset_registry);
        Game {
            world: World::default(),
            asset_registry,
        }
    }

    /// Register an asset type.
    pub fn register_asset<A: Asset>(&mut self) {
        self.asset_registry.register::<A>();
    }

    /// Insert a global resource.
    pub fn add_resource<R: Resource>(&mut self, resource: R) {
        self.world.resources.insert(resource);
    }

    /// Start the game, using the provided function to construct a new [`GameState`] instance.
    pub fn start<S: GameState>(self, factory: fn(&mut World) -> Result<S>) -> Result<()> {
        self.start_inner(Box::new(move |world| {
            factory(world).map(|s| Box::new(s) as Box<dyn GameState>)
        }))
    }

    #[allow(clippy::type_complexity)]
    fn start_inner(
        self,
        factory: Box<dyn FnOnce(&mut World) -> Result<Box<dyn GameState>>>,
    ) -> Result<()> {
        let mut world = self.world;

        let event_loop = EventLoop::new();
        let window = WindowBuilder::new().build(&event_loop)?;

        let mut exe_root = std::env::current_exe().wrap_err("Cannot get executable path")?;
        exe_root.pop();

        if option_env!("RELEASE").is_none() {
            exe_root.pop();
            exe_root.pop();
        }

        let asset_source = DirectorySource::new(exe_root.join("assets"))?;
        let assets = Assets::new(asset_source, self.asset_registry)?;

        let input_map = InputMap::open(&exe_root.join("input.json"))?;
        let input = Input::new(input_map, &window);

        let mut graphics = pollster::block_on(Graphics::new(&window, &assets))?;

        world.resources.insert(assets);
        world.resources.insert(input);
        world.resources.insert(window);
        world.resources.insert(Resolution(graphics.resolution()));
        world.resources.insert(DrawList::new());
        world.resources.insert(FpsCounter::new(60 * 5));

        let state = factory(&mut world)?;
        let mut stack = vec![state];

        let mut current_time = Instant::now();
        let mut time_accumulator = Duration::ZERO;
        let mut tick = 0;

        let mut handle_event = move |event: Event<()>, cf: &mut ControlFlow| -> Result<()> {
            let control_flow = cf;

            let mut input = world.resources.get_mut::<Input>();
            input.record_event(&event);
            drop(input);

            match event {
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => *control_flow = ControlFlow::Exit,
                Event::MainEventsCleared => {
                    let _guard = Profiler::get().time_guard(0, "frame");

                    let window = world.resources.get::<Window>();
                    let size = window.inner_size();
                    drop(window);

                    let new_resolution = Vec2D::new(size.width, size.height);
                    if graphics.resolution() != new_resolution {
                        graphics.resize(new_resolution);
                        world.resources.insert(Resolution(new_resolution));
                    }

                    let new_time = Instant::now();
                    let max_frame_time = world.resources.get_or_default::<MaxFrameTime>().0;
                    let frame_time = (new_time - current_time).min(max_frame_time);
                    current_time = new_time;
                    let mut fps_counter = world.resources.get_mut::<FpsCounter>();
                    fps_counter.add_sample(frame_time);
                    drop(fps_counter);

                    let update_delta: DeltaTime = *world.resources.get_or_default();

                    time_accumulator += frame_time;
                    while time_accumulator >= update_delta.0 {
                        world.resources.insert(Tick(tick));

                        let state = stack.last_mut().unwrap();
                        let trans = state.on_update(&mut world)?;
                        match trans {
                            StateTransition::Continue => {}
                            StateTransition::Pop => {
                                state.on_leave(&mut world)?;
                                stack.pop();
                                let state = stack.last_mut().unwrap();
                                state.on_enter(&mut world)?;
                            }
                            StateTransition::Push(mut state) => {
                                state.on_enter(&mut world)?;
                                stack.push(state);
                            }
                        }

                        let mut input = world.resources.get_mut::<Input>();
                        input.clear_events();

                        tick += 1;
                        time_accumulator -= update_delta.0;
                    }

                    let state = stack.last_mut().unwrap();
                    let alpha = time_accumulator.as_secs_f32() / update_delta.0.as_secs_f32();
                    world.resources.insert(TickAlpha(alpha));
                    state.on_draw(&mut world)?;

                    let _guard = Profiler::get().time_guard(9999, "draw_flush");

                    let assets = world.resources.get::<Assets>();
                    graphics.maintain(&assets);
                    assets.hot_reload();
                    assets.cleanup();

                    let mut draw_list = world.resources.get_mut::<DrawList>();
                    if let Some(mut debug) = DebugDrawList::try_get() {
                        debug.encode(&assets, &mut DrawEncoder::new(&mut draw_list));
                    }

                    graphics.draw(&assets, &draw_list)?;
                    draw_list.clear();

                    *control_flow = ControlFlow::Poll;
                }
                _ => {}
            }
            Ok(())
        };

        event_loop.run(move |event, _, control_flow| {
            if let Err(e) = handle_event(event, control_flow) {
                eprintln!("{:?}", e);
                *control_flow = ControlFlow::Exit;
            }
        })
    }
}

impl Default for Game {
    fn default() -> Self {
        Self::new()
    }
}
