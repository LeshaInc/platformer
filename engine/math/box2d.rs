use std::ops::{Add, Div, Mul, Sub};

use num_traits::{AsPrimitive, FromPrimitive, Zero};

use super::Vec2D;

#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Box2D<T> {
    pub min: Vec2D<T>,
    pub max: Vec2D<T>,
}

impl<T> Box2D<T> {
    pub fn new(min: Vec2D<T>, max: Vec2D<T>) -> Box2D<T> {
        Box2D { min, max }
    }

    pub fn zero() -> Box2D<T>
    where
        T: Zero,
    {
        Box2D::new(Vec2D::zero(), Vec2D::zero())
    }

    pub fn width(&self) -> T
    where
        T: Clone + Sub<Output = T>,
    {
        self.max.x.clone() - self.min.x.clone()
    }

    pub fn height(&self) -> T
    where
        T: Clone + Sub<Output = T>,
    {
        self.max.y.clone() - self.min.y.clone()
    }

    pub fn extents(&self) -> Vec2D<T>
    where
        T: Clone + Sub<Output = T>,
    {
        Vec2D::new(self.width(), self.height())
    }

    pub fn intersects(&self, other: &Box2D<T>) -> bool
    where
        T: PartialOrd,
    {
        (self.min.x <= other.max.x && self.max.x >= other.min.x)
            && (self.min.y <= other.max.y && self.max.y >= other.min.y)
    }

    // TODO: refactor methdos to take &self

    pub fn intersection(self, other: Box2D<T>) -> Box2D<T>
    where
        T: Clone + Ord,
    {
        let mut min = self.min.max(other.min);
        let mut max = self.max.min(other.max);
        min = min.min(max.clone());
        max = max.max(min.clone());
        Box2D::new(min, max)
    }

    pub fn area(self) -> T
    where
        T: Clone + Sub<Output = T> + Mul<Output = T>,
    {
        self.extents().prod()
    }

    pub fn points(self) -> [Vec2D<T>; 4]
    where
        T: Clone,
    {
        [
            self.min.clone(),
            Vec2D::new(self.min.x.clone(), self.max.y.clone()),
            self.max.clone(),
            Vec2D::new(self.max.x.clone(), self.min.y),
        ]
    }

    pub fn center(self) -> Vec2D<T>
    where
        T: Copy + Div<Output = T> + Add<Output = T> + FromPrimitive,
    {
        let two = T::from_u8(2).unwrap();
        (self.min + self.max) / two
    }

    pub fn translate(self, vector: Vec2D<T>) -> Box2D<T>
    where
        T: Clone + Add<Output = T>,
    {
        Box2D::new(self.min + vector.clone(), self.max + vector)
    }

    pub fn inflate(self, vector: Vec2D<T>) -> Box2D<T>
    where
        T: Add<Output = T> + Sub<Output = T> + Clone,
    {
        Box2D::new(self.min - vector.clone(), self.max + vector)
    }

    pub fn contains(&self, point: Vec2D<T>) -> bool
    where
        T: PartialOrd,
    {
        self.min.le(&point).all() && self.max.ge(&point).all()
    }

    pub fn cast<U>(self) -> Box2D<U>
    where
        U: Copy + 'static,
        T: AsPrimitive<U>,
    {
        Box2D::new(self.min.cast(), self.max.cast())
    }
}

impl Box2D<f32> {
    #[allow(clippy::collapsible_else_if)]
    pub fn extend_towards(self, vector: Vec2D<f32>) -> Box2D<f32> {
        if vector.x > 0.0 {
            if vector.y > 0.0 {
                Box2D::new(self.min, self.max + vector)
            } else {
                Box2D::new(
                    self.min + Vec2D::new(0.0, vector.y),
                    self.max + Vec2D::new(vector.x, 0.0),
                )
            }
        } else {
            if vector.y > 0.0 {
                Box2D::new(
                    self.min + Vec2D::new(vector.x, 0.0),
                    self.max + Vec2D::new(0.0, vector.y),
                )
            } else {
                Box2D::new(self.min + vector, self.max)
            }
        }
    }

    pub fn to_tiles(self, scale: f32, size: Vec2D<u32>) -> Box2D<u32> {
        Box2D::new(
            (self.min / scale).map(|v| v.floor().max(0.0) as u32),
            (self.max / scale)
                .map(|v| v.ceil() as u32)
                .min(size - Vec2D::splat(1)),
        )
    }

    pub fn minkowski_diff(self, other: Box2D<f32>) -> Box2D<f32> {
        Box2D::new(self.min - other.max, self.max - other.min)
    }

    pub fn closest_point_on_bounds(self, pt: Vec2D<f32>) -> Vec2D<f32> {
        let mut min_dist = (pt.x - self.min.x).abs();
        let mut bounds_pt = Vec2D::new(self.min.x, pt.y);
        if (self.max.x - pt.x).abs() < min_dist {
            min_dist = (self.max.x - pt.x).abs();
            bounds_pt = Vec2D::new(self.max.x, pt.y);
        }
        if (self.max.y - pt.y).abs() < min_dist {
            min_dist = (self.max.y - pt.y).abs();
            bounds_pt = Vec2D::new(pt.x, self.max.y);
        }
        if (self.min.y - pt.y).abs() < min_dist {
            bounds_pt = Vec2D::new(pt.x, self.min.y);
        }
        bounds_pt
    }
}
