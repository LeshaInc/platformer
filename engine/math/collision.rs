use super::{Box2D, Vec2D};

#[derive(Clone, Debug)]
pub struct Hit {
    pub time: f32,
    pub normal: Vec2D<f32>,
}

pub fn ray_vs_aabb(
    pos: Vec2D<f32>,
    dir: Vec2D<f32>,
    aabb: Box2D<f32>,
    margin: Vec2D<f32>,
) -> Option<Hit> {
    let scale = dir.map(|v| 1.0 / v);
    let sign = scale.map(|v| v.signum());

    let aabb_center = aabb.center();
    let aabb_half = aabb.extents() / 2.0;
    let near_time =
        (aabb_center - sign.elementwise_mul(aabb_half + margin) - pos).elementwise_mul(scale);
    let far_time =
        (aabb_center + sign.elementwise_mul(aabb_half + margin) - pos).elementwise_mul(scale);

    if near_time.x.is_nan() || near_time.y.is_nan() || far_time.x.is_nan() || far_time.y.is_nan() {
        return None;
    }

    if near_time.x > far_time.y || near_time.y > far_time.x {
        return None;
    }

    let near_time_max = near_time.x.max(near_time.y);
    let far_time_min = far_time.x.min(far_time.y);

    if near_time_max >= 1.0 || far_time_min <= 0.0 {
        return None;
    }

    let time = near_time_max.clamp(0.0, 1.0);

    Some(Hit {
        time,
        normal: if near_time.x > near_time.y {
            Vec2D::new(-sign.x, 0.0)
        } else {
            Vec2D::new(0.0, -sign.y)
        },
    })
}

pub fn aabb_vs_aabb_mtv(a_box: Box2D<f32>, b_box: Box2D<f32>) -> Option<Vec2D<f32>> {
    let diff = b_box.minkowski_diff(a_box);
    if diff.contains(Vec2D::zero()) {
        Some(diff.closest_point_on_bounds(Vec2D::zero()))
    } else {
        None
    }
}

pub fn aabb_vs_aabb_sweep(
    a_box: Box2D<f32>,
    a_delta: Vec2D<f32>,
    b_box: Box2D<f32>,
) -> Option<Hit> {
    if a_delta.x.abs() < 1e-5 && a_delta.y.abs() < 1e-5 {
        return None;
    }
    ray_vs_aabb(a_box.center(), a_delta, b_box, a_box.extents() / 2.0)
}
