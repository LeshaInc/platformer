use std::str::FromStr;

use super::{Box2D, Vec2D};

#[derive(Clone, Copy, Debug, Default, PartialEq, PartialOrd)]
pub struct Padding {
    pub top: f32,
    pub right: f32,
    pub bottom: f32,
    pub left: f32,
}

impl Padding {
    pub fn new(top: f32, right: f32, bottom: f32, left: f32) -> Padding {
        Padding {
            top,
            right,
            bottom,
            left,
        }
    }

    pub fn new_equal(v: f32) -> Padding {
        Padding::new(v, v, v, v)
    }

    pub fn offset(self) -> Vec2D<f32> {
        Vec2D::new(self.left, self.top)
    }

    pub fn extents(self) -> Vec2D<f32> {
        Vec2D::new(self.left + self.right, self.top + self.bottom)
    }

    pub fn shrink_rect(&self, rect: &Box2D<f32>) -> Box2D<f32> {
        let min = rect.min + self.offset();
        let max = (rect.max - Vec2D::new(self.right, self.bottom)).fmax(min);
        Box2D::new(min, max)
    }
}

impl FromStr for Padding {
    type Err = ();

    fn from_str(mut s: &str) -> Result<Padding, ()> {
        if !s.starts_with("padding(") {
            return Err(());
        }

        s = &s[8..];

        if !s.ends_with(')') {
            return Err(());
        }

        s = &s[..s.len() - 1];

        let components = s.split(',').collect::<Vec<_>>();

        match components.len() {
            1 => {
                let pad = components[0].trim().parse().map_err(drop)?;
                Ok(Padding::new_equal(pad))
            }
            2 => {
                let vert = components[0].trim().parse().map_err(drop)?;
                let horiz = components[1].trim().parse().map_err(drop)?;
                Ok(Padding::new(vert, horiz, vert, horiz))
            }
            4 => {
                let top = components[0].trim().parse().map_err(drop)?;
                let right = components[1].trim().parse().map_err(drop)?;
                let bottom = components[2].trim().parse().map_err(drop)?;
                let left = components[3].trim().parse().map_err(drop)?;
                Ok(Padding::new(top, right, bottom, left))
            }
            _ => Err(()),
        }
    }
}
