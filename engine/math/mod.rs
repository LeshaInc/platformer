//! Math utilities

mod box2d;
pub mod collision;
mod noise;
mod padding;
mod rgba;
mod transform2d;
mod vec2d;

pub use self::box2d::Box2D;
pub use self::noise::{LayeredValueNoise1D, ValueNoise1D};
pub use self::padding::Padding;
pub use self::rgba::{Rgba, RgbaChannel};
pub use self::transform2d::{Rotation2D, Transform2D};
pub use self::vec2d::Vec2D;

pub fn smootherstep(x: f32) -> f32 {
    x * x * x * (x * (x * 6.0 - 15.0) + 10.0)
}

pub fn lerp(a: f32, b: f32, x: f32) -> f32 {
    a * (1.0 - x) + b * x
}

pub fn solve_quadratic(a: f32, b: f32, c: f32) -> Option<[f32; 2]> {
    let discriminant = b * b - 4.0 * a * c;
    if discriminant < 0.0 {
        return None;
    }

    let mut root0 = (-b - discriminant.sqrt()) / (2.0 * a);
    let mut root1 = (-b + discriminant.sqrt()) / (2.0 * a);

    if root0 > root1 {
        std::mem::swap(&mut root0, &mut root1);
    }

    Some([root0, root1])
}
