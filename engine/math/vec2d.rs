use std::iter::Sum;
use std::ops::{
    Add, AddAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Rem, Sub, SubAssign,
};

use num_traits::real::Real;
use num_traits::{
    AsPrimitive, CheckedAdd, CheckedSub, Float, MulAdd, One, SaturatingAdd, SaturatingSub, Zero,
};

/// 2D vector.
#[repr(C)]
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Vec2D<T> {
    /// X coordinate.
    pub x: T,
    /// Y coordinate.
    pub y: T,
}

impl<T> Vec2D<T> {
    // Constructors

    /// Create a new vector.
    pub const fn new(x: T, y: T) -> Vec2D<T> {
        Vec2D { x, y }
    }

    /// Create a new vector with `x = y`.
    pub fn splat(v: T) -> Vec2D<T>
    where
        T: Copy,
    {
        Vec2D::new(v, v)
    }

    /// Create a new vector with `x = y = 0`.
    pub fn zero() -> Vec2D<T>
    where
        T: Zero,
    {
        Vec2D::new(T::zero(), T::zero())
    }

    /// Create a new vector with `x = y = 1`.
    pub fn one() -> Vec2D<T>
    where
        T: One,
    {
        Vec2D::new(T::one(), T::one())
    }

    /// Create a new vector with `x = 1` and `y = 0`.
    pub fn unit_x() -> Vec2D<T>
    where
        T: Zero + One,
    {
        Vec2D::new(T::one(), T::zero())
    }

    /// Create a new vector with `x = 0` and `y = 1`.
    pub fn unit_y() -> Vec2D<T>
    where
        T: Zero + One,
    {
        Vec2D::new(T::zero(), T::one())
    }

    // Generic mappings

    /// Map x and y with the specified function.
    pub fn map<U, F: FnMut(T) -> U>(self, mut f: F) -> Vec2D<U> {
        Vec2D::new(f(self.x), f(self.y))
    }

    /// Map x with the specified function.
    pub fn map_x<F: FnOnce(T) -> T>(self, f: F) -> Vec2D<T> {
        Vec2D::new(f(self.x), self.y)
    }

    /// Map y with the specified function.
    pub fn map_y<F: FnOnce(T) -> T>(self, f: F) -> Vec2D<T> {
        Vec2D::new(self.x, f(self.y))
    }

    /// Set x to a new value, returning the new vector.
    pub fn set_x(self, x: T) -> Vec2D<T> {
        self.map_x(|_| x)
    }

    /// Set y to a new value, returning the new vector.
    pub fn set_y(self, y: T) -> Vec2D<T> {
        self.map_y(|_| y)
    }

    // Type conversion

    /// Cast vector coordinate type.
    pub fn cast<U>(self) -> Vec2D<U>
    where
        U: Copy + 'static,
        T: AsPrimitive<U>,
    {
        Vec2D::new(self.x.as_(), self.y.as_())
    }

    // Unary operators

    /// Compute `x + y`.
    pub fn sum(self) -> T
    where
        T: Add<Output = T>,
    {
        self.x + self.y
    }

    /// Compute `x * y`.
    pub fn prod(self) -> T
    where
        T: Mul<Output = T>,
    {
        self.x * self.y
    }

    /// Compute squared vector magnitude.
    pub fn mag_squared(self) -> T
    where
        T: Mul<Output = T> + MulAdd<Output = T> + Copy,
    {
        self.x.mul_add(self.x, self.y * self.y)
    }

    /// Compute vector magnitude.
    pub fn mag(self) -> T
    where
        T: Real + MulAdd<Output = T>,
    {
        self.mag_squared().sqrt()
    }

    /// Normalize the vector.
    pub fn normalize(self) -> Vec2D<T>
    where
        T: Real + MulAdd<Output = T>,
    {
        let mag = self.mag();
        self / mag
    }

    /// Try to normalize the vector, returning None if it's a zero vector.
    pub fn try_normalize(self) -> Option<Vec2D<T>>
    where
        T: Real + MulAdd<Output = T>,
    {
        let mag_sq = self.mag_squared();
        if mag_sq < T::epsilon() {
            None
        } else {
            Some(self / mag_sq.sqrt())
        }
    }

    // Binary operators

    /// Subtract `other` from `self`, saturating on numeric bounds.
    pub fn saturating_sub(self, other: Vec2D<T>) -> Vec2D<T>
    where
        T: SaturatingSub,
    {
        let x = self.x.saturating_sub(&other.x);
        let y = self.y.saturating_sub(&other.y);
        Vec2D::new(x, y)
    }

    /// Add `other` to `self`, saturating on numeric bounds.
    pub fn saturating_add(self, other: Vec2D<T>) -> Vec2D<T>
    where
        T: SaturatingAdd,
    {
        let x = self.x.saturating_add(&other.x);
        let y = self.y.saturating_add(&other.y);
        Vec2D::new(x, y)
    }

    /// Add `other` to `self`, returning None in case of a numeric overflow.
    pub fn checked_add(self, other: Vec2D<T>) -> Option<Vec2D<T>>
    where
        T: CheckedAdd,
    {
        let x = self.x.checked_add(&other.x)?;
        let y = self.y.checked_add(&other.y)?;
        Some(Vec2D::new(x, y))
    }

    /// Subtract `other` to `self`, returning None in case of a numeric overflow.
    pub fn checked_sub(self, other: Vec2D<T>) -> Option<Vec2D<T>>
    where
        T: CheckedSub,
    {
        let x = self.x.checked_sub(&other.x)?;
        let y = self.y.checked_sub(&other.y)?;
        Some(Vec2D::new(x, y))
    }

    /// Add `other.x` to `self.x` and subtract `other.y` from `self.y`,
    /// returning None in case of a numeric overflow.
    pub fn checked_add_sub(self, other: Vec2D<T>) -> Option<Vec2D<T>>
    where
        T: CheckedAdd + CheckedSub,
    {
        let x = self.x.checked_add(&other.x)?;
        let y = self.y.checked_sub(&other.y)?;
        Some(Vec2D::new(x, y))
    }

    /// Add `other.y` to `self.y` and subtract `other.x` from `self.x`,
    /// returning None in case of a numeric overflow.
    pub fn checked_sub_add(self, other: Vec2D<T>) -> Option<Vec2D<T>>
    where
        T: CheckedAdd + CheckedSub,
    {
        let x = self.x.checked_sub(&other.x)?;
        let y = self.y.checked_add(&other.y)?;
        Some(Vec2D::new(x, y))
    }

    /// Multiply each `self` coordinate with the corresponding `other` coordinate.
    pub fn elementwise_mul(self, other: Vec2D<T>) -> Vec2D<T>
    where
        T: Mul<Output = T>,
    {
        Vec2D::new(self.x * other.x, self.y * other.y)
    }

    /// Divide each `self` coordinate by the corresponding `other` coordinate.
    pub fn elementwise_div(self, other: Vec2D<T>) -> Vec2D<T>
    where
        T: Div<Output = T>,
    {
        Vec2D::new(self.x / other.x, self.y / other.y)
    }

    /// Take the reminder of division of each `self` coordinate
    /// by the corresponding `other` coordinate.
    pub fn elementwise_rem(self, other: Vec2D<T>) -> Vec2D<T>
    where
        T: Rem<Output = T>,
    {
        Vec2D::new(self.x % other.x, self.y % other.y)
    }

    /// Compute the dot product.
    pub fn dot(self, other: Vec2D<T>) -> T
    where
        T: Mul<Output = T> + MulAdd<Output = T>,
    {
        self.x.mul_add(other.x, self.y * other.y)
    }

    /// Get vector perpendicular to this one
    pub fn perp(self) -> Vec2D<T>
    where
        T: Neg<Output = T>,
    {
        Vec2D::new(-self.y, self.x)
    }

    /// Compute the dot product of `self` and `other.perp()`.
    pub fn perp_dot(self, other: Vec2D<T>) -> T
    where
        T: Mul<Output = T> + Sub<Output = T>,
    {
        self.y * other.x - self.x * other.y
    }

    /// Compute elementwise minimum (infimum).
    pub fn min(self, other: Vec2D<T>) -> Vec2D<T>
    where
        T: Ord,
    {
        Vec2D::new(self.x.min(other.x), self.y.min(other.y))
    }

    /// Compute elementwise maximum (supremum).
    pub fn max(self, other: Vec2D<T>) -> Vec2D<T>
    where
        T: Ord,
    {
        Vec2D::new(self.x.max(other.x), self.y.max(other.y))
    }

    /// Compute floating-point elementwise minimum (infimum).
    pub fn fmin(self, other: Vec2D<T>) -> Vec2D<T>
    where
        T: Float,
    {
        Vec2D::new(self.x.min(other.x), self.y.min(other.y))
    }

    /// Compute floating-point elementwise maximum (supremum).
    pub fn fmax(self, other: Vec2D<T>) -> Vec2D<T>
    where
        T: Float,
    {
        Vec2D::new(self.x.max(other.x), self.y.max(other.y))
    }

    // Comparison operators

    /// Elementwise `==` operator.
    pub fn eq(&self, other: &Vec2D<T>) -> Vec2D<bool>
    where
        T: PartialEq,
    {
        Vec2D::new(self.x == other.x, self.y == other.y)
    }

    /// Elementwise `!=` operator.
    pub fn ne(&self, other: &Vec2D<T>) -> Vec2D<bool>
    where
        T: PartialEq,
    {
        Vec2D::new(self.x != other.x, self.y != other.y)
    }

    /// Elementwise `<` operator.
    pub fn lt(&self, other: &Vec2D<T>) -> Vec2D<bool>
    where
        T: PartialOrd,
    {
        Vec2D::new(self.x < other.x, self.y < other.y)
    }

    /// Elementwise `<=` operator.
    pub fn le(&self, other: &Vec2D<T>) -> Vec2D<bool>
    where
        T: PartialOrd,
    {
        Vec2D::new(self.x <= other.x, self.y <= other.y)
    }

    /// Elementwise `>` operator.
    pub fn gt(&self, other: &Vec2D<T>) -> Vec2D<bool>
    where
        T: PartialOrd,
    {
        Vec2D::new(self.x > other.x, self.y > other.y)
    }

    /// Elementwise `>=` operator.
    pub fn ge(&self, other: &Vec2D<T>) -> Vec2D<bool>
    where
        T: PartialOrd,
    {
        Vec2D::new(self.x >= other.x, self.y >= other.y)
    }
}

impl Vec2D<bool> {
    /// Compute `x && y`.
    pub fn all(self) -> bool {
        self.x && self.y
    }

    /// Compute `!x && !y`.
    pub fn none(self) -> bool {
        !self.x && !self.y
    }

    /// Compute `x || y`.
    pub fn any(self) -> bool {
        self.x || self.y
    }
}

impl Vec2D<u32> {
    /// Divide each `self` coordinate by the corresponding `other` coordinate,
    /// rounding up instead of down.
    pub fn elementwise_div_ceil(self, other: Vec2D<u32>) -> Vec2D<u32> {
        let x = self.x / other.x + (self.x % other.x != 0) as u32;
        let y = self.y / other.y + (self.y % other.y != 0) as u32;
        Vec2D::new(x, y)
    }
}

impl Vec2D<i32> {
    /// Four cardinal directions: positive x, positive y, negative x, negative y.
    pub const DIRECTIONS: [Vec2D<i32>; 4] = [
        Vec2D::new(1, 0),
        Vec2D::new(0, 1),
        Vec2D::new(-1, 0),
        Vec2D::new(0, -1),
    ];
}

impl<T: Add<Output = T>> Add for Vec2D<T> {
    type Output = Vec2D<T>;

    fn add(self, other: Vec2D<T>) -> Vec2D<T> {
        Vec2D::new(self.x + other.x, self.y + other.y)
    }
}

impl<T: AddAssign> AddAssign for Vec2D<T> {
    fn add_assign(&mut self, other: Vec2D<T>) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl<T: Sub<Output = T>> Sub for Vec2D<T> {
    type Output = Vec2D<T>;

    fn sub(self, other: Vec2D<T>) -> Vec2D<T> {
        Vec2D::new(self.x - other.x, self.y - other.y)
    }
}

impl<T: SubAssign> SubAssign for Vec2D<T> {
    fn sub_assign(&mut self, other: Vec2D<T>) {
        self.x -= other.x;
        self.y -= other.y;
    }
}

impl<T: Mul<Output = T> + Copy> Mul<T> for Vec2D<T> {
    type Output = Vec2D<T>;

    fn mul(self, scalar: T) -> Vec2D<T> {
        Vec2D::new(self.x * scalar, self.y * scalar)
    }
}

impl<T: MulAssign + Copy> MulAssign<T> for Vec2D<T> {
    fn mul_assign(&mut self, scalar: T) {
        self.x *= scalar;
        self.y *= scalar;
    }
}

impl<T: Div<Output = T> + Copy> Div<T> for Vec2D<T> {
    type Output = Vec2D<T>;

    fn div(self, scalar: T) -> Vec2D<T> {
        Vec2D::new(self.x / scalar, self.y / scalar)
    }
}

impl<T: DivAssign + Copy> DivAssign<T> for Vec2D<T> {
    fn div_assign(&mut self, scalar: T) {
        self.x /= scalar;
        self.y /= scalar;
    }
}

impl<T: Neg<Output = T>> Neg for Vec2D<T> {
    type Output = Vec2D<T>;

    fn neg(self) -> Vec2D<T> {
        Vec2D::new(-self.x, -self.y)
    }
}

impl<T> Mul<Vec2D<T>> for Vec2D<T>
where
    T: Mul<Output = T> + MulAdd<Output = T>,
{
    type Output = T;

    fn mul(self, other: Vec2D<T>) -> T {
        self.dot(other)
    }
}

impl<T> Index<usize> for Vec2D<T> {
    type Output = T;

    fn index(&self, idx: usize) -> &T {
        match idx {
            0 => &self.x,
            1 => &self.y,
            _ => panic!("index out of bounds for Vec2d"),
        }
    }
}

impl<T> IndexMut<usize> for Vec2D<T> {
    fn index_mut(&mut self, idx: usize) -> &mut T {
        match idx {
            0 => &mut self.x,
            1 => &mut self.y,
            _ => panic!("index out of bounds for Vec2d"),
        }
    }
}

impl<T> From<[T; 2]> for Vec2D<T> {
    fn from([x, y]: [T; 2]) -> Vec2D<T> {
        Vec2D::new(x, y)
    }
}

impl<T> From<Vec2D<T>> for [T; 2] {
    fn from(vec: Vec2D<T>) -> [T; 2] {
        [vec.x, vec.y]
    }
}

impl<T> Sum for Vec2D<T>
where
    T: AddAssign + Zero,
{
    fn sum<I: Iterator<Item = Vec2D<T>>>(iter: I) -> Vec2D<T> {
        let mut sum = Vec2D::zero();
        for val in iter {
            sum += val;
        }
        sum
    }
}
