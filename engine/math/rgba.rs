use std::ops::{Add, AddAssign, Div, Index, IndexMut, Mul, Sub, SubAssign};
use std::str::FromStr;

use num_traits::{AsPrimitive, SaturatingAdd};

#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq, PartialOrd)]
pub struct Rgba<T = f32> {
    pub r: T,
    pub g: T,
    pub b: T,
    pub a: T,
}

impl Rgba {
    pub const WHITE: Rgba = Rgba::new(1.0, 1.0, 1.0, 1.0);
    pub const BLACK: Rgba = Rgba::new(0.0, 0.0, 0.0, 1.0);
    pub const RED: Rgba = Rgba::new(1.0, 0.0, 0.0, 1.0);
    pub const GREEN: Rgba = Rgba::new(0.0, 1.0, 0.0, 1.0);
    pub const BLUE: Rgba = Rgba::new(0.0, 0.0, 1.0, 1.0);
}

impl<T> Rgba<T> {
    pub const fn new(r: T, g: T, b: T, a: T) -> Rgba<T> {
        Rgba { r, g, b, a }
    }

    pub fn splat(v: T) -> Rgba<T>
    where
        T: Copy,
    {
        Rgba::new(v, v, v, v)
    }

    pub fn saturating_add(self, other: Rgba<T>) -> Rgba<T>
    where
        T: SaturatingAdd,
    {
        Rgba::new(
            self.r.saturating_add(&other.r),
            self.g.saturating_add(&other.g),
            self.b.saturating_add(&other.b),
            self.a.saturating_add(&other.a),
        )
    }

    pub fn cast<U>(self) -> Rgba<U>
    where
        U: Copy + 'static,
        T: AsPrimitive<U>,
    {
        Rgba::new(self.r.as_(), self.g.as_(), self.b.as_(), self.a.as_())
    }
}

impl Rgba {
    pub fn luma(self) -> f32 {
        0.2126 * self.r * self.r + 0.7152 * self.g * self.g + 0.0722 * self.b * self.b
    }

    pub fn is_dark(self) -> bool {
        self.luma() < 0.5
    }

    fn parse_hex(input: &str) -> Result<Rgba, ()> {
        if input[1..].starts_with('+') {
            return Err(());
        }

        if input.len() == 7 {
            // #RRGGBB
            let col = u32::from_str_radix(&input[1..], 16).map_err(drop)?;
            let r = (col >> 16) as f32 / 255.0;
            let g = (col >> 8 & 0xFF) as f32 / 255.0;
            let b = (col & 0xFF) as f32 / 255.0;
            Ok(Rgba::new(r, g, b, 1.0))
        } else if input.len() == 9 {
            // #RRGGBBAA
            let col = u32::from_str_radix(&input[1..], 16).map_err(drop)?;
            let r = (col >> 24) as f32 / 255.0;
            let g = (col >> 16 & 0xFF) as f32 / 255.0;
            let b = (col >> 8 & 0xFF) as f32 / 255.0;
            let a = (col & 0xFF) as f32 / 255.0;
            Ok(Rgba::new(r, g, b, a))
        } else {
            Err(())
        }
    }

    fn parse_rgba(mut input: &str) -> Result<Rgba, ()> {
        input = &input[5..]; // strip 'rgba('
        if !input.ends_with(')') {
            return Err(());
        }

        input = &input[..input.len() - 1];
        let mut split = input.splitn(4, ',');

        let r = split.next().ok_or(())?.parse::<f32>().map_err(drop)?;
        let g = split.next().ok_or(())?.parse::<f32>().map_err(drop)?;
        let b = split.next().ok_or(())?.parse::<f32>().map_err(drop)?;
        let a = split.next().ok_or(())?.parse::<f32>().map_err(drop)?;

        Ok(Rgba::new(r, g, b, a))
    }

    fn parse_rgb(mut input: &str) -> Result<Rgba, ()> {
        input = &input[4..]; // strip 'rgb('
        if !input.ends_with(')') {
            return Err(());
        }

        input = &input[..input.len() - 1];
        let mut split = input.splitn(3, ',');

        let r = split.next().ok_or(())?.parse::<f32>().map_err(drop)?;
        let g = split.next().ok_or(())?.parse::<f32>().map_err(drop)?;
        let b = split.next().ok_or(())?.parse::<f32>().map_err(drop)?;
        let a = split.next().ok_or(())?.parse::<f32>().map_err(drop)?;

        Ok(Rgba::new(r, g, b, a))
    }
}

impl<T> From<Rgba<T>> for [T; 4] {
    fn from(v: Rgba<T>) -> [T; 4] {
        [v.r, v.g, v.b, v.a]
    }
}

impl<T> From<[T; 4]> for Rgba<T> {
    fn from([r, g, b, a]: [T; 4]) -> Rgba<T> {
        Rgba::new(r, g, b, a)
    }
}

impl<T> Add for Rgba<T>
where
    T: Add<Output = T>,
{
    type Output = Rgba<T>;

    fn add(self, other: Rgba<T>) -> Rgba<T> {
        Rgba::new(
            self.r + other.r,
            self.g + other.g,
            self.b + other.b,
            self.a + other.a,
        )
    }
}

impl<T> AddAssign for Rgba<T>
where
    T: AddAssign,
{
    fn add_assign(&mut self, other: Rgba<T>) {
        self.r += other.r;
        self.g += other.g;
        self.b += other.b;
        self.a += other.a;
    }
}

impl<T> Sub for Rgba<T>
where
    T: Sub<Output = T>,
{
    type Output = Rgba<T>;

    fn sub(self, other: Rgba<T>) -> Rgba<T> {
        Rgba::new(
            self.r - other.r,
            self.g - other.g,
            self.b - other.b,
            self.a - other.a,
        )
    }
}

impl<T> SubAssign for Rgba<T>
where
    T: SubAssign,
{
    fn sub_assign(&mut self, other: Rgba<T>) {
        self.r -= other.r;
        self.g -= other.g;
        self.b -= other.b;
        self.a -= other.a;
    }
}

impl<T> Div<T> for Rgba<T>
where
    T: Div<Output = T> + Clone,
{
    type Output = Rgba<T>;

    fn div(self, other: T) -> Rgba<T> {
        Rgba::new(
            self.r / other.clone(),
            self.g / other.clone(),
            self.b / other.clone(),
            self.a / other,
        )
    }
}

impl<T> Mul<T> for Rgba<T>
where
    T: Mul<Output = T> + Clone,
{
    type Output = Rgba<T>;

    fn mul(self, other: T) -> Rgba<T> {
        Rgba::new(
            self.r * other.clone(),
            self.g * other.clone(),
            self.b * other.clone(),
            self.a * other,
        )
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum RgbaChannel {
    R,
    G,
    B,
    A,
}

impl RgbaChannel {
    pub const CHANNELS: [RgbaChannel; 4] = [
        RgbaChannel::R,
        RgbaChannel::G,
        RgbaChannel::B,
        RgbaChannel::A,
    ];
}

impl<T> Index<usize> for Rgba<T> {
    type Output = T;

    fn index(&self, idx: usize) -> &T {
        match idx {
            0 => &self.r,
            1 => &self.g,
            2 => &self.b,
            3 => &self.a,
            _ => panic!("index out of bounds"),
        }
    }
}

impl<T> IndexMut<usize> for Rgba<T> {
    fn index_mut(&mut self, idx: usize) -> &mut T {
        match idx {
            0 => &mut self.r,
            1 => &mut self.g,
            2 => &mut self.b,
            3 => &mut self.a,
            _ => panic!("index out of bounds"),
        }
    }
}

impl<T> Index<RgbaChannel> for Rgba<T> {
    type Output = T;

    fn index(&self, channel: RgbaChannel) -> &T {
        match channel {
            RgbaChannel::R => &self.r,
            RgbaChannel::G => &self.g,
            RgbaChannel::B => &self.b,
            RgbaChannel::A => &self.a,
        }
    }
}

impl<T> IndexMut<RgbaChannel> for Rgba<T> {
    fn index_mut(&mut self, channel: RgbaChannel) -> &mut T {
        match channel {
            RgbaChannel::R => &mut self.r,
            RgbaChannel::G => &mut self.g,
            RgbaChannel::B => &mut self.b,
            RgbaChannel::A => &mut self.a,
        }
    }
}

impl FromStr for Rgba {
    type Err = ();

    fn from_str(s: &str) -> Result<Rgba, ()> {
        if s.starts_with('#') {
            Rgba::parse_hex(s)
        } else if s.starts_with("rgba(") {
            Rgba::parse_rgba(s)
        } else if s.starts_with("rgb(") {
            Rgba::parse_rgb(s)
        } else {
            Err(())
        }
    }
}
