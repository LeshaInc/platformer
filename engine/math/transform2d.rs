use std::ops::Mul;

use super::Vec2D;

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Rotation2D {
    cos: f32,
    sin: f32,
}

impl Rotation2D {
    pub fn new(cos: f32, sin: f32) -> Rotation2D {
        Rotation2D { cos, sin }
    }

    pub fn degrees(deg: f32) -> Rotation2D {
        let (sin, cos) = deg.to_radians().sin_cos();
        Rotation2D::new(cos, sin)
    }

    pub fn radians(rad: f32) -> Rotation2D {
        let (sin, cos) = rad.sin_cos();
        Rotation2D::new(cos, sin)
    }

    pub fn unit_x(&self) -> Vec2D<f32> {
        Vec2D::new(self.cos, self.sin)
    }

    pub fn unit_y(&self) -> Vec2D<f32> {
        Vec2D::new(-self.sin, self.cos)
    }

    pub fn rotate(&self, point: Vec2D<f32>) -> Vec2D<f32> {
        Vec2D::new(
            point.x * self.cos - point.y * self.sin,
            point.x * self.sin + point.y * self.cos,
        )
    }

    pub fn conj(&self) -> Rotation2D {
        Rotation2D::new(self.cos, -self.sin)
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Transform2D {
    pub row0: [f32; 3],
    pub row1: [f32; 3],
}

impl Default for Transform2D {
    fn default() -> Transform2D {
        Transform2D::identity()
    }
}

impl Transform2D {
    pub fn identity() -> Transform2D {
        Transform2D {
            row0: [1.0, 0.0, 0.0],
            row1: [0.0, 1.0, 0.0],
        }
    }

    pub fn translation(vector: Vec2D<f32>) -> Transform2D {
        Transform2D {
            row0: [1.0, 0.0, vector.x],
            row1: [0.0, 1.0, vector.y],
        }
    }

    pub fn scaling(vector: Vec2D<f32>) -> Transform2D {
        Transform2D {
            row0: [vector.x, 0.0, 0.0],
            row1: [0.0, vector.y, 0.0],
        }
    }

    pub fn rotation(rotation: Rotation2D) -> Transform2D {
        Transform2D {
            row0: [rotation.cos, -rotation.sin, 0.0],
            row1: [rotation.sin, rotation.cos, 0.0],
        }
    }

    pub fn post_translate(&mut self, vector: Vec2D<f32>) {
        self.row0[2] += vector.x;
        self.row1[2] += vector.y;
    }

    pub fn post_scale(&mut self, scale: Vec2D<f32>) {
        self.row0[0] *= scale.x;
        self.row0[1] *= scale.x;
        self.row0[2] *= scale.x;
        self.row1[0] *= scale.y;
        self.row1[1] *= scale.y;
        self.row1[2] *= scale.y;
    }

    pub fn post_rotate(&mut self, rotation: Rotation2D) {
        *self = Transform2D {
            row0: [
                rotation.cos * self.row0[0] - rotation.sin * self.row1[0],
                rotation.cos * self.row0[1] - rotation.sin * self.row1[1],
                rotation.cos * self.row0[2] - rotation.sin * self.row1[2],
            ],
            row1: [
                rotation.sin * self.row0[0] + rotation.cos * self.row1[0],
                rotation.sin * self.row0[1] + rotation.cos * self.row1[1],
                rotation.sin * self.row0[2] + rotation.cos * self.row1[2],
            ],
        }
    }

    pub fn transform_point(&self, point: Vec2D<f32>) -> Vec2D<f32> {
        Vec2D::new(
            self.row0[0] * point.x + self.row0[1] * point.y + self.row0[2],
            self.row1[0] * point.x + self.row1[1] * point.y + self.row1[2],
        )
    }
}

impl Mul for Transform2D {
    type Output = Transform2D;

    #[allow(clippy::suspicious_operation_groupings)]
    fn mul(self, other: Self) -> Transform2D {
        let a = self;
        let b = other;
        Transform2D {
            row0: [
                a.row0[0] * b.row0[0] + a.row0[1] * b.row1[0],
                a.row0[0] * b.row0[1] + a.row0[1] * b.row1[1],
                a.row0[0] * b.row0[2] + a.row0[1] * b.row1[2] + a.row0[2],
            ],
            row1: [
                a.row1[0] * b.row0[0] + a.row1[1] * b.row1[0],
                a.row1[0] * b.row0[1] + a.row1[1] * b.row1[1],
                a.row1[0] * b.row0[2] + a.row1[1] * b.row1[2] + a.row1[2],
            ],
        }
    }
}
