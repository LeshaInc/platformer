use rand::Rng;

use crate::math::{lerp, smootherstep};

pub struct ValueNoise1D {
    values: Vec<f32>,
}

impl ValueNoise1D {
    pub fn new<R: Rng>(rng: &mut R, size: u32) -> ValueNoise1D {
        ValueNoise1D {
            values: (0..size).map(|_| rng.gen()).collect(),
        }
    }

    pub fn size(&self) -> u32 {
        self.values.len() as _
    }

    pub fn sample(&self, pos: f32) -> f32 {
        if self.values.is_empty() {
            return 0.0;
        }

        let fract = pos.fract();
        let pos = (pos as usize).clamp(0, self.values.len() - 1);
        if fract < 1e-3 || pos == self.values.len() - 1 {
            return self.values[pos];
        }

        let a = self.values[pos];
        let b = self.values[pos + 1];
        lerp(a, b, smootherstep(fract))
    }
}

pub struct LayeredValueNoise1D {
    noises: Vec<ValueNoise1D>,
}

impl LayeredValueNoise1D {
    pub fn new<R: Rng>(rng: &mut R, size: u32, octaves: u32) -> LayeredValueNoise1D {
        assert!(octaves > 0);
        let size = size.next_power_of_two().max(1 << (octaves - 1));
        LayeredValueNoise1D {
            noises: (0..octaves)
                .map(|v| ValueNoise1D::new(rng, size / (1 << v)))
                .collect(),
        }
    }

    pub fn size(&self) -> u32 {
        self.noises[0].size()
    }

    pub fn sample(&self, pos: f32) -> f32 {
        let mut res = 0.0;
        let mut range = 0.0;

        for (i, noise) in self.noises.iter().enumerate() {
            let scale = 1.0 / (1 << i) as f32;
            let amplitude = 1.0 / (1 << (self.noises.len() - i)) as f32;
            range += amplitude;
            res += noise.sample(pos * scale) * amplitude;
        }

        res / range
    }
}
