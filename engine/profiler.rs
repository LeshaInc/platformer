use std::collections::VecDeque;
use std::fmt::Write;
use std::time::{Duration, Instant};

use ahash::AHashMap;
use once_cell::sync::Lazy;
use parking_lot::RwLock;

static INSTANCE: Lazy<Profiler> = Lazy::new(Profiler::default);

const MAX_SAMPLES: usize = 600;

#[derive(Debug, Default)]
pub struct Profiler {
    time_entries: RwLock<AHashMap<(i32, &'static str), TimeEntry>>,
}

#[derive(Debug, Default)]
struct TimeEntry {
    samples: VecDeque<Duration>,
    last: Duration,
}

#[derive(Debug)]
pub struct TimeGuard<'a> {
    profiler: &'a Profiler,
    idx: i32,
    name: &'static str,
    instant: Instant,
}

impl Profiler {
    pub fn get() -> &'static Profiler {
        &*INSTANCE
    }

    pub fn time_guard(&self, idx: i32, name: &'static str) -> TimeGuard<'_> {
        TimeGuard {
            profiler: self,
            idx,
            name,
            instant: Instant::now(),
        }
    }

    pub fn report(&self) -> String {
        let mut report = String::new();

        let time_entries = self.time_entries.read();

        let mut order = time_entries.keys().copied().collect::<Vec<_>>();
        order.sort_unstable();

        let max_len = order.iter().map(|k| k.1.len()).max().unwrap_or(0);

        let _ = writeln!(
            &mut report,
            "{:<6$} | {:>8} | {:>8} | {:>8} | {:>8} | {:>8}",
            "name", "last", "avg", "5%", "50%", "95%", max_len
        );

        for key in order {
            let entry = time_entries.get(&key).unwrap();
            let mut samples = entry.samples.iter().copied().collect::<Vec<_>>();

            let idx = samples.len() / 2;
            let p50 = *samples.select_nth_unstable(idx).1;

            let idx = entry.samples.len() * 5 / 100;
            let p05 = *samples.select_nth_unstable(idx).1;

            let idx = entry.samples.len() * 95 / 100;
            let p95 = *samples.select_nth_unstable(idx).1;

            let sum = samples.iter().sum::<Duration>();
            let avg = sum
                .checked_div(entry.samples.len() as u32)
                .unwrap_or(Duration::ZERO);

            let _ = writeln!(
                &mut report,
                "{:<6$} | {:>8} | {:>8} | {:>8} | {:>8} | {:>8}",
                key.1,
                show_duration(entry.last),
                show_duration(avg),
                show_duration(p05),
                show_duration(p50),
                show_duration(p95),
                max_len
            );
        }

        report
    }
}

impl Drop for TimeGuard<'_> {
    fn drop(&mut self) {
        let elapsed = self.instant.elapsed();
        let mut lock = self.profiler.time_entries.write();
        let entry = lock.entry((self.idx, self.name)).or_default();
        if entry.samples.len() == MAX_SAMPLES {
            entry.samples.pop_front();
        }
        entry.samples.push_back(elapsed);
        entry.last = elapsed;
    }
}

fn show_duration(v: Duration) -> String {
    let secs = v.as_secs_f32();
    if v < Duration::from_micros(1) {
        format!("{:>3}.0ns", v.as_nanos())
    } else if v < Duration::from_millis(1) {
        format!("{:>4.1}us", secs * 1e6)
    } else if v < Duration::from_secs(1) {
        format!("{:>4.1}ms", secs * 1e3)
    } else {
        format!("{:>4.0} s", secs)
    }
}
