struct VertexInput {
    [[location(0)]] pos: vec2<f32>;
    [[location(1)]] z_order: f32;
    [[location(2)]] transform_row0: vec3<f32>;
    [[location(3)]] transform_row1: vec3<f32>;
    [[location(4)]] uv_rect0: vec4<f32>;
    [[location(5)]] uv_rect1: vec4<f32>;
    [[location(6)]] color: vec4<f32>;
};

struct VertexOutput {
    [[builtin(position)]] position: vec4<f32>;
    [[location(0)]] uv0: vec2<f32>;
    [[location(1)]] uv1: vec2<f32>;
    [[location(2)]] mask_enabled: f32;
    [[location(3)]] color: vec4<f32>;
};

[[stage(vertex)]]
fn vert_main(in: VertexInput) -> VertexOutput {
    var out: VertexOutput;
    let transform = mat3x3<f32>(in.transform_row0, in.transform_row1, vec3<f32>(0.0, 0.0, 1.0));
    let pos = vec3<f32>(in.pos.x, in.pos.y, 1.0) * transform;
    out.position = vec4<f32>(pos.x, pos.y, in.z_order, 1.0);
    out.uv0 = in.pos * in.uv_rect0.zw + in.uv_rect0.xy;
    out.uv1 = in.pos * in.uv_rect1.zw + in.uv_rect1.xy;
    out.mask_enabled = 0.0;
    if (in.uv_rect1.z > 0.0) { out.mask_enabled = 1.0; }
    out.color = in.color;
    return out;
}

[[group(0), binding(0)]]
var u_sampler: sampler;
[[group(0), binding(1)]]
var u_texture0: texture_2d<f32>;
[[group(0), binding(2)]]
var u_texture1: texture_2d<f32>;

[[stage(fragment)]]
fn frag_sprite(in: VertexOutput) -> [[location(0)]] vec4<f32> {
    let color = textureSample(u_texture0, u_sampler, in.uv0) * in.color;
    let mask = textureSample(u_texture1, u_sampler, in.uv1);
    let res = color * mask * in.mask_enabled + color * (1.0 - in.mask_enabled);
    if (res.a < 0.01) {
        discard;
    }
    return res;
}

[[stage(fragment)]]
fn frag_glyph(in: VertexOutput) -> [[location(0)]] vec4<f32> {
    let coverage = textureSample(u_texture0, u_sampler, in.uv0).r;
    if (coverage < 0.01) {
        discard;
    }
    return in.color * vec4<f32>(1.0, 1.0, 1.0, coverage);
}
