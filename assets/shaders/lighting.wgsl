struct VertexInput {
    [[location(0)]] pos: vec2<f32>;
    [[location(1)]] uv: vec2<f32>;
};

struct VertexOutput {
    [[builtin(position)]] pos: vec4<f32>;
    [[location(0)]] uv0: vec2<f32>;
    [[location(1)]] uv1: vec2<f32>;
};

[[stage(vertex)]]
fn vert_main(in: VertexInput) -> VertexOutput {
    var out: VertexOutput;
    out.uv0 = in.pos * vec2<f32>(0.5, -0.5) + 0.5;
    out.uv1 = in.uv;
    out.pos = vec4<f32>(in.pos.x, in.pos.y, 0.0, 1.0);
    return out;
}

[[group(0), binding(0)]]
var u_sampler: sampler;
[[group(0), binding(1)]]
var u_texture: texture_2d<f32>;
[[group(0), binding(2)]]
var u_source: texture_2d<f32>;

fn cubic(v: f32) -> vec4<f32> {
    let n = vec4<f32>(1.0, 2.0, 3.0, 4.0) - v;
    let s = n * n * n;
    let x = s.x;
    let y = s.y - 4.0 * s.x;
    let z = s.z - 4.0 * s.y + 6.0 * s.x;
    let w = 6.0 - x - y - z;
    return vec4<f32>(x, y, z, w) * (1.0/6.0);
}

fn textureBicubic(texCoords: vec2<f32>) -> vec4<f32> {
    let texSize = vec2<f32>(textureDimensions(u_texture));
    let invTexSize = 1.0 / texSize;

    let texCoords = texCoords * texSize - 0.5;

    let fxy = fract(texCoords);
    let texCoords = texCoords - fxy;

    let xcubic = cubic(fxy.x);
    let ycubic = cubic(fxy.y);

    let c = texCoords.xxyy + vec2<f32>(-0.5, 1.5).xyxy;

    let s = vec4<f32>(xcubic.xz + xcubic.yw, ycubic.xz + ycubic.yw);
    let offset = (c + vec4<f32>(xcubic.yw, ycubic.yw) / s) * invTexSize.xxyy;

    let sample0 = textureSample(u_texture, u_sampler, offset.xz);
    let sample1 = textureSample(u_texture, u_sampler, offset.yz);
    let sample2 = textureSample(u_texture, u_sampler, offset.xw);
    let sample3 = textureSample(u_texture, u_sampler, offset.yw);

    let sx = s.x / (s.x + s.y);
    let sy = s.z / (s.z + s.w);

    let a = sample3 * (1.0 - sx) + sample2 * sx;
    let b = sample1 * (1.0 - sx) + sample0 * sx;
    return a * (1.0 - sy) + b * sy;
}

[[stage(fragment)]]
fn frag_main(in: VertexOutput) -> [[location(0)]] vec4<f32> {
    let source = textureSample(u_source, u_sampler, in.uv0);
    let light = textureBicubic(in.uv1);
    let light_col = light.rgb + light.a * vec3<f32>(1.0);
    let light_fac = vec3<f32>(
      min(1.0, light_col.r * light_col.r),
      min(1.0, light_col.g * light_col.g),
      min(1.0, light_col.b * light_col.b)
    );
    return vec4<f32>(source.rgb * light_fac, 1.0);
}
